/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     PERCENT = 1,
     AMPERSAND = 2,
     ASTER = 3,
     CLUSTER = 4,
     COLON = 5,
     COMMA = 6,
     DASTER = 7,
     DEFINED_OPERATOR = 8,
     DOT = 9,
     DQUOTE = 10,
     GLOBAL_A = 11,
     LEFTAB = 12,
     LEFTPAR = 13,
     MINUS = 14,
     PLUS = 15,
     POINT_TO = 16,
     QUOTE = 17,
     RIGHTAB = 18,
     RIGHTPAR = 19,
     AND = 20,
     DSLASH = 21,
     EQV = 22,
     EQ = 23,
     EQUAL = 24,
     FFALSE = 25,
     GE = 26,
     GT = 27,
     LE = 28,
     LT = 29,
     NE = 30,
     NEQV = 31,
     NOT = 32,
     OR = 33,
     TTRUE = 34,
     SLASH = 35,
     XOR = 36,
     REFERENCE = 37,
     AT = 38,
     ACROSS = 39,
     ALIGN_WITH = 40,
     ALIGN = 41,
     ALLOCATABLE = 42,
     ALLOCATE = 43,
     ARITHIF = 44,
     ASSIGNMENT = 45,
     ASSIGN = 46,
     ASSIGNGOTO = 47,
     ASYNCHRONOUS = 48,
     ASYNCID = 49,
     ASYNCWAIT = 50,
     BACKSPACE = 51,
     BAD_CCONST = 52,
     BAD_SYMBOL = 53,
     BARRIER = 54,
     BLOCKDATA = 55,
     BLOCK = 56,
     BOZ_CONSTANT = 57,
     BYTE = 58,
     CALL = 59,
     CASE = 60,
     CHARACTER = 61,
     CHAR_CONSTANT = 62,
     CHECK = 63,
     CLOSE = 64,
     COMMON = 65,
     COMPLEX = 66,
     COMPGOTO = 67,
     CONSISTENT_GROUP = 68,
     CONSISTENT_SPEC = 69,
     CONSISTENT_START = 70,
     CONSISTENT_WAIT = 71,
     CONSISTENT = 72,
     CONSTRUCT_ID = 73,
     CONTAINS = 74,
     CONTINUE = 75,
     CORNER = 76,
     CYCLE = 77,
     DATA = 78,
     DEALLOCATE = 79,
     HPF_TEMPLATE = 80,
     DEBUG = 81,
     DEFAULT_CASE = 82,
     DEFINE = 83,
     DERIVED = 84,
     DIMENSION = 85,
     DISTRIBUTE = 86,
     DOWHILE = 87,
     DOUBLEPRECISION = 88,
     DOUBLECOMPLEX = 89,
     DP_CONSTANT = 90,
     DVM_POINTER = 91,
     DYNAMIC = 92,
     ELEMENTAL = 93,
     ELSE = 94,
     ELSEIF = 95,
     ELSEWHERE = 96,
     ENDASYNCHRONOUS = 97,
     ENDDEBUG = 98,
     ENDINTERVAL = 99,
     ENDUNIT = 100,
     ENDDO = 101,
     ENDFILE = 102,
     ENDFORALL = 103,
     ENDIF = 104,
     ENDINTERFACE = 105,
     ENDMODULE = 106,
     ENDON = 107,
     ENDSELECT = 108,
     ENDTASK_REGION = 109,
     ENDTYPE = 110,
     ENDWHERE = 111,
     ENTRY = 112,
     EXIT = 113,
     EOLN = 114,
     EQUIVALENCE = 115,
     ERROR = 116,
     EXTERNAL = 117,
     F90 = 118,
     FIND = 119,
     FORALL = 120,
     FORMAT = 121,
     FUNCTION = 122,
     GATE = 123,
     GEN_BLOCK = 124,
     HEAP = 125,
     HIGH = 126,
     IDENTIFIER = 127,
     IMPLICIT = 128,
     IMPLICITNONE = 129,
     INCLUDE_TO = 130,
     INCLUDE = 131,
     INDEPENDENT = 132,
     INDIRECT_ACCESS = 133,
     INDIRECT_GROUP = 134,
     INDIRECT = 135,
     INHERIT = 136,
     INQUIRE = 137,
     INTERFACEASSIGNMENT = 138,
     INTERFACEOPERATOR = 139,
     INTERFACE = 140,
     INTRINSIC = 141,
     INTEGER = 142,
     INTENT = 143,
     INTERVAL = 144,
     INOUT = 145,
     IN = 146,
     INT_CONSTANT = 147,
     LABEL = 148,
     LABEL_DECLARE = 149,
     LET = 150,
     LOCALIZE = 151,
     LOGICAL = 152,
     LOGICALIF = 153,
     LOOP = 154,
     LOW = 155,
     MAXLOC = 156,
     MAX = 157,
     MAP = 158,
     MINLOC = 159,
     MIN = 160,
     MODULE_PROCEDURE = 161,
     MODULE = 162,
     MULT_BLOCK = 163,
     NAMEEQ = 164,
     NAMELIST = 165,
     NEW_VALUE = 166,
     NEW = 167,
     NULLIFY = 168,
     OCTAL_CONSTANT = 169,
     ONLY = 170,
     ON = 171,
     ON_DIR = 172,
     ONTO = 173,
     OPEN = 174,
     OPERATOR = 175,
     OPTIONAL = 176,
     OTHERWISE = 177,
     OUT = 178,
     OWN = 179,
     PARALLEL = 180,
     PARAMETER = 181,
     PAUSE = 182,
     PLAINDO = 183,
     PLAINGOTO = 184,
     POINTER = 185,
     POINTERLET = 186,
     PREFETCH = 187,
     PRINT = 188,
     PRIVATE = 189,
     PRODUCT = 190,
     PROGRAM = 191,
     PUBLIC = 192,
     PURE = 193,
     RANGE = 194,
     READ = 195,
     REALIGN_WITH = 196,
     REALIGN = 197,
     REAL = 198,
     REAL_CONSTANT = 199,
     RECURSIVE = 200,
     REDISTRIBUTE_NEW = 201,
     REDISTRIBUTE = 202,
     REDUCTION_GROUP = 203,
     REDUCTION_START = 204,
     REDUCTION_WAIT = 205,
     REDUCTION = 206,
     REMOTE_ACCESS_SPEC = 207,
     REMOTE_ACCESS = 208,
     REMOTE_GROUP = 209,
     RESET = 210,
     RESULT = 211,
     RETURN = 212,
     REWIND = 213,
     SAVE = 214,
     SECTION = 215,
     SELECT = 216,
     SEQUENCE = 217,
     SHADOW_ADD = 218,
     SHADOW_COMPUTE = 219,
     SHADOW_GROUP = 220,
     SHADOW_RENEW = 221,
     SHADOW_START_SPEC = 222,
     SHADOW_START = 223,
     SHADOW_WAIT_SPEC = 224,
     SHADOW_WAIT = 225,
     SHADOW = 226,
     STAGE = 227,
     STATIC = 228,
     STAT = 229,
     STOP = 230,
     SUBROUTINE = 231,
     SUM = 232,
     SYNC = 233,
     TARGET = 234,
     TASK = 235,
     TASK_REGION = 236,
     THEN = 237,
     TO = 238,
     TRACEON = 239,
     TRACEOFF = 240,
     TRUNC = 241,
     TYPE = 242,
     TYPE_DECL = 243,
     UNDER = 244,
     UNKNOWN = 245,
     USE = 246,
     VIRTUAL = 247,
     VARIABLE = 248,
     WAIT = 249,
     WHERE = 250,
     WHERE_ASSIGN = 251,
     WHILE = 252,
     WITH = 253,
     WRITE = 254,
     COMMENT = 255,
     WGT_BLOCK = 256,
     HPF_PROCESSORS = 257,
     IOSTAT = 258,
     ERR = 259,
     END = 260,
     OMPDVM_ATOMIC = 261,
     OMPDVM_BARRIER = 262,
     OMPDVM_COPYIN = 263,
     OMPDVM_COPYPRIVATE = 264,
     OMPDVM_CRITICAL = 265,
     OMPDVM_ONETHREAD = 266,
     OMPDVM_DO = 267,
     OMPDVM_DYNAMIC = 268,
     OMPDVM_ENDCRITICAL = 269,
     OMPDVM_ENDDO = 270,
     OMPDVM_ENDMASTER = 271,
     OMPDVM_ENDORDERED = 272,
     OMPDVM_ENDPARALLEL = 273,
     OMPDVM_ENDPARALLELDO = 274,
     OMPDVM_ENDPARALLELSECTIONS = 275,
     OMPDVM_ENDPARALLELWORKSHARE = 276,
     OMPDVM_ENDSECTIONS = 277,
     OMPDVM_ENDSINGLE = 278,
     OMPDVM_ENDWORKSHARE = 279,
     OMPDVM_FIRSTPRIVATE = 280,
     OMPDVM_FLUSH = 281,
     OMPDVM_GUIDED = 282,
     OMPDVM_LASTPRIVATE = 283,
     OMPDVM_MASTER = 284,
     OMPDVM_NOWAIT = 285,
     OMPDVM_NONE = 286,
     OMPDVM_NUM_THREADS = 287,
     OMPDVM_ORDERED = 288,
     OMPDVM_PARALLEL = 289,
     OMPDVM_PARALLELDO = 290,
     OMPDVM_PARALLELSECTIONS = 291,
     OMPDVM_PARALLELWORKSHARE = 292,
     OMPDVM_RUNTIME = 293,
     OMPDVM_SECTION = 294,
     OMPDVM_SECTIONS = 295,
     OMPDVM_SCHEDULE = 296,
     OMPDVM_SHARED = 297,
     OMPDVM_SINGLE = 298,
     OMPDVM_THREADPRIVATE = 299,
     OMPDVM_WORKSHARE = 300,
     OMPDVM_NODES = 301,
     OMPDVM_IF = 302,
     IAND = 303,
     IEOR = 304,
     IOR = 305,
     ACC_REGION = 306,
     ACC_END_REGION = 307,
     ACC_CHECKSECTION = 308,
     ACC_END_CHECKSECTION = 309,
     ACC_GET_ACTUAL = 310,
     ACC_ACTUAL = 311,
     ACC_TARGETS = 312,
     ACC_ASYNC = 313,
     ACC_HOST = 314,
     ACC_CUDA = 315,
     ACC_LOCAL = 316,
     ACC_INLOCAL = 317,
     ACC_CUDA_BLOCK = 318,
     ACC_ROUTINE = 319,
     ACC_TIE = 320,
     BY = 321,
     IO_MODE = 322,
     CP_CREATE = 323,
     CP_LOAD = 324,
     CP_SAVE = 325,
     CP_WAIT = 326,
     FILES = 327,
     VARLIST = 328,
     STATUS = 329,
     EXITINTERVAL = 330,
     TEMPLATE_CREATE = 331,
     TEMPLATE_DELETE = 332,
     SPF_ANALYSIS = 333,
     SPF_PARALLEL = 334,
     SPF_TRANSFORM = 335,
     SPF_NOINLINE = 336,
     SPF_PARALLEL_REG = 337,
     SPF_END_PARALLEL_REG = 338,
     SPF_EXPAND = 339,
     SPF_FISSION = 340,
     SPF_SHRINK = 341,
     SPF_CHECKPOINT = 342,
     SPF_EXCEPT = 343,
     SPF_FILES_COUNT = 344,
     SPF_INTERVAL = 345,
     SPF_TIME = 346,
     SPF_ITER = 347,
     SPF_FLEXIBLE = 348,
     SPF_APPLY_REGION = 349,
     SPF_APPLY_FRAGMENT = 350,
     SPF_CODE_COVERAGE = 351,
     SPF_UNROLL = 352,
     BINARY_OP = 355,
     UNARY_OP = 356
   };
#endif
/* Tokens.  */
#define PERCENT 1
#define AMPERSAND 2
#define ASTER 3
#define CLUSTER 4
#define COLON 5
#define COMMA 6
#define DASTER 7
#define DEFINED_OPERATOR 8
#define DOT 9
#define DQUOTE 10
#define GLOBAL_A 11
#define LEFTAB 12
#define LEFTPAR 13
#define MINUS 14
#define PLUS 15
#define POINT_TO 16
#define QUOTE 17
#define RIGHTAB 18
#define RIGHTPAR 19
#define AND 20
#define DSLASH 21
#define EQV 22
#define EQ 23
#define EQUAL 24
#define FFALSE 25
#define GE 26
#define GT 27
#define LE 28
#define LT 29
#define NE 30
#define NEQV 31
#define NOT 32
#define OR 33
#define TTRUE 34
#define SLASH 35
#define XOR 36
#define REFERENCE 37
#define AT 38
#define ACROSS 39
#define ALIGN_WITH 40
#define ALIGN 41
#define ALLOCATABLE 42
#define ALLOCATE 43
#define ARITHIF 44
#define ASSIGNMENT 45
#define ASSIGN 46
#define ASSIGNGOTO 47
#define ASYNCHRONOUS 48
#define ASYNCID 49
#define ASYNCWAIT 50
#define BACKSPACE 51
#define BAD_CCONST 52
#define BAD_SYMBOL 53
#define BARRIER 54
#define BLOCKDATA 55
#define BLOCK 56
#define BOZ_CONSTANT 57
#define BYTE 58
#define CALL 59
#define CASE 60
#define CHARACTER 61
#define CHAR_CONSTANT 62
#define CHECK 63
#define CLOSE 64
#define COMMON 65
#define COMPLEX 66
#define COMPGOTO 67
#define CONSISTENT_GROUP 68
#define CONSISTENT_SPEC 69
#define CONSISTENT_START 70
#define CONSISTENT_WAIT 71
#define CONSISTENT 72
#define CONSTRUCT_ID 73
#define CONTAINS 74
#define CONTINUE 75
#define CORNER 76
#define CYCLE 77
#define DATA 78
#define DEALLOCATE 79
#define HPF_TEMPLATE 80
#define DEBUG 81
#define DEFAULT_CASE 82
#define DEFINE 83
#define DERIVED 84
#define DIMENSION 85
#define DISTRIBUTE 86
#define DOWHILE 87
#define DOUBLEPRECISION 88
#define DOUBLECOMPLEX 89
#define DP_CONSTANT 90
#define DVM_POINTER 91
#define DYNAMIC 92
#define ELEMENTAL 93
#define ELSE 94
#define ELSEIF 95
#define ELSEWHERE 96
#define ENDASYNCHRONOUS 97
#define ENDDEBUG 98
#define ENDINTERVAL 99
#define ENDUNIT 100
#define ENDDO 101
#define ENDFILE 102
#define ENDFORALL 103
#define ENDIF 104
#define ENDINTERFACE 105
#define ENDMODULE 106
#define ENDON 107
#define ENDSELECT 108
#define ENDTASK_REGION 109
#define ENDTYPE 110
#define ENDWHERE 111
#define ENTRY 112
#define EXIT 113
#define EOLN 114
#define EQUIVALENCE 115
#define ERROR 116
#define EXTERNAL 117
#define F90 118
#define FIND 119
#define FORALL 120
#define FORMAT 121
#define FUNCTION 122
#define GATE 123
#define GEN_BLOCK 124
#define HEAP 125
#define HIGH 126
#define IDENTIFIER 127
#define IMPLICIT 128
#define IMPLICITNONE 129
#define INCLUDE_TO 130
#define INCLUDE 131
#define INDEPENDENT 132
#define INDIRECT_ACCESS 133
#define INDIRECT_GROUP 134
#define INDIRECT 135
#define INHERIT 136
#define INQUIRE 137
#define INTERFACEASSIGNMENT 138
#define INTERFACEOPERATOR 139
#define INTERFACE 140
#define INTRINSIC 141
#define INTEGER 142
#define INTENT 143
#define INTERVAL 144
#define INOUT 145
#define IN 146
#define INT_CONSTANT 147
#define LABEL 148
#define LABEL_DECLARE 149
#define LET 150
#define LOCALIZE 151
#define LOGICAL 152
#define LOGICALIF 153
#define LOOP 154
#define LOW 155
#define MAXLOC 156
#define MAX 157
#define MAP 158
#define MINLOC 159
#define MIN 160
#define MODULE_PROCEDURE 161
#define MODULE 162
#define MULT_BLOCK 163
#define NAMEEQ 164
#define NAMELIST 165
#define NEW_VALUE 166
#define NEW 167
#define NULLIFY 168
#define OCTAL_CONSTANT 169
#define ONLY 170
#define ON 171
#define ON_DIR 172
#define ONTO 173
#define OPEN 174
#define OPERATOR 175
#define OPTIONAL 176
#define OTHERWISE 177
#define OUT 178
#define OWN 179
#define PARALLEL 180
#define PARAMETER 181
#define PAUSE 182
#define PLAINDO 183
#define PLAINGOTO 184
#define POINTER 185
#define POINTERLET 186
#define PREFETCH 187
#define PRINT 188
#define PRIVATE 189
#define PRODUCT 190
#define PROGRAM 191
#define PUBLIC 192
#define PURE 193
#define RANGE 194
#define READ 195
#define REALIGN_WITH 196
#define REALIGN 197
#define REAL 198
#define REAL_CONSTANT 199
#define RECURSIVE 200
#define REDISTRIBUTE_NEW 201
#define REDISTRIBUTE 202
#define REDUCTION_GROUP 203
#define REDUCTION_START 204
#define REDUCTION_WAIT 205
#define REDUCTION 206
#define REMOTE_ACCESS_SPEC 207
#define REMOTE_ACCESS 208
#define REMOTE_GROUP 209
#define RESET 210
#define RESULT 211
#define RETURN 212
#define REWIND 213
#define SAVE 214
#define SECTION 215
#define SELECT 216
#define SEQUENCE 217
#define SHADOW_ADD 218
#define SHADOW_COMPUTE 219
#define SHADOW_GROUP 220
#define SHADOW_RENEW 221
#define SHADOW_START_SPEC 222
#define SHADOW_START 223
#define SHADOW_WAIT_SPEC 224
#define SHADOW_WAIT 225
#define SHADOW 226
#define STAGE 227
#define STATIC 228
#define STAT 229
#define STOP 230
#define SUBROUTINE 231
#define SUM 232
#define SYNC 233
#define TARGET 234
#define TASK 235
#define TASK_REGION 236
#define THEN 237
#define TO 238
#define TRACEON 239
#define TRACEOFF 240
#define TRUNC 241
#define TYPE 242
#define TYPE_DECL 243
#define UNDER 244
#define UNKNOWN 245
#define USE 246
#define VIRTUAL 247
#define VARIABLE 248
#define WAIT 249
#define WHERE 250
#define WHERE_ASSIGN 251
#define WHILE 252
#define WITH 253
#define WRITE 254
#define COMMENT 255
#define WGT_BLOCK 256
#define HPF_PROCESSORS 257
#define IOSTAT 258
#define ERR 259
#define END 260
#define OMPDVM_ATOMIC 261
#define OMPDVM_BARRIER 262
#define OMPDVM_COPYIN 263
#define OMPDVM_COPYPRIVATE 264
#define OMPDVM_CRITICAL 265
#define OMPDVM_ONETHREAD 266
#define OMPDVM_DO 267
#define OMPDVM_DYNAMIC 268
#define OMPDVM_ENDCRITICAL 269
#define OMPDVM_ENDDO 270
#define OMPDVM_ENDMASTER 271
#define OMPDVM_ENDORDERED 272
#define OMPDVM_ENDPARALLEL 273
#define OMPDVM_ENDPARALLELDO 274
#define OMPDVM_ENDPARALLELSECTIONS 275
#define OMPDVM_ENDPARALLELWORKSHARE 276
#define OMPDVM_ENDSECTIONS 277
#define OMPDVM_ENDSINGLE 278
#define OMPDVM_ENDWORKSHARE 279
#define OMPDVM_FIRSTPRIVATE 280
#define OMPDVM_FLUSH 281
#define OMPDVM_GUIDED 282
#define OMPDVM_LASTPRIVATE 283
#define OMPDVM_MASTER 284
#define OMPDVM_NOWAIT 285
#define OMPDVM_NONE 286
#define OMPDVM_NUM_THREADS 287
#define OMPDVM_ORDERED 288
#define OMPDVM_PARALLEL 289
#define OMPDVM_PARALLELDO 290
#define OMPDVM_PARALLELSECTIONS 291
#define OMPDVM_PARALLELWORKSHARE 292
#define OMPDVM_RUNTIME 293
#define OMPDVM_SECTION 294
#define OMPDVM_SECTIONS 295
#define OMPDVM_SCHEDULE 296
#define OMPDVM_SHARED 297
#define OMPDVM_SINGLE 298
#define OMPDVM_THREADPRIVATE 299
#define OMPDVM_WORKSHARE 300
#define OMPDVM_NODES 301
#define OMPDVM_IF 302
#define IAND 303
#define IEOR 304
#define IOR 305
#define ACC_REGION 306
#define ACC_END_REGION 307
#define ACC_CHECKSECTION 308
#define ACC_END_CHECKSECTION 309
#define ACC_GET_ACTUAL 310
#define ACC_ACTUAL 311
#define ACC_TARGETS 312
#define ACC_ASYNC 313
#define ACC_HOST 314
#define ACC_CUDA 315
#define ACC_LOCAL 316
#define ACC_INLOCAL 317
#define ACC_CUDA_BLOCK 318
#define ACC_ROUTINE 319
#define ACC_TIE 320
#define BY 321
#define IO_MODE 322
#define CP_CREATE 323
#define CP_LOAD 324
#define CP_SAVE 325
#define CP_WAIT 326
#define FILES 327
#define VARLIST 328
#define STATUS 329
#define EXITINTERVAL 330
#define TEMPLATE_CREATE 331
#define TEMPLATE_DELETE 332
#define SPF_ANALYSIS 333
#define SPF_PARALLEL 334
#define SPF_TRANSFORM 335
#define SPF_NOINLINE 336
#define SPF_PARALLEL_REG 337
#define SPF_END_PARALLEL_REG 338
#define SPF_EXPAND 339
#define SPF_FISSION 340
#define SPF_SHRINK 341
#define SPF_CHECKPOINT 342
#define SPF_EXCEPT 343
#define SPF_FILES_COUNT 344
#define SPF_INTERVAL 345
#define SPF_TIME 346
#define SPF_ITER 347
#define SPF_FLEXIBLE 348
#define SPF_APPLY_REGION 349
#define SPF_APPLY_FRAGMENT 350
#define SPF_CODE_COVERAGE 351
#define SPF_UNROLL 352
#define BINARY_OP 355
#define UNARY_OP 356




/* Copy the first part of user declarations.  */
#line 354 "gram1.y"

#include <string.h>
#include "inc.h"
#include "extern.h"
#include "defines.h"
#include "fdvm.h"
#include "fm.h"

/* We may use builtin alloca */
#include "compatible.h"
#ifdef _NEEDALLOCAH_
#  include <alloca.h>
#endif

#define EXTEND_NODE 2  /* move the definition to h/ files. */

extern PTR_BFND global_bfnd, pred_bfnd;
extern PTR_SYMB star_symb;
extern PTR_SYMB global_list;
extern PTR_TYPE global_bool;
extern PTR_TYPE global_int;
extern PTR_TYPE global_float;
extern PTR_TYPE global_double;
extern PTR_TYPE global_char;
extern PTR_TYPE global_string;
extern PTR_TYPE global_string_2;
extern PTR_TYPE global_complex;
extern PTR_TYPE global_dcomplex;
extern PTR_TYPE global_gate;
extern PTR_TYPE global_event;
extern PTR_TYPE global_sequence;
extern PTR_TYPE global_default;
extern PTR_LABEL thislabel;
extern PTR_CMNT comments, cur_comment;
extern PTR_BFND last_bfnd;
extern PTR_TYPE impltype[];
extern int nioctl;
extern int maxdim;
extern long yystno;	/* statement label */
extern char stmtbuf[];	/* input buffer */
extern char *commentbuf;	/* comments buffer from scanner */
extern PTR_BLOB head_blob;
extern PTR_BLOB cur_blob;
extern PTR_TYPE vartype; /* variable type */
extern int end_group;
extern char saveall;
extern int privateall;
extern int needkwd;
extern int implkwd;
extern int opt_kwd_hedr;
/* added for FORTRAN 90 */
extern PTR_LLND first_unresolved_call;
extern PTR_LLND last_unresolved_call;
extern int data_stat;
extern char yyquote;

extern int warn_all;
extern int statement_kind; /* kind of statement: 1 - HPF-DVM-directive, 0 - Fortran statement*/ 
int extend_flag = 0;

static int do_name_err;
static int ndim;	/* number of dimension */
/*!!! hpf */
static int explicit_shape; /*  1 if shape specification is explicit */
/* static int varleng;*/	/* variable size */
static int lastwasbranch = NO;	/* set if last stmt was a branch stmt */
static int thiswasbranch = NO;	/* set if this stmt is a branch stmt */
static PTR_SYMB type_var = SMNULL;
static PTR_LLND stat_alloc = LLNULL; /* set if ALLOCATE/DEALLOCATE stmt has STAT-clause*/
/* static int subscripts_status = 0; */
static int type_options,type_opt;   /* The various options used to declare a name -
                                      RECURSIVE, POINTER, OPTIONAL etc.         */
static PTR_BFND module_scope;
static int position = IN_OUTSIDE;            
static int attr_ndim;           /* number of dimensions in DIMENSION (array_spec)
                                   attribute declaration */
static PTR_LLND attr_dims;     /* low level representation of array_spec in
                                   DIMENSION (array_spec) attribute declarartion. */
static int in_vec = NO;	      /* set if processing array constructor */


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 435 "gram1.y"
{
    int token;
    char charv;
    char *charp;
    PTR_BFND bf_node;
    PTR_LLND ll_node;
    PTR_SYMB symbol;
    PTR_TYPE data_type;
    PTR_HASH hash_entry;
    PTR_LABEL label;
}
/* Line 187 of yacc.c.  */
#line 901 "gram1.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */
#line 642 "gram1.y"

void add_scope_level();
void delete_beyond_scope_level();
PTR_HASH look_up_sym();
PTR_HASH just_look_up_sym();
PTR_HASH just_look_up_sym_in_scope();
PTR_HASH look_up_op();
PTR_SYMB make_constant();
PTR_SYMB make_scalar();
PTR_SYMB make_array();
PTR_SYMB make_pointer();
PTR_SYMB make_function();
PTR_SYMB make_external();
PTR_SYMB make_intrinsic();
PTR_SYMB make_procedure();
PTR_SYMB make_process();
PTR_SYMB make_program();
PTR_SYMB make_module();
PTR_SYMB make_common();
PTR_SYMB make_parallel_region();
PTR_SYMB make_derived_type();
PTR_SYMB make_local_entity();
PTR_SYMB make_global_entity();
PTR_TYPE make_type_node();
PTR_TYPE lookup_type(), make_type();
void     process_type();
void     process_interface();
void     bind();
void     late_bind_if_needed();
PTR_SYMB component();
PTR_SYMB lookup_type_symbol();
PTR_SYMB resolve_overloading();
PTR_BFND cur_scope();
PTR_BFND subroutine_call();
PTR_BFND process_call();
PTR_LLND deal_with_options();
PTR_LLND intrinsic_op_node();
PTR_LLND defined_op_node();
int is_substring_ref();
int is_array_section_ref();
PTR_LLND dim_expr(); 
PTR_BFND exit_stat();
PTR_BFND make_do();
PTR_BFND make_pardo();
PTR_BFND make_enddoall();
PTR_TYPE install_array(); 
PTR_SYMB install_entry(); 
void install_param_list();
PTR_LLND construct_entry_list();
void copy_sym_data();
PTR_LLND check_and_install();
PTR_HASH look_up();
PTR_BFND get_bfnd(); 
PTR_BLOB make_blob();
PTR_LABEL make_label();
PTR_LABEL make_label_node();
int is_interface_stat();
PTR_LLND make_llnd (); 
PTR_LLND make_llnd_label (); 
PTR_TYPE make_sa_type(); 
PTR_SYMB procedure_call();
PTR_BFND proc_list();
PTR_SYMB set_id_list();
PTR_LLND set_ll_list();
PTR_LLND add_to_lowLevelList(), add_to_lowList();
PTR_BFND set_stat_list() ;
PTR_BLOB follow_blob();
PTR_SYMB proc_decl_init();
PTR_CMNT make_comment();
PTR_HASH correct_symtab();
char *copyn();
char *convic();
char *StringConcatenation();
int atoi();
PTR_BFND make_logif();
PTR_BFND make_if();
PTR_BFND make_forall();
void startproc();
void match_parameters();
void make_else();
void make_elseif();
void make_endif();
void make_elsewhere();
void make_elsewhere_mask();
void make_endwhere();
void make_endforall();
void make_endselect();
void make_extend();
void make_endextend();
void make_section();
void make_section_extend();
void doinclude();
void endproc();
void err();
void execerr();
void flline();
void warn();
void warn1();
void newprog();
void set_type();
void dclerr();
void enddcl();
void install_const();
void setimpl();
void copy_module_scope();
void delete_symbol();
void replace_symbol_in_expr();
long convci();
void set_expr_type();
void errstr();
void yyerror();
void set_blobs();
void make_loop();
void startioctl();
void endioctl();
void redefine_func_arg_type();
int isResultVar();

/* used by FORTRAN M */
PTR_BFND make_processdo();
PTR_BFND make_processes();
PTR_BFND make_endprocesses();

PTR_BFND make_endparallel();/*OMP*/
PTR_BFND make_parallel();/*OMP*/
PTR_BFND make_endsingle();/*OMP*/
PTR_BFND make_single();/*OMP*/
PTR_BFND make_endmaster();/*OMP*/
PTR_BFND make_master();/*OMP*/
PTR_BFND make_endordered();/*OMP*/
PTR_BFND make_ordered();/*OMP*/
PTR_BFND make_endcritical();/*OMP*/
PTR_BFND make_critical();/*OMP*/
PTR_BFND make_endsections();/*OMP*/
PTR_BFND make_sections();/*OMP*/
PTR_BFND make_ompsection();/*OMP*/
PTR_BFND make_endparallelsections();/*OMP*/
PTR_BFND make_parallelsections();/*OMP*/
PTR_BFND make_endworkshare();/*OMP*/
PTR_BFND make_workshare();/*OMP*/
PTR_BFND make_endparallelworkshare();/*OMP*/
PTR_BFND make_parallelworkshare();/*OMP*/



/* Line 216 of yacc.c.  */
#line 1058 "gram1.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   5799

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  357
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  539
/* YYNRULES -- Number of rules.  */
#define YYNRULES  1297
/* YYNRULES -- Number of states.  */
#define YYNSTATES  2585

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   356

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint16 yytranslate[] =
{
       0,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,   207,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,   233,   234,   235,   236,   237,   238,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,   254,   255,   256,   257,   258,   259,   260,   261,
     262,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,   295,   296,   297,   298,   299,   300,   301,
     302,   303,   304,   305,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,   316,   317,   318,   319,   320,   321,
     322,   323,   324,   325,   326,   327,   328,   329,   330,   331,
     332,   333,   334,   335,   336,   337,   338,   339,   340,   341,
     342,   343,   344,   345,   346,   347,   348,   349,   350,   351,
     352,   353,   354,     1,     2,   355,   356
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     8,    12,    16,    20,    24,    27,
      29,    31,    33,    37,    41,    46,    52,    58,    62,    67,
      71,    72,    75,    78,    81,    83,    85,    90,    96,   101,
     107,   110,   116,   118,   119,   121,   122,   124,   125,   128,
     132,   134,   138,   140,   142,   144,   145,   146,   147,   149,
     151,   153,   155,   157,   159,   161,   163,   165,   167,   169,
     171,   173,   176,   181,   184,   190,   192,   194,   196,   198,
     200,   202,   204,   206,   208,   210,   212,   214,   217,   221,
     227,   233,   236,   238,   240,   242,   244,   246,   248,   250,
     252,   254,   256,   258,   260,   262,   264,   266,   268,   270,
     272,   274,   276,   278,   283,   291,   294,   298,   306,   313,
     314,   317,   323,   325,   330,   332,   334,   336,   339,   341,
     346,   348,   350,   352,   354,   356,   358,   361,   364,   367,
     369,   371,   379,   383,   388,   392,   397,   401,   404,   410,
     411,   414,   417,   423,   424,   429,   435,   436,   439,   443,
     445,   447,   449,   451,   453,   455,   457,   459,   461,   463,
     464,   466,   472,   479,   486,   487,   489,   495,   505,   507,
     509,   512,   515,   516,   517,   520,   523,   529,   534,   539,
     543,   548,   552,   557,   561,   565,   570,   576,   580,   585,
     591,   595,   599,   601,   605,   608,   613,   617,   622,   626,
     630,   634,   638,   642,   646,   648,   653,   655,   657,   662,
     666,   667,   668,   673,   675,   679,   681,   685,   688,   692,
     696,   701,   704,   705,   707,   709,   713,   719,   721,   725,
     726,   728,   736,   738,   742,   745,   748,   752,   754,   756,
     761,   765,   768,   770,   772,   774,   776,   780,   782,   786,
     788,   790,   797,   799,   801,   804,   807,   809,   813,   815,
     818,   821,   823,   827,   829,   833,   839,   841,   843,   845,
     848,   851,   855,   859,   861,   865,   869,   871,   875,   877,
     879,   883,   885,   889,   891,   893,   897,   903,   904,   905,
     907,   912,   917,   919,   923,   927,   930,   932,   936,   940,
     947,   954,   962,   964,   966,   970,   972,   974,   976,   980,
     984,   985,   989,   990,   993,   997,   999,  1001,  1004,  1008,
    1010,  1012,  1014,  1018,  1020,  1024,  1026,  1028,  1032,  1037,
    1038,  1041,  1044,  1046,  1048,  1052,  1054,  1058,  1060,  1061,
    1062,  1063,  1066,  1067,  1069,  1071,  1073,  1076,  1079,  1084,
    1086,  1090,  1092,  1096,  1098,  1100,  1102,  1104,  1108,  1112,
    1116,  1120,  1124,  1127,  1130,  1133,  1137,  1141,  1145,  1149,
    1153,  1157,  1161,  1165,  1169,  1173,  1177,  1180,  1184,  1188,
    1190,  1192,  1194,  1196,  1198,  1200,  1206,  1213,  1218,  1224,
    1228,  1230,  1232,  1238,  1243,  1246,  1247,  1249,  1255,  1256,
    1258,  1260,  1264,  1266,  1270,  1273,  1275,  1277,  1279,  1281,
    1283,  1285,  1289,  1293,  1299,  1301,  1303,  1307,  1310,  1316,
    1321,  1326,  1330,  1333,  1335,  1336,  1337,  1344,  1346,  1348,
    1350,  1355,  1361,  1363,  1368,  1374,  1375,  1377,  1381,  1383,
    1385,  1387,  1390,  1394,  1398,  1401,  1403,  1406,  1409,  1412,
    1416,  1424,  1428,  1432,  1434,  1437,  1440,  1442,  1445,  1449,
    1451,  1453,  1455,  1461,  1469,  1470,  1477,  1482,  1494,  1508,
    1513,  1517,  1521,  1529,  1538,  1542,  1544,  1547,  1550,  1554,
    1556,  1560,  1561,  1563,  1564,  1566,  1568,  1571,  1577,  1584,
    1586,  1590,  1594,  1595,  1598,  1600,  1606,  1614,  1615,  1617,
    1621,  1625,  1632,  1638,  1645,  1650,  1656,  1662,  1665,  1667,
    1669,  1680,  1682,  1686,  1691,  1695,  1699,  1703,  1707,  1714,
    1721,  1727,  1736,  1739,  1743,  1747,  1755,  1763,  1764,  1766,
    1771,  1774,  1779,  1781,  1784,  1787,  1789,  1791,  1792,  1793,
    1794,  1797,  1800,  1803,  1806,  1809,  1811,  1814,  1817,  1821,
    1826,  1829,  1833,  1835,  1839,  1843,  1845,  1847,  1849,  1853,
    1855,  1857,  1862,  1868,  1870,  1872,  1876,  1880,  1882,  1887,
    1889,  1891,  1893,  1896,  1899,  1902,  1904,  1908,  1912,  1917,
    1922,  1924,  1928,  1930,  1936,  1938,  1940,  1942,  1946,  1950,
    1954,  1958,  1962,  1966,  1968,  1972,  1978,  1984,  1990,  1991,
    1992,  1994,  1998,  2000,  2002,  2006,  2010,  2014,  2018,  2021,
    2025,  2029,  2030,  2032,  2034,  2036,  2038,  2040,  2042,  2044,
    2046,  2048,  2050,  2052,  2054,  2056,  2058,  2060,  2062,  2064,
    2066,  2068,  2070,  2072,  2074,  2076,  2078,  2080,  2082,  2084,
    2086,  2088,  2090,  2092,  2094,  2096,  2098,  2100,  2102,  2104,
    2106,  2108,  2110,  2112,  2114,  2116,  2118,  2120,  2122,  2124,
    2126,  2128,  2130,  2132,  2134,  2136,  2138,  2140,  2142,  2144,
    2146,  2148,  2150,  2152,  2154,  2156,  2158,  2162,  2166,  2169,
    2173,  2175,  2179,  2181,  2185,  2187,  2191,  2193,  2198,  2202,
    2204,  2208,  2210,  2214,  2219,  2221,  2226,  2231,  2236,  2240,
    2244,  2246,  2250,  2254,  2256,  2260,  2264,  2266,  2270,  2274,
    2276,  2280,  2281,  2287,  2294,  2303,  2305,  2309,  2311,  2313,
    2315,  2320,  2322,  2323,  2326,  2330,  2333,  2338,  2339,  2341,
    2347,  2352,  2359,  2364,  2366,  2371,  2376,  2378,  2385,  2387,
    2391,  2393,  2397,  2399,  2404,  2406,  2408,  2412,  2414,  2416,
    2420,  2422,  2423,  2425,  2428,  2432,  2434,  2437,  2443,  2448,
    2453,  2460,  2462,  2466,  2468,  2470,  2477,  2482,  2484,  2488,
    2490,  2492,  2494,  2496,  2498,  2502,  2504,  2506,  2508,  2515,
    2520,  2522,  2527,  2529,  2531,  2533,  2535,  2540,  2543,  2551,
    2553,  2558,  2560,  2562,  2574,  2575,  2578,  2582,  2584,  2588,
    2590,  2594,  2596,  2600,  2602,  2606,  2608,  2612,  2614,  2618,
    2627,  2629,  2633,  2636,  2639,  2647,  2649,  2653,  2657,  2659,
    2664,  2666,  2670,  2672,  2674,  2675,  2677,  2679,  2682,  2684,
    2686,  2688,  2690,  2692,  2694,  2696,  2698,  2700,  2702,  2704,
    2713,  2720,  2729,  2736,  2738,  2745,  2752,  2759,  2766,  2768,
    2772,  2778,  2780,  2784,  2791,  2793,  2797,  2806,  2813,  2820,
    2825,  2831,  2837,  2838,  2841,  2844,  2845,  2847,  2851,  2853,
    2858,  2866,  2868,  2872,  2876,  2878,  2882,  2888,  2892,  2896,
    2898,  2902,  2904,  2906,  2910,  2914,  2918,  2922,  2933,  2942,
    2953,  2954,  2955,  2957,  2960,  2965,  2970,  2977,  2979,  2981,
    2983,  2985,  2987,  2989,  2991,  2993,  2995,  2997,  2999,  3006,
    3011,  3016,  3020,  3030,  3032,  3034,  3038,  3040,  3046,  3052,
    3062,  3063,  3065,  3067,  3071,  3075,  3079,  3083,  3087,  3094,
    3098,  3102,  3106,  3110,  3118,  3124,  3126,  3128,  3132,  3137,
    3139,  3141,  3145,  3147,  3149,  3153,  3157,  3160,  3164,  3169,
    3174,  3180,  3186,  3188,  3191,  3196,  3201,  3206,  3207,  3209,
    3212,  3220,  3227,  3231,  3235,  3243,  3249,  3251,  3255,  3257,
    3262,  3265,  3269,  3273,  3278,  3285,  3289,  3292,  3296,  3298,
    3300,  3305,  3311,  3315,  3322,  3325,  3330,  3333,  3335,  3339,
    3343,  3344,  3346,  3350,  3353,  3356,  3359,  3362,  3372,  3378,
    3380,  3384,  3387,  3390,  3393,  3403,  3408,  3410,  3414,  3416,
    3418,  3421,  3422,  3430,  3432,  3437,  3439,  3443,  3445,  3447,
    3449,  3466,  3467,  3471,  3475,  3479,  3483,  3490,  3500,  3506,
    3508,  3512,  3518,  3520,  3522,  3524,  3526,  3528,  3530,  3532,
    3534,  3536,  3538,  3540,  3542,  3544,  3546,  3548,  3550,  3552,
    3554,  3556,  3558,  3560,  3562,  3564,  3566,  3568,  3570,  3572,
    3575,  3578,  3583,  3587,  3592,  3598,  3600,  3602,  3604,  3606,
    3608,  3610,  3612,  3614,  3616,  3622,  3625,  3628,  3631,  3634,
    3637,  3643,  3645,  3647,  3649,  3654,  3659,  3664,  3669,  3671,
    3673,  3675,  3677,  3679,  3681,  3683,  3685,  3687,  3689,  3691,
    3693,  3695,  3697,  3699,  3704,  3708,  3713,  3719,  3721,  3723,
    3725,  3727,  3732,  3736,  3739,  3744,  3748,  3753,  3757,  3762,
    3768,  3770,  3772,  3774,  3776,  3778,  3780,  3782,  3790,  3796,
    3798,  3800,  3802,  3804,  3809,  3813,  3818,  3824,  3826,  3828,
    3833,  3837,  3842,  3848,  3850,  3852,  3855,  3857,  3860,  3865,
    3869,  3874,  3878,  3883,  3889,  3891,  3893,  3895,  3897,  3899,
    3901,  3903,  3905,  3907,  3909,  3911,  3914,  3919,  3923,  3926,
    3931,  3935,  3938,  3942,  3945,  3948,  3951,  3954,  3957,  3960,
    3964,  3967,  3973,  3976,  3982,  3985,  3991,  3993,  3995,  3999,
    4003,  4004,  4005,  4007,  4009,  4011,  4013,  4015,  4017,  4019,
    4023,  4026,  4032,  4037,  4040,  4046,  4051,  4054,  4057,  4059,
    4061,  4065,  4068,  4071,  4074,  4079,  4084,  4089,  4094,  4099,
    4104,  4106,  4108,  4110,  4114,  4117,  4120,  4122,  4124,  4128,
    4131,  4134,  4136,  4138,  4140,  4142,  4144,  4146,  4152,  4158,
    4164,  4168,  4179,  4190,  4192,  4196,  4199,  4200,  4207,  4208,
    4215,  4218,  4220,  4224,  4226,  4228,  4230,  4236,  4242,  4248,
    4250,  4254,  4258,  4260,  4264,  4266,  4268,  4270,  4276,  4282,
    4288,  4290,  4294,  4297,  4303,  4306,  4312,  4318,  4321,  4327,
    4333,  4335,  4337,  4341,  4347,  4349,  4353,  4359,  4365,  4371,
    4377,  4385,  4387,  4391,  4394,  4397,  4400,  4403
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     358,     0,    -1,    -1,   358,   359,   116,    -1,   360,   361,
     578,    -1,   360,   378,   578,    -1,   360,   523,   578,    -1,
     360,   133,   374,    -1,   360,   247,    -1,   257,    -1,     1,
      -1,   150,    -1,   193,   362,   369,    -1,    57,   362,   370,
      -1,   233,   362,   364,   371,    -1,   363,   233,   362,   364,
     371,    -1,   124,   362,   365,   371,   367,    -1,   366,   371,
     367,    -1,   114,   368,   371,   367,    -1,   164,   362,   368,
      -1,    -1,   202,   375,    -1,   195,   375,    -1,    95,   375,
      -1,   368,    -1,   368,    -1,   399,   124,   362,   368,    -1,
     399,   363,   124,   362,   368,    -1,   363,   124,   362,   368,
      -1,   363,   399,   124,   362,   368,    -1,   375,   376,    -1,
     375,   213,    15,   368,    21,    -1,   129,    -1,    -1,   368,
      -1,    -1,   368,    -1,    -1,    15,    21,    -1,    15,   372,
      21,    -1,   373,    -1,   372,     8,   373,    -1,   368,    -1,
       5,    -1,    64,    -1,    -1,    -1,    -1,   383,    -1,   384,
      -1,   385,    -1,   415,    -1,   411,    -1,   579,    -1,   420,
      -1,   421,    -1,   422,    -1,   480,    -1,   401,    -1,   416,
      -1,   426,    -1,   216,   490,    -1,   216,   490,   491,   457,
      -1,   123,   489,    -1,   183,   490,    15,   463,    21,    -1,
     391,    -1,   392,    -1,   397,    -1,   394,    -1,   396,    -1,
     412,    -1,   413,    -1,   414,    -1,   379,    -1,   467,    -1,
     465,    -1,   393,    -1,   142,   490,    -1,   142,   490,   368,
      -1,   141,   490,    15,   381,    21,    -1,   140,   490,    15,
      26,    21,    -1,   107,   532,    -1,    10,    -1,   380,    -1,
     382,    -1,    17,    -1,    16,    -1,     5,    -1,     9,    -1,
      37,    -1,    23,    -1,    22,    -1,    35,    -1,    38,    -1,
      34,    -1,    25,    -1,    32,    -1,    29,    -1,    28,    -1,
      31,    -1,    30,    -1,    33,    -1,    24,    -1,   245,   490,
     491,   368,    -1,   245,     8,   490,   375,   390,   491,   368,
      -1,   112,   490,    -1,   112,   490,   368,    -1,   399,   386,
     368,   490,   473,   405,   410,    -1,   385,     8,   368,   473,
     405,   410,    -1,    -1,     7,     7,    -1,     8,   375,   387,
       7,     7,    -1,   388,    -1,   387,     8,   375,   388,    -1,
     183,    -1,   390,    -1,    44,    -1,    87,   473,    -1,   119,
      -1,   145,    15,   389,    21,    -1,   143,    -1,   178,    -1,
     187,    -1,   216,    -1,   230,    -1,   236,    -1,   375,   148,
      -1,   375,   180,    -1,   375,   147,    -1,   194,    -1,   191,
      -1,   145,   490,    15,   389,    21,   491,   368,    -1,   391,
       8,   368,    -1,   178,   490,   491,   368,    -1,   392,     8,
     368,    -1,   230,   490,   491,   419,    -1,   393,     8,   419,
      -1,   191,   490,    -1,   191,   490,   491,   395,   459,    -1,
      -1,   219,   490,    -1,   194,   490,    -1,   194,   490,   491,
     398,   459,    -1,    -1,   403,   400,   407,   400,    -1,   244,
      15,   368,    21,   400,    -1,    -1,   402,   368,    -1,   401,
       8,   368,    -1,    13,    -1,     6,    -1,   404,    -1,   144,
      -1,   200,    -1,    68,    -1,    90,    -1,    91,    -1,   154,
      -1,    63,    -1,    -1,   406,    -1,     5,   553,   510,   554,
     400,    -1,     5,   553,    15,   554,     5,    21,    -1,     5,
     553,    15,   554,   496,    21,    -1,    -1,   406,    -1,    15,
     574,   408,   409,    21,    -1,    15,   574,   408,   409,     8,
     574,   408,   409,    21,    -1,   496,    -1,     5,    -1,   565,
     496,    -1,   565,     5,    -1,    -1,    -1,    26,   496,    -1,
      18,   496,    -1,    87,   491,   490,   368,   473,    -1,   411,
       8,   368,   473,    -1,    44,   490,   491,   419,    -1,   412,
       8,   419,    -1,   187,   490,   491,   419,    -1,   413,     8,
     419,    -1,   236,   490,   491,   419,    -1,   414,     8,   419,
      -1,    67,   490,   419,    -1,    67,   490,   418,   419,    -1,
     415,   547,   418,   547,   419,    -1,   415,     8,   419,    -1,
     167,   490,   417,   499,    -1,   416,   547,   417,   547,   499,
      -1,   416,     8,   499,    -1,    37,   368,    37,    -1,    23,
      -1,    37,   368,    37,    -1,   368,   473,    -1,   119,   490,
     491,   368,    -1,   420,     8,   368,    -1,   143,   490,   491,
     368,    -1,   421,     8,   368,    -1,   117,   490,   423,    -1,
     422,     8,   423,    -1,    15,   424,    21,    -1,   425,     8,
     425,    -1,   424,     8,   425,    -1,   368,    -1,   368,    15,
     495,    21,    -1,   504,    -1,   427,    -1,    80,   489,   428,
     430,    -1,   427,   547,   430,    -1,    -1,    -1,   431,    37,
     432,    37,    -1,   433,    -1,   431,     8,   433,    -1,   444,
      -1,   432,     8,   444,    -1,   434,   436,    -1,   434,   436,
     437,    -1,   434,   436,   438,    -1,   434,   436,   437,   438,
      -1,   434,   441,    -1,    -1,   368,    -1,   368,    -1,    15,
     439,    21,    -1,    15,   440,     7,   440,    21,    -1,   453,
      -1,   439,     8,   453,    -1,    -1,   453,    -1,    15,   442,
       8,   435,    26,   439,    21,    -1,   443,    -1,   442,     8,
     443,    -1,   436,   437,    -1,   436,   438,    -1,   436,   437,
     438,    -1,   441,    -1,   445,    -1,   434,   435,     5,   445,
      -1,   448,     5,   445,    -1,   434,   435,    -1,   447,    -1,
     449,    -1,   451,    -1,    36,    -1,    36,   246,   513,    -1,
      27,    -1,    27,   246,   513,    -1,    64,    -1,   446,    -1,
     434,   499,    15,   574,   492,    21,    -1,    59,    -1,   448,
      -1,    17,   448,    -1,    16,   448,    -1,   149,    -1,   149,
     246,   513,    -1,   450,    -1,    17,   450,    -1,    16,   450,
      -1,   201,    -1,   201,   246,   513,    -1,    92,    -1,    92,
     246,   513,    -1,    15,   452,     8,   452,    21,    -1,   449,
      -1,   447,    -1,   454,    -1,    17,   454,    -1,    16,   454,
      -1,   453,    17,   454,    -1,   453,    16,   454,    -1,   455,
      -1,   454,     5,   455,    -1,   454,    37,   455,    -1,   456,
      -1,   456,     9,   455,    -1,   149,    -1,   435,    -1,    15,
     453,    21,    -1,   458,    -1,   457,     8,   458,    -1,   419,
      -1,   418,    -1,   460,   462,   461,    -1,   459,     8,   460,
     462,   461,    -1,    -1,    -1,   368,    -1,   177,    15,   381,
      21,    -1,    47,    15,    26,    21,    -1,   464,    -1,   463,
       8,   464,    -1,   368,    26,   496,    -1,   163,   466,    -1,
     368,    -1,   466,     8,   368,    -1,   248,   490,   468,    -1,
     248,   490,   468,     8,   377,   471,    -1,   248,   490,   468,
       8,   377,   172,    -1,   248,   490,   468,     8,   377,   172,
     469,    -1,   368,    -1,   470,    -1,   469,     8,   470,    -1,
     472,    -1,   368,    -1,   472,    -1,   471,     8,   472,    -1,
     368,    18,   368,    -1,    -1,    15,   474,    21,    -1,    -1,
     475,   476,    -1,   474,     8,   476,    -1,   477,    -1,     7,
      -1,   496,     7,    -1,   496,     7,   477,    -1,     5,    -1,
     496,    -1,   479,    -1,   478,     8,   479,    -1,   149,    -1,
     130,   490,   481,    -1,   131,    -1,   482,    -1,   481,     8,
     482,    -1,   483,    15,   486,    21,    -1,    -1,   484,   485,
      -1,   231,   404,    -1,   399,    -1,   487,    -1,   486,     8,
     487,    -1,   488,    -1,   488,    16,   488,    -1,   129,    -1,
      -1,    -1,    -1,     7,     7,    -1,    -1,   494,    -1,   496,
      -1,   514,    -1,   565,   496,    -1,   574,   493,    -1,   494,
       8,   574,   493,    -1,   496,    -1,   495,     8,   496,    -1,
     497,    -1,    15,   496,    21,    -1,   512,    -1,   500,    -1,
     508,    -1,   515,    -1,   496,    17,   496,    -1,   496,    16,
     496,    -1,   496,     5,   496,    -1,   496,    37,   496,    -1,
     496,     9,   496,    -1,   380,   496,    -1,    17,   496,    -1,
      16,   496,    -1,   496,    25,   496,    -1,   496,    29,   496,
      -1,   496,    31,   496,    -1,   496,    28,   496,    -1,   496,
      30,   496,    -1,   496,    32,   496,    -1,   496,    24,   496,
      -1,   496,    33,   496,    -1,   496,    38,   496,    -1,   496,
      35,   496,    -1,   496,    22,   496,    -1,    34,   496,    -1,
     496,    23,   496,    -1,   496,   380,   496,    -1,    17,    -1,
      16,    -1,   368,    -1,   499,    -1,   502,    -1,   501,    -1,
     499,    15,   574,   492,    21,    -1,   499,    15,   574,   492,
      21,   506,    -1,   502,    15,   492,    21,    -1,   502,    15,
     492,    21,   506,    -1,   500,     3,   129,    -1,   499,    -1,
     502,    -1,   499,    15,   574,   492,    21,    -1,   502,    15,
     492,    21,    -1,   499,   506,    -1,    -1,   506,    -1,    15,
     507,     7,   507,    21,    -1,    -1,   496,    -1,   509,    -1,
     509,   246,   513,    -1,   510,    -1,   510,   246,   513,    -1,
     511,   505,    -1,    36,    -1,    27,    -1,   201,    -1,    92,
      -1,   149,    -1,    64,    -1,   499,   246,    64,    -1,   510,
     246,    64,    -1,    15,   497,     8,   497,    21,    -1,   499,
      -1,   510,    -1,   496,     7,   496,    -1,   496,     7,    -1,
     496,     7,   496,     7,   496,    -1,   496,     7,     7,   496,
      -1,     7,   496,     7,   496,    -1,     7,     7,   496,    -1,
       7,   496,    -1,     7,    -1,    -1,    -1,    14,   409,   516,
     571,   517,    20,    -1,   499,    -1,   502,    -1,   503,    -1,
     519,     8,   574,   503,    -1,   519,     8,   574,   565,   499,
      -1,   518,    -1,   520,     8,   574,   518,    -1,   520,     8,
     574,   565,   499,    -1,    -1,   499,    -1,   522,     8,   499,
      -1,   544,    -1,   543,    -1,   526,    -1,   534,   526,    -1,
     102,   552,   532,    -1,   103,   552,   531,    -1,   108,   532,
      -1,   524,    -1,   534,   524,    -1,   535,   544,    -1,   535,
     239,    -1,   534,   535,   239,    -1,    97,   552,    15,   496,
      21,   239,   531,    -1,    96,   552,   531,    -1,   106,   552,
     531,    -1,   527,    -1,    76,   552,    -1,   536,   544,    -1,
     536,    -1,   534,   536,    -1,   105,   552,   531,    -1,   580,
      -1,   844,    -1,   862,    -1,    89,   552,    15,   496,    21,
      -1,    89,   552,   553,   542,   554,   614,   525,    -1,    -1,
       8,   375,   254,    15,   496,    21,    -1,   254,    15,   496,
      21,    -1,   185,   552,   553,   542,   554,   547,   540,    26,
     496,     8,   496,    -1,   185,   552,   553,   542,   554,   547,
     540,    26,   496,     8,   496,     8,   496,    -1,    62,   552,
     528,   531,    -1,    84,   552,   531,    -1,   110,   552,   531,
      -1,   218,   552,   375,    62,    15,   496,    21,    -1,   534,
     218,   552,   375,    62,    15,   496,    21,    -1,    15,   530,
      21,    -1,   496,    -1,   496,     7,    -1,     7,   496,    -1,
     496,     7,   496,    -1,   529,    -1,   530,     8,   529,    -1,
      -1,   368,    -1,    -1,   368,    -1,    75,    -1,   533,     7,
      -1,   155,   552,    15,   496,    21,    -1,   122,   552,    15,
     537,   539,    21,    -1,   538,    -1,   537,     8,   538,    -1,
     540,    26,   514,    -1,    -1,     8,   496,    -1,   368,    -1,
     540,    26,   496,     8,   496,    -1,   540,    26,   496,     8,
     496,     8,   496,    -1,    -1,   149,    -1,   113,   552,   531,
      -1,    98,   552,   531,    -1,    98,   552,    15,   496,    21,
     531,    -1,   252,   552,    15,   496,    21,    -1,   534,   252,
     552,    15,   496,    21,    -1,   545,   496,    26,   496,    -1,
     188,   552,   500,    18,   496,    -1,    48,   552,   479,   240,
     368,    -1,    77,   552,    -1,   546,    -1,   555,    -1,    46,
     552,    15,   496,    21,   479,     8,   479,     8,   479,    -1,
     548,    -1,   548,    15,    21,    -1,   548,    15,   549,    21,
      -1,   214,   552,   507,    -1,   551,   552,   507,    -1,    79,
     552,   531,    -1,   115,   552,   531,    -1,    45,   552,    15,
     521,   519,    21,    -1,    81,   552,    15,   521,   520,    21,
      -1,   170,   552,    15,   522,    21,    -1,   253,   552,    15,
     496,    21,   500,    26,   496,    -1,   152,   429,    -1,   186,
     552,   479,    -1,    49,   552,   368,    -1,    49,   552,   368,
     547,    15,   478,    21,    -1,    69,   552,    15,   478,    21,
     547,   496,    -1,    -1,     8,    -1,    61,   552,   368,   574,
      -1,   574,   550,    -1,   549,     8,   574,   550,    -1,   496,
      -1,   565,   496,    -1,     5,   479,    -1,   184,    -1,   232,
      -1,    -1,    -1,    -1,   556,   562,    -1,   556,   577,    -1,
     556,     5,    -1,   556,     9,    -1,   558,   562,    -1,   560,
      -1,   566,   562,    -1,   566,   561,    -1,   566,   562,   569,
      -1,   566,   561,     8,   569,    -1,   567,   562,    -1,   567,
     562,   571,    -1,   568,    -1,   568,     8,   571,    -1,   557,
     552,   575,    -1,    53,    -1,   215,    -1,   104,    -1,   559,
     552,   575,    -1,   176,    -1,    66,    -1,   139,   552,   575,
     562,    -1,   139,   552,   575,   562,   571,    -1,   577,    -1,
       5,    -1,    15,   576,    21,    -1,    15,   563,    21,    -1,
     564,    -1,   563,     8,   574,   564,    -1,   576,    -1,     5,
      -1,     9,    -1,   565,   496,    -1,   565,     5,    -1,   565,
       9,    -1,   166,    -1,   197,   552,   575,    -1,   256,   552,
     575,    -1,   190,   552,   576,   575,    -1,   190,   552,     5,
     575,    -1,   570,    -1,   569,     8,   570,    -1,   500,    -1,
      15,   569,     8,   541,    21,    -1,   497,    -1,   573,    -1,
     572,    -1,   497,     8,   497,    -1,   497,     8,   573,    -1,
     573,     8,   497,    -1,   573,     8,   573,    -1,   572,     8,
     497,    -1,   572,     8,   573,    -1,   512,    -1,    15,   496,
      21,    -1,    15,   497,     8,   541,    21,    -1,    15,   573,
       8,   541,    21,    -1,    15,   572,     8,   541,    21,    -1,
      -1,    -1,   577,    -1,    15,   576,    21,    -1,   500,    -1,
     508,    -1,   576,   498,   576,    -1,   576,     5,   576,    -1,
     576,    37,   576,    -1,   576,     9,   576,    -1,   498,   576,
      -1,   576,    23,   576,    -1,   129,    26,   496,    -1,    -1,
     257,    -1,   581,    -1,   629,    -1,   604,    -1,   583,    -1,
     594,    -1,   589,    -1,   641,    -1,   644,    -1,   722,    -1,
     586,    -1,   595,    -1,   597,    -1,   599,    -1,   601,    -1,
     649,    -1,   655,    -1,   652,    -1,   781,    -1,   779,    -1,
     605,    -1,   630,    -1,   659,    -1,   711,    -1,   709,    -1,
     710,    -1,   712,    -1,   713,    -1,   714,    -1,   715,    -1,
     716,    -1,   724,    -1,   726,    -1,   731,    -1,   728,    -1,
     730,    -1,   734,    -1,   732,    -1,   733,    -1,   745,    -1,
     749,    -1,   750,    -1,   753,    -1,   752,    -1,   754,    -1,
     755,    -1,   756,    -1,   757,    -1,   658,    -1,   739,    -1,
     740,    -1,   741,    -1,   744,    -1,   758,    -1,   761,    -1,
     766,    -1,   771,    -1,   773,    -1,   774,    -1,   775,    -1,
     776,    -1,   778,    -1,   737,    -1,   780,    -1,    82,   490,
     582,    -1,   581,     8,   582,    -1,   368,   473,    -1,    94,
     490,   584,    -1,   585,    -1,   584,     8,   585,    -1,   368,
      -1,   138,   490,   587,    -1,   588,    -1,   587,     8,   588,
      -1,   368,    -1,   228,   490,   593,   590,    -1,    15,   591,
      21,    -1,   592,    -1,   591,     8,   592,    -1,   496,    -1,
     496,     7,   496,    -1,     7,    15,   495,    21,    -1,   368,
      -1,   259,   490,   368,   473,    -1,   303,   490,   368,   473,
      -1,   594,     8,   368,   473,    -1,   136,   490,   596,    -1,
     595,     8,   596,    -1,   368,    -1,   211,   490,   598,    -1,
     597,     8,   598,    -1,   368,    -1,   205,   490,   600,    -1,
     599,     8,   600,    -1,   368,    -1,    70,   490,   602,    -1,
     601,     8,   602,    -1,   368,    -1,   175,   368,   473,    -1,
      -1,    88,   490,   608,   611,   603,    -1,   204,   552,   608,
     612,   614,   603,    -1,   204,   552,   612,   614,   603,     7,
       7,   606,    -1,   607,    -1,   606,     8,   607,    -1,   608,
      -1,   609,    -1,   368,    -1,   368,    15,   495,    21,    -1,
     368,    -1,    -1,   612,   614,    -1,    15,   613,    21,    -1,
     614,   615,    -1,   613,     8,   614,   615,    -1,    -1,    58,
      -1,    58,    15,   574,   628,    21,    -1,   126,    15,   616,
      21,    -1,   258,    15,   616,     8,   496,    21,    -1,   165,
      15,   496,    21,    -1,     5,    -1,   137,    15,   616,    21,
      -1,    86,    15,   617,    21,    -1,   368,    -1,    15,   618,
      21,   375,   255,   620,    -1,   619,    -1,   618,     8,   619,
      -1,   496,    -1,   496,     7,   496,    -1,   621,    -1,   621,
      15,   622,    21,    -1,   368,    -1,   623,    -1,   622,     8,
     623,    -1,   496,    -1,   770,    -1,    40,   624,   625,    -1,
     368,    -1,    -1,   626,    -1,    17,   627,    -1,   625,    17,
     627,    -1,   496,    -1,   565,   496,    -1,   565,   496,     8,
     565,   496,    -1,    43,   490,   632,   634,    -1,   199,   552,
     633,   634,    -1,   199,   552,   634,     7,     7,   631,    -1,
     633,    -1,   631,     8,   633,    -1,   368,    -1,   499,    -1,
      15,   639,    21,   375,   255,   635,    -1,   638,    15,   636,
      21,    -1,   637,    -1,   636,     8,   637,    -1,   496,    -1,
       5,    -1,   514,    -1,   368,    -1,   640,    -1,   639,     8,
     640,    -1,   368,    -1,     5,    -1,     7,    -1,   642,     7,
       7,   490,   368,   473,    -1,   641,     8,   368,   473,    -1,
     643,    -1,   642,     8,   375,   643,    -1,    82,    -1,   259,
      -1,   303,    -1,    94,    -1,    87,    15,   474,    21,    -1,
     228,   590,    -1,    43,    15,   639,    21,   375,   255,   635,
      -1,    43,    -1,    88,   612,   614,   603,    -1,    88,    -1,
      67,    -1,   399,     8,   375,    93,   490,    15,   645,    21,
       7,     7,   647,    -1,    -1,   646,     7,    -1,   645,     8,
       7,    -1,   648,    -1,   647,     8,   648,    -1,   368,    -1,
     127,   490,   650,    -1,   651,    -1,   650,     8,   651,    -1,
     368,    -1,    74,   490,   653,    -1,   654,    -1,   653,     8,
     654,    -1,   368,    -1,    51,   490,   656,    -1,    51,   490,
       8,   375,    67,     7,     7,   656,    -1,   657,    -1,   656,
       8,   657,    -1,   368,   473,    -1,   168,   552,    -1,   182,
     552,    15,   660,    21,   661,   665,    -1,   499,    -1,   660,
       8,   499,    -1,   614,   173,   662,    -1,   614,    -1,   499,
      15,   663,    21,    -1,   664,    -1,   663,     8,   664,    -1,
     496,    -1,     5,    -1,    -1,   666,    -1,   667,    -1,   666,
     667,    -1,   671,    -1,   694,    -1,   702,    -1,   668,    -1,
     678,    -1,   680,    -1,   679,    -1,   669,    -1,   672,    -1,
     673,    -1,   676,    -1,     8,   375,   209,    15,   717,     7,
     718,    21,    -1,     8,   375,   209,    15,   718,    21,    -1,
       8,   375,    71,    15,   670,     7,   718,    21,    -1,     8,
     375,    71,    15,   718,    21,    -1,   368,    -1,     8,   375,
     169,    15,   675,    21,    -1,     8,   375,   282,    15,   675,
      21,    -1,     8,   375,   191,    15,   675,    21,    -1,     8,
     375,   320,    15,   674,    21,    -1,   496,    -1,   496,     8,
     496,    -1,   496,     8,   496,     8,   496,    -1,   500,    -1,
     675,     8,   500,    -1,     8,   375,   322,    15,   677,    21,
      -1,   662,    -1,   677,     8,   662,    -1,     8,   375,   135,
      15,   717,     7,   735,    21,    -1,     8,   375,   135,    15,
     735,    21,    -1,     8,   375,   229,    15,   496,    21,    -1,
       8,   375,    41,   681,    -1,     8,   375,    41,   681,   681,
      -1,    15,   682,   683,   684,    21,    -1,    -1,   148,     7,
      -1,   180,     7,    -1,    -1,   685,    -1,   684,     8,   685,
      -1,   707,    -1,   707,    15,   686,    21,    -1,   707,    15,
     686,    21,    15,   688,    21,    -1,   687,    -1,   686,     8,
     687,    -1,   496,     7,   496,    -1,   689,    -1,   688,     8,
     689,    -1,   690,     7,   691,     7,   692,    -1,   690,     7,
     691,    -1,   690,     7,   692,    -1,   690,    -1,   691,     7,
     692,    -1,   691,    -1,   692,    -1,   375,   217,   693,    -1,
     375,   157,   693,    -1,   375,   128,   693,    -1,    15,   494,
      21,    -1,     8,   375,   208,    15,   695,   699,   696,     8,
     698,    21,    -1,     8,   375,   208,    15,   695,   699,   696,
      21,    -1,     8,   375,   208,    15,   695,   697,   696,     7,
     698,    21,    -1,    -1,    -1,   368,    -1,   375,   699,    -1,
     698,     8,   375,   699,    -1,   700,    15,   500,    21,    -1,
     701,    15,   675,     8,   496,    21,    -1,   234,    -1,   192,
      -1,   162,    -1,   159,    -1,    35,    -1,    22,    -1,    24,
      -1,    33,    -1,   247,    -1,   158,    -1,   161,    -1,     8,
     375,   223,    15,   704,    21,    -1,     8,   375,   224,   703,
      -1,     8,   375,   226,   703,    -1,     8,   375,   221,    -1,
       8,   375,   221,    15,   707,    15,   591,    21,    21,    -1,
     368,    -1,   705,    -1,   704,     8,   705,    -1,   707,    -1,
     707,    15,   706,    78,    21,    -1,   707,    15,   706,   591,
      21,    -1,   707,    15,   706,   591,    21,    15,   375,    78,
      21,    -1,    -1,   499,    -1,   707,    -1,   708,     8,   707,
      -1,   225,   552,   703,    -1,   224,   552,   703,    -1,   227,
     552,   703,    -1,   226,   552,   703,    -1,   222,   552,   703,
      15,   704,    21,    -1,   206,   552,   697,    -1,   207,   552,
     697,    -1,    72,   552,   670,    -1,    73,   552,   670,    -1,
     210,   552,    15,   717,     7,   718,    21,    -1,   210,   552,
      15,   718,    21,    -1,   368,    -1,   719,    -1,   718,     8,
     719,    -1,   707,    15,   720,    21,    -1,   707,    -1,   721,
      -1,   720,     8,   721,    -1,   496,    -1,     7,    -1,   237,
     490,   723,    -1,   722,     8,   723,    -1,   368,   473,    -1,
     238,   552,   725,    -1,   238,   552,   725,   694,    -1,   238,
     552,   725,   669,    -1,   238,   552,   725,   694,   669,    -1,
     238,   552,   725,   669,   694,    -1,   368,    -1,   111,   552,
      -1,   725,    15,   496,    21,    -1,   725,    15,   514,    21,
      -1,   174,   552,   501,   729,    -1,    -1,   672,    -1,   109,
     552,    -1,   160,   552,   727,   375,   175,   610,   473,    -1,
     160,   552,   727,   375,   323,   500,    -1,   189,   552,   717,
      -1,   212,   552,   717,    -1,   135,   552,    15,   717,     7,
     735,    21,    -1,   135,   552,    15,   735,    21,    -1,   736,
      -1,   735,     8,   736,    -1,   707,    -1,   707,    15,   495,
      21,    -1,   134,   552,    -1,   134,   552,   671,    -1,   134,
     552,   738,    -1,   134,   552,   671,   738,    -1,     8,   375,
     208,    15,   675,    21,    -1,    50,   552,   743,    -1,    99,
     552,    -1,    52,   552,   743,    -1,   368,    -1,   742,    -1,
     742,    15,   495,    21,    -1,   120,   552,   500,    26,   500,
      -1,    83,   552,   748,    -1,    83,   552,   748,    15,   746,
      21,    -1,   574,   747,    -1,   746,     8,   574,   747,    -1,
     565,   496,    -1,   149,    -1,   100,   552,   748,    -1,   146,
     552,   751,    -1,    -1,   496,    -1,   332,   552,   495,    -1,
     101,   552,    -1,   241,   552,    -1,   242,   552,    -1,    56,
     552,    -1,    65,   552,   574,    15,   549,    21,   409,   491,
     675,    -1,   324,   552,    15,   759,    21,    -1,   760,    -1,
     759,     8,   760,    -1,   375,   315,    -1,   375,   318,    -1,
     375,   182,    -1,   220,   552,    15,   762,    26,   627,    21,
     614,   765,    -1,   499,    15,   763,    21,    -1,   764,    -1,
     763,     8,   764,    -1,   617,    -1,   770,    -1,   132,   708,
      -1,    -1,   153,   552,    15,   499,    18,   767,    21,    -1,
     499,    -1,   499,    15,   768,    21,    -1,   769,    -1,   768,
       8,   769,    -1,   770,    -1,     7,    -1,     5,    -1,   325,
     552,   496,     8,   375,   330,    15,   675,    21,     8,   375,
     329,    15,   495,    21,   772,    -1,    -1,     8,   375,   182,
      -1,     8,   375,   318,    -1,   326,   552,   496,    -1,   327,
     552,   496,    -1,   327,   552,   496,     8,   375,   315,    -1,
     328,   552,   496,     8,   375,   331,    15,   499,    21,    -1,
     333,   552,    15,   777,    21,    -1,   503,    -1,   777,     8,
     503,    -1,   334,   552,    15,   660,    21,    -1,   830,    -1,
     783,    -1,   782,    -1,   800,    -1,   803,    -1,   804,    -1,
     805,    -1,   806,    -1,   812,    -1,   815,    -1,   820,    -1,
     821,    -1,   822,    -1,   825,    -1,   826,    -1,   827,    -1,
     828,    -1,   829,    -1,   831,    -1,   832,    -1,   833,    -1,
     834,    -1,   835,    -1,   836,    -1,   837,    -1,   838,    -1,
     839,    -1,   268,   552,    -1,   275,   552,    -1,   291,   552,
     614,   784,    -1,   291,   552,   614,    -1,   547,   614,   785,
     614,    -1,   784,   547,   614,   785,   614,    -1,   787,    -1,
     796,    -1,   791,    -1,   792,    -1,   788,    -1,   789,    -1,
     790,    -1,   794,    -1,   795,    -1,   842,    15,   843,   841,
      21,    -1,   191,   786,    -1,   282,   786,    -1,   285,   786,
      -1,   265,   786,    -1,   299,   786,    -1,    84,    15,   375,
     793,    21,    -1,   191,    -1,   299,    -1,   288,    -1,   304,
      15,   496,    21,    -1,   289,    15,   496,    21,    -1,   208,
      15,   797,    21,    -1,   614,   799,     7,   798,    -1,   675,
      -1,    17,    -1,    16,    -1,     5,    -1,    37,    -1,   162,
      -1,   159,    -1,    35,    -1,    22,    -1,    24,    -1,    33,
      -1,   305,    -1,   306,    -1,   307,    -1,   247,    -1,   297,
     552,   614,   801,    -1,   297,   552,   614,    -1,   547,   614,
     802,   614,    -1,   801,   547,   614,   802,   614,    -1,   787,
      -1,   796,    -1,   788,    -1,   789,    -1,   279,   552,   614,
     819,    -1,   279,   552,   614,    -1,   296,   552,    -1,   269,
     552,   614,   807,    -1,   269,   552,   614,    -1,   272,   552,
     614,   819,    -1,   272,   552,   614,    -1,   547,   614,   808,
     614,    -1,   807,   547,   614,   808,   614,    -1,   787,    -1,
     796,    -1,   788,    -1,   789,    -1,   810,    -1,   809,    -1,
     290,    -1,   298,    15,   375,   811,     8,   496,    21,    -1,
     298,    15,   375,   811,    21,    -1,   230,    -1,    94,    -1,
     284,    -1,   295,    -1,   300,   552,   614,   813,    -1,   300,
     552,   614,    -1,   547,   614,   814,   614,    -1,   813,   547,
     614,   814,   614,    -1,   787,    -1,   788,    -1,   280,   552,
     614,   816,    -1,   280,   552,   614,    -1,   547,   614,   817,
     614,    -1,   816,   547,   614,   817,   614,    -1,   819,    -1,
     818,    -1,   266,   786,    -1,   287,    -1,   302,   552,    -1,
     281,   552,   614,   819,    -1,   281,   552,   614,    -1,   292,
     552,   614,   823,    -1,   292,   552,   614,    -1,   547,   614,
     824,   614,    -1,   823,   547,   614,   824,   614,    -1,   787,
      -1,   796,    -1,   791,    -1,   792,    -1,   788,    -1,   789,
      -1,   790,    -1,   794,    -1,   795,    -1,   810,    -1,   809,
      -1,   276,   552,    -1,   293,   552,   614,   784,    -1,   293,
     552,   614,    -1,   277,   552,    -1,   294,   552,   614,   784,
      -1,   294,   552,   614,    -1,   278,   552,    -1,   301,   490,
     786,    -1,   286,   552,    -1,   273,   552,    -1,   290,   552,
      -1,   274,   552,    -1,   264,   552,    -1,   263,   552,    -1,
     283,   552,   786,    -1,   283,   552,    -1,   267,   552,    15,
     499,    21,    -1,   267,   552,    -1,   271,   552,    15,   499,
      21,    -1,   271,   552,    -1,    37,   368,   842,    37,   843,
      -1,   840,    -1,   499,    -1,   841,     8,   840,    -1,   841,
       8,   499,    -1,    -1,    -1,   845,    -1,   858,    -1,   846,
      -1,   859,    -1,   847,    -1,   848,    -1,   860,    -1,   308,
     552,   849,    -1,   310,   552,    -1,   312,   552,    15,   855,
      21,    -1,   312,   552,    15,    21,    -1,   312,   552,    -1,
     313,   552,    15,   855,    21,    -1,   313,   552,    15,    21,
      -1,   313,   552,    -1,   375,   376,    -1,   850,    -1,   851,
      -1,   850,     8,   851,    -1,   375,   852,    -1,   375,   854,
      -1,   375,   853,    -1,   147,    15,   855,    21,    -1,   148,
      15,   855,    21,    -1,   180,    15,   855,    21,    -1,   318,
      15,   855,    21,    -1,   319,    15,   855,    21,    -1,   314,
      15,   856,    21,    -1,   315,    -1,   675,    -1,   857,    -1,
     856,     8,   857,    -1,   375,   316,    -1,   375,   317,    -1,
     309,    -1,   311,    -1,   321,   490,   861,    -1,   375,   376,
      -1,   375,   853,    -1,   863,    -1,   864,    -1,   865,    -1,
     866,    -1,   871,    -1,   889,    -1,   335,   895,    15,   872,
      21,    -1,   336,   895,    15,   879,    21,    -1,   337,   895,
      15,   884,    21,    -1,   339,   895,   887,    -1,   339,   895,
     887,     8,   375,   351,    15,   867,    21,   869,    -1,   339,
     895,   887,     8,   375,   352,    15,   867,    21,   870,    -1,
     868,    -1,   867,     8,   868,    -1,   375,   353,    -1,    -1,
       8,   375,   352,    15,   867,    21,    -1,    -1,     8,   375,
     351,    15,   867,    21,    -1,   340,   895,    -1,   873,    -1,
     872,     8,   873,    -1,   874,    -1,   875,    -1,   876,    -1,
     375,   208,    15,   698,    21,    -1,   375,   191,    15,   675,
      21,    -1,   375,   183,    15,   877,    21,    -1,   878,    -1,
     877,     8,   878,    -1,   503,    26,   496,    -1,   880,    -1,
     879,     8,   880,    -1,   881,    -1,   882,    -1,   883,    -1,
     375,   228,    15,   704,    21,    -1,   375,    41,    15,   704,
      21,    -1,   375,   209,    15,   718,    21,    -1,   885,    -1,
     884,     8,   885,    -1,   375,   338,    -1,   375,   342,    15,
     660,    21,    -1,   375,   341,    -1,   375,   341,    15,   660,
      21,    -1,   375,   343,    15,   888,    21,    -1,   375,   354,
      -1,   375,   354,    15,   886,    21,    -1,   496,     8,   496,
       8,   496,    -1,   368,    -1,   503,    -1,   888,     8,   503,
      -1,   344,   895,    15,   890,    21,    -1,   891,    -1,   890,
       8,   891,    -1,   375,   244,    15,   892,    21,    -1,   375,
     330,    15,   660,    21,    -1,   375,   345,    15,   660,    21,
      -1,   375,   346,    15,   496,    21,    -1,   375,   347,    15,
     894,     8,   496,    21,    -1,   893,    -1,   892,     8,   893,
      -1,   375,   315,    -1,   375,   350,    -1,   375,   348,    -1,
     375,   349,    -1,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   788,   788,   789,   793,   795,   809,   840,   849,   855,
     875,   884,   900,   912,   922,   929,   935,   940,   945,   969,
     996,  1010,  1012,  1014,  1018,  1035,  1049,  1073,  1089,  1103,
    1121,  1123,  1130,  1134,  1135,  1142,  1143,  1151,  1152,  1154,
    1158,  1159,  1163,  1167,  1173,  1183,  1187,  1192,  1199,  1200,
    1201,  1202,  1203,  1204,  1205,  1206,  1207,  1208,  1209,  1210,
    1211,  1212,  1217,  1222,  1229,  1231,  1232,  1233,  1234,  1235,
    1236,  1237,  1238,  1239,  1240,  1241,  1242,  1245,  1249,  1257,
    1265,  1274,  1282,  1286,  1288,  1292,  1294,  1296,  1298,  1300,
    1302,  1304,  1306,  1308,  1310,  1312,  1314,  1316,  1318,  1320,
    1322,  1324,  1326,  1331,  1340,  1350,  1358,  1368,  1389,  1409,
    1410,  1412,  1416,  1418,  1422,  1426,  1428,  1432,  1438,  1442,
    1444,  1448,  1452,  1456,  1460,  1464,  1470,  1474,  1478,  1484,
    1489,  1496,  1507,  1520,  1531,  1544,  1554,  1567,  1572,  1579,
    1582,  1587,  1592,  1599,  1602,  1612,  1626,  1629,  1648,  1675,
    1677,  1689,  1697,  1698,  1699,  1700,  1701,  1702,  1703,  1708,
    1709,  1713,  1715,  1722,  1727,  1728,  1730,  1732,  1745,  1751,
    1757,  1766,  1775,  1788,  1789,  1792,  1796,  1811,  1826,  1844,
    1865,  1885,  1907,  1924,  1942,  1949,  1956,  1963,  1976,  1983,
    1990,  2001,  2005,  2007,  2012,  2030,  2041,  2053,  2065,  2079,
    2085,  2092,  2098,  2104,  2112,  2119,  2135,  2138,  2147,  2149,
    2153,  2157,  2177,  2181,  2183,  2187,  2188,  2191,  2193,  2195,
    2197,  2199,  2202,  2205,  2209,  2215,  2219,  2223,  2225,  2230,
    2231,  2235,  2239,  2241,  2245,  2247,  2249,  2254,  2258,  2260,
    2262,  2265,  2267,  2268,  2269,  2270,  2271,  2272,  2273,  2274,
    2277,  2278,  2284,  2287,  2288,  2290,  2294,  2295,  2298,  2299,
    2301,  2305,  2306,  2307,  2308,  2310,  2313,  2314,  2323,  2325,
    2332,  2339,  2346,  2355,  2357,  2359,  2363,  2365,  2369,  2378,
    2385,  2392,  2394,  2398,  2402,  2408,  2410,  2415,  2419,  2423,
    2430,  2437,  2447,  2449,  2453,  2465,  2468,  2477,  2490,  2496,
    2502,  2508,  2516,  2526,  2528,  2532,  2534,  2567,  2569,  2573,
    2612,  2613,  2617,  2617,  2622,  2626,  2634,  2643,  2652,  2662,
    2668,  2671,  2673,  2677,  2685,  2700,  2707,  2709,  2713,  2729,
    2729,  2733,  2735,  2747,  2749,  2753,  2759,  2771,  2783,  2800,
    2829,  2830,  2838,  2839,  2843,  2845,  2847,  2858,  2862,  2868,
    2870,  2874,  2876,  2878,  2882,  2884,  2888,  2890,  2892,  2894,
    2896,  2898,  2900,  2902,  2904,  2906,  2908,  2910,  2912,  2914,
    2916,  2918,  2920,  2922,  2924,  2926,  2928,  2930,  2932,  2936,
    2937,  2948,  3022,  3034,  3036,  3040,  3171,  3221,  3265,  3307,
    3365,  3367,  3369,  3408,  3451,  3462,  3463,  3467,  3472,  3473,
    3477,  3479,  3485,  3487,  3493,  3506,  3512,  3519,  3525,  3533,
    3541,  3557,  3567,  3580,  3587,  3589,  3612,  3614,  3616,  3618,
    3620,  3622,  3624,  3626,  3630,  3630,  3630,  3644,  3646,  3669,
    3671,  3673,  3689,  3691,  3693,  3707,  3710,  3712,  3720,  3722,
    3724,  3726,  3780,  3800,  3815,  3824,  3827,  3877,  3883,  3888,
    3906,  3908,  3910,  3912,  3914,  3917,  3923,  3925,  3927,  3930,
    3932,  3934,  3961,  3970,  3979,  3980,  3982,  3987,  3994,  4002,
    4004,  4008,  4011,  4013,  4017,  4023,  4025,  4027,  4029,  4033,
    4035,  4044,  4045,  4052,  4053,  4057,  4061,  4082,  4085,  4089,
    4091,  4098,  4103,  4104,  4115,  4127,  4150,  4175,  4176,  4183,
    4185,  4187,  4189,  4191,  4195,  4272,  4284,  4291,  4293,  4294,
    4296,  4305,  4312,  4319,  4327,  4332,  4337,  4340,  4343,  4346,
    4349,  4352,  4356,  4374,  4379,  4398,  4417,  4421,  4422,  4425,
    4429,  4434,  4441,  4443,  4445,  4449,  4450,  4461,  4476,  4480,
    4487,  4490,  4500,  4513,  4526,  4529,  4531,  4534,  4537,  4541,
    4550,  4553,  4557,  4559,  4565,  4569,  4571,  4573,  4580,  4584,
    4586,  4590,  4592,  4596,  4615,  4631,  4640,  4649,  4651,  4655,
    4681,  4696,  4711,  4728,  4736,  4745,  4753,  4758,  4763,  4785,
    4801,  4803,  4807,  4809,  4816,  4818,  4820,  4824,  4826,  4828,
    4830,  4832,  4834,  4838,  4841,  4844,  4850,  4856,  4865,  4869,
    4876,  4878,  4882,  4884,  4886,  4891,  4896,  4901,  4906,  4915,
    4920,  4926,  4927,  4942,  4943,  4944,  4945,  4946,  4947,  4948,
    4949,  4950,  4951,  4952,  4953,  4954,  4955,  4956,  4957,  4958,
    4959,  4960,  4963,  4964,  4965,  4966,  4967,  4968,  4969,  4970,
    4971,  4972,  4973,  4974,  4975,  4976,  4977,  4978,  4979,  4980,
    4981,  4982,  4983,  4984,  4985,  4986,  4987,  4988,  4989,  4990,
    4991,  4992,  4993,  4994,  4995,  4996,  4997,  4998,  4999,  5000,
    5001,  5002,  5003,  5004,  5005,  5006,  5010,  5012,  5023,  5044,
    5048,  5050,  5054,  5067,  5071,  5073,  5077,  5088,  5099,  5103,
    5105,  5109,  5111,  5113,  5128,  5140,  5160,  5180,  5202,  5208,
    5217,  5225,  5231,  5239,  5246,  5252,  5261,  5265,  5271,  5279,
    5293,  5307,  5312,  5328,  5343,  5371,  5373,  5377,  5379,  5383,
    5412,  5435,  5456,  5457,  5461,  5482,  5484,  5488,  5496,  5500,
    5505,  5507,  5509,  5511,  5517,  5519,  5523,  5533,  5537,  5539,
    5544,  5546,  5550,  5554,  5560,  5570,  5572,  5576,  5578,  5580,
    5587,  5605,  5606,  5610,  5612,  5616,  5623,  5633,  5662,  5677,
    5684,  5702,  5704,  5708,  5722,  5748,  5761,  5777,  5779,  5782,
    5784,  5790,  5794,  5822,  5824,  5828,  5836,  5842,  5845,  5903,
    5967,  5969,  5972,  5976,  5980,  5984,  6001,  6013,  6017,  6021,
    6031,  6036,  6041,  6048,  6057,  6057,  6068,  6079,  6081,  6085,
    6096,  6100,  6102,  6106,  6117,  6121,  6123,  6127,  6139,  6141,
    6148,  6150,  6154,  6170,  6178,  6189,  6191,  6195,  6198,  6203,
    6213,  6215,  6219,  6221,  6230,  6231,  6235,  6237,  6242,  6243,
    6244,  6245,  6246,  6247,  6248,  6249,  6250,  6251,  6252,  6255,
    6260,  6264,  6268,  6272,  6285,  6289,  6293,  6297,  6300,  6302,
    6304,  6308,  6310,  6314,  6318,  6320,  6324,  6329,  6333,  6337,
    6339,  6343,  6352,  6355,  6361,  6368,  6371,  6373,  6377,  6379,
    6383,  6395,  6397,  6401,  6405,  6407,  6411,  6413,  6415,  6417,
    6419,  6421,  6423,  6427,  6431,  6435,  6439,  6443,  6450,  6456,
    6461,  6464,  6467,  6480,  6482,  6486,  6488,  6493,  6499,  6505,
    6511,  6517,  6523,  6529,  6535,  6541,  6550,  6556,  6573,  6575,
    6583,  6591,  6593,  6597,  6601,  6603,  6607,  6609,  6617,  6621,
    6633,  6636,  6654,  6656,  6660,  6662,  6666,  6668,  6672,  6676,
    6680,  6689,  6693,  6697,  6702,  6706,  6718,  6720,  6724,  6729,
    6733,  6735,  6739,  6741,  6745,  6750,  6757,  6780,  6782,  6784,
    6786,  6788,  6792,  6803,  6807,  6822,  6829,  6836,  6837,  6841,
    6845,  6853,  6857,  6861,  6869,  6874,  6888,  6890,  6894,  6896,
    6905,  6907,  6909,  6911,  6947,  6951,  6955,  6959,  6963,  6975,
    6977,  6981,  6984,  6986,  6990,  6995,  7002,  7005,  7013,  7017,
    7022,  7024,  7031,  7036,  7040,  7044,  7048,  7052,  7056,  7059,
    7061,  7065,  7067,  7069,  7073,  7077,  7089,  7091,  7095,  7097,
    7101,  7104,  7107,  7111,  7117,  7129,  7131,  7135,  7137,  7141,
    7149,  7161,  7162,  7164,  7168,  7172,  7174,  7182,  7186,  7189,
    7191,  7195,  7199,  7201,  7202,  7203,  7204,  7205,  7206,  7207,
    7208,  7209,  7210,  7211,  7212,  7213,  7214,  7215,  7216,  7217,
    7218,  7219,  7220,  7221,  7222,  7223,  7224,  7225,  7226,  7229,
    7235,  7241,  7247,  7253,  7257,  7263,  7264,  7265,  7266,  7267,
    7268,  7269,  7270,  7271,  7274,  7279,  7284,  7290,  7296,  7302,
    7307,  7313,  7319,  7325,  7332,  7338,  7344,  7351,  7355,  7357,
    7363,  7370,  7376,  7382,  7388,  7394,  7400,  7406,  7412,  7418,
    7424,  7430,  7436,  7446,  7451,  7457,  7461,  7467,  7468,  7469,
    7470,  7473,  7481,  7487,  7493,  7498,  7504,  7511,  7517,  7521,
    7527,  7528,  7529,  7530,  7531,  7532,  7535,  7544,  7548,  7554,
    7561,  7568,  7575,  7584,  7590,  7596,  7600,  7606,  7607,  7610,
    7616,  7622,  7626,  7633,  7634,  7637,  7643,  7649,  7654,  7662,
    7668,  7673,  7680,  7684,  7690,  7691,  7692,  7693,  7694,  7695,
    7696,  7697,  7698,  7699,  7700,  7704,  7709,  7714,  7721,  7726,
    7732,  7738,  7743,  7748,  7753,  7757,  7762,  7767,  7771,  7776,
    7780,  7786,  7791,  7797,  7802,  7808,  7818,  7822,  7826,  7830,
    7836,  7839,  7843,  7844,  7845,  7846,  7847,  7848,  7849,  7852,
    7856,  7860,  7862,  7864,  7868,  7870,  7872,  7876,  7878,  7882,
    7884,  7888,  7891,  7894,  7899,  7901,  7903,  7905,  7907,  7911,
    7915,  7920,  7924,  7926,  7930,  7932,  7936,  7940,  7944,  7948,
    7950,  7954,  7955,  7956,  7957,  7958,  7959,  7962,  7966,  7970,
    7974,  7976,  7978,  7982,  7984,  7988,  7993,  7994,  7999,  8000,
    8004,  8008,  8010,  8014,  8015,  8016,  8019,  8023,  8027,  8030,
    8032,  8036,  8040,  8042,  8046,  8047,  8048,  8051,  8055,  8059,
    8063,  8065,  8069,  8071,  8073,  8075,  8078,  8080,  8082,  8086,
    8093,  8097,  8099,  8103,  8107,  8109,  8113,  8115,  8117,  8119,
    8121,  8125,  8127,  8131,  8133,  8137,  8139,  8144
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "PERCENT", "AMPERSAND", "ASTER",
  "CLUSTER", "COLON", "COMMA", "DASTER", "DEFINED_OPERATOR", "DOT",
  "DQUOTE", "GLOBAL_A", "LEFTAB", "LEFTPAR", "MINUS", "PLUS", "POINT_TO",
  "QUOTE", "RIGHTAB", "RIGHTPAR", "AND", "DSLASH", "EQV", "EQ", "EQUAL",
  "FFALSE", "GE", "GT", "LE", "LT", "NE", "NEQV", "NOT", "OR", "TTRUE",
  "SLASH", "XOR", "REFERENCE", "AT", "ACROSS", "ALIGN_WITH", "ALIGN",
  "ALLOCATABLE", "ALLOCATE", "ARITHIF", "ASSIGNMENT", "ASSIGN",
  "ASSIGNGOTO", "ASYNCHRONOUS", "ASYNCID", "ASYNCWAIT", "BACKSPACE",
  "BAD_CCONST", "BAD_SYMBOL", "BARRIER", "BLOCKDATA", "BLOCK",
  "BOZ_CONSTANT", "BYTE", "CALL", "CASE", "CHARACTER", "CHAR_CONSTANT",
  "CHECK", "CLOSE", "COMMON", "COMPLEX", "COMPGOTO", "CONSISTENT_GROUP",
  "CONSISTENT_SPEC", "CONSISTENT_START", "CONSISTENT_WAIT", "CONSISTENT",
  "CONSTRUCT_ID", "CONTAINS", "CONTINUE", "CORNER", "CYCLE", "DATA",
  "DEALLOCATE", "HPF_TEMPLATE", "DEBUG", "DEFAULT_CASE", "DEFINE",
  "DERIVED", "DIMENSION", "DISTRIBUTE", "DOWHILE", "DOUBLEPRECISION",
  "DOUBLECOMPLEX", "DP_CONSTANT", "DVM_POINTER", "DYNAMIC", "ELEMENTAL",
  "ELSE", "ELSEIF", "ELSEWHERE", "ENDASYNCHRONOUS", "ENDDEBUG",
  "ENDINTERVAL", "ENDUNIT", "ENDDO", "ENDFILE", "ENDFORALL", "ENDIF",
  "ENDINTERFACE", "ENDMODULE", "ENDON", "ENDSELECT", "ENDTASK_REGION",
  "ENDTYPE", "ENDWHERE", "ENTRY", "EXIT", "EOLN", "EQUIVALENCE", "ERROR",
  "EXTERNAL", "F90", "FIND", "FORALL", "FORMAT", "FUNCTION", "GATE",
  "GEN_BLOCK", "HEAP", "HIGH", "IDENTIFIER", "IMPLICIT", "IMPLICITNONE",
  "INCLUDE_TO", "INCLUDE", "INDEPENDENT", "INDIRECT_ACCESS",
  "INDIRECT_GROUP", "INDIRECT", "INHERIT", "INQUIRE",
  "INTERFACEASSIGNMENT", "INTERFACEOPERATOR", "INTERFACE", "INTRINSIC",
  "INTEGER", "INTENT", "INTERVAL", "INOUT", "IN", "INT_CONSTANT", "LABEL",
  "LABEL_DECLARE", "LET", "LOCALIZE", "LOGICAL", "LOGICALIF", "LOOP",
  "LOW", "MAXLOC", "MAX", "MAP", "MINLOC", "MIN", "MODULE_PROCEDURE",
  "MODULE", "MULT_BLOCK", "NAMEEQ", "NAMELIST", "NEW_VALUE", "NEW",
  "NULLIFY", "OCTAL_CONSTANT", "ONLY", "ON", "ON_DIR", "ONTO", "OPEN",
  "OPERATOR", "OPTIONAL", "OTHERWISE", "OUT", "OWN", "PARALLEL",
  "PARAMETER", "PAUSE", "PLAINDO", "PLAINGOTO", "POINTER", "POINTERLET",
  "PREFETCH", "PRINT", "PRIVATE", "PRODUCT", "PROGRAM", "PUBLIC", "PURE",
  "RANGE", "READ", "REALIGN_WITH", "REALIGN", "REAL", "REAL_CONSTANT",
  "RECURSIVE", "REDISTRIBUTE_NEW", "REDISTRIBUTE", "REDUCTION_GROUP",
  "REDUCTION_START", "REDUCTION_WAIT", "REDUCTION", "REMOTE_ACCESS_SPEC",
  "REMOTE_ACCESS", "REMOTE_GROUP", "RESET", "RESULT", "RETURN", "REWIND",
  "SAVE", "SECTION", "SELECT", "SEQUENCE", "SHADOW_ADD", "SHADOW_COMPUTE",
  "SHADOW_GROUP", "SHADOW_RENEW", "SHADOW_START_SPEC", "SHADOW_START",
  "SHADOW_WAIT_SPEC", "SHADOW_WAIT", "SHADOW", "STAGE", "STATIC", "STAT",
  "STOP", "SUBROUTINE", "SUM", "SYNC", "TARGET", "TASK", "TASK_REGION",
  "THEN", "TO", "TRACEON", "TRACEOFF", "TRUNC", "TYPE", "TYPE_DECL",
  "UNDER", "UNKNOWN", "USE", "VIRTUAL", "VARIABLE", "WAIT", "WHERE",
  "WHERE_ASSIGN", "WHILE", "WITH", "WRITE", "COMMENT", "WGT_BLOCK",
  "HPF_PROCESSORS", "IOSTAT", "ERR", "END", "OMPDVM_ATOMIC",
  "OMPDVM_BARRIER", "OMPDVM_COPYIN", "OMPDVM_COPYPRIVATE",
  "OMPDVM_CRITICAL", "OMPDVM_ONETHREAD", "OMPDVM_DO", "OMPDVM_DYNAMIC",
  "OMPDVM_ENDCRITICAL", "OMPDVM_ENDDO", "OMPDVM_ENDMASTER",
  "OMPDVM_ENDORDERED", "OMPDVM_ENDPARALLEL", "OMPDVM_ENDPARALLELDO",
  "OMPDVM_ENDPARALLELSECTIONS", "OMPDVM_ENDPARALLELWORKSHARE",
  "OMPDVM_ENDSECTIONS", "OMPDVM_ENDSINGLE", "OMPDVM_ENDWORKSHARE",
  "OMPDVM_FIRSTPRIVATE", "OMPDVM_FLUSH", "OMPDVM_GUIDED",
  "OMPDVM_LASTPRIVATE", "OMPDVM_MASTER", "OMPDVM_NOWAIT", "OMPDVM_NONE",
  "OMPDVM_NUM_THREADS", "OMPDVM_ORDERED", "OMPDVM_PARALLEL",
  "OMPDVM_PARALLELDO", "OMPDVM_PARALLELSECTIONS",
  "OMPDVM_PARALLELWORKSHARE", "OMPDVM_RUNTIME", "OMPDVM_SECTION",
  "OMPDVM_SECTIONS", "OMPDVM_SCHEDULE", "OMPDVM_SHARED", "OMPDVM_SINGLE",
  "OMPDVM_THREADPRIVATE", "OMPDVM_WORKSHARE", "OMPDVM_NODES", "OMPDVM_IF",
  "IAND", "IEOR", "IOR", "ACC_REGION", "ACC_END_REGION",
  "ACC_CHECKSECTION", "ACC_END_CHECKSECTION", "ACC_GET_ACTUAL",
  "ACC_ACTUAL", "ACC_TARGETS", "ACC_ASYNC", "ACC_HOST", "ACC_CUDA",
  "ACC_LOCAL", "ACC_INLOCAL", "ACC_CUDA_BLOCK", "ACC_ROUTINE", "ACC_TIE",
  "BY", "IO_MODE", "CP_CREATE", "CP_LOAD", "CP_SAVE", "CP_WAIT", "FILES",
  "VARLIST", "STATUS", "EXITINTERVAL", "TEMPLATE_CREATE",
  "TEMPLATE_DELETE", "SPF_ANALYSIS", "SPF_PARALLEL", "SPF_TRANSFORM",
  "SPF_NOINLINE", "SPF_PARALLEL_REG", "SPF_END_PARALLEL_REG", "SPF_EXPAND",
  "SPF_FISSION", "SPF_SHRINK", "SPF_CHECKPOINT", "SPF_EXCEPT",
  "SPF_FILES_COUNT", "SPF_INTERVAL", "SPF_TIME", "SPF_ITER",
  "SPF_FLEXIBLE", "SPF_APPLY_REGION", "SPF_APPLY_FRAGMENT",
  "SPF_CODE_COVERAGE", "SPF_UNROLL", "BINARY_OP", "UNARY_OP", "$accept",
  "program", "stat", "thislabel", "entry", "new_prog", "proc_attr",
  "procname", "funcname", "typedfunc", "opt_result_clause", "name",
  "progname", "blokname", "arglist", "args", "arg", "filename",
  "needkeyword", "keywordoff", "keyword_if_colon_follow", "spec",
  "interface", "defined_op", "operator", "intrinsic_op", "type_dcl",
  "end_type", "dcl", "options", "attr_spec_list", "attr_spec",
  "intent_spec", "access_spec", "intent", "optional", "static", "private",
  "private_attr", "sequence", "public", "public_attr", "type",
  "opt_key_hedr", "attrib", "att_type", "typespec", "typename", "lengspec",
  "proper_lengspec", "selector", "clause", "end_ioctl", "initial_value",
  "dimension", "allocatable", "pointer", "target", "common", "namelist",
  "namelist_group", "comblock", "var", "external", "intrinsic",
  "equivalence", "equivset", "equivlist", "equi_object", "data", "data1",
  "data_in", "in_data", "datapair", "datalvals", "datarvals", "datalval",
  "data_null", "d_name", "dataname", "datasubs", "datarange",
  "iconexprlist", "opticonexpr", "dataimplieddo", "dlist", "dataelt",
  "datarval", "datavalue", "BOZ_const", "int_const", "unsignedint",
  "real_const", "unsignedreal", "complex_const_data", "complex_part",
  "iconexpr", "iconterm", "iconfactor", "iconprimary", "savelist",
  "saveitem", "use_name_list", "use_key_word", "no_use_key_word",
  "use_name", "paramlist", "paramitem", "module_proc_stmt",
  "proc_name_list", "use_stat", "module_name", "only_list", "only_name",
  "rename_list", "rename_name", "dims", "dimlist", "@1", "dim", "ubound",
  "labellist", "label", "implicit", "implist", "impitem", "imptype", "@2",
  "type_implicit", "letgroups", "letgroup", "letter", "inside", "in_dcl",
  "opt_double_colon", "funarglist", "funarg", "funargs", "subscript_list",
  "expr", "uexpr", "addop", "ident", "lhs", "array_ele_substring_func_ref",
  "structure_component", "array_element", "asubstring", "opt_substring",
  "substring", "opt_expr", "simple_const", "numeric_bool_const",
  "integer_constant", "string_constant", "complex_const", "kind",
  "triplet", "vec", "@3", "@4", "allocate_object", "allocation_list",
  "allocate_object_list", "stat_spec", "pointer_name_list", "exec",
  "do_while", "opt_while", "plain_do", "case", "case_selector",
  "case_value_range", "case_value_range_list", "opt_construct_name",
  "opt_unit_name", "construct_name", "construct_name_colon", "logif",
  "forall", "forall_list", "forall_expr", "opt_forall_cond", "do_var",
  "dospec", "dotarget", "whereable", "iffable", "let", "goto", "opt_comma",
  "call", "callarglist", "callarg", "stop", "end_spec", "intonlyon",
  "intonlyoff", "io", "iofmove", "fmkwd", "iofctl", "ctlkwd", "inquire",
  "infmt", "ioctl", "ctllist", "ioclause", "nameeq", "read", "write",
  "print", "inlist", "inelt", "outlist", "out2", "other", "in_ioctl",
  "start_ioctl", "fexpr", "unpar_fexpr", "cmnt", "dvm_specification",
  "dvm_exec", "dvm_template", "template_obj", "dvm_dynamic",
  "dyn_array_name_list", "dyn_array_name", "dvm_inherit",
  "dummy_array_name_list", "dummy_array_name", "dvm_shadow",
  "shadow_attr_stuff", "sh_width_list", "sh_width", "sh_array_name",
  "dvm_processors", "dvm_indirect_group", "indirect_group_name",
  "dvm_remote_group", "remote_group_name", "dvm_reduction_group",
  "reduction_group_name", "dvm_consistent_group", "consistent_group_name",
  "opt_onto", "dvm_distribute", "dvm_redistribute", "dist_name_list",
  "distributee", "dist_name", "pointer_ar_elem", "processors_name",
  "opt_dist_format_clause", "dist_format_clause", "dist_format_list",
  "opt_key_word", "dist_format", "array_name", "derived_spec",
  "derived_elem_list", "derived_elem", "target_spec", "derived_target",
  "derived_subscript_list", "derived_subscript", "dummy_ident",
  "opt_plus_shadow", "plus_shadow", "shadow_id", "shadow_width",
  "dvm_align", "dvm_realign", "realignee_list", "alignee", "realignee",
  "align_directive_stuff", "align_base", "align_subscript_list",
  "align_subscript", "align_base_name", "dim_ident_list", "dim_ident",
  "dvm_combined_dir", "dvm_attribute_list", "dvm_attribute", "dvm_pointer",
  "dimension_list", "@5", "pointer_var_list", "pointer_var", "dvm_heap",
  "heap_array_name_list", "heap_array_name", "dvm_consistent",
  "consistent_array_name_list", "consistent_array_name", "dvm_asyncid",
  "async_id_list", "async_id", "dvm_new_value", "dvm_parallel_on",
  "ident_list", "opt_on", "distribute_cycles", "par_subscript_list",
  "par_subscript", "opt_spec", "spec_list", "par_spec",
  "remote_access_spec", "consistent_spec", "consistent_group", "new_spec",
  "private_spec", "cuda_block_spec", "sizelist", "variable_list",
  "tie_spec", "tied_array_list", "indirect_access_spec", "stage_spec",
  "across_spec", "in_out_across", "opt_keyword_in_out", "opt_in_out",
  "dependent_array_list", "dependent_array", "dependence_list",
  "dependence", "section_spec_list", "section_spec", "ar_section",
  "low_section", "high_section", "section", "reduction_spec",
  "opt_key_word_r", "no_opt_key_word_r", "reduction_group",
  "reduction_list", "reduction", "reduction_op", "loc_op", "shadow_spec",
  "shadow_group_name", "shadow_list", "shadow", "opt_corner",
  "array_ident", "array_ident_list", "dvm_shadow_start", "dvm_shadow_wait",
  "dvm_shadow_group", "dvm_reduction_start", "dvm_reduction_wait",
  "dvm_consistent_start", "dvm_consistent_wait", "dvm_remote_access",
  "group_name", "remote_data_list", "remote_data", "remote_index_list",
  "remote_index", "dvm_task", "task_array", "dvm_task_region", "task_name",
  "dvm_end_task_region", "task", "dvm_on", "opt_private_spec",
  "dvm_end_on", "dvm_map", "dvm_prefetch", "dvm_reset",
  "dvm_indirect_access", "indirect_list", "indirect_reference",
  "hpf_independent", "hpf_reduction_spec", "dvm_asynchronous",
  "dvm_endasynchronous", "dvm_asyncwait", "async_ident", "async",
  "dvm_f90", "dvm_debug_dir", "debparamlist", "debparam",
  "fragment_number", "dvm_enddebug_dir", "dvm_interval_dir",
  "interval_number", "dvm_exit_interval_dir", "dvm_endinterval_dir",
  "dvm_traceon_dir", "dvm_traceoff_dir", "dvm_barrier_dir", "dvm_check",
  "dvm_io_mode_dir", "mode_list", "mode_spec", "dvm_shadow_add",
  "template_ref", "shadow_axis_list", "shadow_axis", "opt_include_to",
  "dvm_localize", "localize_target", "target_subscript_list",
  "target_subscript", "aster_expr", "dvm_cp_create", "opt_mode",
  "dvm_cp_load", "dvm_cp_save", "dvm_cp_wait", "dvm_template_create",
  "template_list", "dvm_template_delete", "omp_specification_directive",
  "omp_execution_directive", "ompdvm_onethread",
  "omp_parallel_end_directive", "omp_parallel_begin_directive",
  "parallel_clause_list", "parallel_clause", "omp_variable_list_in_par",
  "ompprivate_clause", "ompfirstprivate_clause", "omplastprivate_clause",
  "ompcopyin_clause", "ompshared_clause", "ompdefault_clause", "def_expr",
  "ompif_clause", "ompnumthreads_clause", "ompreduction_clause",
  "ompreduction", "ompreduction_vars", "ompreduction_op",
  "omp_sections_begin_directive", "sections_clause_list",
  "sections_clause", "omp_sections_end_directive", "omp_section_directive",
  "omp_do_begin_directive", "omp_do_end_directive", "do_clause_list",
  "do_clause", "ompordered_clause", "ompschedule_clause", "ompschedule_op",
  "omp_single_begin_directive", "single_clause_list", "single_clause",
  "omp_single_end_directive", "end_single_clause_list",
  "end_single_clause", "ompcopyprivate_clause", "ompnowait_clause",
  "omp_workshare_begin_directive", "omp_workshare_end_directive",
  "omp_parallel_do_begin_directive", "paralleldo_clause_list",
  "paralleldo_clause", "omp_parallel_do_end_directive",
  "omp_parallel_sections_begin_directive",
  "omp_parallel_sections_end_directive",
  "omp_parallel_workshare_begin_directive",
  "omp_parallel_workshare_end_directive", "omp_threadprivate_directive",
  "omp_master_begin_directive", "omp_master_end_directive",
  "omp_ordered_begin_directive", "omp_ordered_end_directive",
  "omp_barrier_directive", "omp_atomic_directive", "omp_flush_directive",
  "omp_critical_begin_directive", "omp_critical_end_directive",
  "omp_common_var", "omp_variable_list", "op_slash_1", "op_slash_0",
  "acc_directive", "acc_region", "acc_checksection", "acc_get_actual",
  "acc_actual", "opt_clause", "acc_clause_list", "acc_clause",
  "data_clause", "targets_clause", "async_clause", "acc_var_list",
  "computer_list", "computer", "acc_end_region", "acc_end_checksection",
  "acc_routine", "opt_targets_clause", "spf_directive", "spf_analysis",
  "spf_parallel", "spf_transform", "spf_parallel_reg",
  "characteristic_list", "characteristic", "opt_clause_apply_fragment",
  "opt_clause_apply_region", "spf_end_parallel_reg", "analysis_spec_list",
  "analysis_spec", "analysis_reduction_spec", "analysis_private_spec",
  "analysis_parameter_spec", "spf_parameter_list", "spf_parameter",
  "parallel_spec_list", "parallel_spec", "parallel_shadow_spec",
  "parallel_across_spec", "parallel_remote_access_spec",
  "transform_spec_list", "transform_spec", "unroll_list", "region_name",
  "array_element_list", "spf_checkpoint", "checkpoint_spec_list",
  "checkpoint_spec", "spf_type_list", "spf_type", "interval_spec",
  "in_unit", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   353,   354,     1,     2,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   281,   282,   283,   284,   285,   286,   287,
     288,   289,   290,   291,   292,   293,   294,   295,   296,   297,
     298,   299,   300,   301,   302,   303,   304,   305,   306,   307,
     308,   309,   310,   311,   312,   313,   314,   315,   316,   317,
     318,   319,   320,   321,   322,   323,   324,   325,   326,   327,
     328,   329,   330,   331,   332,   333,   334,   335,   336,   337,
     338,   339,   340,   341,   342,   343,   344,   345,   346,   347,
     348,   349,   350,   351,   352,   355,   356
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   357,   358,   358,   359,   359,   359,   359,   359,   359,
     359,   360,   361,   361,   361,   361,   361,   361,   361,   361,
     362,   363,   363,   363,   364,   365,   366,   366,   366,   366,
     367,   367,   368,   369,   369,   370,   370,   371,   371,   371,
     372,   372,   373,   373,   374,   375,   376,   377,   378,   378,
     378,   378,   378,   378,   378,   378,   378,   378,   378,   378,
     378,   378,   378,   378,   378,   378,   378,   378,   378,   378,
     378,   378,   378,   378,   378,   378,   378,   379,   379,   379,
     379,   379,   380,   381,   381,   382,   382,   382,   382,   382,
     382,   382,   382,   382,   382,   382,   382,   382,   382,   382,
     382,   382,   382,   383,   383,   384,   384,   385,   385,   386,
     386,   386,   387,   387,   388,   388,   388,   388,   388,   388,
     388,   388,   388,   388,   388,   388,   389,   389,   389,   390,
     390,   391,   391,   392,   392,   393,   393,   394,   394,   395,
     396,   397,   397,   398,   399,   399,   400,   401,   401,   402,
     402,   403,   404,   404,   404,   404,   404,   404,   404,   405,
     405,   406,   406,   406,   407,   407,   407,   407,   408,   408,
     408,   408,   409,   410,   410,   410,   411,   411,   412,   412,
     413,   413,   414,   414,   415,   415,   415,   415,   416,   416,
     416,   417,   418,   418,   419,   420,   420,   421,   421,   422,
     422,   423,   424,   424,   425,   425,   425,   426,   427,   427,
     428,   429,   430,   431,   431,   432,   432,   433,   433,   433,
     433,   433,   434,   435,   436,   437,   438,   439,   439,   440,
     440,   441,   442,   442,   443,   443,   443,   443,   444,   444,
     444,   445,   445,   445,   445,   445,   445,   445,   445,   445,
     445,   445,   446,   447,   447,   447,   448,   448,   449,   449,
     449,   450,   450,   450,   450,   451,   452,   452,   453,   453,
     453,   453,   453,   454,   454,   454,   455,   455,   456,   456,
     456,   457,   457,   458,   458,   459,   459,   460,   461,   462,
     462,   462,   463,   463,   464,   465,   466,   466,   467,   467,
     467,   467,   468,   469,   469,   470,   470,   471,   471,   472,
     473,   473,   475,   474,   474,   476,   476,   476,   476,   477,
     477,   478,   478,   479,   480,   480,   481,   481,   482,   484,
     483,   485,   485,   486,   486,   487,   487,   488,   489,   490,
     491,   491,   492,   492,   493,   493,   493,   494,   494,   495,
     495,   496,   496,   496,   497,   497,   497,   497,   497,   497,
     497,   497,   497,   497,   497,   497,   497,   497,   497,   497,
     497,   497,   497,   497,   497,   497,   497,   497,   497,   498,
     498,   499,   500,   500,   500,   501,   501,   501,   501,   502,
     503,   503,   503,   503,   504,   505,   505,   506,   507,   507,
     508,   508,   508,   508,   508,   509,   509,   509,   509,   510,
     511,   511,   511,   512,   513,   513,   514,   514,   514,   514,
     514,   514,   514,   514,   516,   517,   515,   518,   518,   519,
     519,   519,   520,   520,   520,   521,   522,   522,   523,   523,
     523,   523,   523,   523,   523,   523,   523,   523,   523,   523,
     523,   523,   523,   523,   523,   523,   523,   523,   523,   523,
     523,   523,   524,   524,   525,   525,   525,   526,   526,   527,
     527,   527,   527,   527,   528,   529,   529,   529,   529,   530,
     530,   531,   531,   532,   532,   533,   534,   535,   536,   537,
     537,   538,   539,   539,   540,   541,   541,   542,   542,   543,
     543,   543,   543,   543,   544,   544,   544,   544,   544,   544,
     544,   544,   544,   544,   544,   544,   544,   544,   544,   544,
     544,   544,   545,   546,   546,   546,   546,   547,   547,   548,
     549,   549,   550,   550,   550,   551,   551,   552,   553,   554,
     555,   555,   555,   555,   555,   555,   555,   555,   555,   555,
     555,   555,   555,   555,   556,   557,   557,   557,   558,   559,
     559,   560,   560,   561,   561,   562,   562,   563,   563,   564,
     564,   564,   564,   564,   564,   565,   566,   567,   568,   568,
     569,   569,   570,   570,   571,   571,   571,   572,   572,   572,
     572,   572,   572,   573,   573,   573,   573,   573,   574,   575,
     576,   576,   577,   577,   577,   577,   577,   577,   577,   577,
     577,   578,   578,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   579,   579,   579,   579,   579,   579,   579,   579,
     579,   579,   580,   580,   580,   580,   580,   580,   580,   580,
     580,   580,   580,   580,   580,   580,   580,   580,   580,   580,
     580,   580,   580,   580,   580,   580,   580,   580,   580,   580,
     580,   580,   580,   580,   580,   580,   580,   580,   580,   580,
     580,   580,   580,   580,   580,   580,   581,   581,   582,   583,
     584,   584,   585,   586,   587,   587,   588,   589,   590,   591,
     591,   592,   592,   592,   593,   594,   594,   594,   595,   595,
     596,   597,   597,   598,   599,   599,   600,   601,   601,   602,
     603,   603,   604,   605,   605,   606,   606,   607,   607,   608,
     609,   610,   611,   611,   612,   613,   613,   614,   615,   615,
     615,   615,   615,   615,   615,   615,   616,   617,   618,   618,
     619,   619,   620,   620,   621,   622,   622,   623,   623,   623,
     624,   625,   625,   626,   626,   627,   628,   628,   629,   630,
     630,   631,   631,   632,   633,   634,   635,   636,   636,   637,
     637,   637,   638,   639,   639,   640,   640,   640,   641,   641,
     642,   642,   643,   643,   643,   643,   643,   643,   643,   643,
     643,   643,   643,   644,   646,   645,   645,   647,   647,   648,
     649,   650,   650,   651,   652,   653,   653,   654,   655,   655,
     656,   656,   657,   658,   659,   660,   660,   661,   661,   662,
     663,   663,   664,   664,   665,   665,   666,   666,   667,   667,
     667,   667,   667,   667,   667,   667,   667,   667,   667,   668,
     668,   669,   669,   670,   671,   671,   672,   673,   674,   674,
     674,   675,   675,   676,   677,   677,   678,   678,   679,   680,
     680,   681,   682,   683,   683,   683,   684,   684,   685,   685,
     685,   686,   686,   687,   688,   688,   689,   689,   689,   689,
     689,   689,   689,   690,   691,   692,   693,   694,   694,   694,
     695,   696,   697,   698,   698,   699,   699,   700,   700,   700,
     700,   700,   700,   700,   700,   700,   701,   701,   702,   702,
     702,   702,   702,   703,   704,   704,   705,   705,   705,   705,
     706,   707,   708,   708,   709,   709,   710,   710,   711,   712,
     713,   714,   715,   716,   716,   717,   718,   718,   719,   719,
     720,   720,   721,   721,   722,   722,   723,   724,   724,   724,
     724,   724,   725,   726,   727,   727,   728,   729,   729,   730,
     731,   731,   732,   733,   734,   734,   735,   735,   736,   736,
     737,   737,   737,   737,   738,   739,   740,   741,   742,   743,
     743,   744,   745,   745,   746,   746,   747,   748,   749,   750,
     751,   751,   752,   753,   754,   755,   756,   757,   758,   759,
     759,   760,   760,   760,   761,   762,   763,   763,   764,   764,
     765,   765,   766,   767,   767,   768,   768,   769,   769,   770,
     771,   772,   772,   772,   773,   774,   774,   775,   776,   777,
     777,   778,   779,   780,   780,   780,   780,   780,   780,   780,
     780,   780,   780,   780,   780,   780,   780,   780,   780,   780,
     780,   780,   780,   780,   780,   780,   780,   780,   780,   781,
     782,   783,   783,   784,   784,   785,   785,   785,   785,   785,
     785,   785,   785,   785,   786,   787,   788,   789,   790,   791,
     792,   793,   793,   793,   794,   795,   796,   797,   798,   799,
     799,   799,   799,   799,   799,   799,   799,   799,   799,   799,
     799,   799,   799,   800,   800,   801,   801,   802,   802,   802,
     802,   803,   803,   804,   805,   805,   806,   806,   807,   807,
     808,   808,   808,   808,   808,   808,   809,   810,   810,   811,
     811,   811,   811,   812,   812,   813,   813,   814,   814,   815,
     815,   816,   816,   817,   817,   818,   819,   820,   821,   821,
     822,   822,   823,   823,   824,   824,   824,   824,   824,   824,
     824,   824,   824,   824,   824,   825,   826,   826,   827,   828,
     828,   829,   830,   831,   832,   833,   834,   835,   836,   837,
     837,   838,   838,   839,   839,   840,   841,   841,   841,   841,
     842,   843,   844,   844,   844,   844,   844,   844,   844,   845,
     846,   847,   847,   847,   848,   848,   848,   849,   849,   850,
     850,   851,   851,   851,   852,   852,   852,   852,   852,   853,
     854,   855,   856,   856,   857,   857,   858,   859,   860,   861,
     861,   862,   862,   862,   862,   862,   862,   863,   864,   865,
     866,   866,   866,   867,   867,   868,   869,   869,   870,   870,
     871,   872,   872,   873,   873,   873,   874,   875,   876,   877,
     877,   878,   879,   879,   880,   880,   880,   881,   882,   883,
     884,   884,   885,   885,   885,   885,   885,   885,   885,   886,
     887,   888,   888,   889,   890,   890,   891,   891,   891,   891,
     891,   892,   892,   893,   893,   894,   894,   895
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     3,     3,     3,     3,     3,     2,     1,
       1,     1,     3,     3,     4,     5,     5,     3,     4,     3,
       0,     2,     2,     2,     1,     1,     4,     5,     4,     5,
       2,     5,     1,     0,     1,     0,     1,     0,     2,     3,
       1,     3,     1,     1,     1,     0,     0,     0,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     4,     2,     5,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     2,     3,     5,
       5,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     4,     7,     2,     3,     7,     6,     0,
       2,     5,     1,     4,     1,     1,     1,     2,     1,     4,
       1,     1,     1,     1,     1,     1,     2,     2,     2,     1,
       1,     7,     3,     4,     3,     4,     3,     2,     5,     0,
       2,     2,     5,     0,     4,     5,     0,     2,     3,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       1,     5,     6,     6,     0,     1,     5,     9,     1,     1,
       2,     2,     0,     0,     2,     2,     5,     4,     4,     3,
       4,     3,     4,     3,     3,     4,     5,     3,     4,     5,
       3,     3,     1,     3,     2,     4,     3,     4,     3,     3,
       3,     3,     3,     3,     1,     4,     1,     1,     4,     3,
       0,     0,     4,     1,     3,     1,     3,     2,     3,     3,
       4,     2,     0,     1,     1,     3,     5,     1,     3,     0,
       1,     7,     1,     3,     2,     2,     3,     1,     1,     4,
       3,     2,     1,     1,     1,     1,     3,     1,     3,     1,
       1,     6,     1,     1,     2,     2,     1,     3,     1,     2,
       2,     1,     3,     1,     3,     5,     1,     1,     1,     2,
       2,     3,     3,     1,     3,     3,     1,     3,     1,     1,
       3,     1,     3,     1,     1,     3,     5,     0,     0,     1,
       4,     4,     1,     3,     3,     2,     1,     3,     3,     6,
       6,     7,     1,     1,     3,     1,     1,     1,     3,     3,
       0,     3,     0,     2,     3,     1,     1,     2,     3,     1,
       1,     1,     3,     1,     3,     1,     1,     3,     4,     0,
       2,     2,     1,     1,     3,     1,     3,     1,     0,     0,
       0,     2,     0,     1,     1,     1,     2,     2,     4,     1,
       3,     1,     3,     1,     1,     1,     1,     3,     3,     3,
       3,     3,     2,     2,     2,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     2,     3,     3,     1,
       1,     1,     1,     1,     1,     5,     6,     4,     5,     3,
       1,     1,     5,     4,     2,     0,     1,     5,     0,     1,
       1,     3,     1,     3,     2,     1,     1,     1,     1,     1,
       1,     3,     3,     5,     1,     1,     3,     2,     5,     4,
       4,     3,     2,     1,     0,     0,     6,     1,     1,     1,
       4,     5,     1,     4,     5,     0,     1,     3,     1,     1,
       1,     2,     3,     3,     2,     1,     2,     2,     2,     3,
       7,     3,     3,     1,     2,     2,     1,     2,     3,     1,
       1,     1,     5,     7,     0,     6,     4,    11,    13,     4,
       3,     3,     7,     8,     3,     1,     2,     2,     3,     1,
       3,     0,     1,     0,     1,     1,     2,     5,     6,     1,
       3,     3,     0,     2,     1,     5,     7,     0,     1,     3,
       3,     6,     5,     6,     4,     5,     5,     2,     1,     1,
      10,     1,     3,     4,     3,     3,     3,     3,     6,     6,
       5,     8,     2,     3,     3,     7,     7,     0,     1,     4,
       2,     4,     1,     2,     2,     1,     1,     0,     0,     0,
       2,     2,     2,     2,     2,     1,     2,     2,     3,     4,
       2,     3,     1,     3,     3,     1,     1,     1,     3,     1,
       1,     4,     5,     1,     1,     3,     3,     1,     4,     1,
       1,     1,     2,     2,     2,     1,     3,     3,     4,     4,
       1,     3,     1,     5,     1,     1,     1,     3,     3,     3,
       3,     3,     3,     1,     3,     5,     5,     5,     0,     0,
       1,     3,     1,     1,     3,     3,     3,     3,     2,     3,
       3,     0,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     3,     3,     2,     3,
       1,     3,     1,     3,     1,     3,     1,     4,     3,     1,
       3,     1,     3,     4,     1,     4,     4,     4,     3,     3,
       1,     3,     3,     1,     3,     3,     1,     3,     3,     1,
       3,     0,     5,     6,     8,     1,     3,     1,     1,     1,
       4,     1,     0,     2,     3,     2,     4,     0,     1,     5,
       4,     6,     4,     1,     4,     4,     1,     6,     1,     3,
       1,     3,     1,     4,     1,     1,     3,     1,     1,     3,
       1,     0,     1,     2,     3,     1,     2,     5,     4,     4,
       6,     1,     3,     1,     1,     6,     4,     1,     3,     1,
       1,     1,     1,     1,     3,     1,     1,     1,     6,     4,
       1,     4,     1,     1,     1,     1,     4,     2,     7,     1,
       4,     1,     1,    11,     0,     2,     3,     1,     3,     1,
       3,     1,     3,     1,     3,     1,     3,     1,     3,     8,
       1,     3,     2,     2,     7,     1,     3,     3,     1,     4,
       1,     3,     1,     1,     0,     1,     1,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     8,
       6,     8,     6,     1,     6,     6,     6,     6,     1,     3,
       5,     1,     3,     6,     1,     3,     8,     6,     6,     4,
       5,     5,     0,     2,     2,     0,     1,     3,     1,     4,
       7,     1,     3,     3,     1,     3,     5,     3,     3,     1,
       3,     1,     1,     3,     3,     3,     3,    10,     8,    10,
       0,     0,     1,     2,     4,     4,     6,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     6,     4,
       4,     3,     9,     1,     1,     3,     1,     5,     5,     9,
       0,     1,     1,     3,     3,     3,     3,     3,     6,     3,
       3,     3,     3,     7,     5,     1,     1,     3,     4,     1,
       1,     3,     1,     1,     3,     3,     2,     3,     4,     4,
       5,     5,     1,     2,     4,     4,     4,     0,     1,     2,
       7,     6,     3,     3,     7,     5,     1,     3,     1,     4,
       2,     3,     3,     4,     6,     3,     2,     3,     1,     1,
       4,     5,     3,     6,     2,     4,     2,     1,     3,     3,
       0,     1,     3,     2,     2,     2,     2,     9,     5,     1,
       3,     2,     2,     2,     9,     4,     1,     3,     1,     1,
       2,     0,     7,     1,     4,     1,     3,     1,     1,     1,
      16,     0,     3,     3,     3,     3,     6,     9,     5,     1,
       3,     5,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     2,
       2,     4,     3,     4,     5,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     5,     2,     2,     2,     2,     2,
       5,     1,     1,     1,     4,     4,     4,     4,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     4,     3,     4,     5,     1,     1,     1,
       1,     4,     3,     2,     4,     3,     4,     3,     4,     5,
       1,     1,     1,     1,     1,     1,     1,     7,     5,     1,
       1,     1,     1,     4,     3,     4,     5,     1,     1,     4,
       3,     4,     5,     1,     1,     2,     1,     2,     4,     3,
       4,     3,     4,     5,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     2,     4,     3,     2,     4,
       3,     2,     3,     2,     2,     2,     2,     2,     2,     3,
       2,     5,     2,     5,     2,     5,     1,     1,     3,     3,
       0,     0,     1,     1,     1,     1,     1,     1,     1,     3,
       2,     5,     4,     2,     5,     4,     2,     2,     1,     1,
       3,     2,     2,     2,     4,     4,     4,     4,     4,     4,
       1,     1,     1,     3,     2,     2,     1,     1,     3,     2,
       2,     1,     1,     1,     1,     1,     1,     5,     5,     5,
       3,    10,    10,     1,     3,     2,     0,     6,     0,     6,
       2,     1,     3,     1,     1,     1,     5,     5,     5,     1,
       3,     3,     1,     3,     1,     1,     1,     5,     5,     5,
       1,     3,     2,     5,     2,     5,     5,     2,     5,     5,
       1,     1,     3,     5,     1,     3,     5,     5,     5,     5,
       7,     1,     3,     2,     2,     2,     2,     0
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       2,     0,     1,    10,    11,     9,     0,     0,     3,   150,
     149,   789,   339,   537,   537,   537,   537,   537,   339,   537,
     555,   537,    20,   537,   537,   158,   537,   560,   339,   154,
     537,   339,   537,   537,   339,   485,   537,   537,   537,   338,
     537,   782,   537,   537,   340,   791,   537,   155,   156,   785,
      45,   537,   537,   537,   537,   537,   537,   537,   537,   557,
     537,   537,   483,   483,   537,   537,   537,   339,   537,     0,
     537,   339,   339,   537,   537,   338,    20,   339,   339,   325,
       0,   537,   537,   339,   339,   537,   339,   339,   339,   339,
     152,   339,   537,   211,   537,   157,   537,   537,     0,    20,
     339,   537,   537,   537,   559,   339,   537,   339,   535,   537,
     537,   339,   537,   537,   537,   339,    20,   339,    45,   537,
     537,   153,    45,   537,   339,   537,   537,   537,   339,   537,
     537,   556,   339,   537,   339,   537,   537,   537,   537,   537,
     537,   339,   339,   536,    20,   339,   339,   537,   537,   537,
       0,   339,     8,   339,   537,   537,   537,   783,   537,   537,
     537,   537,   537,   537,   537,   537,   537,   537,   537,   537,
     537,   537,   537,   537,   537,   537,   537,   537,   537,   537,
     537,   537,   537,   537,   339,   537,   784,   537,  1226,   537,
    1227,   537,   537,   339,   537,   537,   537,   537,   537,   537,
     537,   537,  1297,  1297,  1297,  1297,  1297,  1297,   611,     0,
      37,   611,    73,    48,    49,    50,    65,    66,    76,    68,
      69,    67,   109,    58,     0,   146,   151,    52,    70,    71,
      72,    51,    59,    54,    55,    56,    60,   207,    75,    74,
      57,   611,   445,   440,   453,     0,     0,     0,   456,   439,
     438,     0,   508,   511,   537,   509,     0,   537,     0,   537,
     545,     0,     0,   552,    53,   459,   613,   616,   622,   618,
     617,   623,   624,   625,   626,   615,   632,   614,   633,   619,
       0,   780,   620,   627,   629,   628,   660,   634,   636,   637,
     635,   638,   639,   640,   641,   642,   621,   643,   644,   646,
     647,   645,   649,   650,   648,   674,   661,   662,   663,   664,
     651,   652,   653,   655,   654,   656,   657,   658,   659,   665,
     666,   667,   668,   669,   670,   671,   672,   673,   631,   675,
     630,  1034,  1033,  1035,  1036,  1037,  1038,  1039,  1040,  1041,
    1042,  1043,  1044,  1045,  1046,  1047,  1048,  1049,  1032,  1050,
    1051,  1052,  1053,  1054,  1055,  1056,  1057,  1058,   460,  1192,
    1194,  1196,  1197,  1193,  1195,  1198,   461,  1231,  1232,  1233,
    1234,  1235,  1236,     0,     0,   340,     0,     0,     0,     0,
       0,     0,     0,   996,    35,     0,     0,   598,     0,     0,
       0,     0,     0,     0,   454,   507,   481,   210,     0,     0,
       0,   481,     0,   312,   339,   727,     0,   727,   538,     0,
      23,   481,     0,   481,   976,     0,   993,   483,   481,   481,
     481,    32,   484,    81,   444,   959,   481,   953,   105,   481,
      37,   481,     0,   340,     0,     0,    63,     0,     0,   329,
      44,     7,   970,     0,     0,     0,   599,     0,     0,    77,
     340,     0,   990,   522,     0,     0,     0,   296,   295,     0,
       0,   813,     0,     0,   340,     0,     0,   538,     0,   340,
       0,     0,     0,   340,    33,   340,    22,   599,     0,    21,
       0,     0,     0,     0,     0,     0,     0,   398,   340,    45,
     140,     0,     0,     0,     0,     0,     0,     0,     0,   787,
     340,     0,   340,     0,     0,   994,   995,     0,   339,   340,
       0,     0,     0,   599,     0,  1178,  1177,  1182,  1059,   727,
    1184,   727,  1174,  1176,  1060,  1165,  1168,  1171,   727,   727,
     727,  1180,  1173,  1175,   727,   727,   727,   727,  1113,   727,
     727,  1190,  1147,     0,    45,  1200,  1203,  1206,    45,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1250,     0,   612,     4,    20,    20,     0,     0,    45,
       5,     0,     0,     0,     0,     0,    45,    20,     0,     0,
       0,   147,   164,     0,     0,     0,     0,   528,     0,   528,
       0,     0,     0,     0,   528,   222,     6,   486,   537,   537,
     446,   441,     0,   457,   448,   447,   455,    82,   172,     0,
       0,     0,   406,     0,   405,   410,   408,   409,   407,   381,
       0,     0,   351,   382,   354,   384,   383,   355,   400,   402,
     395,   353,   356,   598,   398,   542,   543,     0,   380,   379,
      32,     0,   602,   603,   540,     0,   600,   599,     0,   544,
     599,   564,   547,   546,   600,   550,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    45,     0,   776,   777,   775,
       0,   773,   763,     0,     0,   435,     0,   323,     0,   524,
     978,   979,   975,    45,   310,   808,   810,   977,    36,    13,
     598,     0,   481,     0,   192,     0,   310,     0,   184,     0,
     709,   707,   843,   931,   932,   807,   804,   805,   482,   516,
     222,   435,   310,   676,   987,   982,   470,   341,     0,     0,
       0,     0,     0,   719,   722,   711,     0,   497,   682,   679,
     680,   451,     0,     0,   500,   988,   442,   443,   458,   452,
     471,   106,   499,    45,   517,     0,   199,     0,   382,     0,
       0,    37,    25,   803,   800,   801,   324,   326,     0,     0,
      45,   971,   972,     0,   700,   698,   686,   683,   684,     0,
       0,     0,    78,     0,    45,   991,   989,     0,     0,   952,
       0,    45,     0,    19,     0,     0,     0,     0,   957,     0,
       0,     0,   497,   523,     0,     0,   935,   962,   599,     0,
     599,   600,   139,    34,    12,   143,   576,     0,   764,     0,
       0,     0,   727,   706,   704,   892,   929,   930,     0,   703,
     701,   963,   399,   514,     0,     0,     0,   913,     0,   925,
     924,   927,   926,     0,   691,     0,   689,   694,     0,     0,
      37,    24,     0,   310,   944,   947,     0,    45,     0,   302,
     298,     0,     0,   577,   310,     0,   527,     0,  1117,  1112,
     527,  1149,  1179,     0,   527,   527,   527,   527,   527,   527,
    1172,   310,    46,  1199,  1208,  1209,     0,     0,    46,  1228,
      45,     0,  1024,  1025,     0,   992,   349,     0,     0,    45,
      45,    45,  1280,  1240,    45,     0,     0,    20,    43,    38,
      42,     0,    40,    17,    46,   310,   132,   134,   136,   110,
       0,     0,    20,   339,   148,   538,   598,   165,   146,   310,
     179,   181,   183,   187,   527,   190,   527,   196,   198,   200,
     209,     0,   213,     0,    45,     0,   449,   424,     0,   351,
     364,   363,   376,   362,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   598,     0,     0,   598,     0,     0,   398,
     404,   396,   512,     0,     0,   515,   570,   571,   575,     0,
     567,     0,   569,     0,   608,     0,     0,     0,     0,     0,
     554,   569,   558,     0,     0,   582,   548,   580,     0,     0,
     351,   353,   551,   586,   585,   553,   677,   310,   699,   702,
     705,   708,   310,   339,     0,   945,     0,    45,   758,   178,
       0,     0,     0,     0,     0,     0,   312,   812,     0,   529,
       0,   475,   479,     0,   469,   598,     0,   194,   185,     0,
     321,     0,   208,     0,   678,   598,     0,   786,   319,   316,
     313,   315,   320,   310,   727,   724,   733,   728,     0,     0,
       0,     0,     0,   725,   711,   727,     0,   790,     0,   498,
     539,     0,     0,     0,    18,   204,     0,     0,     0,   206,
     195,     0,   494,   492,   489,     0,    45,     0,   329,     0,
       0,   332,   330,     0,    45,   973,   381,   921,   968,     0,
       0,   966,     0,   561,     0,    87,    88,    86,    85,    91,
      90,   102,    95,    98,    97,   100,    99,    96,   101,    94,
      92,    89,    93,    83,     0,    84,   197,     0,     0,     0,
       0,     0,     0,   297,     0,   188,   436,     0,    45,   958,
     956,   133,   815,     0,     0,     0,   292,   539,   180,     0,
     579,     0,   578,   287,   287,     0,   759,     0,   727,   711,
     939,     0,     0,   936,   284,   283,    62,   281,     0,     0,
       0,     0,     0,     0,     0,   688,   687,   135,    14,   182,
     946,    45,   949,   948,   146,     0,   103,    47,     0,     0,
     695,     0,   727,   527,     0,  1146,  1116,  1111,   727,   527,
    1148,  1191,   727,   527,   727,   527,   527,   527,   727,   527,
     727,   527,   696,     0,     0,     0,     0,  1220,     0,     0,
    1207,  1211,  1213,  1212,    45,  1202,   851,  1221,     0,  1205,
       0,  1229,  1230,     0,     0,   999,    45,    45,    45,     0,
     390,   391,  1029,     0,     0,     0,     0,  1251,  1253,  1254,
    1255,     0,     0,  1262,  1264,  1265,  1266,     0,     0,  1270,
      45,     0,     0,  1284,    28,    37,     0,     0,    39,     0,
      30,   159,   116,   310,   339,   118,   120,     0,   121,   114,
     122,   130,   129,   123,   124,   125,     0,   112,   115,    26,
       0,   310,     0,     0,   144,   177,     0,     0,   222,   222,
       0,   224,   217,   221,     0,     0,     0,   352,     0,   359,
     361,   358,   357,   375,   377,   371,   365,   504,   368,   366,
     369,   367,   370,   372,   374,   360,   373,   378,   598,   411,
     389,     0,   343,     0,   414,   415,   401,   412,   403,     0,
     598,   513,     0,   532,   530,     0,   598,   566,   573,   574,
     572,   601,   610,   605,   607,   609,   606,   604,   565,   549,
       0,     0,     0,   351,     0,     0,     0,     0,     0,   697,
     779,     0,   789,   792,   782,     0,   791,   785,     0,   783,
     784,   781,   774,     0,   429,     0,     0,   506,     0,     0,
       0,     0,   811,   477,   476,     0,   474,     0,   193,     0,
     527,   806,   427,   428,   432,     0,     0,     0,   314,   317,
     176,     0,   598,     0,     0,     0,     0,     0,   712,   723,
     310,   462,   727,   681,     0,   481,     0,     0,   201,     0,
     394,   981,     0,     0,     0,    16,   802,   327,   337,     0,
     333,   335,   331,     0,     0,     0,     0,     0,     0,     0,
     965,   685,   562,    80,    79,   128,   126,   127,   340,     0,
     487,   423,     0,     0,     0,     0,   191,     0,   520,     0,
       0,   727,     0,     0,    64,   527,   505,   601,   138,     0,
     142,    45,     0,   711,     0,     0,     0,     0,   934,     0,
       0,     0,     0,     0,   914,   916,     0,   692,   690,     0,
      45,   951,    45,   950,   145,   340,     0,   502,     0,  1181,
       0,   727,  1183,     0,   727,     0,     0,   727,     0,   727,
       0,   727,     0,   727,     0,     0,     0,    45,     0,     0,
       0,  1210,     0,  1201,  1204,  1003,  1001,  1002,    45,   998,
       0,     0,     0,   350,   598,   598,     0,  1028,  1031,     0,
       0,     0,    45,  1237,     0,     0,     0,    45,  1238,  1272,
    1274,     0,     0,  1277,    45,  1239,     0,     0,     0,     0,
       0,     0,    45,  1283,    15,    29,    41,     0,   173,   160,
     117,     0,    45,     0,    45,    27,   159,   539,   539,   169,
     172,   168,     0,   186,   189,   214,     0,     0,     0,   247,
     245,   252,   249,   263,   256,   261,     0,     0,   215,   238,
     250,   242,   253,   243,   258,   244,     0,   237,     0,   232,
     229,   218,   219,     0,     0,   425,   351,     0,   387,   598,
     347,   344,   345,     0,   398,     0,   534,   533,     0,     0,
     581,   352,     0,     0,     0,   351,   588,   351,   592,   351,
     590,   310,     0,   598,   518,     0,     0,   980,     0,   311,
     478,   480,   172,   322,     0,   598,   519,     0,   984,   598,
     983,   318,   320,   726,     0,     0,     0,   736,     0,     0,
       0,     0,   710,   464,   481,   501,     0,   203,   202,   381,
     493,   490,   488,     0,   491,     0,   328,     0,     0,     0,
       0,     0,     0,   967,     0,  1013,     0,     0,   422,   417,
     954,   955,   721,   310,   961,   437,     0,   816,   818,   824,
     294,   293,     0,   287,     0,     0,   289,   288,     0,   760,
     761,   713,     0,   943,   942,     0,   940,     0,   937,   282,
       0,  1019,  1008,     0,  1006,  1009,   755,     0,     0,   928,
     920,   693,     0,     0,     0,     0,     0,   300,     0,   299,
     307,     0,  1190,     0,  1190,  1190,  1126,     0,  1120,  1122,
    1123,  1121,   727,  1125,  1124,     0,  1190,   727,  1144,  1143,
       0,     0,  1187,  1186,     0,     0,  1190,     0,  1190,     0,
     727,  1065,  1069,  1070,  1071,  1067,  1068,  1072,  1073,  1066,
       0,  1154,  1158,  1159,  1160,  1156,  1157,  1161,  1162,  1155,
    1164,  1163,   727,     0,  1107,  1109,  1110,  1108,   727,     0,
    1137,  1138,   727,     0,     0,     0,     0,     0,     0,  1222,
       0,     0,   852,  1000,     0,  1026,     0,   598,     0,  1030,
       0,     0,    45,  1252,     0,     0,     0,  1263,     0,     0,
       0,     0,  1271,     0,     0,    45,     0,     0,     0,    45,
    1285,     0,     0,     0,   108,   794,     0,   111,     0,   173,
       0,   146,     0,   171,   170,   267,   253,   266,     0,   255,
     260,   254,   259,     0,     0,     0,     0,     0,   222,   212,
     223,   241,     0,   222,   234,   235,     0,     0,     0,     0,
     278,   223,   279,     0,     0,   227,   268,   273,   276,   229,
     220,     0,   503,     0,   413,   385,   388,     0,   346,     0,
     531,   568,   569,     0,     0,   351,     0,     0,     0,   778,
     772,   788,     0,     0,     0,   525,     0,   340,   526,     0,
     986,     0,     0,     0,   740,     0,   738,   735,   730,   734,
     732,     0,    45,     0,   463,   450,   205,   334,   336,     0,
       0,     0,   969,   964,   131,     0,  1012,   421,     0,     0,
     416,   960,     0,     0,    45,   814,   825,   826,   831,   835,
     828,   836,   837,   838,   832,   834,   833,   829,   830,     0,
       0,     0,     0,   285,     0,     0,     0,     0,   938,   933,
     472,     0,  1005,   727,   915,     0,     0,   890,   104,   306,
     301,   303,   305,     0,     0,     0,  1075,   727,  1076,  1077,
      45,  1118,   727,  1145,  1141,   727,  1190,     0,  1074,    45,
    1078,     0,  1079,     0,  1063,   727,  1152,   727,  1105,   727,
    1135,   727,  1214,  1215,  1216,  1224,  1225,    45,  1219,  1217,
    1218,     0,     0,     0,   393,     0,     0,  1259,     0,     0,
       0,     0,     0,     0,     0,     0,  1281,     0,     0,     0,
      45,    45,     0,     0,  1291,     0,     0,     0,     0,     0,
      31,   175,   174,     0,     0,   119,   113,   107,     0,     0,
     161,   598,   166,     0,   248,   246,   264,   257,   262,   216,
     222,   598,     0,   240,   236,   223,     0,   233,     0,   270,
     269,     0,   225,   229,     0,     0,     0,     0,     0,   230,
       0,   426,   386,   348,   397,     0,   583,   595,   597,   596,
       0,   430,     0,     0,   809,     0,   433,     0,   985,   756,
     729,     0,     0,    45,     0,     0,     0,   844,   974,   845,
    1018,     0,  1015,  1017,   420,   419,     0,     0,     0,   817,
       0,   827,     0,   288,     0,     0,   765,   762,   719,   714,
     715,   717,   718,   941,  1007,  1011,     0,     0,   381,     0,
       0,     0,     0,   309,   308,   521,     0,     0,     0,  1119,
    1142,     0,  1189,  1188,     0,     0,     0,  1064,  1153,  1106,
    1136,  1223,     0,     0,   392,     0,     0,  1258,  1257,   902,
     903,   904,   901,   906,   900,   907,   899,   898,   897,   905,
     893,     0,     0,    45,  1256,  1268,  1269,  1267,  1275,  1273,
       0,  1276,     0,  1278,     0,     0,  1243,     0,  1293,  1294,
      45,  1286,  1287,  1288,  1289,  1295,  1296,     0,     0,     0,
     795,   162,   163,     0,     0,   239,   598,   241,     0,   280,
     228,     0,   272,   271,   274,   275,   277,   473,     0,   770,
     769,   771,     0,   767,   431,     0,   997,   434,     0,   741,
     739,     0,   731,     0,     0,     0,  1014,   418,   846,     0,
       0,     0,     0,   911,     0,     0,     0,     0,     0,     0,
       0,   286,   291,   290,     0,     0,     0,  1004,   917,   918,
       0,   842,   891,   891,   304,  1091,  1090,  1089,  1096,  1097,
    1098,  1095,  1092,  1094,  1093,  1102,  1099,  1100,  1101,     0,
    1086,  1130,  1129,  1131,  1132,     0,  1191,  1081,  1083,  1082,
       0,  1085,  1084,     0,  1027,  1261,  1260,     0,     0,     0,
    1282,     0,  1245,    45,  1246,  1248,  1292,     0,   796,     0,
     172,   265,     0,     0,   227,   226,     0,     0,   766,   510,
       0,     0,     0,   466,  1016,   823,   822,     0,   820,   862,
     859,     0,     0,     0,     0,   909,   910,     0,     0,     0,
       0,     0,   716,   922,  1010,    45,     0,     0,     0,     0,
       0,  1128,  1185,  1080,    45,     0,     0,   894,     0,  1244,
      45,  1241,    45,  1242,  1290,     0,     0,   251,   231,   495,
     768,   757,   744,   737,   742,     0,     0,   819,   865,   860,
       0,     0,     0,     0,     0,     0,     0,   848,     0,   854,
       0,   467,   720,     0,     0,   841,    45,    45,   888,  1088,
    1087,     0,     0,   895,     0,  1279,     0,     0,   799,   793,
     797,   167,     0,     0,   465,   821,     0,     0,     0,     0,
     857,     0,   840,     0,   908,   858,     0,   847,     0,   853,
       0,   923,     0,     0,     0,  1127,     0,     0,   354,     0,
       0,     0,   496,     0,   747,     0,   745,   748,   863,   864,
       0,   866,   868,     0,     0,     0,   849,   855,   468,   919,
     889,   887,     0,   896,    45,    45,   798,   750,   751,     0,
     743,     0,   861,     0,   856,   839,     0,     0,     0,     0,
       0,     0,   749,   752,   746,   867,     0,     0,   871,   912,
     850,  1021,  1247,  1249,   753,     0,     0,     0,   869,    45,
    1020,   754,   873,   872,    45,     0,     0,     0,   874,   879,
     881,   882,  1022,  1023,     0,     0,     0,    45,   870,    45,
      45,   598,   885,   884,   883,   875,     0,   877,   878,     0,
     880,     0,    45,   886,   876
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     6,     7,   208,   384,   209,   840,   751,   210,
     903,   619,   804,   689,   569,   901,   902,   441,  2234,  1220,
    1506,   211,   212,   620,  1124,  1125,   213,   214,   215,   579,
    1286,  1287,  1128,  1288,   216,   217,   218,   219,  1153,   220,
     221,  1154,   222,   582,   223,   224,   225,   226,  1578,  1579,
     918,  1590,   937,  1864,   227,   228,   229,   230,   231,   232,
     785,  1164,  1165,   233,   234,   235,   746,  1076,  1077,   236,
     237,   710,   453,   930,   931,  1606,   932,   933,  1902,  1616,
    1621,  1622,  1903,  1904,  1617,  1618,  1619,  1608,  1609,  1610,
    1611,  1876,  1613,  1614,  1615,  1878,  2119,  1906,  1907,  1908,
    1166,  1167,  1478,  1479,  1993,  1727,  1145,  1146,   238,   458,
     239,   850,  2010,  2011,  1759,  2012,  1027,   718,   719,  1050,
    1051,  1039,  1040,   240,   756,   757,   758,   759,  1092,  1439,
    1440,  1441,   397,   374,   404,  1331,  1630,  1332,   885,   999,
     622,   641,   623,   624,   625,   626,  2055,  1079,   970,  1916,
     823,   627,   628,   629,   630,   631,  1336,  1632,   632,  1306,
    1913,  1404,  1385,  1405,  1020,  1137,   241,   242,  1954,   243,
     244,   692,  1032,  1033,   709,   423,   245,   246,   247,   248,
    1083,  1084,  1433,  1923,  1924,  1070,   249,   250,   251,   252,
    1202,   253,   973,  1344,   254,   376,   727,  1422,   255,   256,
     257,   258,   259,   260,   652,   644,   979,   980,   981,   261,
     262,   263,   996,   997,  1002,  1003,  1004,  1333,   769,   645,
     801,   564,   264,   265,   266,   713,   267,   729,   730,   268,
     767,   768,   269,   499,   835,   836,   838,   270,   271,   765,
     272,   820,   273,   814,   274,   701,  1067,   275,   276,  2169,
    2170,  2171,  2172,  1713,  1064,   407,   721,   722,  1063,  1678,
    1742,  1945,  1946,  2423,  2424,  2495,  2496,  2518,  2532,  2533,
    1747,  1943,   277,   278,  1729,   673,   809,   810,  1931,  2272,
    2273,  1932,   670,   671,   279,   280,   281,   282,  2083,  2084,
    2459,  2460,   283,   754,   755,   284,   706,   707,   285,   685,
     686,   286,   287,  1143,  1719,  2159,  2377,  2378,  1975,  1976,
    1977,  1978,  1979,   703,  1980,  1981,  1982,  2438,  1227,  1983,
    2440,  1984,  1985,  1986,  2380,  2428,  2468,  2500,  2501,  2537,
    2538,  2557,  2558,  2559,  2560,  2561,  2572,  1987,  2181,  2397,
     816,  2060,  2220,  2221,  2222,  1988,   828,  1493,  1494,  2005,
    1160,  2394,   288,   289,   290,   291,   292,   293,   294,   295,
     797,  1162,  1163,  1735,  1736,   296,   844,   297,   780,   298,
     781,   299,  1140,   300,   301,   302,   303,   304,  1100,  1101,
     305,   762,   306,   307,   308,   681,   682,   309,   310,  1407,
    1668,   715,   311,   312,   776,   313,   314,   315,   316,   317,
     318,   319,  1234,  1235,   320,  1170,  1743,  1744,  2307,   321,
    1706,  2151,  2152,  1745,   322,  2550,   323,   324,   325,   326,
    1243,   327,   328,   329,   330,   331,   332,  1203,  1790,   862,
    1768,  1769,  1770,  1794,  1795,  1796,  2340,  1797,  1798,  1771,
    2187,  2450,  2329,   333,  1209,  1818,   334,   335,   336,   337,
    1193,  1772,  1773,  1774,  2335,   338,  1211,  1822,   339,  1199,
    1777,  1778,  1779,   340,   341,   342,  1205,  1812,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,  1783,  1784,   863,  1515,   358,   359,   360,
     361,   362,   873,   874,   875,  1221,  1222,  1223,  1228,  1828,
    1829,   363,   364,   365,   879,   366,   367,   368,   369,   370,
    2235,  2236,  2411,  2413,   371,  1246,  1247,  1248,  1249,  1250,
    2056,  2057,  1252,  1253,  1254,  1255,  1256,  1258,  1259,  2069,
     893,  2067,   372,  1262,  1263,  2073,  2074,  2079,   557
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -2208
static const yytype_int16 yypact[] =
{
   -2208,    77, -2208, -2208, -2208, -2208,    35,  4734, -2208, -2208,
   -2208,   146, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,   792, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208,    40, -2208, -2208,   810,   148, -2208, -2208, -2208,    40,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208,    44,    44, -2208, -2208, -2208, -2208, -2208,    44,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
     242, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,    44, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208,   460, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
     509,   560, -2208, -2208, -2208, -2208, -2208,    40, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208,    40, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,   354,  1632,
     623,   354, -2208, -2208, -2208,   654,   709,   758,   788, -2208,
   -2208, -2208,   513,   808,    44, -2208, -2208,   814,   847,   880,
     882,   154,   572,   905,   911,   920, -2208,   258, -2208, -2208,
   -2208,   354, -2208, -2208, -2208,   946,    -5,  1977,  2354, -2208,
   -2208,  1787, -2208,   866, -2208, -2208,  2654, -2208,   960, -2208,
   -2208,  1562,   960,   951, -2208, -2208,   987, -2208, -2208, -2208,
     989,   993,  1039,  1049,  1067, -2208, -2208, -2208, -2208,  1069,
     889, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208,  1083, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208,   135,    44,  1091,  1087,  1103,   961,    44,
      44,   125,    44, -2208,    44,    44,  1115, -2208,   175,  1120,
      44,    44,    44,    44, -2208, -2208,    44, -2208,  1124,    44,
     997,    44,  1142, -2208, -2208, -2208,    44, -2208,  1140,    44,
   -2208,    44,  1156,   178, -2208,   997, -2208,    44,    44,    44,
      44, -2208, -2208, -2208, -2208, -2208,    44, -2208,    44,    44,
     623,    44,  1158,  1091,    44,  1160, -2208,    44,    44, -2208,
   -2208, -2208,  1184,  1181,    44,    44, -2208,  1183,  1185,    44,
    1091,  1203,  1787, -2208,  1205,  1219,    44, -2208,  1234,    44,
    1212, -2208,  1236,    44,  1091,  1248,  1250, -2208,   961,  1091,
      44,    44,  2638,    69,    44,   107, -2208, -2208,   199, -2208,
     301,    44,    44,    44,  1277,    44,    44,  1787,   109, -2208,
   -2208,  1279,    44,    44,    44,    44,    44,  2870,    44, -2208,
    1091,    44,  1091,    44,    44, -2208, -2208,    44, -2208,  1091,
      44,  1322,  1325, -2208,    44, -2208, -2208,  1336, -2208, -2208,
    1343, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208,  1348, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208,    44, -2208, -2208,  1351,  1359, -2208,  1375,
    1787,  1787,  1787,  1787,  1787,  1382,  1385,  1405,  1407,  1417,
      44, -2208,  1424, -2208, -2208, -2208, -2208,  1136,   174, -2208,
   -2208,    44,    44,    44,    44,  1408, -2208, -2208,  1310,    44,
      44, -2208,   637,    44,    44,    44,    44,    44,   500,    44,
    1212,    44,    44,  1158, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208,  1220, -2208, -2208, -2208, -2208, -2208, -2208,  1787,
    1787,  1787, -2208,  1787, -2208, -2208, -2208, -2208, -2208, -2208,
    1787,   877, -2208,   124,  1466, -2208,  1429, -2208,  1224,  1230,
    1463, -2208, -2208,  1458,  1787, -2208, -2208,  2630, -2208, -2208,
    1454,  2068,  1466, -2208, -2208,   833,     3, -2208,  2630, -2208,
   -2208, -2208,  1474,   305,   181,  3422,  3422,    44,    44,    44,
      44,    44,    44,    44,  1478, -2208,    44, -2208, -2208, -2208,
     627, -2208, -2208,  1475,    44, -2208,  1787, -2208,  1251,   703,
   -2208,  1477, -2208, -2208,  1482,  1493, -2208, -2208, -2208, -2208,
   -2208,  2930,    44,  1488, -2208,    44,  1482,    44, -2208,   961,
   -2208, -2208, -2208, -2208, -2208, -2208,  1496, -2208, -2208, -2208,
   -2208, -2208,  1482, -2208, -2208,  1494, -2208, -2208,   668,  1479,
      44,   698,   116, -2208,  1495,  1341,  1787,  1368, -2208,  1513,
   -2208, -2208,  1787,  1787, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208,    44, -2208,    44,  1508,   629,
      44,   623, -2208, -2208,  1517, -2208,  1530, -2208,  1526,  1595,
   -2208,  1536, -2208,    44, -2208, -2208, -2208,  1537, -2208,   960,
    1485,  3488, -2208,    44, -2208,  5761, -2208,    44,  1787, -2208,
    1535, -2208,    44, -2208,    44,    44,    44,  1466,   167,    44,
      44,    44,  1368, -2208,    44,   312, -2208, -2208, -2208,  2068,
     833, -2208, -2208, -2208, -2208, -2208, -2208,   135, -2208,  1475,
    1544,  1495, -2208, -2208, -2208, -2208, -2208, -2208,    44, -2208,
   -2208, -2208,  5761, -2208,   175,  1491,    44, -2208,  1539, -2208,
   -2208, -2208, -2208,  1540,  3567,   706, -2208, -2208,   460,    44,
     623, -2208,    44,  1482, -2208,  1548,  1542, -2208,    44, -2208,
    1549,  1787,  1787, -2208,  1482,    44,   184,    44,  1273,  1273,
     189,  1273, -2208,  1546,   222,   224,   232,   233,   261,   310,
   -2208,  1482,   449, -2208,  1556, -2208,   120,   173,  1258, -2208,
   -2208,  3609,  5761,  3648,  3723,  1566,  5761,    44,    44, -2208,
   -2208, -2208, -2208,  1567, -2208,    44,    44, -2208, -2208, -2208,
   -2208,   708, -2208, -2208,  1363,  1482, -2208, -2208, -2208, -2208,
    3026,    44, -2208, -2208, -2208, -2208, -2208, -2208, -2208,  1482,
   -2208, -2208, -2208, -2208,  1572, -2208,  1572, -2208, -2208, -2208,
   -2208,   575, -2208,   455, -2208,  1568, -2208, -2208,  3764,  1573,
    1575,  1575,  2712, -2208,  1787,  1787,  1787,  1787,  1787,  1787,
    1787,  1787,  1787,  1787,  1787,  1787,  1787,  1787,  1787,  1787,
    1787,  1787,  1787, -2208,  1522,  1459,  1569,   514,   364,  1787,
   -2208, -2208, -2208,   712,  1532, -2208, -2208, -2208, -2208,   729,
   -2208,  1633,  1080,  1787,  1585,  2068,  2068,  2068,  2068,  2068,
   -2208,  1437, -2208,   305,   305,  1466,  1587, -2208,  3422,  5761,
     100,   140, -2208,  1589,  1591, -2208, -2208,  1482, -2208, -2208,
   -2208, -2208,  1482, -2208,   661, -2208,   135, -2208, -2208, -2208,
      44,  3795,    44,  1588,  1787,  1533, -2208, -2208,    44, -2208,
    1787,  3831, -2208,   731, -2208, -2208,  1565, -2208, -2208,   733,
   -2208,    44, -2208,    44, -2208, -2208,  1479, -2208, -2208, -2208,
   -2208, -2208,  3865,  1482, -2208, -2208, -2208,  1592,  1594,  1596,
    1597,  1598,  1600, -2208,  1341, -2208,    44, -2208,  3896, -2208,
   -2208,    44,  3956,  4036, -2208,  1601,   754,  1602,  1463, -2208,
   -2208,    44, -2208,  1609, -2208,  1599, -2208,    44, -2208,  1489,
     588, -2208, -2208,   -32, -2208, -2208,  1613, -2208,  1606,  1625,
     762, -2208,    44,  3422,  1612, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208,  1614, -2208, -2208,   302,  1615,  1616,
    4071,  3230,   -62, -2208,  1604, -2208, -2208,   763, -2208, -2208,
   -2208, -2208, -2208,   768,  1611,   785, -2208, -2208, -2208,  1787,
   -2208,  1503, -2208, -2208, -2208,   800, -2208,  1637, -2208,  1341,
    1624,  1638,   839, -2208, -2208, -2208,  1644, -2208,  1640,  1641,
    1627,    44,  1787,  1787,  2870, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208,  1649,  1654, -2208,    85, -2208, -2208,  4105,  4166,
   -2208,  1643, -2208,   316,  1647, -2208, -2208, -2208, -2208,   319,
   -2208, -2208, -2208,   337, -2208,   343,   423,   444, -2208,   479,
   -2208,   483, -2208,  1650,  1655,  1657,  1658, -2208,  1659,  1660,
   -2208, -2208, -2208, -2208, -2208, -2208,  1466,  1668,  1656, -2208,
    1661, -2208, -2208,   -67,   840, -2208, -2208, -2208, -2208,  1787,
     681,   770, -2208,   843,   844,   475,   851, -2208, -2208, -2208,
   -2208,    65,   883, -2208, -2208, -2208, -2208,   477,   903, -2208,
   -2208,   300,   908, -2208, -2208,   623,    44,   129, -2208,  1663,
   -2208,  1674, -2208,  1482, -2208, -2208, -2208,  1672, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208,  1076, -2208, -2208, -2208,
      44,  1482,   108,  1814, -2208, -2208,    44,    44, -2208,  1369,
     455, -2208,  1673, -2208,  1621,  1787,  3422, -2208,  1787,  1575,
    1575,   159,   159,  2712,   644,  1131,  2831,  5761,  2831,  2831,
    2831,  2831,  2831,  1131,  1858,  1575,  1858,  3446,  1569, -2208,
   -2208,  1669,  1684,  2759, -2208, -2208, -2208, -2208, -2208,  1686,
   -2208, -2208,   961,  5761, -2208,  1787, -2208, -2208, -2208, -2208,
    5761,   166,  5761,  1585,  1585,   945,  1585,   495, -2208,  1587,
    1688,   305,  4207,  1691,  1694,  1695,  3422,  3422,  3422, -2208,
   -2208,    44,  1679, -2208, -2208,  1689,  1495, -2208,   460, -2208,
   -2208, -2208, -2208,  1450, -2208,   913,   961, -2208,   961,   919,
    1699,   944, -2208,  5761,  1787,  2930, -2208,   947, -2208,   961,
    1572, -2208,   799,   955, -2208,   948,  1541,   971, -2208,  2121,
   -2208,   116, -2208,  1693,    44,    44,  1787,    44, -2208, -2208,
    1482, -2208, -2208, -2208,  1470,    44,  1787,    44, -2208,    44,
   -2208,  1466,  1787,  1692,  3230, -2208, -2208, -2208, -2208,   978,
   -2208,  1696, -2208,  1701,  1702,  1709,  1502,  1787,    44,    44,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,  1091,    44,
   -2208,  3266,  1916,  1705,    44,    44, -2208,    44, -2208,  1538,
      44, -2208,  1787,    44, -2208,  1572,  5761, -2208,  1719,    88,
    1719, -2208,    44,  1341,  1721,  3301,    44,    44, -2208,   175,
    1787,   672,  1787,   983, -2208,  1715,   988,  5761, -2208,    51,
   -2208, -2208, -2208, -2208, -2208,  1091,    43, -2208,    44, -2208,
     497, -2208, -2208,   -42, -2208,   194,  1110, -2208,   942, -2208,
     321, -2208,   -47, -2208,    44,    44,    44, -2208,    44,    44,
     449, -2208,    44, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
    1401,  1421,  1406,  5761, -2208,  1569,    44, -2208, -2208,  1717,
    1723,  1725, -2208, -2208,  1726,  1730,  1731, -2208, -2208, -2208,
    1732,  1733,  1735,  1736, -2208, -2208,   776,  1738,  1739,  1740,
    1742,  1744, -2208, -2208, -2208, -2208, -2208,    44,   299, -2208,
   -2208,  1746, -2208,  1727, -2208, -2208,  1674, -2208, -2208, -2208,
   -2208,  5761,  2276, -2208, -2208, -2208,   573,   425,   425,  1518,
    1519, -2208, -2208,  1520,  1523,  1524,   579,    44, -2208, -2208,
   -2208, -2208,  1763, -2208, -2208, -2208,  1673, -2208,  1764, -2208,
     469,  1756, -2208,  1759,  4238, -2208,  1754,  1758,  1463, -2208,
   -2208,  4303, -2208,  1787,  1787,  1532, -2208,  5761,  2630,   305,
   -2208,   147,  3422,  3422,  3422,   151, -2208,   170, -2208,   180,
   -2208,  1482,    44, -2208, -2208,  1769,   990, -2208,  1773, -2208,
    5761, -2208, -2208, -2208,  1787, -2208, -2208,  1787, -2208, -2208,
   -2208, -2208,  5761, -2208,  1541,  1787,  1760, -2208,  1762,  1768,
    4334,  1777, -2208,    75,    44, -2208,   992, -2208, -2208,  1765,
    5761, -2208, -2208,  4303, -2208,  1489, -2208,  1489,    44,    44,
      44,   998,   999, -2208,    44,  1778,  1779,  1787,  4379,  3360,
   -2208, -2208, -2208,  1482,  1466, -2208,  1784, -2208,  1635,  1802,
    5761, -2208,    44, -2208,  1797,  1798, -2208, -2208,  1560,  1809,
   -2208, -2208,  1813, -2208,  5761,  1008, -2208,  1013, -2208, -2208,
    4410, -2208, -2208,  1014, -2208, -2208,  5761,  1801,    44, -2208,
   -2208, -2208,  1810,  1812,  1628,  1766,    44,    44,  1822,  1838,
   -2208,   638, -2208,  1832, -2208, -2208, -2208,  1834, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208,   497, -2208, -2208, -2208, -2208,
     -42,    44, -2208, -2208,  1017,  1837, -2208,  1839, -2208,  1841,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
    1110, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208,   942, -2208, -2208, -2208, -2208, -2208,   321,
   -2208, -2208, -2208,   -47,  1836,  1845,  1847,   865,  1032, -2208,
    1848,  1849,  1466, -2208,  1857, -2208,  1862,  1569,  1852, -2208,
      44,    44, -2208, -2208,    44,    44,    44, -2208,    44,    44,
      44,  1787, -2208,  1867,  1869, -2208,    44,    44,  1787, -2208,
   -2208,  1864,  1787,  1787, -2208, -2208,  1870, -2208,  1177,   299,
    2449, -2208,  1033, -2208,  5761, -2208, -2208, -2208,  1885, -2208,
   -2208, -2208, -2208,   514,   514,   514,   514,   514,  1369, -2208,
    1879,  1895,  1886,  1369,  1756, -2208,   455,   469,   172,   172,
   -2208, -2208, -2208,  1034,  1846,   931,   294, -2208,  1896,   469,
   -2208,  1787, -2208,  1884, -2208,  1463, -2208,  2759,  5761,  1887,
   -2208, -2208,   833,  1881,  1889,  1040,  1890,  1891,  1892, -2208,
   -2208, -2208,  1899,    17,   961, -2208,    44,  1091,  5761,    17,
    5761,  1541,  1787,  1894,  4467,  1042, -2208, -2208, -2208, -2208,
   -2208,  1787, -2208,  1902, -2208, -2208, -2208, -2208, -2208,  1057,
    1066,  1074, -2208, -2208, -2208,   716, -2208,  5761,  1787,  1787,
    4732, -2208,    44,    44, -2208, -2208,  1802, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,  1901,
      88,  1903,  3488, -2208,    44,    44,    44,  3301, -2208, -2208,
   -2208,   672, -2208, -2208, -2208,  2801,    44, -2208, -2208,  1822,
    1914, -2208, -2208,    44,    44,  1787, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208,   194, -2208, -2208,
   -2208,  1787, -2208,  1787, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208,    44,    44,  1907,  1028,  1904,  1078, -2208,  1085,  1169,
    1092,  1111,  1116,  1130,  1144,  1149, -2208,  1159,  5067,  1910,
   -2208, -2208,   290,  1164, -2208,  1168,  1193,  5107,   874,  1926,
   -2208,  5761,  5761,  1195,  1928, -2208, -2208, -2208,  1921,  5138,
   -2208, -2208, -2208,   573, -2208, -2208, -2208, -2208, -2208, -2208,
    1369, -2208,    44, -2208, -2208,  1935,  1929, -2208,   901,   294,
     294,   469, -2208,   469,   172,   172,   172,   172,   172,  1228,
    5169, -2208, -2208, -2208, -2208,  1787, -2208, -2208, -2208, -2208,
    1828, -2208,    44,  1954,  1493,    44, -2208,    44, -2208,  5200,
   -2208,  1787,  1787, -2208,  5231,  1710,  1787, -2208, -2208, -2208,
   -2208,  1198, -2208, -2208,  5761,  5761,  1787,  1217,  1950, -2208,
     603, -2208,  1787, -2208,  1945,  1946, -2208, -2208,  1956,  1961,
   -2208, -2208, -2208, -2208, -2208,  1840,  1952,  1218,  1967,  1971,
    1229,   950,    44, -2208, -2208,  5761,  1286,  1960,    -1, -2208,
   -2208,  1947, -2208, -2208,   -60,  5262,  5293, -2208, -2208, -2208,
   -2208, -2208,  1235,  1962,  1036,  1787,    44, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208,  1970,  1972, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
      44, -2208,  1787, -2208,  1629,  1240, -2208,  1245, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208,  1787,  1979,  1982,
   -2208, -2208, -2208,  1814,  1973, -2208,  1569, -2208,   469, -2208,
    1228,  1975,   294,   294, -2208, -2208, -2208, -2208,  5324, -2208,
    4303, -2208,  1246, -2208, -2208,   961,  1668, -2208,  1541,  5761,
   -2208,  1737, -2208,  1976,  5355,   716, -2208,  5761, -2208,  2560,
    1983,  1984,  1985,  1987,  1988,    44,    44,  1989,  1990,  1991,
    5386, -2208, -2208, -2208,  1787,    44,    44, -2208, -2208,  1994,
      44, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,  2003,
   -2208, -2208, -2208, -2208, -2208,  1285, -2208, -2208, -2208, -2208,
    1992, -2208, -2208,  2004, -2208,  5761, -2208,    44,    44,  1169,
   -2208,  5417, -2208, -2208,  2006,  2008, -2208,  5448, -2208,  2010,
   -2208, -2208,  1997,  1291,  1228, -2208,  1787,  1828, -2208, -2208,
    1787,    44,  1787, -2208, -2208, -2208,  5761,  1296, -2208, -2208,
    1983,    44,    44,    44,    44, -2208, -2208,  1787,  1787,    44,
    1787,  1305, -2208, -2208,  2011, -2208,  1317,  2013,  1324,    44,
    1787, -2208, -2208, -2208, -2208,   665,  2019, -2208,  1787, -2208,
   -2208, -2208, -2208, -2208, -2208,    44,  2000, -2208, -2208,  5479,
   -2208,  5761, -2208, -2208,  1996,  5510,  2560, -2208,   427, -2208,
    2021,  1326,  2025,  1327,  2022,  1328,  5541,  5572,  2015, -2208,
    1331,  5603, -2208,    44,  1963, -2208, -2208, -2208, -2208,  1668,
   -2208,  5634,  1711, -2208,  1787,  5761,  1682,  1697, -2208,  2034,
   -2208, -2208,  1787,  2188, -2208, -2208,  2037,  2038,    44,    44,
   -2208,    44, -2208,  2870, -2208, -2208,  1787, -2208,    44, -2208,
    1787, -2208,  2026,  1338,  1346, -2208,  2036,  5665,  1073,  2040,
    2044,    44,  5761,    44,  5761,  1357, -2208, -2208, -2208, -2208,
    1362, -2208,  2045,  1380,  1396,  1400,  5696, -2208,  5761, -2208,
   -2208, -2208,  1787, -2208, -2208, -2208, -2208, -2208,  2032,  2188,
   -2208,    44, -2208,  1787, -2208, -2208,  2031,  1787,  1403,  1404,
    1415,  1787,  2046, -2208, -2208, -2208,  5730,  1419, -2208, -2208,
    5761,  2053, -2208, -2208, -2208,  1787,  1787,  1787,  2047, -2208,
   -2208, -2208,  5761, -2208, -2208,   -56,   453,  1422, -2208,  2058,
    2062, -2208, -2208, -2208,  2057,  2057,  2057, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208,   182,  2075, -2208,  1925,
   -2208,  1441, -2208, -2208, -2208
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -2208, -2208, -2208, -2208, -2208,   -14,  1865,  1190, -2208, -2208,
    -684,   -38, -2208, -2208,  -373, -2208,   822, -2208,   -50,  -720,
   -2208, -2208, -2208,  2030,    98, -2208, -2208, -2208, -2208, -2208,
   -2208,   223,   512,   914, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208,  -173,  -900, -2208, -2208, -2208,  1006,   515,  1516,
   -2208,  -151, -1555,   237, -2208, -2208, -2208, -2208, -2208, -2208,
    1521,  -290,  -365, -2208, -2208, -2208,  1515, -2208,  -682, -2208,
   -2208, -2208, -2208,  1402, -2208, -2208,   812, -1254, -1559,  1180,
     498, -1527,  -143,     4,  1186, -2208,   225,   230, -1781, -2208,
   -1553, -1238, -1523,  -329, -2208,    27, -1583, -1803,  -802, -2208,
   -2208,   633,   969,   401,   -36,   138, -2208,   652, -2208, -2208,
   -2208, -2208, -2208,   -52, -2208, -1448,  -340,  1107, -2208,  1088,
     730,   752,  -376, -2208, -2208,  1053, -2208, -2208, -2208, -2208,
     447,   448,  2071,  3050,  -364, -1299,   234,  -422, -1007,   326,
    -499,  -520,  1561,   542,  1703,  -877,  -873, -2208, -2208,  -617,
    -607,  -228, -2208,  -814, -2208,  -576,  -942, -1112, -2208, -2208,
   -2208,   213, -2208, -2208,  1443, -2208, -2208,  1912, -2208,  1913,
   -2208, -2208,   761, -2208,  -379,    -7, -2208, -2208,  1918,  1922,
   -2208,   737, -2208,  -735,  -262,  1370, -2208,  1095, -2208, -2208,
       6, -2208,  1135,   536, -2208,  4535,  -423, -1072, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208,  -191, -2208,   534,  -909, -2208,
   -2208, -2208,   296, -1274,  -635,  1178,  -952,  -182,  -395,  -430,
     873,   284, -2208, -2208, -2208,  1525, -2208, -2208,  1104, -2208,
   -2208,  1077, -2208,  1340, -1967,  1009, -2208, -2208, -2208,  1527,
   -2208,  1534, -2208,  1545, -2208,  1550, -1010, -2208, -2208, -2208,
    -118,  -254, -2208, -2208, -2208,  -416, -2208,   586,   784,  -572,
     786, -2208,    58, -2208, -2208, -2208,  -318, -2208, -2208, -2208,
   -1918, -2208, -2208, -2208, -2208, -2208, -1433,  -541,   214, -2208,
    -160, -2208,  1411,  1194, -2208, -2208,  1197, -2208, -2208, -2208,
   -2208,  -272, -2208, -2208,  1134, -2208, -2208,  1182, -2208,   289,
    1199, -2208, -2208,  -868, -2208, -2207, -2208,  -200, -2208, -2208,
     253, -2208,  -771,  -384,  1789,  1444, -2208, -2208, -1571, -2208,
   -2208, -2208, -2208, -2208,  -146, -2208, -2208, -2208,  -286, -2208,
    -311, -2208,  -328, -2208,  -331, -1968, -1128,  -757, -2208,   -69,
    -480,  -991, -2077, -2208, -2208, -2208,  -489, -1791,   499, -2208,
    -754, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
    -485, -1456,   759, -2208,   248, -2208,  1582, -2208,  1745, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -1432,   802,
   -2208,  1492, -2208, -2208, -2208, -2208,  1872, -2208, -2208, -2208,
     314,  1844, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208,   718, -2208, -2208, -2208,   259, -2208, -2208,
   -2208, -2208,   -24, -1902, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208,   606,   462,  -529,
   -1312, -1231, -1317, -1437, -1428, -1427, -2208, -1421, -1419, -1228,
   -2208, -2208, -2208, -2208, -2208,   445, -2208, -2208, -2208, -2208,
   -2208,   488, -1417, -1415, -2208, -2208, -2208,   442, -2208, -2208,
     486, -2208,  -625, -2208, -2208, -2208, -2208,   458, -2208, -2208,
   -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2208, -2208, -2208,   245, -2208,   247,   -61, -2208, -2208, -2208,
   -2208, -2208, -2208, -2208,  1050, -2208,  1398, -2208,  -826, -2208,
     231, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208, -2208,
   -2005,   -74, -2208, -2208, -2208, -2208,   732, -2208, -2208, -2208,
   -2208,    76, -2208,   726, -2208, -2208, -2208, -2208,   721, -2208,
   -2208, -2208, -2208, -2208,   715, -2208,    48, -2208,  1260
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1191
static const yytype_int16 yytable[] =
{
     410,   821,   678,   817,   829,   830,   831,   832,   704,  1098,
    1241,   674,   870,   971,  1242,  1085,  1702,  1389,  1294,  1463,
    1244,  1005,   716,   698,   422,   422,  1338,   975,   643,  1627,
    1737,   430,   731,   643,   734,  1872,   567,  1905,  2177,   737,
     738,   739,   800,  1875,   792,  1607,  1365,   740,  1891,  1730,
     742,  1230,   744,  2061,  1418,  2063,   424,   743,  1760,  1074,
     457,  1612,   437,  2153,   812,  1345,  2237,   649,   476,   747,
     653,   655,   479,  1877,  1182,  1475,   402,     2,     3,  1001,
    1001,  1804,   806,  1952,    46,   459,   773,  1640,  1183,  1895,
    1805,  1806,   793,  2331,  1910,  2109,  2110,  1807,   697,  1808,
     789,  1810,   474,  1811,  2313,   794,  1554,  1937,  1366,   802,
     939,   805,  2103,  1464,   402,  1535,   402,    74,   853,  -541,
    -584,  1056,  1752,  1587,   824,   989,  2562,  1959,  1960,  1961,
     501,  2337,  1018,   683,   898,  1724,   839,  1443,   842,   963,
     667,  1225,   668,  1241,  1762,   848,   421,  1384,  -593,  1484,
      96,     8,   724,  1335,  1335,  -594,  1000,  1000,  1231,  -587,
    -593,   373,   587,   405,   944,  1496,  1403,  -594,   945,  -339,
    -384,  -587,   421,   421,  1057,  1138,  1444,  -527,  -591,   898,
     109,  -565,  2439,   978,  1270,  -137,   581,  1897,  -589,  -563,
    -591,  -527,   594,   733,  1229,   899,   960,   594,   694,  1793,
    -589,  1803,  1058,  1816,  1791,   693,  1801,   982,  1814,   908,
    1820,   984,   695,   598,   807,  1757,  -584,   421,   991,   920,
     921,   922,   923,  -141,  1776,   -61,   811,     4,  2338,  2332,
     594,  1781,   594,  1196,  1197,  1764,  1200,   588,   590,  2339,
     594,   594,  1059,   595,   643,  1195,  1838,   599,  1536,   421,
    1445,  1537,   990,  1060,   421,   992,  -593,   617,   421,  1753,
    -541,  1465,  2563,  -594,   421,  1725,   594,  -587,  1156,   594,
    2058,  2507,  2407,  -527,  1555,  -339,  1281,  -339,  1099,  1282,
     989,  1061,  -565,  2333,  1504,  1792,  -591,  1802,  1799,  1815,
    1809,  1821,  1817,  1556,  2334,  -565,  -589,  -563,   924,  2116,
   -1115,   421,   421,   421,   421, -1140,   440,   421,  1065,  1019,
    2564,  2262,  2263,  1034,  2108,   965,   405,  1862,   594,  2255,
     994,  1900,  1694,   421,   594,  1863,  -137,   594,   421,  1953,
    1149,  2117,  1038,  1161,     5,   669,   672,  2106, -1062,  2565,
   -1151,   679,   680,   684,   680,   594,   688,   690, -1167, -1170,
     696,   594,   700,   702,   702,   705,  1037,  -584,   708,  1879,
    1881,   712,  1339,   708,  -141,  1640,   -61,  2104,   723,  1151,
     964,   728,  1044,   708,  1062,   708,  1804, -1104,  1086,   422,
     708,   708,   708,  2153,  1592,  1805,  1806,  -527,   708,  2062,
     741,   708,  1807,   708,  1808,  1158,  1810,  -593,  1811,   752,
     753,  2157,  1435,  1150,  -594,  1152,   764,   766,  -587,   643,
     736,   772,  1503,   643,  1646,  1648,  1650,  1495,   779,  1686,
     643,   783,  1001,  -565,  1633,  1501, -1134,  -591,  1337,  1148,
     421,   594, -1114,   796,   421, -1139,   803,  -589,  -563,   825,
    1701, -1115,   723,   813,   815,   815, -1140,   819,   796,  1455,
    1456,   974,   594, -1061,   827,   827,   827,   827,   827, -1150,
     837,  1430,   989,   841,   989,   843,   779,  1178,  1452,   846,
    1300,   989,   849,  1731,  1177,   497,   854,  1179,  1588, -1062,
    2202, -1151,  1457,  1793,  1897,  1898,  1899,   594,  1791, -1167,
   -1170,   594,  1292,   421,   872,   570,  1803,  1667,   878,  1363,
     985,  1801,  1816,  1180,   986,   871,  2505,  1814,  1029,  2529,
    2530,  1820,  1762,   617,  1190,  1870,  1871,  1603, -1104,   904,
     575,   576,   892,   694,   507,   596,   910,  1001,  2260,  1763,
     900,  1212,   988,   905,   906,   907,   696,   695,  2053, -1166,
    1875,   913,   914,  2257,  1567,   919,   696,   696,   696,   696,
    2180,   895,   896,   927,   928,  1353,  1354,  1355,  1356,  1357,
   -1169,  2497,  2167,   911,  2276,  1271,  2184, -1134,   508,  1792,
    1877,   643,  1799, -1114,  1604,  2466, -1139,   621,  1103,  1295,
     589,  2564,  1802,  1298,   421,  1809,  1091,  1888,  1815,  1597,
    1598,  1817,  1821,  2435, -1061, -1103,  1213,  1214,   421, -1133,
   -1150,  2578,  2580,  1764,  1000,  2238,  1765,  2467,    50,  -527,
    2565,   563,  1299,  2544,  2584,  1014,  1889,  2497,  1900,   712,
    1007,   764,   819,   813,   700,  1012,  1605,  2551,   843,  1215,
    1568,   989,   965,  1025,  1607,  1016,   696,   577,   568,  2102,
    2239,   965,   915,   421,  2290,  1569,  1570,  1571,  1017,   944,
    1612,    25,   916,   945,   708,  1081,    29,  1036,  1549,   696,
     946,   947,   571,   617,  2015,  1603,  1550,  1369,   965,  1241,
    2566,  1625,  1370,  1839,  1752,  2364,  1046,  1741,    47,    48,
   -1166,   960,  1053,  1551,  -382,  1023,  2453,  1675,  1762,  1047,
    1646,  1648,  1650,   904,  1098,  1098,  1544,  1085,  1824,  1825,
    1826, -1169,  1830,  1831,  1372,  1763,  1054,  1075,   118,  1080,
    1093,   594,  1082,  1410,  1174,   122,  1267,   572,  -527,  1055,
    1340,  1741,  1604,  2150,  1127,  1096,  1345,  1175,  1373,  1268,
    1001,  1132,    90,  1341,  1293,  1126, -1103,  1346,  2291,  1395,
   -1133,  1399,    95,  1374,  1133,  1687,  1134,  1688,  1375,  1376,
    1347,  1141,  1396,  1144,  1400,  1377,   696,   643,   643,   643,
     643,   643,  1427,  1216,  1217,  1942,   573,  1218,  1219,   669,
    1449,  1467,  1443,  -383,  1605,  1428,  1470,  2406,   775,  1764,
    1096,  1328,  1765,  1450,  1468,  1545,   696,  1766,   121,  1471,
    1001,  1001,  1001,  1473,  1716,  1767,   574,  1185,   642,  -792,
    -792,   696,  -382,   642,   696,  2416,  1474,  1000,  1016,  1626,
    1186,  1753,  2292,   822,   963,  1559,   580,   402,  1560,  1561,
    1562,  1481,   583,   834,  2293,   403,  2294,  2295,  2449,  2296,
    1233,  1563,  2297,   989,   989,   989,   989,   989,   985,  1245,
    1251,  1257,   986,  1679,  1261,  1681,  2102,  1487,  1538,   638,
     639,  1546,  1470,   974,  2396,   584,   987,  1264,   841,  1552,
    1488,  1539,  1192,  1406,  1547,  1548,  1198,  1645,  1647,  1649,
     988,  1204,  1553,  1289,  1208,  1210,   881,   882,   883,   884,
     886,   633,   944,  1266,  1304,  1445,   945,   607,   585,  1378,
     586,  1557,  1574,   946,   947,  1301,   664,   665,  1290,   948,
     949,   950,   951,   952,  1558,   953,   954,   955,   956,   957,
     958,  1564,   959,   591,   960,   961,  1572,  2114,  2115,   592,
    1379,  1653,  2259,  2298,  1565,  2299,  2433,  1239,   593,  1573,
    1296,  1593,  1297,  1580,  1654,   938,   940,   941,  -230,   942,
    1657,  2094,  2095,  2096,  2097,  2098,   943,  2114,  2115,  2431,
     985,  1586,  1046,   597,   986,  1340,  1665,  2362,  -383,   656,
     822,   638,   639,  1241,  1380,  1659,  1636,  1383,  1662,  1666,
     966,  2090,  2209,  1241,  2210,   648,   749,  2066,   669,  1669,
    2064,  2065,   988,  2211,  1387,  2212,  1695,  1989,  2075,  2076,
     684,  1748,  1670,   725,  1495,   657,  1239,   658,  1399,  1696,
    1239,   659,  1021,   705,  1749,   787,  1239,  1449,  1633,  1751,
    1655,  1935,   795,  1956,   642,  2504,  1997,  1031,  2271,  1962,
    1963,  1487,  2001,  1663,  2132,  2027,  1785,  1919,  1420,  1998,
    2137,  -387,  1667,   728,  1999,  2002,   904,  2503,  2028,  -385,
    2047,  2091,  2111,   969,  1446,  1052,  1685,   660,  -587,   753,
    2142,   969,  1068,  2048,  2092,  2112,  1241,   661,  1072,  1073,
    2131,  1914,  1403,  2143,   766,  1532,  1001,  1001,  1001,  1335,
    1335,  1335,  1335,  1335,  1532,   662,   965,   663,  2147,   421,
    1682,  -852,  1532,  1583,  1584,   985,  2206,  2148,  1469,   986,
    1495,   666,  1495,  1532,  1704,  2149,   638,   639,   402,  2207,
    2223,  1351,   675,   987,  1130,   856,  2208,   858,  2213,  2214,
     677,  2215,  2216,  2224,   859,   860,   861,   988,   676,  1748,
     864,   865,   866,   867,  1487,   868,   869,  1853,  1854,   646,
     691,  1499,  2225,  1762,   654,   699,   944,  2226,  1748,   711,
     945,  1756,  2217,  1925,  1647,  1649,   714,   946,   947,   717,
    1763,  2227,  1470,   948,   949,   726,   951,  1470,  1635,   953,
     954,   955,   956,   957,  1638,  2228,   959,  2230,   960,   961,
    2229,   732,  2240,   745,  1530,   750,  1470,  1188,  1189,   642,
    2231,  2045,  2046,   642,  2218,  2241,  1540,  1541,  1542,  2242,
     642,  2209,   760,  2210,  1785,   995,   763,  2219,   770,  1511,
     771,  1470,  2211,  2248,  2212,  1514,  2285,  1786,  1922,  1517,
    1566,  1519,  1517,  1517,  2243,  1521,  2249,  1523,   774,  2286,
     777,  1272,  2245,  2246,  1764,  1532,  1174,  1765,  1575,   900,
    1674,  1787,  1766,  2016,   778,  2018,  2019,  1487,  2288,  2309,
    1767,  1788,   782,  1532,  2114,  2115,  1789,  2023,  2353,   784,
    2311,   786,  1585,  2353,  2367,  2271,  2343,  2030,   696,  2032,
     897,  2354,  1301,   790,  1273,   791,  2355,  2368,  1880,  1882,
    1309,  1310,  1311,  1312,  1313,  1314,  1315,  1316,  1317,  1318,
    1319,  1320,  1321,  1322,  1323,  1324,  1325,  1326,  1327,  1359,
    1360,  2315,   818,  2400,   826,   822,  1275,  2391,  2122,  2111,
    1343,  1762,  2316,  2317,  2426,  1955,  2401,  1350,  2318,  1352,
    2319,  1929,  2418,  1239,  2264,  2265,  2266,  2427,  1763,  2320,
    1276,  2321,  1277,  2322,  1362,  1487,  2442,  2213,  2214,  1241,
    2215,  2216,  2447,  1651,  1449,  1487,  1748,   851,  2445,  2478,
     852,   642,   605,   606,  1592,  2448,  2223,  2470,  2472,  2474,
     886,   855,  2479,  1241,  2223,  1278,  1393,  2350,   857,  2510,
    1279,  2217,  1837, -1190,  1280,  2519,   876,  2511,  1281,  2370,
    2521,  1282,  1052,  1971,   877,  1786,  1677,  1677,  2520,  1677,
    1926,  1927,  1928,  2522,  1596,  1597,  1598,   708,  1449,  1075,
     880,  1075,  1764,  1283,  1689,  1765,  1599,   887,  1159,  1787,
     888,  2524,   989,  2218,  1487,  1600,  1664,  1284,  1174,  1788,
     643,  1239,  2353,  1285,  1789,   909,  2219,  2525,  1226,  1226,
     889,  2526,   890,  2353,  2541,  2542,  1712,  2547,  1601,   787,
    2567,  1728,   891,  1602,   912,  1144,  2543,  2573,  2574,   894,
    2548,  1726,   985,  2568,   966,  2323,   986,  1917,  2324,  1629,
    1754,   696,  1755,   638,   639,  2483,  2484,  1462,  1358,   936,
     987,  1603,  2583,   558,   559,   560,   561,   562,  1758,   965,
     967,  1933,  1206,  1207,   988,  1476,   968,  1827,   969,   972,
     983,  1722,   993,  1939,  1048,  1013,  1049,  1941,  1233,   607,
     807,  1022,  1024,   608,   609,   610,   611,  1026,   886,  1497,
     834,  1028,  1245,  1035,  1041,  2528,   612,  1251,   985,  1045,
     405,  1104,   986,   613,  1257,   614,  1066,  1069,  1604,   638,
     639,  1071,  1261,   963,  1477,  1087,   987,   642,   642,   642,
     642,   642,  1127,  2325,  1868,   995,   995,  1342,  1088,  1861,
     988,  1089,   607,   615,  1094,  1102,   608,   609,   610,   611,
    1131,  1157,  2393,  1168,  1171,  1172,  1181,  1187,  2133,   612,
    1195,  1201,   787,  1184,  1224,  1543,   613,   651,   614,  1890,
    1605,   616,  1216,  2135,  1239,  1260,  1269,   637,   638,   639,
     594,  1308,  1901,  1305,   945,   787,  1329,  2122,  1330,   612,
    -342,  2326,  2327,  2328,   986,  1361,   615,  1367,   614,  1368,
    1390,  1689,  1398,  1388,  1689,  1689,  1689,  1412,   421,  1413,
    1429,  1414,  1415,  1416,  1930,  1417,  1426,  1432,  1438,  1591,
    -935,  1447,  2179,  1431,   616,  1434,   615,  1098,   617,  2434,
    1495,  1624,  1448,  1453,  1459,  1454,  1458,  1472,  1348,  1485,
    1411,  1466,  1349,   607,  1482,  1486,   708,   608,   609,   610,
     611,  1419,  1489,  1492,   616,  1490,  1491,  1500,    25,  1631,
     612,   421,  1502,    29,  1509,  1524,  1964,   613,  1512,   614,
    1525,  1637,  1526,  1527,  1528,  1529,  1532,  1533,  1577,   915,
     618,   617,  1534,  1623,  1082,    47,    48,  1582,  1620,  2481,
    1628,   640,  1629,  1634,   373,    25,  1639,   615,   978,  1642,
      29,  2312,  1643,  1644,   403,  1652,  1658,   978,  1675,  1684,
    1444,   617,  1697,  1692,  2502,  1098,  1698,  1699,  2008,  2009,
    1660,  1031,    47,    48,  1700,   616,  1711,  1723,  1732,  1716,
    1750,  1834,  1840,   618,  1867,  1672,  1835,  1836,  1841,    90,
    1842,  1844,  1680,  2026,  1483,  1845,  1846,  1848,  1849,    95,
    1850,  1851,   886,  1855,  1856,  1857,   565,  1858,  1690,  1859,
    1693,  1865,   421,   618,  1883,  1884,  1885,  2502,  1893,  1886,
    1887,  1909,  1896,   886,  1911,  1914,    90,  1934,  1510,  1915,
    1936,  1947,   617,  1948,  1513,  1951,    95,  1708,  1516,  1949,
    1518,  -494,  2059,  1965,  1520,   121,  1522,   607,  1720,  1972,
    1966,   608,   609,   610,   611,  2072,  2385,  2386,  1973,  2078,
    1974,  1734,  1991,  1992,   612,  1994,  1740,  1995,  1746,  1589,
    1996,   613,  2003,   614,   607,  2006,  1090,  2007,   608,   609,
     610,   611,   121,  2269,   618,  1461,  1753,  1752,   607,   150,
    2013,   612,   608,   609,   610,   611,  2014,  2017,   613,  2020,
     614,   615,  2029,  2113,  2031,   612,  2033,  2042,  2105,  1901,
    1901,  1901,   613,   944,   614,   566,  2043,   945,  2044,  2049,
    2050,  1901,  2051,  2054,   946,   947,   150,  2052,   615,   616,
     948,   949,  2070,   951,  2071,  2080,   953,   954,   955,   956,
     957,  2085,   615,  2093,  -381,   960,  2430,  2432,   684,  2369,
    2100,  2101,  2145,   995,  2121,  2118,   616,  2125,  2124,  2253,
    2126,  2127,  2128,  2129,  2130,  2140,   421,  2146,  1874,  2256,
     616,   944,  2182,  1709,  2160,   945,   607,  2162,  2204,  2164,
    2205,  2233,   946,   947,  2247,  2250,   617,  1710,   948,   949,
     950,   951,  2251,   421,   953,   954,   955,   956,   957,   958,
    -224,   959,  1726,   960,   961,  2258,  1930,   421,  2168,  1918,
     822,  1343,  2275,   617,  2283,  2289,  2302,  2303,  2178,  2305,
    2188,  2304,  2306,  2308,  -843,  2183,  1758,   617,  2310,  2194,
     978,  2330,  2352,  2344,  2336,  2347,  2358,  2348,   618,  2359,
    1938,  2372,  2371,  1940,  2361,   748,  2365,  1827,  2379,  2381,
    2382,  1944,  2383,  2384,  2387,  2388,  2389,  1714,  1683,  2395,
    2399,  2463,  2404,  2403,  2410,   618,  2412,  2415,  2417,  2443,
    2446,  2461,    13,    14,   748,    15,    16,  2454,  2469,   618,
      20,   748,  2471,  1967,  2489,  1970,  2477,  2473,    23,   808,
    2486,  2482,  2491,    27,  2498,  2499,    30,  2509,  2490,  2531,
    1761,  2512,  2539,  2564,    37,  2514,    38,  1718,    40,  2515,
    2523,  2549,  2554,  2545,  1890,  2569,  1226,  1226,  1226,  2570,
    1226,  1226,  2571,  1901,  1832,  1901,  1901,  1901,  1901,  1901,
    1901,    59,  2582,   799,   638,   639,  1265,   578,   787,  1576,
    2165,  2086,    70,  2281,  1866,   612,  1442,  1775,   917,  1505,
    1780,  1869,  2360,  1800,   614,  1813,  2087,  1819,   929,  1823,
    1595,   926,  1042,  1302,  1894,  2363,    85,  2261,  2099,  1303,
    2254,  2107,  1739,  1480,  1990,  1721,  1048,  2301,  2163,    93,
    2314,   607,   615,  1391,  1408,   608,   609,   610,   611,  1671,
    1656,  1437,  1957,   815,  2009,  1958,   436,   102,   612,  2581,
     925,  2123,  2136,   104,  1043,   613,  1661,   614,   600,   601,
     616,   108,  1147,   110,   602,   112,   788,   114,   603,  1691,
    1397,  1920,  1921,  2349,   119,  1423,  1364,  2068,  1176,  1451,
     642,   995,  1006,  1498,  2077,   615,  1008,  2392,  2081,  2082,
    2072,   130,   131,  1741,  1009,  1673,  2089,   640,   607,  1676,
    2280,  2534,   608,   609,   610,   611,  1010,  2420,  2166,   143,
    1382,  1381,  1011,   616,   748,   612,   604,   617,  1155,  2516,
    1901,  1436,   613,  1401,   614,  2134,  2465,  1392,  2493,  2161,
     155,   761,  1139,   156,  2429,  2535,  2553,  2120,  2577,  2575,
    1226,  1226,  1226,  1631,  2398,  2173,  1738,  2004,  1015,   845,
     421,  1703,   615,  1095,   687,  2138,  1833,   827,   827,   735,
    2174,  2374,  2035,  2022,  2039,  2041,  2025,  2168,  2139,   618,
     617,  2037,  2193,  2191,  1531,  2402,  1232,  2144,  2201,  2409,
     616,  1873,  2346,  1847,  1843,  1852,   607,  1860,  2356,     0,
     608,   609,   610,   611,  2154,  2155,     0,     0,     0,     0,
       0,     0,     0,   612,     0,     0,  1078,     0,     0,     0,
     613,     0,   614,     0,     0,     0,     0,   421,     0,     0,
       0,     0,   618,  1734,  1097,     0,     0,     0,     0,     0,
       0,   834,     0,  2422,     0,     0,     0,   617,  1129,     0,
     615,  2185,     0,  1096,  1096,  2444,  1135,  1136,     0,     0,
       0,  1142,     0,     0,  2452,     0,     0,  2195,  2021,  2196,
    2456,     0,  2457,  2024,     0,     0,     0,     0,   616,     0,
       0,     0,     0,     0,     0,     0,  2034,  2458,     0,  1097,
       0,     0,   787,  1226,     0,     0,     0,  1169,     0,   618,
       0,     0,   787,     0,     0,     0,  2059,  2059,  2036,    13,
      14,     0,    15,    16,  2038,   421,     0,    20,  2040,     0,
       0,     0,     0,     0,     0,    23,  1191,     0,  1194,     0,
      27,     0,     0,    30,     0,   617,     0,     0,     0,     0,
       0,    37,     0,    38,     0,    40,     0,   748,   748,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  1240,  1142,
       0,  2268,     0,  2458,  2088,  2517,  2270,     0,    59,   607,
       0,     0,     0,   608,   609,   610,   611,  2279,  1944,    70,
       0,     0,  2284,     0,     0,   787,   612,   618,     0,     0,
       0,   787,  2287,   613,     0,   614,     0,     0,  2300,     0,
       0,     0,     0,    85,     0,     0,     0,     0,     0,  2555,
       0,     0,     0,     0,  2556,     0,    93,     0,     0,     0,
       0,     0,     0,   615,  1226,     0,     0,  2556,     0,  2576,
    2579,     0,     0,     0,   102,     0,     0,     0,  1334,  1334,
     104,  2345,  2579,     0,     0,     0,     0,     0,   108,     0,
     110,   616,   112,     0,   114,     0,     0,     0,     0,     0,
       0,   119,     0,     0,   748,   748,     0,     0,  2351,     0,
       0,     0,     0,     0,     0,  2375,     0,     0,   130,   131,
     607,     0,     0,  2357,   608,   609,   610,   611,   421,  1591,
       0,  1240,     0,     0,     0,     0,   143,   612,     0,  2175,
       0,     0,     0,  1226,   613,     0,   614,     0,   617,     0,
       0,     0,     0,  2186,  1402,     0,     0,   155,  2189,     0,
     156,  2190,     0,     0,     0,  2376,     0,     0,     0,     0,
       0,  2197,     0,  2198,   615,  2199,     0,  2200,     0,     0,
     886,     0,     0,     0,     0,   976,     0,     0,     0,   977,
       0,     0,   748,   798,     0,   799,   638,   639,     0,     0,
     618,   962,   616,   799,   638,   639,     0,   612,     0,   635,
       0,     0,     0,   636,     0,   612,   614,     0,     0,   637,
     638,   639,     0,     0,   614,     0,     0,  1226,     0,     0,
       0,   612,     0,     0,     0,     0,     0,     0,     0,   421,
     614,     0,  2419,  2270,   615,     0,  2421,     0,  2425,     0,
       0,     0,   615,     0,     0,     0,     0,     0,     0,   617,
       0,     0,     0,  2436,  2437,     0,  2441,   944,   615,     0,
       0,   945,   616,     0,     0,     0,  2451,     0,   946,   947,
     616,     0,  1097,     0,  2455,   949,     0,   951,     0,     0,
     953,   954,   955,   956,   957,     0,   616,     0,   787,   960,
       0,     0,  2376,     0,     0,     0,     0,     0,     0,   640,
       0,   618,     0,     0,     0,     0,  1461,   640,     0,   607,
       0,     0,   787,   608,   609,   610,   611,     0,     0,   617,
    2487,     0,     0,   640,     0,     0,   612,   617,  2492,  2494,
       0,     0,     0,   613,     0,   614,   978,     0,     0,   834,
       0,  1123,  2506,   617,     0,   962,  2508,     0,   833,     0,
       0,   607,     0,     0,     0,   608,   609,   610,   611,     0,
       0,     0,     0,   615,     0,     0,     0,     0,   612,     0,
       0,   618,     0,     0,     0,   613,   944,   614,   886,   618,
     945,     0,     0,     0,     0,  2494,     0,   946,   947,  2536,
       0,   616,   962,  2540,   949,   618, -1191,  1746,  1594, -1191,
   -1191, -1191, -1191, -1191,   962,   615,     0,     0,   960,     0,
       0,  1746,  2552,  2536,     0,     0,     0,   833,     0,  2176,
     607,     0,     0,     0,   608,   609,   610,   611,   421,  2405,
    1226,     0,     0,   616,     0,     0,     0,   612,     0,     0,
       0,     0,     0,     0,   613,     0,   614,     0,   617,     0,
       0,   962,   962,   962,   962,     0,   962,     0,     0,     0,
       0,     0,   748,     0,     0,   978,     0,     0,     0,     0,
     421,     0,     0,     0,   615,     0,     0,  1030,     0,     0,
     607,  1226,     0,     0,   608,   609,   610,   611,     0,     0,
     617,     0,     0,     0,     0,     0,     0,   612,     0,     0,
     618,     0,   616,     0,   613,     0,   614,     0,   962,     0,
     962,   962,   962,   962,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  1078,     0,
    1078,     0,     0,     0,   615,     0,  2488,     0,     0,   421,
       0,     0,   618,     0,     0,     0,     0,     0,     0,  1097,
    1097,     0,     0,     0,     0,     0,     0,     0,     0,   617,
    1705,     0,   616,     0,     0,     0,   748,     0,  1715,   962,
       0,  1717,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   808,     0,     0,     0,  1097,  1097,     0,
       0,   962,     0,     0,     0,     0,     0,     0,     0,   421,
       0,   962,   375,     0,     0,     0,     0,     0,   381,   748,
    1272,   618,     0,     0,     0,     0,  1782,     0,   388,   617,
       0,   390,   962,     0,   393,   748,   748,   748,     0,   748,
     748,   399,     0,   748,     0,   406,     0,     0,   962,   409,
       0,     0,   962,   962,     0,     0,     0,  1240,     0,     0,
       0,     0,     0,  1273,     0,     0,     0,   428,     0,  1274,
       0,   432,   433,     0,     0,     0,     0,   438,   439,     0,
       0,   618,     0,   444,   445,     0,   447,   448,   449,   450,
       0,   451,     0,     0,     0,  1275,     0,     0,     0,     0,
     460,     0,     0,     0,     0,   464,     0,   466,     0,     0,
     962,   469,     0,     0,     0,   473,     0,   475,  1892,  1276,
       0,  1277,     0,     0,   481,     0,     0,     0,   485,     0,
       0,     0,   488,     0,   490,     0,     0,     0,     0,     0,
       0,   498,   500,     0,     0,   502,   503,     0,     0,     0,
     748,   509,     0,   510,  1278,     0,     0,   514,     0,  1279,
       0,     0,     0,  1280,     0,     0,     0,  1281,   962,   962,
    1282,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   541,     0,   543,  1461,     0,     0,
     607,     0,  1283,   548,   608,   609,   610,   611,     0,     0,
       0,     0,     0,     0,     0,     0,  1284,   612,     0,   748,
     748,   748,  1285,     0,   613,     0,   614,     0,     0,     0,
       0,     0,     0,  1707,     0,     0,   607,     0,     0,     0,
     608,   609,   610,   611,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   612,   615,     0,     0,     0,     0,     0,
     613,     0,   614,     0,     0,     0,     0,     0,  1733,  1097,
       0,   607,     0,     0,     0,   608,   609,   610,   611,     0,
       0,     0,   616,     0,     0,     0,     0,     0,   612,     0,
     615,     0,     0,     0,     0,   613,     0,   614,     0,   962,
     962,   962,   962,   962,   962,   962,   962,   962,   962,   962,
     962,   962,   962,   962,   962,   962,   962,   962,   616,   421,
       0,     0,     0,     0,     0,   615,     0,  1969,     0,     0,
     607,     0,     0,   962,   608,   609,   610,   611,     0,   617,
     962,     0,   962,     0,     0,     0,     0,   612,     0,     0,
       0,     0,   962,   616,   613,   421,   614,     0,     0,     0,
       0,  1240,   748,     0,     0,  1097,  1097,  1097,     0,  1142,
    1142,  1240,     0,     0,     0,   617,     0,  1142,  1142,     0,
       0,     0,     0,   962,   615,     0,     0,     0,     0,     0,
     421,   618,   607,     0,     0,     0,   608,   998,   610,   611,
       0,     0,     0,     0,  1334,  1334,  1334,  1334,  1334,   612,
     617,   944,   616,     0,   720,   945,   613,     0,   614,     0,
       0,     0,   946,   947,     0,     0,     0,   618,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,     0,   960,   961,     0,   615,     0,     0,   421,
       0,     0,   962,  1105,  1240,     0,     0,  1106,   607,     0,
    1402,     0,   618,     0,  1107,  1108,   962,     0,     0,   617,
    1109,  1110,  1111,  1112,   616,     0,  1113,  1114,  1115,  1116,
    1117,  1118,  1119,  1120,     0,  1121,  1122,   962,     0,     0,
       0,     0,     0,   748,  2158,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   421,     0,     0,     0,     0,   808,     0,   847,     0,
       0,   618,     0,     0,     0,     0,     0,  1097,     0,     0,
       0,   617,   944,   962,  1173,     0,   945,   607,     0,     0,
       0,     0,     0,   946,   947,     0,     0,     0,  2192,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,     0,   960,   961,     0,     0,     0,     0,
       0,     0,   748,  2203,   944,     0,     0,  1236,   945,   607,
       0,   962,     0,   618,     0,   946,   947,     0,     0,     0,
       0,   948,   949,   950,   951,     0,     0,   953,   954,   955,
     956,   957,   958,     0,   959,     0,   960,   961,     0,     0,
       0,     0,     0,   944,   962,     0,  1237,   945,   607,     0,
       0,   962,     0,  1892,   946,   947,     0,   962,     0,     0,
     948,   949,   950,   951,     0,     0,   953,   954,   955,   956,
     957,   958,     0,   959,     0,   960,   961,     0,     0,     0,
     962,     0,     0,  2274,     0,     0,   748,     0,  2277,     0,
       0,     0,   962,     0,     0,     0,     0,     0,     0,     0,
     962,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     962,     0,     0,   962,     0,     0,     0,     0,   944,     0,
       0,  1238,   945,   607,     0,     0,     0,     0,   962,   946,
     947,     0,     0,     0,     0,   948,   949,   950,   951,     0,
     962,   953,   954,   955,   956,   957,   958,     0,   959,     0,
     960,   961,     0,     0,   962,     0,     0,  1240,     0,   944,
     962,     0,     0,   945,   607,     0,   962,     0,     0,     0,
     946,   947,     0,     0,     0,  1307,   948,   949,   950,   951,
       0,  1240,   953,   954,   955,   956,   957,   958,     0,   959,
     944,   960,   961,     0,   945,   607,     0,     0,     0,     0,
       0,   946,   947,     0,     0,     0,  1386,   948,   949,   950,
     951,     0,     0,   953,   954,   955,   956,   957,   958,     0,
     959,     0,   960,   961,     0,     0,   944,     0,  1394,     0,
     945,   607,     0,     0,     0,     0,     0,   946,   947,     0,
       0,     0,     0,   948,   949,   950,   951,     0,     0,   953,
     954,   955,   956,   957,   958,     0,   959,  1097,   960,   961,
     944,  1097,  1409,     0,   945,   607,     0,     0,     0,     0,
       0,   946,   947,     0,     0,     0,     0,   948,   949,   950,
     951,     0,     0,   953,   954,   955,   956,   957,   958,     0,
     959,   944,   960,   961,   962,   945,   607,     0,   748,   748,
       0,     0,   946,   947,     0,     0,     0,  1421,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,     0,   960,   961,     0,     0,     0,     0,     0,
       0,     0,  1097,  1097,  1097,  1097,     0,     0,   962,     0,
    2158,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     748,   944,     0,  1291,     0,   945,   607,     0,   962,     0,
     962,     0,   946,   947,   962,     0,     0,  1424,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,     0,   960,   961,     0,     0,   962,     0,     0,
     962,     0,     0,     0,  1097,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  1123,     0,     0,     0,     0,     0,     0,  1097,
    1097,     0,  1097,     0,     0,     0,     0,     0,     0,  2158,
       0,   944,     0,     0,     0,   945,   607,     0,     0,     0,
       0,     0,   946,   947,     0,     0,     0,  1425,   948,   949,
     950,   951,     0,  1371,   953,   954,   955,   956,   957,   958,
       0,   959,     0,   960,   961,     0,   944,     0,     0,     0,
     945,   607,  1097,     0,     0,     0,     0,   946,   947,     0,
       0,     0,  1460,   948,   949,   950,   951,     0,   962,   953,
     954,   955,   956,   957,   958,     0,   959,   962,   960,   961,
     944,   962,   962,     0,   945,   607,     0,     0,     0,   962,
       0,   946,   947,     0,     0,     0,  1507,   948,   949,   950,
     951,     0,     0,   953,   954,   955,   956,   957,   958,     0,
     959,     0,   960,   961,     0,     0,     0,     0,     0,     0,
     962,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   962,
       0,   944,     0,     0,   962,   945,   607,     0,     0,     0,
       0,     0,   946,   947,   962,   962,     0,  1508,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,     0,   960,   961,     0,     0,     0,     0,     0,
       0,     0,   944,     0,     0,   962,   945,   607,     0,     0,
       0,     0,     0,   946,   947,   962,   962,     0,  1641,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,   944,   960,   961,     0,   945,   607,     0,
       0,     0,     0,     0,   946,   947,     0,     0,     0,  1912,
     948,   949,   950,   951,     0,     0,   953,   954,   955,   956,
     957,   958,     0,   959,     0,   960,   961,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   962,     0,
     962,     0,     0,     0,     0,     0,     0,     0,   944,   962,
    1709,     0,   945,   607,   962,     0,     0,   962,     0,   946,
     947,     0,     0,     0,  1581,   948,   949,   950,   951,     0,
     962,   953,   954,   955,   956,   957,   958,     0,   959,   944,
     960,   961,     0,   945,   607,     0,     0,     0,     0,     0,
     946,   947,     0,     0,     0,  1950,   948,   949,   950,   951,
       0,     0,   953,   954,   955,   956,   957,   958,     0,   959,
       0,   960,   961,     0,     0,   962,     0,     0,     0,     0,
       0,   962,     0,     0,   944,     0,  1968,   962,   945,   607,
       0,     0,     0,     0,     0,   946,   947,     0,     0,     0,
       0,   948,   949,   950,   951,     0,   962,   953,   954,   955,
     956,   957,   958,     0,   959,   944,   960,   961,     0,   945,
     607,     0,     0,     0,     0,     0,   946,   947,     0,     0,
       0,  2000,   948,   949,   950,   951,     0,     0,   953,   954,
     955,   956,   957,   958,     0,   959,     0,   960,   961,   962,
       0,   962,     0,     0,     0,   962,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   962,   962,     0,     0,
       0,   962,   944,     0,  2141,     0,   945,   607,     0,     0,
       0,   962,     0,   946,   947,   962,     0,     0,     0,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,     0,   960,   961,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   962,     0,     0,
       0,     0,   962,     0,   962,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   962,     0,   962,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   377,
     378,   379,   380,     0,   382,     0,   383,     0,   385,   386,
       0,   387,     0,     0,     0,   389,   962,   391,   392,     0,
     962,   394,   395,   396,     0,   398,     0,   400,   401,     0,
       0,   408,   962,     0,     0,     0,   411,   412,   413,   414,
     415,   416,   417,   418,     0,   419,   420,     0,     0,   425,
     426,   427,     0,   429,     0,   431,     0,     0,   434,   435,
       0,     0,     0,     0,     0,     0,   442,   443,     0,     0,
     446,     0,     0,     0,     0,     0,     0,   452,     0,   454,
       0,   455,   456,     0,     0,     0,   461,   462,   463,     0,
       0,   465,     0,     0,   467,   468,     0,   470,   471,   472,
       0,     0,     0,     0,   477,   478,     0,     0,   480,     0,
     482,   483,   484,     0,   486,   487,     0,     0,   489,     0,
     491,   492,   493,   494,   495,   496,     0,     0,     0,     0,
       0,     0,   504,   505,   506,     0,     0,     0,     0,   511,
     512,   513,     0,   515,   516,   517,   518,   519,   520,   521,
     522,   523,   524,   525,   526,   527,   528,   529,   530,   531,
     532,   533,   534,   535,   536,   537,   538,   539,   540,     0,
     542,     0,   544,     0,   545,     0,   546,   547,     0,   549,
     550,   551,   552,   553,   554,   555,   556,   944,     0,  2156,
       9,   945,   607,     0,     0,     0,     0,    10,   946,   947,
       0,     0,     0,     0,   948,   949,   950,   951,     0,     0,
     953,   954,   955,   956,   957,   958,     0,   959,     0,   960,
     961,     0,     0,     0,     0,     0,     0,    11,    12,    13,
      14,     0,    15,    16,    17,    18,    19,    20,     0,   634,
      21,    22,   647,     0,   650,    23,    24,    25,     0,    26,
      27,    28,    29,    30,    31,     0,    32,    33,    34,    35,
      36,    37,     0,    38,    39,    40,    41,    42,    43,     0,
       0,    44,    45,    46,    47,    48,     0,     0,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
       0,    71,     0,    72,    73,     0,    74,    75,    76,     0,
       0,    77,     0,     0,    78,    79,     0,    80,    81,    82,
      83,     0,    84,    85,    86,    87,    88,    89,    90,    91,
      92,     0,     0,     0,     0,     0,    93,    94,    95,    96,
       0,     0,     0,     0,    97,     0,     0,    98,    99,     0,
       0,   100,   101,     0,   102,     0,     0,     0,   103,     0,
     104,     0,   105,     0,     0,     0,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,     0,   116,   117,   118,
       0,   119,     0,   120,   121,     0,   122,     0,   123,   124,
     125,   126,     0,     0,   127,   128,   129,     0,   130,   131,
     132,     0,   133,   134,   135,     0,   136,     0,   137,   138,
     139,   140,   141,     0,   142,     0,   143,   144,     0,     0,
     145,   146,   147,     0,     0,   148,   149,     0,   150,   151,
       0,   152,   153,     0,     0,     0,   154,   155,     0,     0,
     156,     0,     0,   157,     0,     0,     0,   158,   159,     0,
       0,   160,   161,   162,     0,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,     0,   174,     0,     0,
     175,     0,     0,     0,   176,   177,   178,   179,   180,     0,
     181,   182,     0,     0,   183,   184,   185,   186,     0,     0,
       0,     0,   187,   188,   189,   190,   191,   192,     0,     0,
       0,     0,     0,     0,     0,   193,     0,     0,   194,   195,
     196,   197,   198,     0,     0,     0,   199,   200,   201,   202,
     203,   204,   944,   205,   206,  2232,   945,   607,   207,     0,
       0,     0,     0,   946,   947,     0,     0,     0,     0,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,     0,   960,   961,     0,     0,     0,     0,
       0,     0,   944,     0,     0,     0,   945,   607,     0,     0,
       0,     0,     0,   946,   947,     0,     0,     0,  2244,   948,
     949,   950,   951,   934,   935,   953,   954,   955,   956,   957,
     958,     0,   959,   944,   960,   961,     0,   945,   607,     0,
       0,     0,     0,     0,   946,   947,     0,     0,     0,  2252,
     948,   949,   950,   951,     0,     0,   953,   954,   955,   956,
     957,   958,     0,   959,   944,   960,   961,     0,   945,   607,
       0,     0,     0,     0,     0,   946,   947,     0,     0,     0,
    2267,   948,   949,   950,   951,     0,     0,   953,   954,   955,
     956,   957,   958,     0,   959,   944,   960,   961,  2278,   945,
     607,     0,     0,     0,     0,     0,   946,   947,     0,     0,
       0,     0,   948,   949,   950,   951,     0,     0,   953,   954,
     955,   956,   957,   958,     0,   959,   944,   960,   961,     0,
     945,   607,     0,     0,     0,     0,     0,   946,   947,     0,
       0,     0,  2282,   948,   949,   950,   951,     0,     0,   953,
     954,   955,   956,   957,   958,     0,   959,   944,   960,   961,
       0,   945,   607,     0,     0,     0,     0,     0,   946,   947,
       0,     0,     0,  2341,   948,   949,   950,   951,     0,     0,
     953,   954,   955,   956,   957,   958,     0,   959,   944,   960,
     961,     0,   945,   607,     0,     0,     0,     0,     0,   946,
     947,     0,     0,     0,  2342,   948,   949,   950,   951,     0,
       0,   953,   954,   955,   956,   957,   958,     0,   959,   944,
     960,   961,  2366,   945,   607,     0,     0,     0,     0,     0,
     946,   947,     0,     0,     0,     0,   948,   949,   950,   951,
       0,     0,   953,   954,   955,   956,   957,   958,     0,   959,
     944,   960,   961,     0,   945,   607,     0,     0,     0,     0,
       0,   946,   947,     0,     0,     0,  2373,   948,   949,   950,
     951,     0,     0,   953,   954,   955,   956,   957,   958,     0,
     959,   944,   960,   961,  2390,   945,   607,     0,     0,     0,
       0,     0,   946,   947,     0,     0,     0,     0,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,   944,   960,   961,  2408,   945,   607,     0,     0,
       0,     0,     0,   946,   947,     0,     0,     0,     0,   948,
     949,   950,   951,     0,     0,   953,   954,   955,   956,   957,
     958,     0,   959,   944,   960,   961,     0,   945,   607,     0,
       0,     0,     0,     0,   946,   947,     0,     0,     0,  2414,
     948,   949,   950,   951,     0,     0,   953,   954,   955,   956,
     957,   958,     0,   959,   944,   960,   961,  2462,   945,   607,
       0,     0,     0,     0,     0,   946,   947,     0,     0,     0,
       0,   948,   949,   950,   951,     0,     0,   953,   954,   955,
     956,   957,   958,     0,   959,   944,   960,   961,     0,   945,
     607,     0,     0,     0,     0,     0,   946,   947,     0,     0,
       0,  2464,   948,   949,   950,   951,     0,     0,   953,   954,
     955,   956,   957,   958,     0,   959,   944,   960,   961,     0,
     945,   607,     0,     0,     0,     0,     0,   946,   947,     0,
       0,     0,  2475,   948,   949,   950,   951,     0,     0,   953,
     954,   955,   956,   957,   958,     0,   959,   944,   960,   961,
    2476,   945,   607,     0,     0,     0,     0,     0,   946,   947,
       0,     0,     0,     0,   948,   949,   950,   951,     0,     0,
     953,   954,   955,   956,   957,   958,     0,   959,   944,   960,
     961,  2480,   945,   607,     0,     0,     0,     0,     0,   946,
     947,     0,     0,     0,     0,   948,   949,   950,   951,     0,
       0,   953,   954,   955,   956,   957,   958,     0,   959,   944,
     960,   961,     0,   945,   607,     0,     0,     0,     0,     0,
     946,   947,     0,     0,     0,  2485,   948,   949,   950,   951,
       0,     0,   953,   954,   955,   956,   957,   958,     0,   959,
     944,   960,   961,     0,   945,   607,     0,     0,     0,     0,
       0,   946,   947,     0,     0,     0,  2513,   948,   949,   950,
     951,     0,     0,   953,   954,   955,   956,   957,   958,     0,
     959,   944,   960,   961,  2527,   945,   607,     0,     0,     0,
       0,     0,   946,   947,     0,     0,     0,     0,   948,   949,
     950,   951,     0,     0,   953,   954,   955,   956,   957,   958,
       0,   959,     0,   960,   961,   944,     0,  2546,     0,   945,
     607,     0,     0,     0,     0,     0,   946,   947,     0,     0,
       0,     0,   948,   949,   950,   951,     0,     0,   953,   954,
     955,   956,   957,   958,     0,   959,   944,   960,   961,     0,
     945,   607,     0,     0,     0,     0,     0,   946,   947,     0,
       0,     0,     0,   948,   949,   950,   951,     0,     0,   953,
     954,   955,   956,   957,   958,     0,   959,     0,   960,   961
};

static const yytype_int16 yycheck[] =
{
      50,   486,   378,   483,   493,   494,   495,   496,   392,   763,
     887,   375,   541,   630,   887,   750,  1448,  1024,   918,  1131,
     888,   656,   401,   388,    62,    63,   968,   634,   256,  1328,
    1486,    69,   411,   261,   413,  1590,   209,  1620,  2005,   418,
     419,   420,   472,  1596,   467,  1299,   998,   426,  1607,  1482,
     429,   877,   431,  1844,  1064,  1846,    63,   430,  1506,   743,
      98,  1299,    76,  1965,   480,   974,  2071,   258,   118,   433,
     261,   262,   122,  1596,   845,  1147,     7,     0,     1,   655,
     656,  1518,   477,     8,    89,    99,   450,  1361,   845,  1616,
    1518,  1518,   468,    94,  1621,  1898,  1899,  1518,   388,  1518,
     464,  1518,   116,  1518,  2181,   469,    41,  1662,     8,   473,
     609,   475,  1893,   175,     7,   182,     7,   122,   513,   116,
      20,     5,    71,    15,   488,   645,   182,  1698,  1699,  1700,
     144,   191,   673,     8,     5,    47,   500,   169,   502,    15,
       5,    21,     7,  1020,   191,   509,   129,  1020,     8,  1159,
     155,   116,   406,   967,   968,     8,   655,   656,   878,     8,
      20,    15,     8,    15,     5,  1172,  1043,    20,     9,   129,
       3,    20,   129,   129,    58,     8,   208,    23,     8,     5,
     185,    15,  2389,   166,   904,   116,   224,    15,     8,     8,
      20,    37,     8,    15,    21,    21,    37,     8,    23,  1516,
      20,  1518,    86,  1520,  1516,   387,  1518,   637,  1520,   574,
    1522,   641,    37,   218,    15,   172,   116,   129,   648,   584,
     585,   586,   587,   116,   266,   116,   480,   150,   288,   230,
       8,    37,     8,   858,   859,   282,   861,   231,   232,   299,
       8,     8,   126,   237,   472,   287,  1545,   252,   315,   129,
     282,   318,   647,   137,   129,   650,   116,   149,   129,   208,
     257,   323,   318,   116,   129,   177,     8,   116,   809,     8,
    1841,  2478,  2349,    15,   209,   129,   191,   129,   763,   194,
     800,   165,   116,   284,  1184,  1516,   116,  1518,  1516,  1520,
    1518,  1522,  1520,   228,   295,   129,   116,   116,   588,     5,
     116,   129,   129,   129,   129,   116,    64,   129,   724,   674,
     128,  2114,  2115,   692,  1897,     3,    15,    18,     8,  2100,
      15,   149,  1434,   129,     8,    26,   257,     8,   129,   254,
      18,    37,   697,   818,   257,   373,   374,  1896,   116,   157,
     116,   379,   380,   381,   382,     8,   384,   385,   116,   116,
     388,     8,   390,   391,   392,   393,   696,   257,   396,  1597,
    1598,   399,   969,   401,   257,  1639,   257,  1894,   406,   799,
     246,   409,   712,   411,   258,   413,  1813,   116,   751,   417,
     418,   419,   420,  2285,  1293,  1813,  1813,   129,   426,  1845,
     428,   429,  1813,   431,  1813,   811,  1813,   257,  1813,   437,
     438,  1972,  1086,   798,   257,   800,   444,   445,   257,   637,
     417,   449,  1183,   641,  1366,  1367,  1368,  1171,   456,  1426,
     648,   459,   998,   257,  1333,  1182,   116,   257,    64,   794,
     129,     8,   116,   471,   129,   116,   474,   257,   257,   489,
    1447,   257,   480,   481,   482,   483,   257,   485,   486,   147,
     148,   633,     8,   116,   492,   493,   494,   495,   496,   116,
     498,  1078,   982,   501,   984,   503,   504,   840,  1103,   507,
      15,   991,   510,  1483,   839,    15,   514,   842,  1292,   257,
    2051,   257,   180,  1800,    15,    16,    17,     8,  1800,   257,
     257,     8,   915,   129,   544,   211,  1813,  1406,   548,   998,
       5,  1813,  1819,   843,     9,   543,  2473,  1819,   690,  2514,
    2515,  1823,   191,   149,   854,  1587,  1588,    92,   257,   569,
       7,     8,   560,    23,    15,   241,   576,  1103,  2111,   208,
     568,   871,    37,   571,   572,   573,   574,    37,  1837,   116,
    2093,   579,   580,  2102,   244,   583,   584,   585,   586,   587,
    2006,   565,   566,   591,   592,   985,   986,   987,   988,   989,
     116,  2463,  1995,   577,  2135,   905,  2014,   257,     8,  1800,
    2093,   799,  1800,   257,   149,   148,   257,   251,   769,   919,
       8,   128,  1813,     8,   129,  1813,   759,     8,  1819,    16,
      17,  1819,  1823,  2384,   257,   116,   147,   148,   129,   116,
     257,  2569,  2570,   282,  1103,   315,   285,   180,    95,    37,
     157,   257,    37,  2531,  2582,   665,    37,  2519,   149,   657,
     658,   659,   660,   661,   662,   663,   201,  2545,   666,   180,
     330,  1151,     3,   683,  1888,     8,   674,   124,    15,  1893,
     350,     3,     5,   129,    41,   345,   346,   347,    21,     5,
    1888,    63,    15,     9,   692,    26,    68,   695,   183,   697,
      16,    17,     8,   149,    26,    92,   191,  1007,     3,  1546,
     217,  1306,  1012,  1546,    71,  2258,     8,     5,    90,    91,
     257,    37,   720,   208,     3,   679,    21,    15,   191,    21,
    1642,  1643,  1644,   743,  1448,  1449,    15,  1432,  1524,  1525,
    1526,   257,  1528,  1529,    43,   208,     8,   745,   195,   747,
     760,     8,   750,  1053,     8,   202,     8,     8,    15,    21,
       8,     5,   149,     7,   774,   763,  1635,    21,    67,    21,
    1306,   781,   144,    21,   916,   773,   257,     8,   135,     8,
     257,     8,   154,    82,   782,  1427,   784,  1429,    87,    88,
      21,   789,    21,   791,    21,    94,   794,   985,   986,   987,
     988,   989,     8,   314,   315,  1674,     8,   318,   319,   807,
       8,     8,   169,     3,   201,    21,     8,  2348,   452,   282,
     818,   963,   285,    21,    21,    15,   824,   290,   200,    21,
    1366,  1367,  1368,     8,   191,   298,     8,   847,   256,     7,
       8,   839,     3,   261,   842,  2360,    21,  1306,     8,  1308,
     848,   208,   209,   487,    15,   338,     8,     7,   341,   342,
     343,    21,     8,   497,   221,    15,   223,   224,  2399,   226,
     880,   354,   229,  1353,  1354,  1355,  1356,  1357,     5,   889,
     890,   891,     9,  1415,   894,  1417,  2100,     8,     8,    16,
      17,     8,     8,  1035,  2310,     8,    23,   895,   896,     8,
      21,    21,   856,  1045,    21,    21,   860,  1366,  1367,  1368,
      37,   865,    21,   911,   868,   869,   550,   551,   552,   553,
     554,    15,     5,   897,   934,   282,     9,    10,     8,   228,
       8,     8,  1265,    16,    17,   933,     7,     8,   912,    22,
      23,    24,    25,    26,    21,    28,    29,    30,    31,    32,
      33,     8,    35,     8,    37,    38,     8,    16,    17,     8,
     259,     8,    21,   320,    21,   322,  2382,     8,     8,    21,
     924,  1296,   926,  1273,    21,   609,   610,   611,     7,   613,
      21,  1883,  1884,  1885,  1886,  1887,   620,    16,    17,  2381,
       5,  1291,     8,     7,     9,     8,     8,  2256,     3,     8,
     634,    16,    17,  1840,   303,    21,  1342,  1017,    21,    21,
      15,  1871,    22,  1850,    24,    15,   434,  1850,  1016,     8,
    1848,  1849,    37,    33,  1022,    35,     8,  1722,  1856,  1857,
    1028,     8,    21,   407,  1748,     8,     8,     8,     8,    21,
       8,     8,   676,  1041,    21,   463,     8,     8,  1917,    21,
    1386,    21,   470,    21,   472,  2471,     8,   691,  2130,    21,
      21,     8,     8,  1399,  1933,     8,    84,  1634,  1066,    21,
    1939,     3,  1941,  1071,    21,    21,  1086,  2469,    21,     3,
       8,     8,     8,    15,  1094,   719,  1425,     8,     8,  1087,
       8,    15,   726,    21,    21,    21,  1933,     8,   732,   733,
    1933,    21,  1939,    21,  1102,     8,  1642,  1643,  1644,  1883,
    1884,  1885,  1886,  1887,     8,     8,     3,     8,    21,   129,
    1420,     8,     8,     7,     8,     5,     8,    21,  1138,     9,
    1844,     8,  1846,     8,  1458,    21,    16,    17,     7,    21,
       8,    21,    15,    23,   778,   519,    21,   521,   158,   159,
     149,   161,   162,    21,   528,   529,   530,    37,    15,     8,
     534,   535,   536,   537,     8,   539,   540,   351,   352,   256,
      15,  1181,    21,   191,   261,    15,     5,    21,     8,    15,
       9,  1505,   192,  1642,  1643,  1644,   149,    16,    17,     7,
     208,    21,     8,    22,    23,    15,    25,     8,  1340,    28,
      29,    30,    31,    32,  1346,    21,    35,     8,    37,    38,
      21,    15,     8,    15,  1224,    15,     8,   851,   852,   637,
      21,   316,   317,   641,   234,    21,  1236,  1237,  1238,    21,
     648,    22,     8,    24,    84,   653,    15,   247,    15,  1193,
      15,     8,    33,     8,    35,  1199,     8,   265,  1638,  1203,
    1260,  1205,  1206,  1207,    21,  1209,    21,  1211,    15,    21,
      15,    44,   348,   349,   282,     8,     8,   285,  1266,  1267,
    1412,   289,   290,  1762,    15,  1764,  1765,     8,    21,    21,
     298,   299,     8,     8,    16,    17,   304,  1776,     8,    37,
      21,    15,  1290,     8,     8,  2367,    21,  1786,  1296,  1788,
     124,    21,  1300,    15,    87,    15,    21,    21,  1597,  1598,
     944,   945,   946,   947,   948,   949,   950,   951,   952,   953,
     954,   955,   956,   957,   958,   959,   960,   961,   962,   993,
     994,     5,    15,     8,    15,   969,   119,  2304,  1915,     8,
     974,   191,    16,    17,     8,  1684,    21,   981,    22,   983,
      24,  1651,    21,     8,  2116,  2117,  2118,    21,   208,    33,
     143,    35,   145,    37,   998,     8,    21,   158,   159,  2206,
     161,   162,     8,  1371,     8,     8,     8,    15,    21,     8,
      15,   799,   247,   248,  2253,    21,     8,    21,    21,    21,
    1024,    15,    21,  2230,     8,   178,  1030,  2230,    15,    21,
     183,   192,  1544,    15,   187,     8,    15,    21,   191,  2278,
       8,   194,  1046,  1713,    15,   265,  1414,  1415,    21,  1417,
    1642,  1643,  1644,    21,    15,    16,    17,  1425,     8,  1427,
      15,  1429,   282,   216,  1432,   285,    27,    15,   812,   289,
      15,    21,  1922,   234,     8,    36,  1400,   230,     8,   299,
    1638,     8,     8,   236,   304,     7,   247,    21,   876,   877,
      15,    21,    15,     8,    21,    21,  1464,     8,    59,   887,
       8,  1481,    15,    64,   124,  1473,    21,  2565,  2566,    15,
      21,  1479,     5,    21,    15,   159,     9,  1629,   162,     8,
    1500,  1489,  1502,    16,    17,  2446,  2447,  1131,    21,   239,
      23,    92,    21,   203,   204,   205,   206,   207,  1506,     3,
     246,  1653,   866,   867,    37,  1149,   246,  1527,    15,    21,
      26,  1475,     8,  1665,     5,     7,     7,  1669,  1538,    10,
      15,   240,    15,    14,    15,    16,    17,    15,  1172,  1173,
    1174,     8,  1552,    15,     8,  2512,    27,  1557,     5,    15,
      15,    26,     9,    34,  1564,    36,   175,   149,   149,    16,
      17,     8,  1572,    15,    21,     8,    23,   985,   986,   987,
     988,   989,  1582,   247,  1584,   993,   994,     5,     8,  1577,
      37,    15,    10,    64,     8,     8,    14,    15,    16,    17,
      15,     7,  2306,    62,    15,    15,     8,     8,  1934,    27,
     287,    15,  1020,    21,     8,  1239,    34,     5,    36,  1607,
     201,    92,   314,  1937,     8,     8,   213,    15,    16,    17,
       8,     8,  1620,    15,     9,  1043,    64,  2204,   129,    27,
      21,   305,   306,   307,     9,     8,    64,     8,    36,     8,
      67,  1639,    37,    15,  1642,  1643,  1644,    15,   129,    15,
       8,    15,    15,    15,  1652,    15,    15,     8,   129,  1293,
       7,    15,  2006,  1081,    92,    26,    64,  2381,   149,  2383,
    2384,  1305,     7,    21,    18,    21,    21,    26,     5,    15,
    1054,    37,     9,    10,     7,     7,  1684,    14,    15,    16,
      17,  1065,     8,    26,    92,    15,    15,     8,    63,  1333,
      27,   129,     8,    68,    21,    15,  1704,    34,    21,    36,
      15,  1345,    15,    15,    15,    15,     8,    21,    15,     5,
     201,   149,    21,    62,  1722,    90,    91,    15,    15,  2443,
      21,   129,     8,     7,    15,    63,     8,    64,   166,     8,
      68,  2181,     8,     8,    15,   255,     7,   166,    15,   239,
     208,   149,    16,    21,  2468,  2469,    15,    15,  1756,  1757,
    1394,  1395,    90,    91,    15,    92,    21,     8,     7,   191,
      15,   330,    15,   201,     7,  1409,   315,   331,    15,   144,
      15,    15,  1416,  1781,  1158,    15,    15,    15,    15,   154,
      15,    15,  1426,    15,    15,    15,   124,    15,  1432,    15,
    1434,    15,   129,   201,   246,   246,   246,  2521,     5,   246,
     246,    15,     8,  1447,    15,    21,   144,     8,  1192,    21,
       7,    21,   149,    21,  1198,     8,   154,  1461,  1202,    21,
    1204,    26,  1842,    15,  1208,   200,  1210,    10,  1472,    15,
      21,    14,    15,    16,    17,  1855,  2295,  2296,   173,  1859,
       8,  1485,    15,    15,    27,   255,  1490,     8,  1492,     5,
       7,    34,    21,    36,    10,    15,   231,    15,    14,    15,
      16,    17,   200,     5,   201,     7,   208,    71,    10,   244,
      18,    27,    14,    15,    16,    17,     8,    15,    34,    15,
      36,    64,    15,     7,    15,    27,    15,    21,  1896,  1897,
    1898,  1899,    34,     5,    36,   233,    21,     9,    21,    21,
      21,  1909,    15,    21,    16,    17,   244,    15,    64,    92,
      22,    23,    15,    25,    15,    21,    28,    29,    30,    31,
      32,    21,    64,     8,    15,    37,  2381,  2382,  1936,  2275,
       5,    15,  1952,  1361,    20,     9,    92,    26,    21,  2091,
      21,    21,    21,    21,    15,    21,   129,    15,  1592,  2101,
      92,     5,     8,     7,  1974,     9,    10,    26,    21,    26,
      26,    21,    16,    17,     8,     7,   149,    21,    22,    23,
      24,    25,    21,   129,    28,    29,    30,    31,    32,    33,
      15,    35,  1990,    37,    38,    26,  1994,   129,  1996,  1633,
    1634,  1635,     8,   149,   254,    15,    21,    21,  2006,     8,
    2020,    15,   132,    21,     7,  2013,  2014,   149,     7,  2029,
     166,    21,   353,    21,    37,    15,     7,    15,   201,     7,
    1664,    15,   255,  1667,    21,   434,    21,  2047,    15,    15,
      15,  1675,    15,    15,    15,    15,    15,  1465,  1422,    15,
       7,    15,     8,    21,     8,   201,     8,     7,    21,     8,
       7,    21,    45,    46,   463,    48,    49,     8,     7,   201,
      53,   470,     7,  1707,   352,  1709,    21,    15,    61,   478,
     329,    78,     8,    66,     7,     7,    69,    21,   351,    17,
    1508,    15,    21,   128,    77,    15,    79,  1471,    81,    15,
      15,     8,    15,    17,  2102,     7,  1524,  1525,  1526,     7,
    1528,  1529,    15,  2111,  1532,  2113,  2114,  2115,  2116,  2117,
    2118,   104,     7,    15,    16,    17,   896,   222,  1546,  1267,
    1992,  1868,   115,  2143,  1582,    27,  1090,  1511,   582,  1185,
    1514,  1586,  2253,  1517,    36,  1519,  1869,  1521,   593,  1523,
    1298,   590,   710,   933,  1616,  2258,   139,  2113,  1888,   933,
    2093,  1896,  1489,  1154,  1723,  1473,     5,  2163,  1990,   152,
    2182,    10,    64,  1026,  1046,    14,    15,    16,    17,  1409,
    1388,  1088,  1695,  2181,  2182,  1697,    75,   170,    27,  2571,
     589,  1917,  1939,   176,   711,    34,  1395,    36,   246,   246,
      92,   184,   792,   186,   246,   188,   463,   190,   246,  1432,
    1035,  1635,  1638,  2223,   197,  1071,   998,  1851,   838,  1102,
    1638,  1639,   657,  1174,  1858,    64,   659,  2305,  1862,  1863,
    2240,   214,   215,     5,   660,  1411,  1870,   129,    10,  1413,
    2142,  2519,    14,    15,    16,    17,   661,  2367,  1994,   232,
    1016,  1014,   662,    92,   653,    27,   239,   149,   807,  2491,
    2258,  1087,    34,  1041,    36,  1936,  2426,  1028,    40,  1976,
     253,   442,   788,   256,  2380,  2521,  2547,  1911,  2569,  2567,
    1698,  1699,  1700,  1917,  2313,  1997,  1487,  1748,   666,   504,
     129,  1449,    64,   761,   382,  1941,  1538,  2295,  2296,   415,
    2001,  2285,  1800,  1775,  1819,  1823,  1780,  2305,  1942,   201,
     149,  1813,  2027,  2026,  1224,  2336,   878,  1951,  2047,  2353,
      92,     5,  2206,  1557,  1552,  1564,    10,  1572,  2240,    -1,
      14,    15,    16,    17,  1968,  1969,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    27,    -1,    -1,   745,    -1,    -1,    -1,
      34,    -1,    36,    -1,    -1,    -1,    -1,   129,    -1,    -1,
      -1,    -1,   201,  1997,   763,    -1,    -1,    -1,    -1,    -1,
      -1,  2005,    -1,  2371,    -1,    -1,    -1,   149,   777,    -1,
      64,  2015,    -1,  2381,  2382,  2395,   785,   786,    -1,    -1,
      -1,   790,    -1,    -1,  2404,    -1,    -1,  2031,  1772,  2033,
    2410,    -1,  2412,  1777,    -1,    -1,    -1,    -1,    92,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  1790,  2415,    -1,   818,
      -1,    -1,  1840,  1841,    -1,    -1,    -1,   826,    -1,   201,
      -1,    -1,  1850,    -1,    -1,    -1,  2446,  2447,  1812,    45,
      46,    -1,    48,    49,  1818,   129,    -1,    53,  1822,    -1,
      -1,    -1,    -1,    -1,    -1,    61,   855,    -1,   857,    -1,
      66,    -1,    -1,    69,    -1,   149,    -1,    -1,    -1,    -1,
      -1,    77,    -1,    79,    -1,    81,    -1,   876,   877,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   887,   888,
      -1,  2125,    -1,  2491,     5,  2493,  2130,    -1,   104,    10,
      -1,    -1,    -1,    14,    15,    16,    17,  2141,  2142,   115,
      -1,    -1,  2146,    -1,    -1,  1933,    27,   201,    -1,    -1,
      -1,  1939,  2156,    34,    -1,    36,    -1,    -1,  2162,    -1,
      -1,    -1,    -1,   139,    -1,    -1,    -1,    -1,    -1,  2549,
      -1,    -1,    -1,    -1,  2554,    -1,   152,    -1,    -1,    -1,
      -1,    -1,    -1,    64,  1972,    -1,    -1,  2567,    -1,  2569,
    2570,    -1,    -1,    -1,   170,    -1,    -1,    -1,   967,   968,
     176,  2205,  2582,    -1,    -1,    -1,    -1,    -1,   184,    -1,
     186,    92,   188,    -1,   190,    -1,    -1,    -1,    -1,    -1,
      -1,   197,    -1,    -1,   993,   994,    -1,    -1,  2232,    -1,
      -1,    -1,    -1,    -1,    -1,     5,    -1,    -1,   214,   215,
      10,    -1,    -1,  2247,    14,    15,    16,    17,   129,  2253,
      -1,  1020,    -1,    -1,    -1,    -1,   232,    27,    -1,  2003,
      -1,    -1,    -1,  2051,    34,    -1,    36,    -1,   149,    -1,
      -1,    -1,    -1,  2017,  1043,    -1,    -1,   253,  2022,    -1,
     256,  2025,    -1,    -1,    -1,  2289,    -1,    -1,    -1,    -1,
      -1,  2035,    -1,  2037,    64,  2039,    -1,  2041,    -1,    -1,
    2304,    -1,    -1,    -1,    -1,     5,    -1,    -1,    -1,     9,
      -1,    -1,  1081,     5,    -1,    15,    16,    17,    -1,    -1,
     201,   621,    92,    15,    16,    17,    -1,    27,    -1,     5,
      -1,    -1,    -1,     9,    -1,    27,    36,    -1,    -1,    15,
      16,    17,    -1,    -1,    36,    -1,    -1,  2135,    -1,    -1,
      -1,    27,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   129,
      36,    -1,  2366,  2367,    64,    -1,  2370,    -1,  2372,    -1,
      -1,    -1,    64,    -1,    -1,    -1,    -1,    -1,    -1,   149,
      -1,    -1,    -1,  2387,  2388,    -1,  2390,     5,    64,    -1,
      -1,     9,    92,    -1,    -1,    -1,  2400,    -1,    16,    17,
      92,    -1,  1171,    -1,  2408,    23,    -1,    25,    -1,    -1,
      28,    29,    30,    31,    32,    -1,    92,    -1,  2206,    37,
      -1,    -1,  2426,    -1,    -1,    -1,    -1,    -1,    -1,   129,
      -1,   201,    -1,    -1,    -1,    -1,     7,   129,    -1,    10,
      -1,    -1,  2230,    14,    15,    16,    17,    -1,    -1,   149,
    2454,    -1,    -1,   129,    -1,    -1,    27,   149,  2462,  2463,
      -1,    -1,    -1,    34,    -1,    36,   166,    -1,    -1,  2473,
      -1,   771,  2476,   149,    -1,   775,  2480,    -1,     7,    -1,
      -1,    10,    -1,    -1,    -1,    14,    15,    16,    17,    -1,
      -1,    -1,    -1,    64,    -1,    -1,    -1,    -1,    27,    -1,
      -1,   201,    -1,    -1,    -1,    34,     5,    36,  2512,   201,
       9,    -1,    -1,    -1,    -1,  2519,    -1,    16,    17,  2523,
      -1,    92,   822,  2527,    23,   201,    25,  2531,  1297,    28,
      29,    30,    31,    32,   834,    64,    -1,    -1,    37,    -1,
      -1,  2545,  2546,  2547,    -1,    -1,    -1,     7,    -1,    78,
      10,    -1,    -1,    -1,    14,    15,    16,    17,   129,  2347,
    2348,    -1,    -1,    92,    -1,    -1,    -1,    27,    -1,    -1,
      -1,    -1,    -1,    -1,    34,    -1,    36,    -1,   149,    -1,
      -1,   881,   882,   883,   884,    -1,   886,    -1,    -1,    -1,
      -1,    -1,  1361,    -1,    -1,   166,    -1,    -1,    -1,    -1,
     129,    -1,    -1,    -1,    64,    -1,    -1,     7,    -1,    -1,
      10,  2399,    -1,    -1,    14,    15,    16,    17,    -1,    -1,
     149,    -1,    -1,    -1,    -1,    -1,    -1,    27,    -1,    -1,
     201,    -1,    92,    -1,    34,    -1,    36,    -1,   938,    -1,
     940,   941,   942,   943,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1427,    -1,
    1429,    -1,    -1,    -1,    64,    -1,  2454,    -1,    -1,   129,
      -1,    -1,   201,    -1,    -1,    -1,    -1,    -1,    -1,  1448,
    1449,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   149,
    1459,    -1,    92,    -1,    -1,    -1,  1465,    -1,  1467,   999,
      -1,  1470,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1482,    -1,    -1,    -1,  1486,  1487,    -1,
      -1,  1021,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   129,
      -1,  1031,    12,    -1,    -1,    -1,    -1,    -1,    18,  1508,
      44,   201,    -1,    -1,    -1,    -1,  1515,    -1,    28,   149,
      -1,    31,  1052,    -1,    34,  1524,  1525,  1526,    -1,  1528,
    1529,    41,    -1,  1532,    -1,    45,    -1,    -1,  1068,    49,
      -1,    -1,  1072,  1073,    -1,    -1,    -1,  1546,    -1,    -1,
      -1,    -1,    -1,    87,    -1,    -1,    -1,    67,    -1,    93,
      -1,    71,    72,    -1,    -1,    -1,    -1,    77,    78,    -1,
      -1,   201,    -1,    83,    84,    -1,    86,    87,    88,    89,
      -1,    91,    -1,    -1,    -1,   119,    -1,    -1,    -1,    -1,
     100,    -1,    -1,    -1,    -1,   105,    -1,   107,    -1,    -1,
    1130,   111,    -1,    -1,    -1,   115,    -1,   117,  1607,   143,
      -1,   145,    -1,    -1,   124,    -1,    -1,    -1,   128,    -1,
      -1,    -1,   132,    -1,   134,    -1,    -1,    -1,    -1,    -1,
      -1,   141,   142,    -1,    -1,   145,   146,    -1,    -1,    -1,
    1639,   151,    -1,   153,   178,    -1,    -1,   157,    -1,   183,
      -1,    -1,    -1,   187,    -1,    -1,    -1,   191,  1188,  1189,
     194,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   184,    -1,   186,     7,    -1,    -1,
      10,    -1,   216,   193,    14,    15,    16,    17,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   230,    27,    -1,  1698,
    1699,  1700,   236,    -1,    34,    -1,    36,    -1,    -1,    -1,
      -1,    -1,    -1,     7,    -1,    -1,    10,    -1,    -1,    -1,
      14,    15,    16,    17,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    27,    64,    -1,    -1,    -1,    -1,    -1,
      34,    -1,    36,    -1,    -1,    -1,    -1,    -1,     7,  1748,
      -1,    10,    -1,    -1,    -1,    14,    15,    16,    17,    -1,
      -1,    -1,    92,    -1,    -1,    -1,    -1,    -1,    27,    -1,
      64,    -1,    -1,    -1,    -1,    34,    -1,    36,    -1,  1309,
    1310,  1311,  1312,  1313,  1314,  1315,  1316,  1317,  1318,  1319,
    1320,  1321,  1322,  1323,  1324,  1325,  1326,  1327,    92,   129,
      -1,    -1,    -1,    -1,    -1,    64,    -1,     7,    -1,    -1,
      10,    -1,    -1,  1343,    14,    15,    16,    17,    -1,   149,
    1350,    -1,  1352,    -1,    -1,    -1,    -1,    27,    -1,    -1,
      -1,    -1,  1362,    92,    34,   129,    36,    -1,    -1,    -1,
      -1,  1840,  1841,    -1,    -1,  1844,  1845,  1846,    -1,  1848,
    1849,  1850,    -1,    -1,    -1,   149,    -1,  1856,  1857,    -1,
      -1,    -1,    -1,  1393,    64,    -1,    -1,    -1,    -1,    -1,
     129,   201,    10,    -1,    -1,    -1,    14,    15,    16,    17,
      -1,    -1,    -1,    -1,  1883,  1884,  1885,  1886,  1887,    27,
     149,     5,    92,    -1,   404,     9,    34,    -1,    36,    -1,
      -1,    -1,    16,    17,    -1,    -1,    -1,   201,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,    -1,    37,    38,    -1,    64,    -1,    -1,   129,
      -1,    -1,  1462,     5,  1933,    -1,    -1,     9,    10,    -1,
    1939,    -1,   201,    -1,    16,    17,  1476,    -1,    -1,   149,
      22,    23,    24,    25,    92,    -1,    28,    29,    30,    31,
      32,    33,    34,    35,    -1,    37,    38,  1497,    -1,    -1,
      -1,    -1,    -1,  1972,  1973,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   129,    -1,    -1,    -1,    -1,  1995,    -1,   508,    -1,
      -1,   201,    -1,    -1,    -1,    -1,    -1,  2006,    -1,    -1,
      -1,   149,     5,  1543,     7,    -1,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,    -1,    -1,    -1,  2027,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,  2051,  2052,     5,    -1,    -1,     8,     9,    10,
      -1,  1591,    -1,   201,    -1,    16,    17,    -1,    -1,    -1,
      -1,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,    -1,    37,    38,    -1,    -1,
      -1,    -1,    -1,     5,  1624,    -1,     8,     9,    10,    -1,
      -1,  1631,    -1,  2102,    16,    17,    -1,  1637,    -1,    -1,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,    -1,    37,    38,    -1,    -1,    -1,
    1660,    -1,    -1,  2132,    -1,    -1,  2135,    -1,  2137,    -1,
      -1,    -1,  1672,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1680,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1690,    -1,    -1,  1693,    -1,    -1,    -1,    -1,     5,    -1,
      -1,     8,     9,    10,    -1,    -1,    -1,    -1,  1708,    16,
      17,    -1,    -1,    -1,    -1,    22,    23,    24,    25,    -1,
    1720,    28,    29,    30,    31,    32,    33,    -1,    35,    -1,
      37,    38,    -1,    -1,  1734,    -1,    -1,  2206,    -1,     5,
    1740,    -1,    -1,     9,    10,    -1,  1746,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
      -1,  2230,    28,    29,    30,    31,    32,    33,    -1,    35,
       5,    37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,
      -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,    -1,    37,    38,    -1,    -1,     5,    -1,     7,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    -1,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,  2306,    37,    38,
       5,  2310,     7,    -1,     9,    10,    -1,    -1,    -1,    -1,
      -1,    16,    17,    -1,    -1,    -1,    -1,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,     5,    37,    38,  1874,     9,    10,    -1,  2347,  2348,
      -1,    -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  2381,  2382,  2383,  2384,    -1,    -1,  1918,    -1,
    2389,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    2399,     5,    -1,   913,    -1,     9,    10,    -1,  1938,    -1,
    1940,    -1,    16,    17,  1944,    -1,    -1,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,    -1,    37,    38,    -1,    -1,  1967,    -1,    -1,
    1970,    -1,    -1,    -1,  2443,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1992,    -1,    -1,    -1,    -1,    -1,    -1,  2468,
    2469,    -1,  2471,    -1,    -1,    -1,    -1,    -1,    -1,  2478,
      -1,     5,    -1,    -1,    -1,     9,    10,    -1,    -1,    -1,
      -1,    -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,
      24,    25,    -1,  1013,    28,    29,    30,    31,    32,    33,
      -1,    35,    -1,    37,    38,    -1,     5,    -1,    -1,    -1,
       9,    10,  2521,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    -1,  2068,    28,
      29,    30,    31,    32,    33,    -1,    35,  2077,    37,    38,
       5,  2081,  2082,    -1,     9,    10,    -1,    -1,    -1,  2089,
      -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,    -1,
    2120,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  2139,
      -1,     5,    -1,    -1,  2144,     9,    10,    -1,    -1,    -1,
      -1,    -1,    16,    17,  2154,  2155,    -1,    21,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     5,    -1,    -1,  2185,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,  2195,  2196,    -1,    21,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,     5,    37,    38,    -1,     9,    10,    -1,
      -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,    -1,    37,    38,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  2268,    -1,
    2270,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     5,  2279,
       7,    -1,     9,    10,  2284,    -1,    -1,  2287,    -1,    16,
      17,    -1,    -1,    -1,  1274,    22,    23,    24,    25,    -1,
    2300,    28,    29,    30,    31,    32,    33,    -1,    35,     5,
      37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
      -1,    37,    38,    -1,    -1,  2345,    -1,    -1,    -1,    -1,
      -1,  2351,    -1,    -1,     5,    -1,     7,  2357,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      -1,    22,    23,    24,    25,    -1,  2376,    28,    29,    30,
      31,    32,    33,    -1,    35,     5,    37,    38,    -1,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    21,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,    -1,    37,    38,  2419,
      -1,  2421,    -1,    -1,    -1,  2425,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  2436,  2437,    -1,    -1,
      -1,  2441,     5,    -1,     7,    -1,     9,    10,    -1,    -1,
      -1,  2451,    -1,    16,    17,  2455,    -1,    -1,    -1,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  2487,    -1,    -1,
      -1,    -1,  2492,    -1,  2494,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,  2506,    -1,  2508,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    14,
      15,    16,    17,    -1,    19,    -1,    21,    -1,    23,    24,
      -1,    26,    -1,    -1,    -1,    30,  2536,    32,    33,    -1,
    2540,    36,    37,    38,    -1,    40,    -1,    42,    43,    -1,
      -1,    46,  2552,    -1,    -1,    -1,    51,    52,    53,    54,
      55,    56,    57,    58,    -1,    60,    61,    -1,    -1,    64,
      65,    66,    -1,    68,    -1,    70,    -1,    -1,    73,    74,
      -1,    -1,    -1,    -1,    -1,    -1,    81,    82,    -1,    -1,
      85,    -1,    -1,    -1,    -1,    -1,    -1,    92,    -1,    94,
      -1,    96,    97,    -1,    -1,    -1,   101,   102,   103,    -1,
      -1,   106,    -1,    -1,   109,   110,    -1,   112,   113,   114,
      -1,    -1,    -1,    -1,   119,   120,    -1,    -1,   123,    -1,
     125,   126,   127,    -1,   129,   130,    -1,    -1,   133,    -1,
     135,   136,   137,   138,   139,   140,    -1,    -1,    -1,    -1,
      -1,    -1,   147,   148,   149,    -1,    -1,    -1,    -1,   154,
     155,   156,    -1,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,    -1,
     185,    -1,   187,    -1,   189,    -1,   191,   192,    -1,   194,
     195,   196,   197,   198,   199,   200,   201,     5,    -1,     7,
       6,     9,    10,    -1,    -1,    -1,    -1,    13,    16,    17,
      -1,    -1,    -1,    -1,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,    -1,    37,
      38,    -1,    -1,    -1,    -1,    -1,    -1,    43,    44,    45,
      46,    -1,    48,    49,    50,    51,    52,    53,    -1,   254,
      56,    57,   257,    -1,   259,    61,    62,    63,    -1,    65,
      66,    67,    68,    69,    70,    -1,    72,    73,    74,    75,
      76,    77,    -1,    79,    80,    81,    82,    83,    84,    -1,
      -1,    87,    88,    89,    90,    91,    -1,    -1,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
      -1,   117,    -1,   119,   120,    -1,   122,   123,   124,    -1,
      -1,   127,    -1,    -1,   130,   131,    -1,   133,   134,   135,
     136,    -1,   138,   139,   140,   141,   142,   143,   144,   145,
     146,    -1,    -1,    -1,    -1,    -1,   152,   153,   154,   155,
      -1,    -1,    -1,    -1,   160,    -1,    -1,   163,   164,    -1,
      -1,   167,   168,    -1,   170,    -1,    -1,    -1,   174,    -1,
     176,    -1,   178,    -1,    -1,    -1,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,    -1,   193,   194,   195,
      -1,   197,    -1,   199,   200,    -1,   202,    -1,   204,   205,
     206,   207,    -1,    -1,   210,   211,   212,    -1,   214,   215,
     216,    -1,   218,   219,   220,    -1,   222,    -1,   224,   225,
     226,   227,   228,    -1,   230,    -1,   232,   233,    -1,    -1,
     236,   237,   238,    -1,    -1,   241,   242,    -1,   244,   245,
      -1,   247,   248,    -1,    -1,    -1,   252,   253,    -1,    -1,
     256,    -1,    -1,   259,    -1,    -1,    -1,   263,   264,    -1,
      -1,   267,   268,   269,    -1,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   281,    -1,   283,    -1,    -1,
     286,    -1,    -1,    -1,   290,   291,   292,   293,   294,    -1,
     296,   297,    -1,    -1,   300,   301,   302,   303,    -1,    -1,
      -1,    -1,   308,   309,   310,   311,   312,   313,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   321,    -1,    -1,   324,   325,
     326,   327,   328,    -1,    -1,    -1,   332,   333,   334,   335,
     336,   337,     5,   339,   340,     8,     9,    10,   344,    -1,
      -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    -1,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,    -1,    37,    38,    -1,    -1,    -1,    -1,
      -1,    -1,     5,    -1,    -1,    -1,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    21,    22,
      23,    24,    25,   598,   599,    28,    29,    30,    31,    32,
      33,    -1,    35,     5,    37,    38,    -1,     9,    10,    -1,
      -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,     5,    37,    38,    -1,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      21,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,     5,    37,    38,     8,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    -1,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,     5,    37,    38,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,     5,    37,    38,
      -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,
      -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,     5,    37,
      38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,    -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,     5,
      37,    38,     8,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    -1,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
       5,    37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,
      -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,     5,    37,    38,     8,     9,    10,    -1,    -1,    -1,
      -1,    -1,    16,    17,    -1,    -1,    -1,    -1,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,     5,    37,    38,     8,     9,    10,    -1,    -1,
      -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    -1,    22,
      23,    24,    25,    -1,    -1,    28,    29,    30,    31,    32,
      33,    -1,    35,     5,    37,    38,    -1,     9,    10,    -1,
      -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    -1,    28,    29,    30,    31,
      32,    33,    -1,    35,     5,    37,    38,     8,     9,    10,
      -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,    -1,
      -1,    22,    23,    24,    25,    -1,    -1,    28,    29,    30,
      31,    32,    33,    -1,    35,     5,    37,    38,    -1,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    21,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,     5,    37,    38,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,     5,    37,    38,
       8,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,
      -1,    -1,    -1,    -1,    22,    23,    24,    25,    -1,    -1,
      28,    29,    30,    31,    32,    33,    -1,    35,     5,    37,
      38,     8,     9,    10,    -1,    -1,    -1,    -1,    -1,    16,
      17,    -1,    -1,    -1,    -1,    22,    23,    24,    25,    -1,
      -1,    28,    29,    30,    31,    32,    33,    -1,    35,     5,
      37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,    -1,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
      -1,    -1,    28,    29,    30,    31,    32,    33,    -1,    35,
       5,    37,    38,    -1,     9,    10,    -1,    -1,    -1,    -1,
      -1,    16,    17,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    -1,    28,    29,    30,    31,    32,    33,    -1,
      35,     5,    37,    38,     8,     9,    10,    -1,    -1,    -1,
      -1,    -1,    16,    17,    -1,    -1,    -1,    -1,    22,    23,
      24,    25,    -1,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,    -1,    37,    38,     5,    -1,     7,    -1,     9,
      10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,    -1,
      -1,    -1,    22,    23,    24,    25,    -1,    -1,    28,    29,
      30,    31,    32,    33,    -1,    35,     5,    37,    38,    -1,
       9,    10,    -1,    -1,    -1,    -1,    -1,    16,    17,    -1,
      -1,    -1,    -1,    22,    23,    24,    25,    -1,    -1,    28,
      29,    30,    31,    32,    33,    -1,    35,    -1,    37,    38
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,   358,     0,     1,   150,   257,   359,   360,   116,     6,
      13,    43,    44,    45,    46,    48,    49,    50,    51,    52,
      53,    56,    57,    61,    62,    63,    65,    66,    67,    68,
      69,    70,    72,    73,    74,    75,    76,    77,    79,    80,
      81,    82,    83,    84,    87,    88,    89,    90,    91,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   117,   119,   120,   122,   123,   124,   127,   130,   131,
     133,   134,   135,   136,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   152,   153,   154,   155,   160,   163,   164,
     167,   168,   170,   174,   176,   178,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   193,   194,   195,   197,
     199,   200,   202,   204,   205,   206,   207,   210,   211,   212,
     214,   215,   216,   218,   219,   220,   222,   224,   225,   226,
     227,   228,   230,   232,   233,   236,   237,   238,   241,   242,
     244,   245,   247,   248,   252,   253,   256,   259,   263,   264,
     267,   268,   269,   271,   272,   273,   274,   275,   276,   277,
     278,   279,   280,   281,   283,   286,   290,   291,   292,   293,
     294,   296,   297,   300,   301,   302,   303,   308,   309,   310,
     311,   312,   313,   321,   324,   325,   326,   327,   328,   332,
     333,   334,   335,   336,   337,   339,   340,   344,   361,   363,
     366,   378,   379,   383,   384,   385,   391,   392,   393,   394,
     396,   397,   399,   401,   402,   403,   404,   411,   412,   413,
     414,   415,   416,   420,   421,   422,   426,   427,   465,   467,
     480,   523,   524,   526,   527,   533,   534,   535,   536,   543,
     544,   545,   546,   548,   551,   555,   556,   557,   558,   559,
     560,   566,   567,   568,   579,   580,   581,   583,   586,   589,
     594,   595,   597,   599,   601,   604,   605,   629,   630,   641,
     642,   643,   644,   649,   652,   655,   658,   659,   709,   710,
     711,   712,   713,   714,   715,   716,   722,   724,   726,   728,
     730,   731,   732,   733,   734,   737,   739,   740,   741,   744,
     745,   749,   750,   752,   753,   754,   755,   756,   757,   758,
     761,   766,   771,   773,   774,   775,   776,   778,   779,   780,
     781,   782,   783,   800,   803,   804,   805,   806,   812,   815,
     820,   821,   822,   825,   826,   827,   828,   829,   830,   831,
     832,   833,   834,   835,   836,   837,   838,   839,   844,   845,
     846,   847,   848,   858,   859,   860,   862,   863,   864,   865,
     866,   871,   889,    15,   490,   490,   552,   552,   552,   552,
     552,   490,   552,   552,   362,   552,   552,   552,   490,   552,
     490,   552,   552,   490,   552,   552,   552,   489,   552,   490,
     552,   552,     7,    15,   491,    15,   490,   612,   552,   490,
     375,   552,   552,   552,   552,   552,   552,   552,   552,   552,
     552,   129,   368,   532,   532,   552,   552,   552,   490,   552,
     368,   552,   490,   490,   552,   552,   489,   362,   490,   490,
      64,   374,   552,   552,   490,   490,   552,   490,   490,   490,
     490,   490,   552,   429,   552,   552,   552,   368,   466,   362,
     490,   552,   552,   552,   490,   552,   490,   552,   552,   490,
     552,   552,   552,   490,   362,   490,   375,   552,   552,   375,
     552,   490,   552,   552,   552,   490,   552,   552,   490,   552,
     490,   552,   552,   552,   552,   552,   552,    15,   490,   590,
     490,   362,   490,   490,   552,   552,   552,    15,     8,   490,
     490,   552,   552,   552,   490,   552,   552,   552,   552,   552,
     552,   552,   552,   552,   552,   552,   552,   552,   552,   552,
     552,   552,   552,   552,   552,   552,   552,   552,   552,   552,
     552,   490,   552,   490,   552,   552,   552,   552,   490,   552,
     552,   552,   552,   552,   552,   552,   552,   895,   895,   895,
     895,   895,   895,   257,   578,   124,   233,   399,    15,   371,
     578,     8,     8,     8,     8,     7,     8,   124,   363,   386,
       8,   368,   400,     8,     8,     8,     8,     8,   547,     8,
     547,     8,     8,     8,     8,   547,   578,     7,   218,   252,
     524,   526,   535,   536,   239,   544,   544,    10,    14,    15,
      16,    17,    27,    34,    36,    64,    92,   149,   201,   368,
     380,   496,   497,   499,   500,   501,   502,   508,   509,   510,
     511,   512,   515,    15,   552,     5,     9,    15,    16,    17,
     129,   498,   500,   508,   562,   576,   577,   552,    15,   562,
     552,     5,   561,   562,   577,   562,     8,     8,     8,     8,
       8,     8,     8,     8,     7,     8,     8,     5,     7,   368,
     639,   640,   368,   632,   491,    15,    15,   149,   479,   368,
     368,   742,   743,     8,   368,   656,   657,   743,   368,   370,
     368,    15,   528,   574,    23,    37,   368,   418,   419,    15,
     368,   602,   368,   670,   670,   368,   653,   654,   368,   531,
     428,    15,   368,   582,   149,   748,   531,     7,   474,   475,
     490,   613,   614,   368,   608,   614,    15,   553,   368,   584,
     585,   531,    15,    15,   531,   748,   532,   531,   531,   531,
     531,   368,   531,   371,   531,    15,   423,   491,   499,   500,
      15,   365,   368,   368,   650,   651,   481,   482,   483,   484,
       8,   671,   738,    15,   368,   596,   368,   587,   588,   575,
      15,    15,   368,   491,    15,   496,   751,    15,    15,   368,
     725,   727,     8,   368,    37,   417,    15,   500,   501,   491,
      15,    15,   553,   479,   491,   500,   368,   717,     5,    15,
     576,   577,   491,   368,   369,   491,   575,    15,   499,   633,
     634,   608,   612,   368,   600,   368,   697,   697,    15,   368,
     598,   717,   496,   507,   491,   375,    15,   368,   703,   703,
     703,   703,   703,     7,   496,   591,   592,   368,   593,   491,
     364,   368,   491,   368,   723,   725,   368,   490,   491,   368,
     468,    15,    15,   575,   368,    15,   614,    15,   614,   614,
     614,   614,   786,   842,   614,   614,   614,   614,   614,   614,
     786,   368,   375,   849,   850,   851,    15,    15,   375,   861,
      15,   496,   496,   496,   496,   495,   496,    15,    15,    15,
      15,    15,   368,   887,    15,   362,   362,   124,     5,    21,
     368,   372,   373,   367,   375,   368,   368,   368,   419,     7,
     375,   362,   124,   368,   368,     5,    15,   406,   407,   368,
     419,   419,   419,   419,   418,   499,   417,   368,   368,   423,
     430,   431,   433,   434,   552,   552,   239,   409,   496,   497,
     496,   496,   496,   496,     5,     9,    16,    17,    22,    23,
      24,    25,    26,    28,    29,    30,    31,    32,    33,    35,
      37,    38,   380,    15,   246,     3,    15,   246,   246,    15,
     505,   506,    21,   549,   574,   507,     5,     9,   166,   563,
     564,   565,   576,    26,   576,     5,     9,    23,    37,   498,
     575,   576,   575,     8,    15,   500,   569,   570,    15,   496,
     497,   512,   571,   572,   573,   571,   582,   368,   596,   598,
     600,   602,   368,     7,   375,   723,     8,    21,   634,   419,
     521,   496,   240,   547,    15,   375,    15,   473,     8,   574,
       7,   496,   529,   530,   531,    15,   368,   473,   419,   478,
     479,     8,   430,   521,   473,    15,     8,    21,     5,     7,
     476,   477,   496,   368,     8,    21,     5,    58,    86,   126,
     137,   165,   258,   615,   611,   612,   175,   603,   496,   149,
     542,     8,   496,   496,   367,   368,   424,   425,   499,   504,
     368,    26,   368,   537,   538,   540,   371,     8,     8,    15,
     231,   399,   485,   375,     8,   738,   368,   499,   707,   717,
     735,   736,     8,   562,    26,     5,     9,    16,    17,    22,
      23,    24,    25,    28,    29,    30,    31,    32,    33,    34,
      35,    37,    38,   380,   381,   382,   368,   375,   389,   499,
     496,    15,   375,   368,   368,   499,   499,   522,     8,   672,
     729,   368,   499,   660,   368,   463,   464,   542,   419,    18,
     575,   576,   575,   395,   398,   639,   634,     7,   612,   614,
     707,   717,   718,   719,   418,   419,   457,   458,    62,   499,
     762,    15,    15,     7,     8,    21,   590,   419,   371,   419,
     473,     8,   669,   694,    21,   375,   368,     8,   496,   496,
     473,   499,   547,   807,   499,   287,   819,   819,   547,   816,
     819,    15,   547,   784,   547,   823,   784,   784,   547,   801,
     547,   813,   473,   147,   148,   180,   314,   315,   318,   319,
     376,   852,   853,   854,     8,    21,   500,   675,   855,    21,
     855,   376,   853,   375,   759,   760,     8,     8,     8,     8,
     499,   502,   503,   777,   660,   375,   872,   873,   874,   875,
     876,   375,   879,   880,   881,   882,   883,   375,   884,   885,
       8,   375,   890,   891,   368,   364,   362,     8,    21,   213,
     376,   473,    44,    87,    93,   119,   143,   145,   178,   183,
     187,   191,   194,   216,   230,   236,   387,   388,   390,   368,
     362,   490,   553,   574,   400,   473,   547,   547,     8,    37,
      15,   368,   436,   441,   375,    15,   516,    21,     8,   496,
     496,   496,   496,   496,   496,   496,   496,   496,   496,   496,
     496,   496,   496,   496,   496,   496,   496,   496,   574,    64,
     129,   492,   494,   574,   499,   510,   513,    64,   513,   507,
       8,    21,     5,   496,   550,   565,     8,    21,     5,     9,
     496,    21,   496,   576,   576,   576,   576,   576,    21,   569,
     569,     8,   496,   497,   572,   573,     8,     8,     8,   473,
     473,   490,    43,    67,    82,    87,    88,    94,   228,   259,
     303,   643,   640,   375,   503,   519,    21,   368,    15,   495,
      67,   474,   657,   496,     7,     8,    21,   549,    37,     8,
      21,   654,   499,   502,   518,   520,   574,   746,   476,     7,
     473,   614,    15,    15,    15,    15,    15,    15,   603,   614,
     368,    21,   554,   585,    21,    21,    15,     8,    21,     8,
     506,   500,     8,   539,    26,   367,   651,   482,   129,   486,
     487,   488,   404,   169,   208,   282,   375,    15,     7,     8,
      21,   588,   571,    21,    21,   147,   148,   180,    21,    18,
      21,     7,   496,   514,   175,   323,    37,     8,    21,   375,
       8,    21,    26,     8,    21,   554,   496,    21,   459,   460,
     459,    21,     7,   614,   603,    15,     7,     8,    21,     8,
      15,    15,    26,   704,   705,   707,   495,   496,   592,   375,
       8,   694,     8,   669,   400,   390,   377,    21,    21,    21,
     614,   547,    21,   614,   547,   843,   614,   547,   614,   547,
     614,   547,   614,   547,    15,    15,    15,    15,    15,    15,
     375,   851,     8,    21,    21,   182,   315,   318,     8,    21,
     375,   375,   375,   496,    15,    15,     8,    21,    21,   183,
     191,   208,     8,    21,    41,   209,   228,     8,    21,   338,
     341,   342,   343,   354,     8,    21,   375,   244,   330,   345,
     346,   347,     8,    21,   371,   368,   373,    15,   405,   406,
     473,   490,    15,     7,     8,   368,   473,    15,   510,     5,
     408,   496,   565,   419,   499,   433,    15,    16,    17,    27,
      36,    59,    64,    92,   149,   201,   432,   434,   444,   445,
     446,   447,   448,   449,   450,   451,   436,   441,   442,   443,
      15,   437,   438,    62,   496,   571,   497,   492,    21,     8,
     493,   496,   514,   565,     7,   574,   479,   496,   574,     8,
     570,    21,     8,     8,     8,   497,   573,   497,   573,   497,
     573,   368,   255,     8,    21,   479,   478,    21,     7,    21,
     496,   529,    21,   479,   547,     8,    21,   565,   747,     8,
      21,   477,   496,   615,   574,    15,   617,   368,   616,   616,
     496,   616,   473,   614,   239,   531,   495,   425,   425,   368,
     496,   538,    21,   496,   514,     8,    21,    16,    15,    15,
      15,   495,   735,   736,   491,   499,   767,     7,   496,     7,
      21,    21,   368,   610,   500,   499,   191,   499,   614,   661,
     496,   464,   547,     8,    47,   177,   368,   462,   375,   631,
     633,   603,     7,     7,   496,   720,   721,   718,   719,   458,
     496,     5,   617,   763,   764,   770,   496,   627,     8,    21,
      15,    21,    71,   208,   375,   375,   491,   172,   368,   471,
     472,   500,   191,   208,   282,   285,   290,   298,   787,   788,
     789,   796,   808,   809,   810,   614,   266,   817,   818,   819,
     614,    37,   499,   840,   841,    84,   265,   289,   299,   304,
     785,   787,   788,   789,   790,   791,   792,   794,   795,   796,
     614,   787,   788,   789,   790,   791,   792,   794,   795,   796,
     809,   810,   824,   614,   787,   788,   789,   796,   802,   614,
     787,   788,   814,   614,   855,   855,   855,   375,   856,   857,
     855,   855,   500,   760,   330,   315,   331,   574,   492,   503,
      15,    15,    15,   873,    15,    15,    15,   880,    15,    15,
      15,    15,   885,   351,   352,    15,    15,    15,    15,    15,
     891,   368,    18,    26,   410,    15,   389,     7,   375,   405,
     554,   554,   409,     5,   496,   447,   448,   449,   452,   448,
     450,   448,   450,   246,   246,   246,   246,   246,     8,    37,
     368,   435,   499,     5,   437,   438,     8,    15,    16,    17,
     149,   368,   435,   439,   440,   453,   454,   455,   456,    15,
     438,    15,    21,   517,    21,    21,   506,   574,   496,   507,
     550,   564,   576,   540,   541,   497,   541,   541,   541,   473,
     368,   635,   638,   574,     8,    21,     7,   409,   496,   574,
     496,   574,   565,   628,   496,   618,   619,    21,    21,    21,
      21,     8,     8,   254,   525,   531,    21,   487,   488,   675,
     675,   675,    21,    21,   368,    15,    21,   496,     7,     7,
     496,   473,    15,   173,     8,   665,   666,   667,   668,   669,
     671,   672,   673,   676,   678,   679,   680,   694,   702,   540,
     460,    15,    15,   461,   255,     8,     7,     8,    21,    21,
      21,     8,    21,    21,   705,   706,    15,    15,   368,   368,
     469,   470,   472,    18,     8,    26,   786,    15,   786,   786,
      15,   614,   808,   786,   614,   817,   368,     8,    21,    15,
     786,    15,   786,    15,   614,   785,   614,   824,   614,   802,
     614,   814,    21,    21,    21,   316,   317,     8,    21,    21,
      21,    15,    15,   492,    21,   503,   877,   878,   675,   375,
     698,   704,   718,   704,   660,   660,   503,   888,   496,   886,
      15,    15,   375,   892,   893,   660,   660,   496,   375,   894,
      21,   496,   496,   645,   646,    21,   388,   410,     5,   496,
     400,     8,    21,     8,   513,   513,   513,   513,   513,   444,
       5,    15,   434,   445,   438,   368,   435,   443,   453,   454,
     454,     8,    21,     7,    16,    17,     5,    37,     9,   453,
     496,    20,   506,   493,    21,    26,    21,    21,    21,    21,
      15,   503,   565,   479,   656,   491,   518,   565,   747,   496,
      21,     7,     8,    21,   496,   375,    15,    21,    21,    21,
       7,   768,   769,   770,   496,   496,     7,   675,   499,   662,
     375,   667,    26,   462,    26,   381,   635,   633,   368,   606,
     607,   608,   609,   721,   764,   614,    78,   591,   368,   670,
     718,   695,     8,   368,   472,   496,   614,   797,   375,   614,
     614,   842,   499,   840,   375,   496,   496,   614,   614,   614,
     614,   857,   675,   499,    21,    26,     8,    21,    21,    22,
      24,    33,    35,   158,   159,   161,   162,   192,   234,   247,
     699,   700,   701,     8,    21,    21,    21,    21,    21,    21,
       8,    21,     8,    21,   375,   867,   868,   867,   315,   350,
       8,    21,    21,    21,    21,   348,   349,     8,     8,    21,
       7,    21,    21,   574,   452,   445,   574,   435,    26,    21,
     453,   440,   454,   454,   455,   455,   455,    21,   496,     5,
     496,   514,   636,   637,   499,     8,   675,   499,     8,   496,
     619,   375,    21,   254,   496,     8,    21,   496,    21,    15,
      41,   135,   209,   221,   223,   224,   226,   229,   320,   322,
     496,   461,    21,    21,    15,     8,   132,   765,    21,    21,
       7,    21,   697,   699,   470,     5,    16,    17,    22,    24,
      33,    35,    37,   159,   162,   247,   305,   306,   307,   799,
      21,    94,   230,   284,   295,   811,    37,   191,   288,   299,
     793,    21,    21,    21,    21,   496,   878,    15,    15,   375,
     503,   496,   353,     8,    21,    21,   893,   496,     7,     7,
     408,    21,   492,   439,   453,    21,     8,     8,    21,   479,
     565,   255,    15,    21,   769,     5,   496,   663,   664,    15,
     681,    15,    15,    15,    15,   703,   703,    15,    15,    15,
       8,   495,   607,   707,   708,    15,   718,   696,   696,     7,
       8,    21,   843,    21,     8,   500,   675,   699,     8,   868,
       8,   869,     8,   870,    21,     7,   409,    21,    21,   496,
     637,   496,   368,   620,   621,   496,     8,    21,   682,   681,
     717,   735,   717,   718,   707,   704,   496,   496,   674,   662,
     677,   496,    21,     8,   375,    21,     7,     8,    21,   675,
     798,   496,   375,    21,     8,   496,   375,   375,   368,   647,
     648,    21,     8,    15,    21,   664,   148,   180,   683,     7,
      21,     7,    21,    15,    21,    21,     8,    21,     8,    21,
       8,   707,    78,   698,   698,    21,   329,   496,   500,   352,
     351,     8,   496,    40,   496,   622,   623,   770,     7,     7,
     684,   685,   707,   735,   718,   591,   496,   662,   496,    21,
      21,    21,    15,    21,    15,    15,   648,   368,   624,     8,
      21,     8,    21,    15,    21,    21,    21,     8,   495,   867,
     867,    17,   625,   626,   623,   685,   496,   686,   687,    21,
     496,    21,    21,    21,   627,    17,     7,     8,    21,     8,
     772,   627,   496,   687,    15,   375,   375,   688,   689,   690,
     691,   692,   182,   318,   128,   157,   217,     8,    21,     7,
       7,    15,   693,   693,   693,   689,   375,   691,   692,   375,
     692,   494,     7,    21,   692
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 788 "gram1.y"
    { (yyval.bf_node) = BFNULL; ;}
    break;

  case 3:
#line 790 "gram1.y"
    { (yyval.bf_node) = set_stat_list((yyvsp[(1) - (3)].bf_node),(yyvsp[(2) - (3)].bf_node)); ;}
    break;

  case 4:
#line 794 "gram1.y"
    { lastwasbranch = NO;  (yyval.bf_node) = BFNULL; ;}
    break;

  case 5:
#line 796 "gram1.y"
    {
	       if ((yyvsp[(2) - (3)].bf_node) != BFNULL) 
               {	    
	          (yyvsp[(2) - (3)].bf_node)->label = (yyvsp[(1) - (3)].label);
	          (yyval.bf_node) = (yyvsp[(2) - (3)].bf_node);
	 	  if (is_openmp_stmt) {            /*OMP*/
			is_openmp_stmt = 0;
			if((yyvsp[(2) - (3)].bf_node)) {                        /*OMP*/
				if ((yyvsp[(2) - (3)].bf_node)->decl_specs != -BIT_OPENMP) (yyvsp[(2) - (3)].bf_node)->decl_specs = BIT_OPENMP; /*OMP*/
			}                               /*OMP*/
		  }                                       /*OMP*/
               }
	    ;}
    break;

  case 6:
#line 810 "gram1.y"
    { PTR_BFND p;

	     if(lastwasbranch && ! thislabel)
               /*if (warn_all)
		 warn("statement cannot be reached", 36);*/
	     lastwasbranch = thiswasbranch;
	     thiswasbranch = NO;
	     if((yyvsp[(2) - (3)].bf_node)) (yyvsp[(2) - (3)].bf_node)->label = (yyvsp[(1) - (3)].label);
	     if((yyvsp[(1) - (3)].label) && (yyvsp[(2) - (3)].bf_node)) (yyvsp[(1) - (3)].label)->statbody = (yyvsp[(2) - (3)].bf_node); /*8.11.06 podd*/
	     if((yyvsp[(1) - (3)].label)) {
		/*$1->statbody = $2;*/ /*8.11.06 podd*/
		if((yyvsp[(1) - (3)].label)->labtype == LABFORMAT)
		  err("label already that of a format",39);
		else
		  (yyvsp[(1) - (3)].label)->labtype = LABEXEC;
	     }
	     if (is_openmp_stmt) {            /*OMP*/
			is_openmp_stmt = 0;
			if((yyvsp[(2) - (3)].bf_node)) {                        /*OMP*/
				if ((yyvsp[(2) - (3)].bf_node)->decl_specs != -BIT_OPENMP) (yyvsp[(2) - (3)].bf_node)->decl_specs = BIT_OPENMP; /*OMP*/
			}                               /*OMP*/
	     }                                       /*OMP*/
             for (p = pred_bfnd; (yyvsp[(1) - (3)].label) && 
		  ((p->variant == FOR_NODE)||(p->variant == WHILE_NODE)) &&
                  (p->entry.for_node.doend) &&
		  (p->entry.for_node.doend->stateno == (yyvsp[(1) - (3)].label)->stateno);
		  p = p->control_parent)
                ++end_group;
	     (yyval.bf_node) = (yyvsp[(2) - (3)].bf_node);
     ;}
    break;

  case 7:
#line 841 "gram1.y"
    { /* PTR_LLND p; */
			doinclude( (yyvsp[(3) - (3)].charp) );
/*			p = make_llnd(fi, STRING_VAL, LLNULL, LLNULL, SMNULL);
			p->entry.string_val = $3;
			p->type = global_string;
			$$ = get_bfnd(fi, INCLUDE_STAT, SMNULL, p, LLNULL); */
			(yyval.bf_node) = BFNULL;
		;}
    break;

  case 8:
#line 850 "gram1.y"
    {
	      err("Unclassifiable statement", 10);
	      flline();
	      (yyval.bf_node) = BFNULL;
	    ;}
    break;

  case 9:
#line 856 "gram1.y"
    { PTR_CMNT p;
              PTR_BFND bif; 
	    
              if (last_bfnd && last_bfnd->control_parent &&((last_bfnd->control_parent->variant == LOGIF_NODE)
	         ||(last_bfnd->control_parent->variant == FORALL_STAT)))
  	         bif = last_bfnd->control_parent;
              else
                 bif = last_bfnd;
              p=bif->entry.Template.cmnt_ptr;
              if(p)
                 p->string = StringConcatenation(p->string,commentbuf);
              else
              {
                 p = make_comment(fi,commentbuf, FULL);
                 bif->entry.Template.cmnt_ptr = p;
              }
 	      (yyval.bf_node) = BFNULL;         
            ;}
    break;

  case 10:
#line 876 "gram1.y"
    { 
	      flline();	 needkwd = NO;	inioctl = NO;
/*!!!*/
              opt_kwd_ = NO; intonly = NO; opt_kwd_hedr = NO; opt_kwd_r = NO; as_op_kwd_= NO; optcorner = NO;
	      yyerrok; yyclearin;  (yyval.bf_node) = BFNULL;
	    ;}
    break;

  case 11:
#line 885 "gram1.y"
    {
	    if(yystno)
	      {
	      (yyval.label) = thislabel =	make_label_node(fi,yystno);
	      thislabel->scope = cur_scope();
	      if (thislabel->labdefined && (thislabel->scope == cur_scope()))
		 errstr("Label %s already defined",convic(thislabel->stateno),40);
	      else
		 thislabel->labdefined = YES;
	      }
	    else
	      (yyval.label) = thislabel = LBNULL;
	    ;}
    break;

  case 12:
#line 901 "gram1.y"
    { PTR_BFND p;

	        if (pred_bfnd != global_bfnd)
		    err("Misplaced PROGRAM statement", 33);
		p = get_bfnd(fi,PROG_HEDR, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL);
		(yyvsp[(3) - (3)].symbol)->entry.prog_decl.prog_hedr=p;
 		set_blobs(p, global_bfnd, NEW_GROUP1);
	        add_scope_level(p, NO);
	        position = IN_PROC;
	    ;}
    break;

  case 13:
#line 913 "gram1.y"
    {  PTR_BFND q = BFNULL;

	      (yyvsp[(3) - (3)].symbol)->variant = PROCEDURE_NAME;
	      (yyvsp[(3) - (3)].symbol)->decl = YES;   /* variable declaration has been seen. */
	      q = get_bfnd(fi,BLOCK_DATA, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL);
	      set_blobs(q, global_bfnd, NEW_GROUP1);
              add_scope_level(q, NO);
	    ;}
    break;

  case 14:
#line 923 "gram1.y"
    { 
              install_param_list((yyvsp[(3) - (4)].symbol), (yyvsp[(4) - (4)].symbol), LLNULL, PROCEDURE_NAME); 
	      /* if there is only a control end the control parent is not set */
              
	     ;}
    break;

  case 15:
#line 930 "gram1.y"
    { install_param_list((yyvsp[(4) - (5)].symbol), (yyvsp[(5) - (5)].symbol), LLNULL, PROCEDURE_NAME); 
              if((yyvsp[(1) - (5)].ll_node)->variant == RECURSIVE_OP) 
                   (yyvsp[(4) - (5)].symbol)->attr = (yyvsp[(4) - (5)].symbol)->attr | RECURSIVE_BIT;
              pred_bfnd->entry.Template.ll_ptr3 = (yyvsp[(1) - (5)].ll_node);
            ;}
    break;

  case 16:
#line 936 "gram1.y"
    {
              install_param_list((yyvsp[(3) - (5)].symbol), (yyvsp[(4) - (5)].symbol), (yyvsp[(5) - (5)].ll_node), FUNCTION_NAME);  
  	      pred_bfnd->entry.Template.ll_ptr1 = (yyvsp[(5) - (5)].ll_node);
            ;}
    break;

  case 17:
#line 941 "gram1.y"
    {
              install_param_list((yyvsp[(1) - (3)].symbol), (yyvsp[(2) - (3)].symbol), (yyvsp[(3) - (3)].ll_node), FUNCTION_NAME); 
	      pred_bfnd->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
	    ;}
    break;

  case 18:
#line 946 "gram1.y"
    {PTR_BFND p, bif;
	     PTR_SYMB q = SMNULL;
             PTR_LLND l = LLNULL;

	     if(parstate==OUTSIDE || procclass==CLMAIN || procclass==CLBLOCK)
	        err("Misplaced ENTRY statement", 35);

	     bif = cur_scope();
	     if (bif->variant == FUNC_HEDR) {
	        q = make_function((yyvsp[(2) - (4)].hash_entry), bif->entry.Template.symbol->type, LOCAL);
	        l = construct_entry_list(q, (yyvsp[(3) - (4)].symbol), FUNCTION_NAME); 
             }
             else if ((bif->variant == PROC_HEDR) || 
                      (bif->variant == PROS_HEDR) || /* added for FORTRAN M */
                      (bif->variant == PROG_HEDR)) {
	             q = make_procedure((yyvsp[(2) - (4)].hash_entry),LOCAL);
  	             l = construct_entry_list(q, (yyvsp[(3) - (4)].symbol), PROCEDURE_NAME); 
             }
	     p = get_bfnd(fi,ENTRY_STAT, q, l, (yyvsp[(4) - (4)].ll_node), LLNULL);
	     set_blobs(p, pred_bfnd, SAME_GROUP);
             q->decl = YES;   /*4.02.03*/
             q->entry.proc_decl.proc_hedr = p; /*5.02.03*/
	    ;}
    break;

  case 19:
#line 970 "gram1.y"
    { PTR_SYMB s;
	      PTR_BFND p;
/*
	      s = make_global_entity($3, MODULE_NAME, global_default, NO);
	      s->decl = YES;  
	      p = get_bfnd(fi, MODULE_STMT, s, LLNULL, LLNULL, LLNULL);
	      s->entry.Template.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
*/
	      /*position = IN_MODULE;*/


               s = make_module((yyvsp[(3) - (3)].hash_entry));
	       s->decl = YES;   /* variable declaration has been seen. */
	        if (pred_bfnd != global_bfnd)
		    err("Misplaced MODULE statement", 33);
              p = get_bfnd(fi, MODULE_STMT, s, LLNULL, LLNULL, LLNULL);
	      s->entry.Template.func_hedr = p; /* !!!????*/
	      set_blobs(p, global_bfnd, NEW_GROUP1);
	      add_scope_level(p, NO);	
	      position =  IN_MODULE;    /*IN_PROC*/
              privateall = 0;
            ;}
    break;

  case 20:
#line 996 "gram1.y"
    { newprog(); 
	      if (position == IN_OUTSIDE)
	           position = IN_PROC;
              else if (position != IN_INTERNAL_PROC){ 
                if(!is_interface_stat(pred_bfnd))
	           position--;
              }
              else {
                if(!is_interface_stat(pred_bfnd))
                  err("Internal procedures can not contain procedures",304);
              }
	    ;}
    break;

  case 21:
#line 1011 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, RECURSIVE_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 22:
#line 1013 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, PURE_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 23:
#line 1015 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, ELEMENTAL_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 24:
#line 1019 "gram1.y"
    { PTR_BFND p;

	      (yyval.symbol) = make_procedure((yyvsp[(1) - (1)].hash_entry), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
             /* if (pred_bfnd != global_bfnd)
		 {
	         err("Misplaced SUBROUTINE statement", 34);
		 }  
              */
	      p = get_bfnd(fi,PROC_HEDR, (yyval.symbol), LLNULL, LLNULL, LLNULL);
              (yyval.symbol)->entry.proc_decl.proc_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 25:
#line 1036 "gram1.y"
    { PTR_BFND p;

	      (yyval.symbol) = make_function((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
             /* if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement", 34); */
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, LLNULL, LLNULL);
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 26:
#line 1050 "gram1.y"
    { PTR_BFND p;
             PTR_LLND l;

	      (yyval.symbol) = make_function((yyvsp[(4) - (4)].hash_entry), (yyvsp[(1) - (4)].data_type), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
              l->type = (yyvsp[(1) - (4)].data_type);
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, l, LLNULL);
              (yyval.symbol)->entry.func_decl.func_hedr = p;
            /*  if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement", 34);*/
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
/*
	      $$ = make_function($4, $1, LOCAL);
	      $$->decl = YES;
	      p = get_bfnd(fi,FUNC_HEDR, $$, LLNULL, LLNULL, LLNULL);
              if (pred_bfnd != global_bfnd)
	         errstr("cftn.gram: misplaced SUBROUTINE statement.");
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
*/
           ;}
    break;

  case 27:
#line 1074 "gram1.y"
    { PTR_BFND p;
             PTR_LLND l;
	      (yyval.symbol) = make_function((yyvsp[(5) - (5)].hash_entry), (yyvsp[(1) - (5)].data_type), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              if((yyvsp[(2) - (5)].ll_node)->variant == RECURSIVE_OP)
	         (yyval.symbol)->attr = (yyval.symbol)->attr | RECURSIVE_BIT;
              l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
              l->type = (yyvsp[(1) - (5)].data_type);
             /* if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement", 34);*/
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, l, (yyvsp[(2) - (5)].ll_node));
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 28:
#line 1090 "gram1.y"
    { PTR_BFND p;

	      (yyval.symbol) = make_function((yyvsp[(4) - (4)].hash_entry), TYNULL, LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              if((yyvsp[(1) - (4)].ll_node)->variant == RECURSIVE_OP)
	        (yyval.symbol)->attr = (yyval.symbol)->attr | RECURSIVE_BIT;
              /*if (pred_bfnd != global_bfnd)
	         err("Misplaced FUNCTION statement",34);*/
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, LLNULL, (yyvsp[(1) - (4)].ll_node));
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 29:
#line 1104 "gram1.y"
    { PTR_BFND p;
              PTR_LLND l;
	      (yyval.symbol) = make_function((yyvsp[(5) - (5)].hash_entry), (yyvsp[(2) - (5)].data_type), LOCAL);
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
              if((yyvsp[(1) - (5)].ll_node)->variant == RECURSIVE_OP)
	        (yyval.symbol)->attr = (yyval.symbol)->attr | RECURSIVE_BIT;
              l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
              l->type = (yyvsp[(2) - (5)].data_type);
             /* if (pred_bfnd != global_bfnd)
	          err("Misplaced FUNCTION statement",34);*/
	      p = get_bfnd(fi,FUNC_HEDR, (yyval.symbol), LLNULL, l, (yyvsp[(1) - (5)].ll_node));
              (yyval.symbol)->entry.func_decl.func_hedr = p;
	      set_blobs(p, pred_bfnd, NEW_GROUP1);
              add_scope_level(p, NO);
            ;}
    break;

  case 30:
#line 1122 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 31:
#line 1124 "gram1.y"
    { PTR_SYMB s;
              s = make_scalar((yyvsp[(4) - (5)].hash_entry), TYNULL, LOCAL);
              (yyval.ll_node) = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
            ;}
    break;

  case 32:
#line 1131 "gram1.y"
    { (yyval.hash_entry) = look_up_sym(yytext); ;}
    break;

  case 33:
#line 1134 "gram1.y"
    { (yyval.symbol) = make_program(look_up_sym("_MAIN")); ;}
    break;

  case 34:
#line 1136 "gram1.y"
    {
              (yyval.symbol) = make_program((yyvsp[(1) - (1)].hash_entry));
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
            ;}
    break;

  case 35:
#line 1142 "gram1.y"
    { (yyval.symbol) = make_program(look_up_sym("_BLOCK")); ;}
    break;

  case 36:
#line 1144 "gram1.y"
    {
              (yyval.symbol) = make_program((yyvsp[(1) - (1)].hash_entry)); 
	      (yyval.symbol)->decl = YES;   /* variable declaration has been seen. */
	    ;}
    break;

  case 37:
#line 1151 "gram1.y"
    { (yyval.symbol) = SMNULL; ;}
    break;

  case 38:
#line 1153 "gram1.y"
    { (yyval.symbol) = SMNULL; ;}
    break;

  case 39:
#line 1155 "gram1.y"
    { (yyval.symbol) = (yyvsp[(2) - (3)].symbol); ;}
    break;

  case 41:
#line 1160 "gram1.y"
    { (yyval.symbol) = set_id_list((yyvsp[(1) - (3)].symbol), (yyvsp[(3) - (3)].symbol)); ;}
    break;

  case 42:
#line 1164 "gram1.y"
    {
	      (yyval.symbol) = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, IO);
            ;}
    break;

  case 43:
#line 1168 "gram1.y"
    { (yyval.symbol) = make_scalar(look_up_sym("*"), TYNULL, IO); ;}
    break;

  case 44:
#line 1174 "gram1.y"
    { char *s;

	      s = copyn(yyleng+1, yytext);
	      s[yyleng] = '\0';
	      (yyval.charp) = s;
	    ;}
    break;

  case 45:
#line 1183 "gram1.y"
    { needkwd = 1; ;}
    break;

  case 46:
#line 1187 "gram1.y"
    { needkwd = NO; ;}
    break;

  case 47:
#line 1192 "gram1.y"
    { colon_flag = YES; ;}
    break;

  case 61:
#line 1213 "gram1.y"
    {
	      saveall = YES;
	      (yyval.bf_node) = get_bfnd(fi,SAVE_DECL, SMNULL, LLNULL, LLNULL, LLNULL);
	    ;}
    break;

  case 62:
#line 1218 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,SAVE_DECL, SMNULL, (yyvsp[(4) - (4)].ll_node), LLNULL, LLNULL);
            ;}
    break;

  case 63:
#line 1223 "gram1.y"
    { PTR_LLND p;

	      p = make_llnd(fi,STMT_STR, LLNULL, LLNULL, SMNULL);
	      p->entry.string_val = copys(stmtbuf);
	      (yyval.bf_node) = get_bfnd(fi,FORMAT_STAT, SMNULL, p, LLNULL, LLNULL);
             ;}
    break;

  case 64:
#line 1230 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,PARAM_DECL, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 77:
#line 1246 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, INTERFACE_STMT, SMNULL, LLNULL, LLNULL, LLNULL); 
              add_scope_level((yyval.bf_node), NO);     
            ;}
    break;

  case 78:
#line 1250 "gram1.y"
    { PTR_SYMB s;

	      s = make_procedure((yyvsp[(3) - (3)].hash_entry), LOCAL);
	      s->variant = INTERFACE_NAME;
	      (yyval.bf_node) = get_bfnd(fi, INTERFACE_STMT, s, LLNULL, LLNULL, LLNULL);
              add_scope_level((yyval.bf_node), NO);
	    ;}
    break;

  case 79:
#line 1258 "gram1.y"
    { PTR_SYMB s;

	      s = make_function((yyvsp[(4) - (5)].hash_entry), global_default, LOCAL);
	      s->variant = INTERFACE_NAME;
	      (yyval.bf_node) = get_bfnd(fi, INTERFACE_OPERATOR, s, LLNULL, LLNULL, LLNULL);
              add_scope_level((yyval.bf_node), NO);
	    ;}
    break;

  case 80:
#line 1266 "gram1.y"
    { PTR_SYMB s;


	      s = make_procedure(look_up_sym("="), LOCAL);
	      s->variant = INTERFACE_NAME;
	      (yyval.bf_node) = get_bfnd(fi, INTERFACE_ASSIGNMENT, s, LLNULL, LLNULL, LLNULL);
              add_scope_level((yyval.bf_node), NO);
	    ;}
    break;

  case 81:
#line 1275 "gram1.y"
    { parstate = INDCL;
              (yyval.bf_node) = get_bfnd(fi, CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL); 
	      /*process_interface($$);*/ /*podd 01.02.03*/
              delete_beyond_scope_level(pred_bfnd);
	    ;}
    break;

  case 82:
#line 1283 "gram1.y"
    { (yyval.hash_entry) = look_up_sym(yytext); ;}
    break;

  case 83:
#line 1287 "gram1.y"
    { (yyval.hash_entry) = (yyvsp[(1) - (1)].hash_entry); ;}
    break;

  case 84:
#line 1289 "gram1.y"
    { (yyval.hash_entry) = (yyvsp[(1) - (1)].hash_entry); ;}
    break;

  case 85:
#line 1293 "gram1.y"
    { (yyval.hash_entry) = look_up_op(PLUS); ;}
    break;

  case 86:
#line 1295 "gram1.y"
    { (yyval.hash_entry) = look_up_op(MINUS); ;}
    break;

  case 87:
#line 1297 "gram1.y"
    { (yyval.hash_entry) = look_up_op(ASTER); ;}
    break;

  case 88:
#line 1299 "gram1.y"
    { (yyval.hash_entry) = look_up_op(DASTER); ;}
    break;

  case 89:
#line 1301 "gram1.y"
    { (yyval.hash_entry) = look_up_op(SLASH); ;}
    break;

  case 90:
#line 1303 "gram1.y"
    { (yyval.hash_entry) = look_up_op(DSLASH); ;}
    break;

  case 91:
#line 1305 "gram1.y"
    { (yyval.hash_entry) = look_up_op(AND); ;}
    break;

  case 92:
#line 1307 "gram1.y"
    { (yyval.hash_entry) = look_up_op(OR); ;}
    break;

  case 93:
#line 1309 "gram1.y"
    { (yyval.hash_entry) = look_up_op(XOR); ;}
    break;

  case 94:
#line 1311 "gram1.y"
    { (yyval.hash_entry) = look_up_op(NOT); ;}
    break;

  case 95:
#line 1313 "gram1.y"
    { (yyval.hash_entry) = look_up_op(EQ); ;}
    break;

  case 96:
#line 1315 "gram1.y"
    { (yyval.hash_entry) = look_up_op(NE); ;}
    break;

  case 97:
#line 1317 "gram1.y"
    { (yyval.hash_entry) = look_up_op(GT); ;}
    break;

  case 98:
#line 1319 "gram1.y"
    { (yyval.hash_entry) = look_up_op(GE); ;}
    break;

  case 99:
#line 1321 "gram1.y"
    { (yyval.hash_entry) = look_up_op(LT); ;}
    break;

  case 100:
#line 1323 "gram1.y"
    { (yyval.hash_entry) = look_up_op(LE); ;}
    break;

  case 101:
#line 1325 "gram1.y"
    { (yyval.hash_entry) = look_up_op(NEQV); ;}
    break;

  case 102:
#line 1327 "gram1.y"
    { (yyval.hash_entry) = look_up_op(EQV); ;}
    break;

  case 103:
#line 1332 "gram1.y"
    {
             PTR_SYMB s;
         
             type_var = s = make_derived_type((yyvsp[(4) - (4)].hash_entry), TYNULL, LOCAL);	
             (yyval.bf_node) = get_bfnd(fi, STRUCT_DECL, s, LLNULL, LLNULL, LLNULL);
             add_scope_level((yyval.bf_node), NO);
	   ;}
    break;

  case 104:
#line 1341 "gram1.y"
    { PTR_SYMB s;
         
             type_var = s = make_derived_type((yyvsp[(7) - (7)].hash_entry), TYNULL, LOCAL);	
	     s->attr = s->attr | type_opt;
             (yyval.bf_node) = get_bfnd(fi, STRUCT_DECL, s, (yyvsp[(5) - (7)].ll_node), LLNULL, LLNULL);
             add_scope_level((yyval.bf_node), NO);
	   ;}
    break;

  case 105:
#line 1351 "gram1.y"
    {
	     (yyval.bf_node) = get_bfnd(fi, CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL);
	     if (type_var != SMNULL)
               process_type(type_var, (yyval.bf_node));
             type_var = SMNULL;
	     delete_beyond_scope_level(pred_bfnd);
           ;}
    break;

  case 106:
#line 1359 "gram1.y"
    {
             (yyval.bf_node) = get_bfnd(fi, CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL);
	     if (type_var != SMNULL)
               process_type(type_var, (yyval.bf_node));
             type_var = SMNULL;
	     delete_beyond_scope_level(pred_bfnd);	
           ;}
    break;

  case 107:
#line 1369 "gram1.y"
    { 
	      PTR_LLND q, r, l;
	     /* PTR_SYMB s;*/
	      PTR_TYPE t;
	      int type_opts;

	      vartype = (yyvsp[(1) - (7)].data_type);
              if((yyvsp[(6) - (7)].ll_node) && vartype->variant != T_STRING)
                errstr("Non character entity  %s  has length specification",(yyvsp[(3) - (7)].hash_entry)->ident,41);
              t = make_type_node(vartype, (yyvsp[(6) - (7)].ll_node));
	      type_opts = type_options;
	      if ((yyvsp[(5) - (7)].ll_node)) type_opts = type_opts | DIMENSION_BIT;
	      if ((yyvsp[(5) - (7)].ll_node))
		 q = deal_with_options((yyvsp[(3) - (7)].hash_entry), t, type_opts, (yyvsp[(5) - (7)].ll_node), ndim, (yyvsp[(7) - (7)].ll_node), (yyvsp[(5) - (7)].ll_node));
	      else q = deal_with_options((yyvsp[(3) - (7)].hash_entry), t, type_opts, attr_dims, attr_ndim, (yyvsp[(7) - (7)].ll_node), (yyvsp[(5) - (7)].ll_node));
	      r = make_llnd(fi, EXPR_LIST, q, LLNULL, SMNULL);
	      l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
	      l->type = vartype;
	      (yyval.bf_node) = get_bfnd(fi,VAR_DECL, SMNULL, r, l, (yyvsp[(2) - (7)].ll_node));
	    ;}
    break;

  case 108:
#line 1390 "gram1.y"
    { 
	      PTR_LLND q, r;
	    /*  PTR_SYMB s;*/
              PTR_TYPE t;
	      int type_opts;
              if((yyvsp[(5) - (6)].ll_node) && vartype->variant != T_STRING)
                errstr("Non character entity  %s  has length specification",(yyvsp[(3) - (6)].hash_entry)->ident,41);
              t = make_type_node(vartype, (yyvsp[(5) - (6)].ll_node));
	      type_opts = type_options;
	      if ((yyvsp[(4) - (6)].ll_node)) type_opts = type_opts | DIMENSION_BIT;
	      if ((yyvsp[(4) - (6)].ll_node))
		 q = deal_with_options((yyvsp[(3) - (6)].hash_entry), t, type_opts, (yyvsp[(4) - (6)].ll_node), ndim, (yyvsp[(6) - (6)].ll_node), (yyvsp[(4) - (6)].ll_node));
	      else q = deal_with_options((yyvsp[(3) - (6)].hash_entry), t, type_opts, attr_dims, attr_ndim, (yyvsp[(6) - (6)].ll_node), (yyvsp[(4) - (6)].ll_node));
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (6)].bf_node)->entry.Template.ll_ptr1);
       	    ;}
    break;

  case 109:
#line 1409 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 110:
#line 1411 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 111:
#line 1413 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (5)].ll_node); ;}
    break;

  case 112:
#line 1417 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 113:
#line 1419 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); ;}
    break;

  case 114:
#line 1423 "gram1.y"
    { type_options = type_options | PARAMETER_BIT; 
              (yyval.ll_node) = make_llnd(fi, PARAMETER_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 115:
#line 1427 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 116:
#line 1429 "gram1.y"
    { type_options = type_options | ALLOCATABLE_BIT;
              (yyval.ll_node) = make_llnd(fi, ALLOCATABLE_OP, LLNULL, LLNULL, SMNULL);
	    ;}
    break;

  case 117:
#line 1433 "gram1.y"
    { type_options = type_options | DIMENSION_BIT;
	      attr_ndim = ndim;
	      attr_dims = (yyvsp[(2) - (2)].ll_node);
              (yyval.ll_node) = make_llnd(fi, DIMENSION_OP, (yyvsp[(2) - (2)].ll_node), LLNULL, SMNULL);
            ;}
    break;

  case 118:
#line 1439 "gram1.y"
    { type_options = type_options | EXTERNAL_BIT;
              (yyval.ll_node) = make_llnd(fi, EXTERNAL_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 119:
#line 1443 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (4)].ll_node); ;}
    break;

  case 120:
#line 1445 "gram1.y"
    { type_options = type_options | INTRINSIC_BIT;
              (yyval.ll_node) = make_llnd(fi, INTRINSIC_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 121:
#line 1449 "gram1.y"
    { type_options = type_options | OPTIONAL_BIT;
              (yyval.ll_node) = make_llnd(fi, OPTIONAL_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 122:
#line 1453 "gram1.y"
    { type_options = type_options | POINTER_BIT;
              (yyval.ll_node) = make_llnd(fi, POINTER_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 123:
#line 1457 "gram1.y"
    { type_options = type_options | SAVE_BIT; 
              (yyval.ll_node) = make_llnd(fi, SAVE_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 124:
#line 1461 "gram1.y"
    { type_options = type_options | SAVE_BIT; 
              (yyval.ll_node) = make_llnd(fi, STATIC_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 125:
#line 1465 "gram1.y"
    { type_options = type_options | TARGET_BIT; 
              (yyval.ll_node) = make_llnd(fi, TARGET_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 126:
#line 1471 "gram1.y"
    { type_options = type_options | IN_BIT;  type_opt = IN_BIT; 
              (yyval.ll_node) = make_llnd(fi, IN_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 127:
#line 1475 "gram1.y"
    { type_options = type_options | OUT_BIT;  type_opt = OUT_BIT; 
              (yyval.ll_node) = make_llnd(fi, OUT_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 128:
#line 1479 "gram1.y"
    { type_options = type_options | INOUT_BIT;  type_opt = INOUT_BIT;
              (yyval.ll_node) = make_llnd(fi, INOUT_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 129:
#line 1485 "gram1.y"
    { type_options = type_options | PUBLIC_BIT; 
              type_opt = PUBLIC_BIT;
              (yyval.ll_node) = make_llnd(fi, PUBLIC_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 130:
#line 1490 "gram1.y"
    { type_options =  type_options | PRIVATE_BIT;
               type_opt = PRIVATE_BIT;
              (yyval.ll_node) = make_llnd(fi, PRIVATE_OP, LLNULL, LLNULL, SMNULL);
            ;}
    break;

  case 131:
#line 1497 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(7) - (7)].hash_entry), TYNULL, LOCAL);
	      s->attr = s->attr | type_opt;	
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, INTENT_STMT, SMNULL, r, (yyvsp[(4) - (7)].ll_node), LLNULL);
	    ;}
    break;

  case 132:
#line 1508 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);	
	      s->attr = s->attr | type_opt;
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
  	    ;}
    break;

  case 133:
#line 1521 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(4) - (4)].hash_entry), TYNULL, LOCAL);	
	      s->attr = s->attr | OPTIONAL_BIT;
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, OPTIONAL_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 134:
#line 1532 "gram1.y"
    { 
	      PTR_LLND q, r;
	      PTR_SYMB s;

              s = make_scalar((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);	
	      s->attr = s->attr | OPTIONAL_BIT;
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
  	    ;}
    break;

  case 135:
#line 1545 "gram1.y"
    { 
	      PTR_LLND r;
	      PTR_SYMB s;

              s = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol; 
              s->attr = s->attr | SAVE_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, STATIC_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 136:
#line 1555 "gram1.y"
    { 
	      PTR_LLND r;
	      PTR_SYMB s;

              s = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol;
              s->attr = s->attr | SAVE_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
  	    ;}
    break;

  case 137:
#line 1568 "gram1.y"
    {
	      privateall = 1;
	      (yyval.bf_node) = get_bfnd(fi, PRIVATE_STMT, SMNULL, LLNULL, LLNULL, LLNULL);
	    ;}
    break;

  case 138:
#line 1573 "gram1.y"
    {
	      /*type_options = type_options | PRIVATE_BIT;*/
	      (yyval.bf_node) = get_bfnd(fi, PRIVATE_STMT, SMNULL, (yyvsp[(5) - (5)].ll_node), LLNULL, LLNULL);
            ;}
    break;

  case 139:
#line 1579 "gram1.y"
    {type_opt = PRIVATE_BIT;;}
    break;

  case 140:
#line 1583 "gram1.y"
    { 
	      (yyval.bf_node) = get_bfnd(fi, SEQUENCE_STMT, SMNULL, LLNULL, LLNULL, LLNULL);
            ;}
    break;

  case 141:
#line 1588 "gram1.y"
    {
	      /*saveall = YES;*/ /*14.03.03*/
	      (yyval.bf_node) = get_bfnd(fi, PUBLIC_STMT, SMNULL, LLNULL, LLNULL, LLNULL);
	    ;}
    break;

  case 142:
#line 1593 "gram1.y"
    {
	      /*type_options = type_options | PUBLIC_BIT;*/
	      (yyval.bf_node) = get_bfnd(fi, PUBLIC_STMT, SMNULL, (yyvsp[(5) - (5)].ll_node), LLNULL, LLNULL);
            ;}
    break;

  case 143:
#line 1599 "gram1.y"
    {type_opt = PUBLIC_BIT;;}
    break;

  case 144:
#line 1603 "gram1.y"
    {
	      type_options = 0;
              /* following block added by dbg */
	      ndim = 0;
	      attr_ndim = 0;
	      attr_dims = LLNULL;
	      /* end section added by dbg */
              (yyval.data_type) = make_type_node((yyvsp[(1) - (4)].data_type), (yyvsp[(3) - (4)].ll_node));
            ;}
    break;

  case 145:
#line 1613 "gram1.y"
    { PTR_TYPE t;

	      type_options = 0;
	      ndim = 0;
	      attr_ndim = 0;
	      attr_dims = LLNULL;
              t = lookup_type((yyvsp[(3) - (5)].hash_entry));
	      vartype = t;
	      (yyval.data_type) = make_type_node(t, LLNULL);
            ;}
    break;

  case 146:
#line 1626 "gram1.y"
    {opt_kwd_hedr = YES;;}
    break;

  case 147:
#line 1631 "gram1.y"
    { PTR_TYPE p;
	      PTR_LLND q;
	      PTR_SYMB s;
              s = (yyvsp[(2) - (2)].hash_entry)->id_attr;
	      if (s)
		   s->attr = (yyvsp[(1) - (2)].token);
	      else {
		p = undeftype ? global_unknown : impltype[*(yyvsp[(2) - (2)].hash_entry)->ident - 'a'];
                s = install_entry((yyvsp[(2) - (2)].hash_entry), SOFT);
		s->attr = (yyvsp[(1) - (2)].token);
                set_type(s, p, LOCAL);
	      }
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(2) - (2)].hash_entry)->id_attr);
	      q = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,ATTR_DECL, SMNULL, q, LLNULL, LLNULL);
	    ;}
    break;

  case 148:
#line 1650 "gram1.y"
    { PTR_TYPE p;
	      PTR_LLND q, r;
	      PTR_SYMB s;
	      int att;

	      att = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1->entry.Template.ll_ptr1->
		    entry.Template.symbol->attr;
              s = (yyvsp[(3) - (3)].hash_entry)->id_attr;
	      if (s)
		   s->attr = att;
	      else {
		p = undeftype ? global_unknown : impltype[*(yyvsp[(3) - (3)].hash_entry)->ident - 'a'];
                s = install_entry((yyvsp[(3) - (3)].hash_entry), SOFT);
		s->attr = att;
                set_type(s, p, LOCAL);
	      }
	      q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].hash_entry)->id_attr);
	      for (r = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1;
		   r->entry.list.next;
		   r = r->entry.list.next) ;
	      r->entry.list.next = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);

	    ;}
    break;

  case 149:
#line 1676 "gram1.y"
    { (yyval.token) = ATT_GLOBAL; ;}
    break;

  case 150:
#line 1678 "gram1.y"
    { (yyval.token) = ATT_CLUSTER; ;}
    break;

  case 151:
#line 1690 "gram1.y"
    {
/*		  varleng = ($1<0 || $1==TYLONG ? 0 : typesize[$1]); */
		  vartype = (yyvsp[(1) - (1)].data_type);
		;}
    break;

  case 152:
#line 1697 "gram1.y"
    { (yyval.data_type) = global_int; ;}
    break;

  case 153:
#line 1698 "gram1.y"
    { (yyval.data_type) = global_float; ;}
    break;

  case 154:
#line 1699 "gram1.y"
    { (yyval.data_type) = global_complex; ;}
    break;

  case 155:
#line 1700 "gram1.y"
    { (yyval.data_type) = global_double; ;}
    break;

  case 156:
#line 1701 "gram1.y"
    { (yyval.data_type) = global_dcomplex; ;}
    break;

  case 157:
#line 1702 "gram1.y"
    { (yyval.data_type) = global_bool; ;}
    break;

  case 158:
#line 1703 "gram1.y"
    { (yyval.data_type) = global_string; ;}
    break;

  case 159:
#line 1708 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 160:
#line 1710 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 161:
#line 1714 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, LEN_OP, (yyvsp[(3) - (5)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 162:
#line 1716 "gram1.y"
    { PTR_LLND l;

                 l = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL); 
                 l->entry.string_val = (char *)"*";
                 (yyval.ll_node) = make_llnd(fi, LEN_OP, l,l, SMNULL);
                ;}
    break;

  case 163:
#line 1723 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi, LEN_OP, (yyvsp[(5) - (6)].ll_node), (yyvsp[(5) - (6)].ll_node), SMNULL);;}
    break;

  case 164:
#line 1727 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 165:
#line 1729 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 166:
#line 1731 "gram1.y"
    { /*$$ = make_llnd(fi, PAREN_OP, $2, LLNULL, SMNULL);*/  (yyval.ll_node) = (yyvsp[(3) - (5)].ll_node);  ;}
    break;

  case 167:
#line 1739 "gram1.y"
    { if((yyvsp[(7) - (9)].ll_node)->variant==LENGTH_OP && (yyvsp[(3) - (9)].ll_node)->variant==(yyvsp[(7) - (9)].ll_node)->variant)
                (yyvsp[(7) - (9)].ll_node)->variant=KIND_OP;
                (yyval.ll_node) = make_llnd(fi, CONS, (yyvsp[(3) - (9)].ll_node), (yyvsp[(7) - (9)].ll_node), SMNULL); 
            ;}
    break;

  case 168:
#line 1746 "gram1.y"
    { if(vartype->variant == T_STRING)
                (yyval.ll_node) = make_llnd(fi,LENGTH_OP,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL);
              else
                (yyval.ll_node) = make_llnd(fi,KIND_OP,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL);
            ;}
    break;

  case 169:
#line 1752 "gram1.y"
    { PTR_LLND l;
	      l = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	      l->entry.string_val = (char *)"*";
              (yyval.ll_node) = make_llnd(fi,LENGTH_OP,l,LLNULL,SMNULL);
            ;}
    break;

  case 170:
#line 1758 "gram1.y"
    { /* $$ = make_llnd(fi, SPEC_PAIR, $2, LLNULL, SMNULL); */
	     char *q;
             q = (yyvsp[(1) - (2)].ll_node)->entry.string_val;
  	     if (strcmp(q, "len") == 0)
               (yyval.ll_node) = make_llnd(fi,LENGTH_OP,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
             else
                (yyval.ll_node) = make_llnd(fi,KIND_OP,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);              
            ;}
    break;

  case 171:
#line 1767 "gram1.y"
    { PTR_LLND l;
	      l = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	      l->entry.string_val = (char *)"*";
              (yyval.ll_node) = make_llnd(fi,LENGTH_OP,l,LLNULL,SMNULL);
            ;}
    break;

  case 172:
#line 1775 "gram1.y"
    {endioctl();;}
    break;

  case 173:
#line 1788 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 174:
#line 1790 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node); ;}
    break;

  case 175:
#line 1793 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, POINTST_OP, LLNULL, (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 176:
#line 1797 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! (yyvsp[(5) - (5)].ll_node)) {
		err("No dimensions in DIMENSION statement", 42);
	      }
              if(statement_kind == 1) /*DVM-directive*/
                err("No shape specification", 65);                
	      s = make_array((yyvsp[(4) - (5)].hash_entry), TYNULL, (yyvsp[(5) - (5)].ll_node), ndim, LOCAL);
	      s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(5) - (5)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(5) - (5)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,DIM_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 177:
#line 1812 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! (yyvsp[(4) - (4)].ll_node)) {
		err("No dimensions in DIMENSION statement", 42);
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
	      s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 178:
#line 1828 "gram1.y"
    {/* PTR_SYMB s;*/
	      PTR_LLND r;

	         /*if(!$5) {
		   err("No dimensions in ALLOCATABLE statement",305);		
	           }
	          s = make_array($4, TYNULL, $5, ndim, LOCAL);
	          s->attr = s->attr | ALLOCATABLE_BIT;
	          q = make_llnd(fi,ARRAY_REF, $5, LLNULL, s);
	          s->type->entry.ar_decl.ranges = $5;
                  r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                */
              (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr | ALLOCATABLE_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, ALLOCATABLE_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 179:
#line 1846 "gram1.y"
    {  /*PTR_SYMB s;*/
	      PTR_LLND r;

	        /*  if(! $4) {
		      err("No dimensions in ALLOCATABLE statement",305);
		
	            }
	           s = make_array($3, TYNULL, $4, ndim, LOCAL);
	           s->attr = s->attr | ALLOCATABLE_BIT;
	           q = make_llnd(fi,ARRAY_REF, $4, LLNULL, s);
	           s->type->entry.ar_decl.ranges = $4;
	           r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                */
              (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr | ALLOCATABLE_BIT;
              r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 180:
#line 1866 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND  r;
           
	          /*  if(! $5) {
		      err("No dimensions in POINTER statement",306);	    
	              } 
	             s = make_array($4, TYNULL, $5, ndim, LOCAL);
	             s->attr = s->attr | POINTER_BIT;
	             q = make_llnd(fi,ARRAY_REF, $5, LLNULL, s);
	             s->type->entry.ar_decl.ranges = $5;
                   */

                  /*s = make_pointer( $4->entry.Template.symbol->parent, TYNULL, LOCAL);*/ /*17.02.03*/
                 /*$4->entry.Template.symbol->attr = $4->entry.Template.symbol->attr | POINTER_BIT;*/
              s = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol; /*17.02.03*/
              s->attr = s->attr | POINTER_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, POINTER_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 181:
#line 1886 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND r;

     	        /*  if(! $4) {
	        	err("No dimensions in POINTER statement",306);
	            }
	           s = make_array($3, TYNULL, $4, ndim, LOCAL);
	           s->attr = s->attr | POINTER_BIT;
	           q = make_llnd(fi,ARRAY_REF, $4, LLNULL, s);
	           s->type->entry.ar_decl.ranges = $4;
                */

                /*s = make_pointer( $3->entry.Template.symbol->parent, TYNULL, LOCAL);*/ /*17.02.03*/
                /*$3->entry.Template.symbol->attr = $3->entry.Template.symbol->attr | POINTER_BIT;*/
              s = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol; /*17.02.03*/
              s->attr = s->attr | POINTER_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 182:
#line 1908 "gram1.y"
    {/* PTR_SYMB s;*/
	      PTR_LLND r;


	     /* if(! $5) {
		err("No dimensions in TARGET statement",307);
	      }
	      s = make_array($4, TYNULL, $5, ndim, LOCAL);
	      s->attr = s->attr | TARGET_BIT;
	      q = make_llnd(fi,ARRAY_REF, $5, LLNULL, s);
	      s->type->entry.ar_decl.ranges = $5;
             */
              (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr = (yyvsp[(4) - (4)].ll_node)->entry.Template.symbol->attr | TARGET_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi, TARGET_STMT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 183:
#line 1925 "gram1.y"
    {  /*PTR_SYMB s;*/
	      PTR_LLND r;

	     /* if(! $4) {
		err("No dimensions in TARGET statement",307);
	      }
	      s = make_array($3, TYNULL, $4, ndim, LOCAL);
	      s->attr = s->attr | TARGET_BIT;
	      q = make_llnd(fi,ARRAY_REF, $4, LLNULL, s);
	      s->type->entry.ar_decl.ranges = $4;
              */
              (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr = (yyvsp[(3) - (3)].ll_node)->entry.Template.symbol->attr | TARGET_BIT;
	      r = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 184:
#line 1943 "gram1.y"
    { PTR_LLND p, q;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,COMM_STAT, SMNULL, q, LLNULL, LLNULL);
	    ;}
    break;

  case 185:
#line 1950 "gram1.y"
    { PTR_LLND p, q;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, (yyvsp[(3) - (4)].symbol));
	      (yyval.bf_node) = get_bfnd(fi,COMM_STAT, SMNULL, q, LLNULL, LLNULL);
	    ;}
    break;

  case 186:
#line 1957 "gram1.y"
    { PTR_LLND p, q;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(5) - (5)].ll_node), LLNULL, SMNULL);
	      q = make_llnd(fi,COMM_LIST, p, LLNULL, (yyvsp[(3) - (5)].symbol));
	      add_to_lowList(q, (yyvsp[(1) - (5)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 187:
#line 1964 "gram1.y"
    { PTR_LLND p, r;

              p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      /*q = make_llnd(fi,COMM_LIST, p, LLNULL, SMNULL);*/
	      for (r = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1;
		   r->entry.list.next;
		   r = r->entry.list.next);
	      add_to_lowLevelList(p, r->entry.Template.ll_ptr1);
	    ;}
    break;

  case 188:
#line 1977 "gram1.y"
    { PTR_LLND q, r;

              q = make_llnd(fi,EXPR_LIST, (yyvsp[(4) - (4)].ll_node), LLNULL, SMNULL);
	      r = make_llnd(fi,NAMELIST_LIST, q, LLNULL, (yyvsp[(3) - (4)].symbol));
	      (yyval.bf_node) = get_bfnd(fi,NAMELIST_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 189:
#line 1984 "gram1.y"
    { PTR_LLND q, r;

              q = make_llnd(fi,EXPR_LIST, (yyvsp[(5) - (5)].ll_node), LLNULL, SMNULL);
	      r = make_llnd(fi,NAMELIST_LIST, q, LLNULL, (yyvsp[(3) - (5)].symbol));
	      add_to_lowList(r, (yyvsp[(1) - (5)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 190:
#line 1991 "gram1.y"
    { PTR_LLND q, r;

              q = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      for (r = (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1;
		   r->entry.list.next;
		   r = r->entry.list.next);
	      add_to_lowLevelList(q, r->entry.Template.ll_ptr1);
	    ;}
    break;

  case 191:
#line 2002 "gram1.y"
    { (yyval.symbol) =  make_local_entity((yyvsp[(2) - (3)].hash_entry), NAMELIST_NAME,global_default,LOCAL); ;}
    break;

  case 192:
#line 2006 "gram1.y"
    { (yyval.symbol) = NULL; /*make_common(look_up_sym("*"));*/ ;}
    break;

  case 193:
#line 2008 "gram1.y"
    { (yyval.symbol) = make_common((yyvsp[(2) - (3)].hash_entry)); ;}
    break;

  case 194:
#line 2013 "gram1.y"
    {  PTR_SYMB s;
	
	      if((yyvsp[(2) - (2)].ll_node)) {
		s = make_array((yyvsp[(1) - (2)].hash_entry), TYNULL, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
                s->attr = s->attr | DIMENSION_BIT;
		s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
		(yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);
	      }
	      else {
		s = make_scalar((yyvsp[(1) - (2)].hash_entry), TYNULL, LOCAL);	
		(yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
	      }

          ;}
    break;

  case 195:
#line 2031 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_external((yyvsp[(4) - (4)].hash_entry), TYNULL);
	      s->attr = s->attr | EXTERNAL_BIT;
              q = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      p = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,EXTERN_STAT, SMNULL, p, LLNULL, LLNULL);
	    ;}
    break;

  case 196:
#line 2042 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_external((yyvsp[(3) - (3)].hash_entry), TYNULL);
	      s->attr = s->attr | EXTERNAL_BIT;
              p = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      q = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 197:
#line 2054 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_intrinsic((yyvsp[(4) - (4)].hash_entry), TYNULL); /*make_function($3, TYNULL, NO);*/
	      s->attr = s->attr | INTRINSIC_BIT;
              q = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      p = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,INTRIN_STAT, SMNULL, p,
			     LLNULL, LLNULL);
	    ;}
    break;

  case 198:
#line 2066 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_intrinsic((yyvsp[(3) - (3)].hash_entry), TYNULL); /* make_function($3, TYNULL, NO);*/
	      s->attr = s->attr | INTRINSIC_BIT;
              p = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      q = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 199:
#line 2080 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,EQUI_STAT, SMNULL, (yyvsp[(3) - (3)].ll_node),
			     LLNULL, LLNULL);
	    ;}
    break;

  case 200:
#line 2086 "gram1.y"
    { 
	      add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	    ;}
    break;

  case 201:
#line 2093 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,EQUI_LIST, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL);
           ;}
    break;

  case 202:
#line 2099 "gram1.y"
    { PTR_LLND p;
	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(1) - (3)].ll_node), p, SMNULL);
	    ;}
    break;

  case 203:
#line 2105 "gram1.y"
    { PTR_LLND p;

	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(p, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 204:
#line 2113 "gram1.y"
    {  PTR_SYMB s;
           s=make_scalar((yyvsp[(1) - (1)].hash_entry),TYNULL,LOCAL);
           (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
           s->attr = s->attr | EQUIVALENCE_BIT;
            /*$$=$1; $$->entry.Template.symbol->attr = $$->entry.Template.symbol->attr | EQUIVALENCE_BIT; */
        ;}
    break;

  case 205:
#line 2120 "gram1.y"
    {  PTR_SYMB s;
           s=make_array((yyvsp[(1) - (4)].hash_entry),TYNULL,LLNULL,0,LOCAL);
           (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, s);
           s->attr = s->attr | EQUIVALENCE_BIT;
            /*$$->entry.Template.symbol->attr = $$->entry.Template.symbol->attr | EQUIVALENCE_BIT; */
        ;}
    break;

  case 207:
#line 2139 "gram1.y"
    { PTR_LLND p;
              data_stat = NO;
	      p = make_llnd(fi,STMT_STR, LLNULL, LLNULL,
			    SMNULL);
              p->entry.string_val = copys(stmtbuf);
	      (yyval.bf_node) = get_bfnd(fi,DATA_DECL, SMNULL, p, LLNULL, LLNULL);
            ;}
    break;

  case 210:
#line 2153 "gram1.y"
    {data_stat = YES;;}
    break;

  case 211:
#line 2157 "gram1.y"
    {
	      if (parstate == OUTSIDE)
	         { PTR_BFND p;

		   p = get_bfnd(fi,PROG_HEDR,
                                make_program(look_up_sym("_MAIN")),
                                LLNULL, LLNULL, LLNULL);
		   set_blobs(p, global_bfnd, NEW_GROUP1);
	           add_scope_level(p, NO);
		   position = IN_PROC; 
	  	   /*parstate = INDCL;*/
                 }
	      if(parstate < INDCL)
		{
		  /* enddcl();*/
		  parstate = INDCL;
		}
	    ;}
    break;

  case 222:
#line 2202 "gram1.y"
    {;;}
    break;

  case 223:
#line 2206 "gram1.y"
    { (yyval.symbol)= make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);;}
    break;

  case 224:
#line 2210 "gram1.y"
    { (yyval.symbol)= make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL); 
              (yyval.symbol)->attr = (yyval.symbol)->attr | DATA_BIT; 
            ;}
    break;

  case 225:
#line 2216 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_SUBS, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 226:
#line 2220 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_RANGE, (yyvsp[(2) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), SMNULL); ;}
    break;

  case 227:
#line 2224 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 228:
#line 2226 "gram1.y"
    { (yyval.ll_node) = add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].ll_node)); ;}
    break;

  case 229:
#line 2230 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 230:
#line 2232 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 231:
#line 2236 "gram1.y"
    {(yyval.ll_node)= make_llnd(fi, DATA_IMPL_DO, (yyvsp[(2) - (7)].ll_node), (yyvsp[(6) - (7)].ll_node), (yyvsp[(4) - (7)].symbol)); ;}
    break;

  case 232:
#line 2240 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 233:
#line 2242 "gram1.y"
    { (yyval.ll_node) = add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].ll_node)); ;}
    break;

  case 234:
#line 2246 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(2) - (2)].ll_node), LLNULL, (yyvsp[(1) - (2)].symbol)); ;}
    break;

  case 235:
#line 2248 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(2) - (2)].ll_node), LLNULL, (yyvsp[(1) - (2)].symbol)); ;}
    break;

  case 236:
#line 2250 "gram1.y"
    {
              (yyvsp[(2) - (3)].ll_node)->entry.Template.ll_ptr2 = (yyvsp[(3) - (3)].ll_node);
              (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(2) - (3)].ll_node), LLNULL, (yyvsp[(1) - (3)].symbol)); 
            ;}
    break;

  case 237:
#line 2255 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DATA_ELT, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 251:
#line 2279 "gram1.y"
    {if((yyvsp[(2) - (6)].ll_node)->entry.Template.symbol->variant != TYPE_NAME)
               errstr("Undefined type %s",(yyvsp[(2) - (6)].ll_node)->entry.Template.symbol->ident,319); 
           ;}
    break;

  case 268:
#line 2324 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ICON_EXPR, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 269:
#line 2326 "gram1.y"
    {
              PTR_LLND p;

              p = intrinsic_op_node("+", UNARY_ADD_OP, (yyvsp[(2) - (2)].ll_node), LLNULL);
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 270:
#line 2333 "gram1.y"
    {
              PTR_LLND p;
 
              p = intrinsic_op_node("-", MINUS_OP, (yyvsp[(2) - (2)].ll_node), LLNULL);
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 271:
#line 2340 "gram1.y"
    {
              PTR_LLND p;
 
              p = intrinsic_op_node("+", ADD_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node));
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 272:
#line 2347 "gram1.y"
    {
              PTR_LLND p;
 
              p = intrinsic_op_node("-", SUBT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node));
              (yyval.ll_node) = make_llnd(fi,ICON_EXPR, p, LLNULL, SMNULL);
            ;}
    break;

  case 273:
#line 2356 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 274:
#line 2358 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("*", MULT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 275:
#line 2360 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("/", DIV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 276:
#line 2364 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 277:
#line 2366 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("**", EXP_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 278:
#line 2370 "gram1.y"
    {
              PTR_LLND p;

              p = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
              p->entry.ival = atoi(yytext);
              p->type = global_int;
              (yyval.ll_node) = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
            ;}
    break;

  case 279:
#line 2379 "gram1.y"
    {
              PTR_LLND p;
 
              p = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol));
              (yyval.ll_node) = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
            ;}
    break;

  case 280:
#line 2386 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(2) - (3)].ll_node), LLNULL, SMNULL);
            ;}
    break;

  case 281:
#line 2393 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 282:
#line 2395 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 283:
#line 2399 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
             (yyval.ll_node)->entry.Template.symbol->attr = (yyval.ll_node)->entry.Template.symbol->attr | SAVE_BIT;
           ;}
    break;

  case 284:
#line 2403 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,COMM_LIST, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol)); 
            (yyval.ll_node)->entry.Template.symbol->attr = (yyval.ll_node)->entry.Template.symbol->attr | SAVE_BIT;
          ;}
    break;

  case 285:
#line 2409 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(2) - (3)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 286:
#line 2411 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), EXPR_LIST); ;}
    break;

  case 287:
#line 2415 "gram1.y"
    { as_op_kwd_ = YES; ;}
    break;

  case 288:
#line 2419 "gram1.y"
    { as_op_kwd_ = NO; ;}
    break;

  case 289:
#line 2424 "gram1.y"
    { 
             PTR_SYMB s; 
             s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);	
	     s->attr = s->attr | type_opt;
	     (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
            ;}
    break;

  case 290:
#line 2431 "gram1.y"
    { PTR_SYMB s;
	      s = make_function((yyvsp[(3) - (4)].hash_entry), global_default, LOCAL);
	      s->variant = INTERFACE_NAME;
              s->attr = s->attr | type_opt;
              (yyval.ll_node) = make_llnd(fi,OPERATOR_OP, LLNULL, LLNULL, s);
	    ;}
    break;

  case 291:
#line 2438 "gram1.y"
    { PTR_SYMB s;
	      s = make_procedure(look_up_sym("="), LOCAL);
	      s->variant = INTERFACE_NAME;
              s->attr = s->attr | type_opt;
              (yyval.ll_node) = make_llnd(fi,ASSIGNMENT_OP, LLNULL, LLNULL, s);
	    ;}
    break;

  case 292:
#line 2448 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 293:
#line 2450 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 294:
#line 2454 "gram1.y"
    { PTR_SYMB p;

                /* The check if name and expr have compatible types has
                   not been done yet. */ 
		p = make_constant((yyvsp[(1) - (3)].hash_entry), TYNULL);
 	        p->attr = p->attr | PARAMETER_BIT;
                p->entry.const_value = (yyvsp[(3) - (3)].ll_node);
		(yyval.ll_node) = make_llnd(fi,CONST_REF, LLNULL, LLNULL, p);
	    ;}
    break;

  case 295:
#line 2466 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, MODULE_PROC_STMT, SMNULL, (yyvsp[(2) - (2)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 296:
#line 2469 "gram1.y"
    { PTR_SYMB s;
 	      PTR_LLND q;

	      s = make_function((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	      s->variant = ROUTINE_NAME;
              q = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	    ;}
    break;

  case 297:
#line 2478 "gram1.y"
    { PTR_LLND p, q;
              PTR_SYMB s;

	      s = make_function((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);
	      s->variant = ROUTINE_NAME;
              p = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
	      q = make_llnd(fi,EXPR_LIST, p, LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 298:
#line 2491 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL);
              /*add_scope_level($3->entry.Template.func_hedr, YES);*/ /*17.06.01*/
              copy_module_scope((yyvsp[(3) - (3)].symbol),LLNULL); /*17.03.03*/
              colon_flag = NO;
            ;}
    break;

  case 299:
#line 2497 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (6)].symbol), (yyvsp[(6) - (6)].ll_node), LLNULL, LLNULL); 
              /*add_scope_level(module_scope, YES); *//* 17.06.01*/
              copy_module_scope((yyvsp[(3) - (6)].symbol),(yyvsp[(6) - (6)].ll_node)); /*17.03.03 */
              colon_flag = NO;
            ;}
    break;

  case 300:
#line 2503 "gram1.y"
    { PTR_LLND l;

	      l = make_llnd(fi, ONLY_NODE, LLNULL, LLNULL, SMNULL);
              (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (6)].symbol), l, LLNULL, LLNULL);
            ;}
    break;

  case 301:
#line 2509 "gram1.y"
    { PTR_LLND l;

	      l = make_llnd(fi, ONLY_NODE, (yyvsp[(7) - (7)].ll_node), LLNULL, SMNULL);
              (yyval.bf_node) = get_bfnd(fi, USE_STMT, (yyvsp[(3) - (7)].symbol), l, LLNULL, LLNULL);
            ;}
    break;

  case 302:
#line 2517 "gram1.y"
    {
              if ((yyvsp[(1) - (1)].hash_entry)->id_attr == SMNULL)
	         warn1("Unknown module %s", (yyvsp[(1) - (1)].hash_entry)->ident,308);
              (yyval.symbol) = make_global_entity((yyvsp[(1) - (1)].hash_entry), MODULE_NAME, global_default, NO);
	      module_scope = (yyval.symbol)->entry.Template.func_hedr;
           
            ;}
    break;

  case 303:
#line 2527 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 304:
#line 2529 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 305:
#line 2533 "gram1.y"
    {  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 306:
#line 2535 "gram1.y"
    {  PTR_HASH oldhash,copyhash;
	       PTR_SYMB oldsym, newsym;
	       PTR_LLND m;

	       oldhash = just_look_up_sym_in_scope(module_scope, (yyvsp[(1) - (1)].hash_entry)->ident);
	       if (oldhash == HSNULL) {
                  errstr("Unknown identifier %s.", (yyvsp[(1) - (1)].hash_entry)->ident,309);
	          (yyval.ll_node)= LLNULL;
	       }
	       else {
                 oldsym = oldhash->id_attr;
                 copyhash=just_look_up_sym_in_scope(cur_scope(), (yyvsp[(1) - (1)].hash_entry)->ident);
	         if( copyhash && copyhash->id_attr && copyhash->id_attr->entry.Template.tag==module_scope->id)
                 {
                   newsym = copyhash->id_attr;
                   newsym->entry.Template.tag = 0;
                 }
                 else
                 {
	           newsym = make_local_entity((yyvsp[(1) - (1)].hash_entry), oldsym->variant, oldsym->type,LOCAL);
	           /* copies data in entry.Template structure and attr */
	           copy_sym_data(oldsym, newsym);	         
	             /*newsym->entry.Template.base_name = oldsym;*//*19.03.03*/
                 }
	  	/* l = make_llnd(fi, VAR_REF, LLNULL, LLNULL, oldsym);*/
		 m = make_llnd(fi, VAR_REF, LLNULL, LLNULL, newsym);
		 (yyval.ll_node) = make_llnd(fi, RENAME_NODE, m, LLNULL, oldsym);
 	      }
            ;}
    break;

  case 307:
#line 2568 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 308:
#line 2570 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 309:
#line 2574 "gram1.y"
    {  PTR_HASH oldhash,copyhash;
	       PTR_SYMB oldsym, newsym;
	       PTR_LLND l, m;

	       oldhash = just_look_up_sym_in_scope(module_scope, (yyvsp[(3) - (3)].hash_entry)->ident);
	       if (oldhash == HSNULL) {
                  errstr("Unknown identifier %s", (yyvsp[(3) - (3)].hash_entry)->ident,309);
	          (yyval.ll_node)= LLNULL;
	       }
	       else {
                 oldsym = oldhash->id_attr;
                 copyhash = just_look_up_sym_in_scope(cur_scope(), (yyvsp[(3) - (3)].hash_entry)->ident);
	         if(copyhash && copyhash->id_attr && copyhash->id_attr->entry.Template.tag==module_scope->id)
                 {
                    delete_symbol(copyhash->id_attr);
                    copyhash->id_attr = SMNULL;
                 }
                   newsym = make_local_entity((yyvsp[(1) - (3)].hash_entry), oldsym->variant, oldsym->type, LOCAL);
	           /* copies data in entry.Template structure and attr */
	           copy_sym_data(oldsym, newsym);	
                         
	           /*newsym->entry.Template.base_name = oldsym;*//*19.03.03*/
	  	 l  = make_llnd(fi, VAR_REF, LLNULL, LLNULL, oldsym);
		 m  = make_llnd(fi, VAR_REF, LLNULL, LLNULL, newsym);
		 (yyval.ll_node) = make_llnd(fi, RENAME_NODE, m, l, SMNULL);
 	      }
            ;}
    break;

  case 310:
#line 2612 "gram1.y"
    { ndim = 0;	explicit_shape = 1; (yyval.ll_node) = LLNULL; ;}
    break;

  case 311:
#line 2614 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 312:
#line 2617 "gram1.y"
    { ndim = 0; explicit_shape = 1;;}
    break;

  case 313:
#line 2618 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, (yyvsp[(2) - (2)].ll_node), LLNULL, SMNULL);
	      (yyval.ll_node)->type = global_default;
	    ;}
    break;

  case 314:
#line 2623 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 315:
#line 2627 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
	      ++ndim;
	    ;}
    break;

  case 316:
#line 2635 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = make_llnd(fi, DDOT, LLNULL, LLNULL, SMNULL);
	      ++ndim;
              explicit_shape = 0;
	    ;}
    break;

  case 317:
#line 2644 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (2)].ll_node), LLNULL, SMNULL);
	      ++ndim;
              explicit_shape = 0;
	    ;}
    break;

  case 318:
#line 2653 "gram1.y"
    {
	      if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		(yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      ++ndim;
	    ;}
    break;

  case 319:
#line 2663 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,STAR_RANGE, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->type = global_default;
              explicit_shape = 0;
	    ;}
    break;

  case 321:
#line 2672 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 322:
#line 2674 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 323:
#line 2678 "gram1.y"
    {PTR_LABEL p;
	     p = make_label_node(fi,convci(yyleng, yytext));
	     p->scope = cur_scope();
	     (yyval.ll_node) = make_llnd_label(fi,LABEL_REF, p);
	  ;}
    break;

  case 324:
#line 2686 "gram1.y"
    { /*PTR_LLND l;*/

          /*   l = make_llnd(fi, EXPR_LIST, $3, LLNULL, SMNULL);*/
             (yyval.bf_node) = get_bfnd(fi,IMPL_DECL, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);
             redefine_func_arg_type();
           ;}
    break;

  case 325:
#line 2701 "gram1.y"
    { /*undeftype = YES;
	    setimpl(TYNULL, (int)'a', (int)'z'); FB COMMENTED---> NOT QUITE RIGHT BUT AVOID PB WITH COMMON*/
	    (yyval.bf_node) = get_bfnd(fi,IMPL_DECL, SMNULL, LLNULL, LLNULL, LLNULL);
	  ;}
    break;

  case 326:
#line 2708 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 327:
#line 2710 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 328:
#line 2714 "gram1.y"
    { 

            (yyval.ll_node) = make_llnd(fi, IMPL_TYPE, (yyvsp[(3) - (4)].ll_node), LLNULL, SMNULL);
            (yyval.ll_node)->type = vartype;
          ;}
    break;

  case 329:
#line 2729 "gram1.y"
    { implkwd = YES; ;}
    break;

  case 330:
#line 2730 "gram1.y"
    { vartype = (yyvsp[(2) - (2)].data_type); ;}
    break;

  case 331:
#line 2734 "gram1.y"
    { (yyval.data_type) = (yyvsp[(2) - (2)].data_type); ;}
    break;

  case 332:
#line 2736 "gram1.y"
    { (yyval.data_type) = (yyvsp[(1) - (1)].data_type);;}
    break;

  case 333:
#line 2748 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 334:
#line 2750 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 335:
#line 2754 "gram1.y"
    {
	      setimpl(vartype, (int)(yyvsp[(1) - (1)].charv), (int)(yyvsp[(1) - (1)].charv));
	      (yyval.ll_node) = make_llnd(fi,CHAR_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.cval = (yyvsp[(1) - (1)].charv);
	    ;}
    break;

  case 336:
#line 2760 "gram1.y"
    { PTR_LLND p,q;
	      
	      setimpl(vartype, (int)(yyvsp[(1) - (3)].charv), (int)(yyvsp[(3) - (3)].charv));
	      p = make_llnd(fi,CHAR_VAL, LLNULL, LLNULL, SMNULL);
	      p->entry.cval = (yyvsp[(1) - (3)].charv);
	      q = make_llnd(fi,CHAR_VAL, LLNULL, LLNULL, SMNULL);
	      q->entry.cval = (yyvsp[(3) - (3)].charv);
	      (yyval.ll_node)= make_llnd(fi,DDOT, p, q, SMNULL);
	    ;}
    break;

  case 337:
#line 2772 "gram1.y"
    {
	      if(yyleng!=1 || yytext[0]<'a' || yytext[0]>'z')
		{
		  err("IMPLICIT item must be single letter", 37);
		  (yyval.charv) = '\0';
		}
	      else (yyval.charv) = yytext[0];
	    ;}
    break;

  case 338:
#line 2783 "gram1.y"
    {
	      if (parstate == OUTSIDE)
	         { PTR_BFND p;

		   p = get_bfnd(fi,PROG_HEDR,
                                make_program(look_up_sym("_MAIN")),
                                LLNULL, LLNULL, LLNULL);
		   set_blobs(p, global_bfnd, NEW_GROUP1);
	           add_scope_level(p, NO);
		   position = IN_PROC; 
	  	   parstate = INSIDE;
                 }
	  
	    ;}
    break;

  case 339:
#line 2800 "gram1.y"
    { switch(parstate)
		{
                case OUTSIDE:  
			{ PTR_BFND p;

			  p = get_bfnd(fi,PROG_HEDR,
                                       make_program(look_up_sym("_MAIN")),
                                       LLNULL, LLNULL, LLNULL);
			  set_blobs(p, global_bfnd, NEW_GROUP1);
			  add_scope_level(p, NO);
			  position = IN_PROC; 
	  		  parstate = INDCL; }
	                  break;
                case INSIDE:    parstate = INDCL;
                case INDCL:     break;

                case INDATA:
                         /*  err(
                     "Statement order error: declaration after DATA or function statement", 
                                 29);*/
                              break;

                default:
                           err("Declaration among executables", 30);
                }
        ;}
    break;

  case 342:
#line 2838 "gram1.y"
    { (yyval.ll_node) = LLNULL; endioctl(); ;}
    break;

  case 343:
#line 2840 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);  endioctl();;}
    break;

  case 344:
#line 2844 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 345:
#line 2846 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 346:
#line 2848 "gram1.y"
    { PTR_LLND l;
	      l = make_llnd(fi, KEYWORD_ARG, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL);
	      l->type = (yyvsp[(2) - (2)].ll_node)->type;
              (yyval.ll_node) = l; 
	    ;}
    break;

  case 347:
#line 2859 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node), LLNULL, EXPR_LIST);
              endioctl(); 
            ;}
    break;

  case 348:
#line 2863 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST);
              endioctl(); 
            ;}
    break;

  case 349:
#line 2869 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 350:
#line 2871 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 351:
#line 2875 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 352:
#line 2877 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 353:
#line 2879 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 354:
#line 2883 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 355:
#line 2885 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 356:
#line 2889 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 357:
#line 2891 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("+", ADD_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 358:
#line 2893 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("-", SUBT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 359:
#line 2895 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("*", MULT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 360:
#line 2897 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("/", DIV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 361:
#line 2899 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("**", EXP_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 362:
#line 2901 "gram1.y"
    { (yyval.ll_node) = defined_op_node((yyvsp[(1) - (2)].hash_entry), (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 363:
#line 2903 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("+", UNARY_ADD_OP, (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 364:
#line 2905 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("-", MINUS_OP, (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 365:
#line 2907 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".eq.", EQ_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 366:
#line 2909 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".gt.", GT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 367:
#line 2911 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".lt.", LT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 368:
#line 2913 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".ge.", GTEQL_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 369:
#line 2915 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".ge.", LTEQL_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 370:
#line 2917 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".ne.", NOTEQL_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 371:
#line 2919 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".eqv.", EQV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 372:
#line 2921 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".neqv.", NEQV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 373:
#line 2923 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".xor.", XOR_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 374:
#line 2925 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".or.", OR_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 375:
#line 2927 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".and.", AND_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 376:
#line 2929 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node(".not.", NOT_OP, (yyvsp[(2) - (2)].ll_node), LLNULL); ;}
    break;

  case 377:
#line 2931 "gram1.y"
    { (yyval.ll_node) = intrinsic_op_node("//", CONCAT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 378:
#line 2933 "gram1.y"
    { (yyval.ll_node) = defined_op_node((yyvsp[(2) - (3)].hash_entry), (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node)); ;}
    break;

  case 379:
#line 2936 "gram1.y"
    { (yyval.token) = ADD_OP; ;}
    break;

  case 380:
#line 2937 "gram1.y"
    { (yyval.token) = SUBT_OP; ;}
    break;

  case 381:
#line 2949 "gram1.y"
    { PTR_SYMB s;
	      PTR_TYPE t;
	     /* PTR_LLND l;*/

       	      if (!(s = (yyvsp[(1) - (1)].hash_entry)->id_attr))
              {
	         s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	     	 s->decl = SOFT;
	      } 
	
	      switch (s->variant)
              {
	      case CONST_NAME:
		   (yyval.ll_node) = make_llnd(fi,CONST_REF,LLNULL,LLNULL, s);
		   t = s->type;
	           if ((t != TYNULL) &&
                       ((t->variant == T_ARRAY) ||  (t->variant == T_STRING) ))
                                 (yyval.ll_node)->variant = ARRAY_REF;

                   (yyval.ll_node)->type = t;
	           break;
	      case DEFAULT:   /* if common region with same name has been
                                 declared. */
		   s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	     	   s->decl = SOFT;

	      case VARIABLE_NAME:
                   (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL, s);
	           t = s->type;
	           if (t != TYNULL) {
                     if ((t->variant == T_ARRAY) ||  (t->variant == T_STRING) ||
                         ((t->variant == T_POINTER) && (t->entry.Template.base_type->variant == T_ARRAY) ) )
                         (yyval.ll_node)->variant = ARRAY_REF;

/*  	              if (t->variant == T_DERIVED_TYPE)
                         $$->variant = RECORD_REF; */
	           }
                   (yyval.ll_node)->type = t;
	           break;
	      case TYPE_NAME:
  	           (yyval.ll_node) = make_llnd(fi,TYPE_REF,LLNULL,LLNULL, s);
	           (yyval.ll_node)->type = s->type;
	           break;
	      case INTERFACE_NAME:
  	           (yyval.ll_node) = make_llnd(fi, INTERFACE_REF,LLNULL,LLNULL, s);
	           (yyval.ll_node)->type = s->type;
	           break;
              case FUNCTION_NAME:
                   if(isResultVar(s)) {
                     (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL, s);
	             t = s->type;
	             if (t != TYNULL) {
                       if ((t->variant == T_ARRAY) ||  (t->variant == T_STRING) ||
                         ((t->variant == T_POINTER) && (t->entry.Template.base_type->variant == T_ARRAY) ) )
                         (yyval.ll_node)->variant = ARRAY_REF;
	             }
                     (yyval.ll_node)->type = t;
	             break;
                   }                                        
	      default:
  	           (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL, s);
	           (yyval.ll_node)->type = s->type;
	           break;
	      }
             /* if ($$->variant == T_POINTER) {
	         l = $$;
	         $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $$->type = l->type->entry.Template.base_type;
	      }
              */ /*11.02.03*/
           ;}
    break;

  case 382:
#line 3023 "gram1.y"
    { PTR_SYMB  s;
	      (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); 
              s= (yyval.ll_node)->entry.Template.symbol;
              if ((((yyvsp[(1) - (1)].ll_node)->variant == VAR_REF) || ((yyvsp[(1) - (1)].ll_node)->variant == ARRAY_REF))  && (s->scope !=cur_scope()))  /*global_bfnd*/
              {
	          if(((s->variant == FUNCTION_NAME) && (!isResultVar(s))) || (s->variant == PROCEDURE_NAME) || (s->variant == ROUTINE_NAME))
                  { s = (yyval.ll_node)->entry.Template.symbol =  make_scalar(s->parent, TYNULL, LOCAL);
		    (yyval.ll_node)->type = s->type;  
		  }
              }
            ;}
    break;

  case 383:
#line 3035 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 384:
#line 3037 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 385:
#line 3041 "gram1.y"
    { int num_triplets;
	      PTR_SYMB s;  /*, sym;*/
	      /* PTR_LLND l; */
	      PTR_TYPE tp;
	      /* l = $1; */
	      s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol;
            
	      /* Handle variable to function conversion. */
	      if (((yyvsp[(1) - (5)].ll_node)->variant == VAR_REF) && 
	          (((s->variant == VARIABLE_NAME) && (s->type) &&
                    (s->type->variant != T_ARRAY)) ||
  	            (s->variant == ROUTINE_NAME))) {
	        s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol =  make_function(s->parent, TYNULL, LOCAL);
	        (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
              }
	      if (((yyvsp[(1) - (5)].ll_node)->variant == VAR_REF) && (s->variant == FUNCTION_NAME)) { 
                if(isResultVar(s))
	          (yyvsp[(1) - (5)].ll_node)->variant = ARRAY_REF;
                else
                  (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
              }
	      if (((yyvsp[(1) - (5)].ll_node)->variant == VAR_REF) && (s->variant == PROGRAM_NAME)) {
                 errstr("The name '%s' is invalid in this context",s->ident,285);
                 (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
              }
              /* l = $1; */
	      num_triplets = is_array_section_ref((yyvsp[(4) - (5)].ll_node));
	      switch ((yyvsp[(1) - (5)].ll_node)->variant)
              {
	      case TYPE_REF:
                   (yyvsp[(1) - (5)].ll_node)->variant = STRUCTURE_CONSTRUCTOR;                  
                   (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
                   (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                   (yyval.ll_node)->type =  lookup_type(s->parent); 
	          /* $$ = make_llnd(fi, STRUCTURE_CONSTRUCTOR, $1, $4, SMNULL);
	           $$->type = $1->type;*//*18.02.03*/
	           break;
	      case INTERFACE_REF:
	       /*  sym = resolve_overloading(s, $4);
	           if (sym != SMNULL)
	  	   {
	              l = make_llnd(fi, FUNC_CALL, $4, LLNULL, sym);
	              l->type = sym->type;
	              $$ = $1; $$->variant = OVERLOADED_CALL;
	              $$->entry.Template.ll_ptr1 = l;
	              $$->type = sym->type;
	           }
	           else {
	             errstr("can't resolve call %s", s->ident,310);
	           }
	           break;
                 */ /*podd 01.02.03*/

                   (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;

	      case FUNC_CALL:
                   (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
                   (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                   if(s->type) 
                     (yyval.ll_node)->type = s->type;
                   else
                     (yyval.ll_node)->type = global_default;
	           /*late_bind_if_needed($$);*/ /*podd 02.02.23*/
	           break;
	      case DEREF_OP:
              case ARRAY_REF:
	           /* array element */
	           if (num_triplets == 0) {
                       if ((yyvsp[(4) - (5)].ll_node) == LLNULL) {
                           s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol = make_function(s->parent, TYNULL, LOCAL);
                           s->entry.func_decl.num_output = 1;
                           (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
                           (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                       } else if ((yyvsp[(1) - (5)].ll_node)->type->variant == T_STRING) {
                           PTR_LLND temp = (yyvsp[(4) - (5)].ll_node);
                           int num_input = 0;

                           while (temp) {
                             ++num_input;
                             temp = temp->entry.Template.ll_ptr2;
                           }
                           (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
                           s = (yyvsp[(1) - (5)].ll_node)->entry.Template.symbol = make_function(s->parent, TYNULL, LOCAL);
                           s->entry.func_decl.num_output = 1;
                           s->entry.func_decl.num_input = num_input;
                           (yyvsp[(1) - (5)].ll_node)->variant = FUNC_CALL;
                           (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                       } else {
       	                   (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	                   (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                           (yyval.ll_node)->type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type;
                       }
                   }
                   /* substring */
	           else if ((num_triplets == 1) && 
                            ((yyvsp[(1) - (5)].ll_node)->type->variant == T_STRING)) {
    	           /*
                     $1->entry.Template.ll_ptr1 = $4;
	             $$ = $1; $$->type = global_string;
                   */
	                  (yyval.ll_node) = make_llnd(fi, 
			  ARRAY_OP, LLNULL, LLNULL, SMNULL);
    	                  (yyval.ll_node)->entry.Template.ll_ptr1 = (yyvsp[(1) - (5)].ll_node);
       	                  (yyval.ll_node)->entry.Template.ll_ptr2 = (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1;
	                  (yyval.ll_node)->type = global_string;
                   }           
                   /* array section */
                   else {
    	             (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	             (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node); tp = make_type(fi, T_ARRAY);     /**18.03.17*/
                     tp->entry.ar_decl.base_type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type; /**18.03.17 $1->type */
	             tp->entry.ar_decl.num_dimensions = num_triplets;
	             (yyval.ll_node)->type = tp;
                   }
	           break;
	      default:
                    if((yyvsp[(1) - (5)].ll_node)->entry.Template.symbol)
                      errstr("Can't subscript %s",(yyvsp[(1) - (5)].ll_node)->entry.Template.symbol->ident, 44);
                    else
	              err("Can't subscript",44);
             }
             /*if ($$->variant == T_POINTER) {
	        l = $$;
	        $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	        $$->type = l->type->entry.Template.base_type;
	     }
              */  /*11.02.03*/

	     endioctl(); 
           ;}
    break;

  case 386:
#line 3172 "gram1.y"
    { int num_triplets;
	      PTR_SYMB s;
	      PTR_LLND l;

	      s = (yyvsp[(1) - (6)].ll_node)->entry.Template.symbol;
/*              if ($1->type->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */
	      if (((yyvsp[(1) - (6)].ll_node)->type->variant != T_ARRAY) ||
                  ((yyvsp[(1) - (6)].ll_node)->type->entry.ar_decl.base_type->variant != T_STRING)) {
	         errstr("Can't take substring of %s", s->ident, 45);
              }
              else {
  	        num_triplets = is_array_section_ref((yyvsp[(4) - (6)].ll_node));
	           /* array element */
                if (num_triplets == 0) {
                   (yyvsp[(1) - (6)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (6)].ll_node);
                  /* $1->entry.Template.ll_ptr2 = $6;*/
	          /* $$ = $1;*/
                   l=(yyvsp[(1) - (6)].ll_node);
                   /*$$->type = $1->type->entry.ar_decl.base_type;*/
                   l->type = global_string;  /**18.03.17* $1->type->entry.ar_decl.base_type;*/
                }
                /* array section */
                else {
    	           (yyvsp[(1) - (6)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (6)].ll_node);
    	           /*$1->entry.Template.ll_ptr2 = $6;
	           $$ = $1; $$->type = make_type(fi, T_ARRAY);
                   $$->type->entry.ar_decl.base_type = $1->type;
	           $$->type->entry.ar_decl.num_dimensions = num_triplets;
                  */
                   l = (yyvsp[(1) - (6)].ll_node); l->type = make_type(fi, T_ARRAY);
                   l->type->entry.ar_decl.base_type = global_string;   /**18.03.17* $1->type*/
	           l->type->entry.ar_decl.num_dimensions = num_triplets;
               }
                (yyval.ll_node) = make_llnd(fi, ARRAY_OP, l, (yyvsp[(6) - (6)].ll_node), SMNULL);
	        (yyval.ll_node)->type = l->type;
              
              /* if ($$->variant == T_POINTER) {
	          l = $$;
	          $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	          $$->type = l->type->entry.Template.base_type;
	       }
               */  /*11.02.03*/
             }
             endioctl();
          ;}
    break;

  case 387:
#line 3222 "gram1.y"
    {  int num_triplets;
	      PTR_LLND l,l1,l2;
              PTR_TYPE tp;

         /*   if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

              num_triplets = is_array_section_ref((yyvsp[(3) - (4)].ll_node));
              (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
              l2 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr2;  
              l1 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1;                
              if(l2 && l2->type->variant == T_STRING)/*substring*/
                if(num_triplets == 1){
	           l = make_llnd(fi, ARRAY_OP, LLNULL, LLNULL, SMNULL);
    	           l->entry.Template.ll_ptr1 = l2;
       	           l->entry.Template.ll_ptr2 = (yyvsp[(3) - (4)].ll_node)->entry.Template.ll_ptr1;
	           l->type = global_string; 
                   (yyval.ll_node)->entry.Template.ll_ptr2 = l;                                          
                } else
                   err("Can't subscript",44);
              else if (l2 && l2->type->variant == T_ARRAY) {
                 if(num_triplets > 0) { /*array section*/
                   tp = make_type(fi,T_ARRAY);
                   tp->entry.ar_decl.base_type = (yyvsp[(1) - (4)].ll_node)->type->entry.ar_decl.base_type;
                   tp->entry.ar_decl.num_dimensions = num_triplets;
                   (yyval.ll_node)->type = tp;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                   l2->type = (yyval.ll_node)->type;   
                  }                 
                 else {  /*array element*/
                   l2->type = l2->type->entry.ar_decl.base_type;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);   
                   if(l1->type->variant != T_ARRAY)  
                     (yyval.ll_node)->type = l2->type;
                 }
              } else 
                   {err("Can't subscript",44); /*fprintf(stderr,"%d  %d",$1->variant,l2);*/}
                   /*errstr("Can't subscript %s",l2->entry.Template.symbol->ident,441);*/
         ;}
    break;

  case 388:
#line 3266 "gram1.y"
    { int num_triplets;
	      PTR_LLND l,q;

          /*     if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

              (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
	      if (((yyvsp[(1) - (5)].ll_node)->type->variant != T_ARRAY) &&
                  ((yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type->variant != T_STRING)) {
	         err("Can't take substring",45);
              }
              else {
  	        num_triplets = is_array_section_ref((yyvsp[(3) - (5)].ll_node));
                l = (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr2;
                if(l) {
                /* array element */
	        if (num_triplets == 0) {
                   l->entry.Template.ll_ptr1 = (yyvsp[(3) - (5)].ll_node);       	           
                   l->type = global_string;
                }
                /* array section */
                else {	
    	             l->entry.Template.ll_ptr1 = (yyvsp[(3) - (5)].ll_node);
	             l->type = make_type(fi, T_ARRAY);
                     l->type->entry.ar_decl.base_type = global_string;
	             l->type->entry.ar_decl.num_dimensions = num_triplets;
                }
	        q = make_llnd(fi, ARRAY_OP, l, (yyvsp[(5) - (5)].ll_node), SMNULL);
	        q->type = l->type;
                (yyval.ll_node)->entry.Template.ll_ptr2 = q;
                if((yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1->type->variant != T_ARRAY)  
                     (yyval.ll_node)->type = q->type;
               }
             }
          ;}
    break;

  case 389:
#line 3308 "gram1.y"
    { PTR_TYPE t;
	      PTR_SYMB  field;
	    /*  PTR_BFND at_scope;*/
              PTR_LLND l;


/*              if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

	      t = (yyvsp[(1) - (3)].ll_node)->type; 
	      
	      if (( ( ((yyvsp[(1) - (3)].ll_node)->variant == VAR_REF) 
	          ||  ((yyvsp[(1) - (3)].ll_node)->variant == CONST_REF) 
                  ||  ((yyvsp[(1) - (3)].ll_node)->variant == ARRAY_REF)
                  ||  ((yyvsp[(1) - (3)].ll_node)->variant == RECORD_REF)) && (t->variant == T_DERIVED_TYPE)) 
	          ||((((yyvsp[(1) - (3)].ll_node)->variant == ARRAY_REF) || ((yyvsp[(1) - (3)].ll_node)->variant == RECORD_REF)) && (t->variant == T_ARRAY) &&
                      (t = t->entry.ar_decl.base_type) && (t->variant == T_DERIVED_TYPE))) 
                {
                 t->name = lookup_type_symbol(t->name);
	         if ((field = component(t->name, yytext))) {                   
	            l =  make_llnd(fi, VAR_REF, LLNULL, LLNULL, field);
                    l->type = field->type;
                    if(field->type->variant == T_ARRAY || field->type->variant == T_STRING)
                      l->variant = ARRAY_REF; 
                    (yyval.ll_node) = make_llnd(fi, RECORD_REF, (yyvsp[(1) - (3)].ll_node), l, SMNULL);
                    if((yyvsp[(1) - (3)].ll_node)->type->variant != T_ARRAY)
                       (yyval.ll_node)->type = field->type;
                    else {
                       (yyval.ll_node)->type = make_type(fi,T_ARRAY);
                       if(field->type->variant != T_ARRAY) 
	                 (yyval.ll_node)->type->entry.ar_decl.base_type = field->type;
                       else
                         (yyval.ll_node)->type->entry.ar_decl.base_type = field->type->entry.ar_decl.base_type;
	               (yyval.ll_node)->type->entry.ar_decl.num_dimensions = t->entry.ar_decl.num_dimensions;
                       }
                 }
                  else  
                    errstr("Illegal component  %s", yytext,311);
              }                     
               else 
                    errstr("Can't take component  %s", yytext,311);
             ;}
    break;

  case 390:
#line 3366 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 391:
#line 3368 "gram1.y"
    {(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 392:
#line 3370 "gram1.y"
    {  int num_triplets;
               PTR_TYPE tp;
              /* PTR_LLND l;*/
	      if ((yyvsp[(1) - (5)].ll_node)->type->variant == T_ARRAY)
              {
  	         num_triplets = is_array_section_ref((yyvsp[(4) - (5)].ll_node));
	         /* array element */
	         if (num_triplets == 0) {
       	            (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
       	            (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
                    (yyval.ll_node)->type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type;
                 }
                 /* substring */
	       /*  else if ((num_triplets == 1) && 
                          ($1->type->variant == T_STRING)) {
    	                  $1->entry.Template.ll_ptr1 = $4;
	                  $$ = $1; $$->type = global_string;
                 }   */ /*podd*/        
                 /* array section */
                 else {
    	             (yyvsp[(1) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	             (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node); tp = make_type(fi, T_ARRAY);
                     tp->entry.ar_decl.base_type = (yyvsp[(1) - (5)].ll_node)->type->entry.ar_decl.base_type;  /**18.03.17* $1->type */
	             tp->entry.ar_decl.num_dimensions = num_triplets;
                     (yyval.ll_node)->type = tp;
                 }
             } 
             else err("can't subscript",44);

            /* if ($$->variant == T_POINTER) {
	        l = $$;
	        $$ = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	        $$->type = l->type->entry.Template.base_type;
	     }
             */  /*11.02.03*/

            endioctl();
           ;}
    break;

  case 393:
#line 3410 "gram1.y"
    {  int num_triplets;
	      PTR_LLND l,l1,l2;

         /*   if ($1->variant == T_POINTER) {
	         l = $1;
	         $1 = make_llnd(fi, DEREF_OP, l, LLNULL, SMNULL);
	         $1->type = l->type->entry.Template.base_type;
	      } */

              num_triplets = is_array_section_ref((yyvsp[(3) - (4)].ll_node));
              (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
              l2 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr2;  
              l1 = (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1;                
              if(l2 && l2->type->variant == T_STRING)/*substring*/
                if(num_triplets == 1){
	           l = make_llnd(fi, ARRAY_OP, LLNULL, LLNULL, SMNULL);
    	           l->entry.Template.ll_ptr1 = l2;
       	           l->entry.Template.ll_ptr2 = (yyvsp[(3) - (4)].ll_node)->entry.Template.ll_ptr1;
	           l->type = global_string; 
                   (yyval.ll_node)->entry.Template.ll_ptr2 = l;                                          
                } else
                   err("Can't subscript",44);
              else if (l2 && l2->type->variant == T_ARRAY) {
                 if(num_triplets > 0) { /*array section*/
                   (yyval.ll_node)->type = make_type(fi,T_ARRAY);
                   (yyval.ll_node)->type->entry.ar_decl.base_type = l2->type->entry.ar_decl.base_type;
                   (yyval.ll_node)->type->entry.ar_decl.num_dimensions = num_triplets;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                   l2->type = (yyval.ll_node)->type;   
                  }                 
                 else {  /*array element*/
                   l2->type = l2->type->entry.ar_decl.base_type;
                   l2->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);   
                   if(l1->type->variant != T_ARRAY)  
                     (yyval.ll_node)->type = l2->type;
                 }
              } else 
                   err("Can't subscript",44);
         ;}
    break;

  case 394:
#line 3452 "gram1.y"
    { 
	      if ((yyvsp[(1) - (2)].ll_node)->type->variant == T_STRING) {
                 (yyvsp[(1) - (2)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (2)].ll_node);
                 (yyval.ll_node) = (yyvsp[(1) - (2)].ll_node); (yyval.ll_node)->type = global_string;
              }
              else errstr("can't subscript of %s", (yyvsp[(1) - (2)].ll_node)->entry.Template.symbol->ident,44);
            ;}
    break;

  case 395:
#line 3462 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 396:
#line 3464 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 397:
#line 3468 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, (yyvsp[(2) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), SMNULL); ;}
    break;

  case 398:
#line 3472 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 399:
#line 3474 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 400:
#line 3478 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 401:
#line 3480 "gram1.y"
    { PTR_TYPE t;
               t = make_type_node((yyvsp[(1) - (3)].ll_node)->type, (yyvsp[(3) - (3)].ll_node));
               (yyval.ll_node) = (yyvsp[(1) - (3)].ll_node);
               (yyval.ll_node)->type = t;
             ;}
    break;

  case 402:
#line 3486 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 403:
#line 3488 "gram1.y"
    { PTR_TYPE t;
               t = make_type_node((yyvsp[(1) - (3)].ll_node)->type, (yyvsp[(3) - (3)].ll_node));
               (yyval.ll_node) = (yyvsp[(1) - (3)].ll_node);
               (yyval.ll_node)->type = t;
             ;}
    break;

  case 404:
#line 3494 "gram1.y"
    {
              if ((yyvsp[(2) - (2)].ll_node) != LLNULL)
              {
		 (yyval.ll_node) = make_llnd(fi, ARRAY_OP, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL); 
                 (yyval.ll_node)->type = global_string;
              }
	      else 
                 (yyval.ll_node) = (yyvsp[(1) - (2)].ll_node);
             ;}
    break;

  case 405:
#line 3507 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,BOOL_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.bval = 1;
	      (yyval.ll_node)->type = global_bool;
	    ;}
    break;

  case 406:
#line 3513 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,BOOL_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.bval = 0;
	      (yyval.ll_node)->type = global_bool;
	    ;}
    break;

  case 407:
#line 3520 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,FLOAT_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
	      (yyval.ll_node)->type = global_float;
	    ;}
    break;

  case 408:
#line 3526 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,DOUBLE_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
	      (yyval.ll_node)->type = global_double;
	    ;}
    break;

  case 409:
#line 3534 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.ival = atoi(yytext);
	      (yyval.ll_node)->type = global_int;
	    ;}
    break;

  case 410:
#line 3542 "gram1.y"
    { PTR_TYPE t;
	      PTR_LLND p,q;
	      (yyval.ll_node) = make_llnd(fi,STRING_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
              if(yyquote=='\"') 
	        t = global_string_2;
              else
	        t = global_string;

	      p = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
	      p->entry.ival = yyleng;
	      p->type = global_int;
              q = make_llnd(fi, LEN_OP, p, LLNULL, SMNULL); 
              (yyval.ll_node)->type = make_type_node(t, q);
	    ;}
    break;

  case 411:
#line 3558 "gram1.y"
    { PTR_TYPE t;
	      (yyval.ll_node) = make_llnd(fi,STRING_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
              if(yyquote=='\"') 
	        t = global_string_2;
              else
	        t = global_string;
	      (yyval.ll_node)->type = make_type_node(t, (yyvsp[(1) - (3)].ll_node));
            ;}
    break;

  case 412:
#line 3568 "gram1.y"
    { PTR_TYPE t;
	      (yyval.ll_node) = make_llnd(fi,STRING_VAL, LLNULL, LLNULL, SMNULL);
	      (yyval.ll_node)->entry.string_val = copys(yytext);
              if(yyquote=='\"') 
	        t = global_string_2;
              else
	        t = global_string;
	      (yyval.ll_node)->type = make_type_node(t, (yyvsp[(1) - (3)].ll_node));
            ;}
    break;

  case 413:
#line 3581 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,COMPLEX_VAL, (yyvsp[(2) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), SMNULL);
	      (yyval.ll_node)->type = global_complex;
	    ;}
    break;

  case 414:
#line 3588 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 415:
#line 3590 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 416:
#line 3613 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),SMNULL); ;}
    break;

  case 417:
#line 3615 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (2)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 418:
#line 3617 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,make_llnd(fi,DDOT,(yyvsp[(1) - (5)].ll_node),(yyvsp[(3) - (5)].ll_node),SMNULL),(yyvsp[(5) - (5)].ll_node),SMNULL); ;}
    break;

  case 419:
#line 3619 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,make_llnd(fi,DDOT,(yyvsp[(1) - (4)].ll_node),LLNULL,SMNULL),(yyvsp[(4) - (4)].ll_node),SMNULL); ;}
    break;

  case 420:
#line 3621 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, make_llnd(fi,DDOT,LLNULL,(yyvsp[(2) - (4)].ll_node),SMNULL),(yyvsp[(4) - (4)].ll_node),SMNULL); ;}
    break;

  case 421:
#line 3623 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL),(yyvsp[(3) - (3)].ll_node),SMNULL); ;}
    break;

  case 422:
#line 3625 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,(yyvsp[(2) - (2)].ll_node),SMNULL); ;}
    break;

  case 423:
#line 3627 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL); ;}
    break;

  case 424:
#line 3630 "gram1.y"
    {in_vec=YES;;}
    break;

  case 425:
#line 3630 "gram1.y"
    {in_vec=NO;;}
    break;

  case 426:
#line 3631 "gram1.y"
    { PTR_TYPE array_type;
             (yyval.ll_node) = make_llnd (fi,CONSTRUCTOR_REF,(yyvsp[(4) - (6)].ll_node),LLNULL,SMNULL); 
             /*$$->type = $2->type;*/ /*28.02.03*/
             array_type = make_type(fi, T_ARRAY);
	     array_type->entry.ar_decl.num_dimensions = 1;
             if((yyvsp[(4) - (6)].ll_node)->type->variant == T_ARRAY)
	       array_type->entry.ar_decl.base_type = (yyvsp[(4) - (6)].ll_node)->type->entry.ar_decl.base_type;
             else
               array_type->entry.ar_decl.base_type = (yyvsp[(4) - (6)].ll_node)->type;
             (yyval.ll_node)->type = array_type;
           ;}
    break;

  case 427:
#line 3645 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 428:
#line 3647 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 429:
#line 3670 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 430:
#line 3672 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); endioctl(); ;}
    break;

  case 431:
#line 3674 "gram1.y"
    { stat_alloc = make_llnd(fi, SPEC_PAIR, (yyvsp[(4) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL);
                  endioctl();
                ;}
    break;

  case 432:
#line 3690 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 433:
#line 3692 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); endioctl(); ;}
    break;

  case 434:
#line 3694 "gram1.y"
    { stat_alloc = make_llnd(fi, SPEC_PAIR, (yyvsp[(4) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL);
             endioctl();
           ;}
    break;

  case 435:
#line 3707 "gram1.y"
    {stat_alloc = LLNULL;;}
    break;

  case 436:
#line 3711 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 437:
#line 3713 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 438:
#line 3721 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 439:
#line 3723 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 440:
#line 3725 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 441:
#line 3727 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(2) - (2)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (2)].ll_node);
            ;}
    break;

  case 442:
#line 3781 "gram1.y"
    { PTR_BFND biff;

	      (yyval.bf_node) = get_bfnd(fi,CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL); 
	      bind(); 
	      biff = cur_scope();
	      if ((biff->variant == FUNC_HEDR) || (biff->variant == PROC_HEDR)
		  || (biff->variant == PROS_HEDR) 
	          || (biff->variant == PROG_HEDR)
                  || (biff->variant == BLOCK_DATA)) {
                if(biff->control_parent == global_bfnd) position = IN_OUTSIDE;
		else if(!is_interface_stat(biff->control_parent)) position++;
              } else if  (biff->variant == MODULE_STMT)
                position = IN_OUTSIDE;
	      else err("Unexpected END statement read", 52);
             /* FB ADDED set the control parent so the empty function unparse right*/
              if ((yyval.bf_node))
                (yyval.bf_node)->control_parent = biff;
              delete_beyond_scope_level(pred_bfnd);
            ;}
    break;

  case 443:
#line 3803 "gram1.y"
    {
              make_extend((yyvsp[(3) - (3)].symbol));
              (yyval.bf_node) = BFNULL; 
              /* delete_beyond_scope_level(pred_bfnd); */
             ;}
    break;

  case 444:
#line 3816 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,CONTROL_END, SMNULL, LLNULL, LLNULL, LLNULL); 
	    bind(); 
	    delete_beyond_scope_level(pred_bfnd);
	    position = IN_OUTSIDE;
          ;}
    break;

  case 445:
#line 3825 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 446:
#line 3828 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(2) - (2)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (2)].ll_node);
            ;}
    break;

  case 447:
#line 3878 "gram1.y"
    { thiswasbranch = NO;
              (yyvsp[(1) - (2)].bf_node)->variant = LOGIF_NODE;
              (yyval.bf_node) = make_logif((yyvsp[(1) - (2)].bf_node), (yyvsp[(2) - (2)].bf_node));
	      set_blobs((yyvsp[(1) - (2)].bf_node), pred_bfnd, SAME_GROUP);
	    ;}
    break;

  case 448:
#line 3884 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node);
	      set_blobs((yyval.bf_node), pred_bfnd, NEW_GROUP1); 
            ;}
    break;

  case 449:
#line 3889 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(2) - (3)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (3)].ll_node);
	      set_blobs((yyval.bf_node), pred_bfnd, NEW_GROUP1); 
            ;}
    break;

  case 450:
#line 3907 "gram1.y"
    { make_elseif((yyvsp[(4) - (7)].ll_node),(yyvsp[(7) - (7)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL;;}
    break;

  case 451:
#line 3909 "gram1.y"
    { make_else((yyvsp[(3) - (3)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL; ;}
    break;

  case 452:
#line 3911 "gram1.y"
    { make_endif((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 453:
#line 3913 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 454:
#line 3915 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, CONTAINS_STMT, SMNULL, LLNULL, LLNULL, LLNULL); ;}
    break;

  case 455:
#line 3918 "gram1.y"
    { thiswasbranch = NO;
              (yyvsp[(1) - (2)].bf_node)->variant = FORALL_STAT;
              (yyval.bf_node) = make_logif((yyvsp[(1) - (2)].bf_node), (yyvsp[(2) - (2)].bf_node));
	      set_blobs((yyvsp[(1) - (2)].bf_node), pred_bfnd, SAME_GROUP);
	    ;}
    break;

  case 456:
#line 3924 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 457:
#line 3926 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(2) - (2)].bf_node); (yyval.bf_node)->entry.Template.ll_ptr3 = (yyvsp[(1) - (2)].ll_node);;}
    break;

  case 458:
#line 3928 "gram1.y"
    { make_endforall((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 459:
#line 3931 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 460:
#line 3933 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 461:
#line 3935 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 462:
#line 3962 "gram1.y"
    { 	     
	     /*  if($5 && $5->labdefined)
		 execerr("no backward DO loops", (char *)NULL); */
	       (yyval.bf_node) = make_do(WHILE_NODE, LBNULL, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL);
	       /*$$->entry.Template.ll_ptr3 = $1;*/	     
           ;}
    break;

  case 463:
#line 3971 "gram1.y"
    {
               if( (yyvsp[(4) - (7)].label) && (yyvsp[(4) - (7)].label)->labdefined)
		  err("No backward DO loops", 46);
	        (yyval.bf_node) = make_do(WHILE_NODE, (yyvsp[(4) - (7)].label), SMNULL, (yyvsp[(7) - (7)].ll_node), LLNULL, LLNULL);            
	    ;}
    break;

  case 464:
#line 3979 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 465:
#line 3981 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(5) - (6)].ll_node);;}
    break;

  case 466:
#line 3983 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (4)].ll_node);;}
    break;

  case 467:
#line 3988 "gram1.y"
    {  
               if( (yyvsp[(4) - (11)].label) && (yyvsp[(4) - (11)].label)->labdefined)
		  err("No backward DO loops", 46);
	        (yyval.bf_node) = make_do(FOR_NODE, (yyvsp[(4) - (11)].label), (yyvsp[(7) - (11)].symbol), (yyvsp[(9) - (11)].ll_node), (yyvsp[(11) - (11)].ll_node), LLNULL);            
	    ;}
    break;

  case 468:
#line 3995 "gram1.y"
    {
               if( (yyvsp[(4) - (13)].label) && (yyvsp[(4) - (13)].label)->labdefined)
		  err("No backward DO loops", 46);
	        (yyval.bf_node) = make_do(FOR_NODE, (yyvsp[(4) - (13)].label), (yyvsp[(7) - (13)].symbol), (yyvsp[(9) - (13)].ll_node), (yyvsp[(11) - (13)].ll_node), (yyvsp[(13) - (13)].ll_node));            
	    ;}
    break;

  case 469:
#line 4003 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, CASE_NODE, (yyvsp[(4) - (4)].symbol), (yyvsp[(3) - (4)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 470:
#line 4005 "gram1.y"
    { /*PTR_LLND p;*/
	     /* p = make_llnd(fi, DEFAULT, LLNULL, LLNULL, SMNULL); */
	      (yyval.bf_node) = get_bfnd(fi, DEFAULT_NODE, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL); ;}
    break;

  case 471:
#line 4009 "gram1.y"
    { make_endselect((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 472:
#line 4012 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, SWITCH_NODE, SMNULL, (yyvsp[(6) - (7)].ll_node), LLNULL, LLNULL) ; ;}
    break;

  case 473:
#line 4014 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, SWITCH_NODE, SMNULL, (yyvsp[(7) - (8)].ll_node), LLNULL, (yyvsp[(1) - (8)].ll_node)) ; ;}
    break;

  case 474:
#line 4018 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 475:
#line 4024 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 476:
#line 4026 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, (yyvsp[(1) - (2)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 477:
#line 4028 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, LLNULL, (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 478:
#line 4030 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL); ;}
    break;

  case 479:
#line 4034 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, EXPR_LIST, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 480:
#line 4036 "gram1.y"
    { PTR_LLND p;
	      
	      p = make_llnd(fi, EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(p, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 481:
#line 4044 "gram1.y"
    { (yyval.symbol) = SMNULL; ;}
    break;

  case 482:
#line 4046 "gram1.y"
    { (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), CONSTRUCT_NAME, global_default,
                                     LOCAL); ;}
    break;

  case 483:
#line 4052 "gram1.y"
    {(yyval.hash_entry) = HSNULL;;}
    break;

  case 484:
#line 4054 "gram1.y"
    { (yyval.hash_entry) = (yyvsp[(1) - (1)].hash_entry);;}
    break;

  case 485:
#line 4058 "gram1.y"
    {(yyval.hash_entry) = look_up_sym(yytext);;}
    break;

  case 486:
#line 4062 "gram1.y"
    { PTR_SYMB s;
	             s = make_local_entity( (yyvsp[(1) - (2)].hash_entry), CONSTRUCT_NAME, global_default, LOCAL);             
                    (yyval.ll_node) = make_llnd(fi, VAR_REF, LLNULL, LLNULL, s);
                   ;}
    break;

  case 487:
#line 4083 "gram1.y"
    { (yyval.bf_node) = make_if((yyvsp[(4) - (5)].ll_node)); ;}
    break;

  case 488:
#line 4086 "gram1.y"
    { (yyval.bf_node) = make_forall((yyvsp[(4) - (6)].ll_node),(yyvsp[(5) - (6)].ll_node)); ;}
    break;

  case 489:
#line 4090 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, EXPR_LIST, (yyvsp[(1) - (1)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 490:
#line 4092 "gram1.y"
    { PTR_LLND p;	      
	      p = make_llnd(fi, EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(p, (yyvsp[(1) - (3)].ll_node));
	    ;}
    break;

  case 491:
#line 4099 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi, FORALL_OP, (yyvsp[(3) - (3)].ll_node), LLNULL, (yyvsp[(1) - (3)].symbol)); ;}
    break;

  case 492:
#line 4103 "gram1.y"
    { (yyval.ll_node)=LLNULL;;}
    break;

  case 493:
#line 4105 "gram1.y"
    { (yyval.ll_node)=(yyvsp[(2) - (2)].ll_node);;}
    break;

  case 494:
#line 4116 "gram1.y"
    { PTR_SYMB  s;
              s = (yyvsp[(1) - (1)].hash_entry)->id_attr;
      	      if (!s || s->variant == DEFAULT)
              {
	         s = make_scalar((yyvsp[(1) - (1)].hash_entry), TYNULL, LOCAL);
	     	 s->decl = SOFT;
	      }
              (yyval.symbol) = s; 
	 ;}
    break;

  case 495:
#line 4129 "gram1.y"
    { PTR_SYMB s;
              PTR_LLND l;
              int vrnt;

            /*  s = make_scalar($1, TYNULL, LOCAL);*/ /*16.02.03*/
              s = (yyvsp[(1) - (5)].symbol);
	      if (s->variant != CONST_NAME) {
                if(in_vec) 
                   vrnt=SEQ;
                else
                   vrnt=DDOT;     
                l = make_llnd(fi, SEQ, make_llnd(fi, vrnt, (yyvsp[(3) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL),
                              LLNULL, SMNULL);
		(yyval.ll_node) = make_llnd(fi,IOACCESS, LLNULL, l, s);
		do_name_err = NO;
	      }
	      else {
		err("Symbolic constant not allowed as DO variable", 47);
		do_name_err = YES;
	      }
	    ;}
    break;

  case 496:
#line 4152 "gram1.y"
    { PTR_SYMB s;
              PTR_LLND l;
              int vrnt;
              /*s = make_scalar($1, TYNULL, LOCAL);*/ /*16.02.03*/
              s = (yyvsp[(1) - (7)].symbol);
	      if( s->variant != CONST_NAME ) {
                if(in_vec) 
                   vrnt=SEQ;
                else
                   vrnt=DDOT;     
                l = make_llnd(fi, SEQ, make_llnd(fi, vrnt, (yyvsp[(3) - (7)].ll_node), (yyvsp[(5) - (7)].ll_node), SMNULL), (yyvsp[(7) - (7)].ll_node),
                              SMNULL);
		(yyval.ll_node) = make_llnd(fi,IOACCESS, LLNULL, l, s);
		do_name_err = NO;
	      }
	      else {
		err("Symbolic constant not allowed as DO variable", 47);
		do_name_err = YES;
	      }
	    ;}
    break;

  case 497:
#line 4175 "gram1.y"
    { (yyval.label) = LBNULL; ;}
    break;

  case 498:
#line 4177 "gram1.y"
    {
	       (yyval.label)  = make_label_node(fi,convci(yyleng, yytext));
	       (yyval.label)->scope = cur_scope();
	    ;}
    break;

  case 499:
#line 4184 "gram1.y"
    { make_endwhere((yyvsp[(3) - (3)].symbol)); (yyval.bf_node) = BFNULL; ;}
    break;

  case 500:
#line 4186 "gram1.y"
    { make_elsewhere((yyvsp[(3) - (3)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL; ;}
    break;

  case 501:
#line 4188 "gram1.y"
    { make_elsewhere_mask((yyvsp[(4) - (6)].ll_node),(yyvsp[(6) - (6)].symbol)); lastwasbranch = NO; (yyval.bf_node) = BFNULL; ;}
    break;

  case 502:
#line 4190 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, WHERE_BLOCK_STMT, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 503:
#line 4192 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, WHERE_BLOCK_STMT, SMNULL, (yyvsp[(5) - (6)].ll_node), LLNULL, (yyvsp[(1) - (6)].ll_node)); ;}
    break;

  case 504:
#line 4197 "gram1.y"
    { PTR_LLND p, r;
             PTR_SYMB s1, s2 = SMNULL, s3, arg_list;
	     PTR_HASH hash_entry;

	   /*  if (just_look_up_sym("=") != HSNULL) {
	        p = intrinsic_op_node("=", EQUAL, $2, $4);
   	        $$ = get_bfnd(fi, OVERLOADED_ASSIGN_STAT, SMNULL, p, $2, $4);
             }	      
             else */ if ((yyvsp[(2) - (4)].ll_node)->variant == FUNC_CALL) {
                if(parstate==INEXEC){
                  	  err("Declaration among executables", 30);
                 /*   $$=BFNULL;*/
 	         (yyval.bf_node) = get_bfnd(fi,STMTFN_STAT, SMNULL, (yyvsp[(2) - (4)].ll_node), LLNULL, LLNULL);
                } 
                else {	         
  	         (yyvsp[(2) - (4)].ll_node)->variant = STMTFN_DECL;
		 /* $2->entry.Template.ll_ptr2 = $4; */
                 if( (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1) {
		   r = (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1->entry.Template.ll_ptr1;
                   if(r->variant != VAR_REF && r->variant != ARRAY_REF){
                     err("A dummy argument of a statement function must be a scalar identifier", 333);
                     s1 = SMNULL;
                   }
                   else                       
		     s1 = r ->entry.Template.symbol;
                 } else
                   s1 = SMNULL;
		 if (s1)
	            s1->scope = cur_scope();
 	         (yyval.bf_node) = get_bfnd(fi,STMTFN_STAT, SMNULL, (yyvsp[(2) - (4)].ll_node), LLNULL, LLNULL);
	         add_scope_level((yyval.bf_node), NO);
                 arg_list = SMNULL;
		 if (s1) 
                 {
	            /*arg_list = SMNULL;*/
                    p = (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1;
                    while (p != LLNULL)
                    {
		    /*   if (p->entry.Template.ll_ptr1->variant != VAR_REF) {
			  errstr("cftn.gram:1: illegal statement function %s.", $2->entry.Template.symbol->ident);
			  break;
		       } 
                    */
                       r = p->entry.Template.ll_ptr1;
                       if(r->variant != VAR_REF && r->variant != ARRAY_REF){
                         err("A dummy argument of a statement function must be a scalar identifier", 333);
                         break;
                       }
	               hash_entry = look_up_sym(r->entry.Template.symbol->parent->ident);
	               s3 = make_scalar(hash_entry, s1->type, IO);
                       replace_symbol_in_expr(s3,(yyvsp[(4) - (4)].ll_node));
	               if (arg_list == SMNULL) 
                          s2 = arg_list = s3;
             	       else 
                       {
                          s2->id_list = s3;
                          s2 = s3;
                       }
                       p = p->entry.Template.ll_ptr2;
                    }
                 }
  		    (yyvsp[(2) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
		    install_param_list((yyvsp[(2) - (4)].ll_node)->entry.Template.symbol,
				       arg_list, LLNULL, FUNCTION_NAME);
	            delete_beyond_scope_level((yyval.bf_node));
		 
		/* else
		    errstr("cftn.gram: Illegal statement function declaration %s.", $2->entry.Template.symbol->ident); */
               }
	     }
	     else {
		(yyval.bf_node) = get_bfnd(fi,ASSIGN_STAT,SMNULL, (yyvsp[(2) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), LLNULL);
                 parstate = INEXEC;
             }
	  ;}
    break;

  case 505:
#line 4273 "gram1.y"
    { /*PTR_SYMB s;*/
	
	      /*s = make_scalar($2, TYNULL, LOCAL);*/
  	      (yyval.bf_node) = get_bfnd(fi, POINTER_ASSIGN_STAT, SMNULL, (yyvsp[(3) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), LLNULL);
	    ;}
    break;

  case 506:
#line 4285 "gram1.y"
    { PTR_SYMB p;

	      p = make_scalar((yyvsp[(5) - (5)].hash_entry), TYNULL, LOCAL);
	      p->variant = LABEL_VAR;
  	      (yyval.bf_node) = get_bfnd(fi,ASSLAB_STAT, p, (yyvsp[(3) - (5)].ll_node),LLNULL,LLNULL);
            ;}
    break;

  case 507:
#line 4292 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,CONT_STAT,SMNULL,LLNULL,LLNULL,LLNULL); ;}
    break;

  case 509:
#line 4295 "gram1.y"
    { inioctl = NO; ;}
    break;

  case 510:
#line 4297 "gram1.y"
    { PTR_LLND	p;

	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(10) - (10)].ll_node), LLNULL, SMNULL);
	      p = make_llnd(fi,EXPR_LIST, (yyvsp[(8) - (10)].ll_node), p, SMNULL);
	      (yyval.bf_node)= get_bfnd(fi,ARITHIF_NODE, SMNULL, (yyvsp[(4) - (10)].ll_node),
			    make_llnd(fi,EXPR_LIST, (yyvsp[(6) - (10)].ll_node), p, SMNULL), LLNULL);
	      thiswasbranch = YES;
            ;}
    break;

  case 511:
#line 4306 "gram1.y"
    {
	      (yyval.bf_node) = subroutine_call((yyvsp[(1) - (1)].symbol), LLNULL, LLNULL, PLAIN);
/*	      match_parameters($1, LLNULL);
	      $$= get_bfnd(fi,PROC_STAT, $1, LLNULL, LLNULL, LLNULL);
*/	      endioctl(); 
            ;}
    break;

  case 512:
#line 4313 "gram1.y"
    {
	      (yyval.bf_node) = subroutine_call((yyvsp[(1) - (3)].symbol), LLNULL, LLNULL, PLAIN);
/*	      match_parameters($1, LLNULL);
	      $$= get_bfnd(fi,PROC_STAT,$1,LLNULL,LLNULL,LLNULL);
*/	      endioctl(); 
	    ;}
    break;

  case 513:
#line 4320 "gram1.y"
    {
	      (yyval.bf_node) = subroutine_call((yyvsp[(1) - (4)].symbol), (yyvsp[(3) - (4)].ll_node), LLNULL, PLAIN);
/*	      match_parameters($1, $3);
	      $$= get_bfnd(fi,PROC_STAT,$1,$3,LLNULL,LLNULL);
*/	      endioctl(); 
	    ;}
    break;

  case 514:
#line 4328 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,RETURN_STAT,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);
	      thiswasbranch = YES;
	    ;}
    break;

  case 515:
#line 4333 "gram1.y"
    {
	      (yyval.bf_node) = get_bfnd(fi,(yyvsp[(1) - (3)].token),SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);
	      thiswasbranch = ((yyvsp[(1) - (3)].token) == STOP_STAT);
	    ;}
    break;

  case 516:
#line 4338 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, CYCLE_STMT, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL); ;}
    break;

  case 517:
#line 4341 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, EXIT_STMT, (yyvsp[(3) - (3)].symbol), LLNULL, LLNULL, LLNULL); ;}
    break;

  case 518:
#line 4344 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, ALLOCATE_STMT,  SMNULL, (yyvsp[(5) - (6)].ll_node), stat_alloc, LLNULL); ;}
    break;

  case 519:
#line 4347 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, DEALLOCATE_STMT, SMNULL, (yyvsp[(5) - (6)].ll_node), stat_alloc , LLNULL); ;}
    break;

  case 520:
#line 4350 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, NULLIFY_STMT, SMNULL, (yyvsp[(4) - (5)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 521:
#line 4353 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi, WHERE_NODE, SMNULL, (yyvsp[(4) - (8)].ll_node), (yyvsp[(6) - (8)].ll_node), (yyvsp[(8) - (8)].ll_node)); ;}
    break;

  case 522:
#line 4371 "gram1.y"
    {(yyval.ll_node) = LLNULL;;}
    break;

  case 523:
#line 4375 "gram1.y"
    {
	      (yyval.bf_node)=get_bfnd(fi,GOTO_NODE,SMNULL,LLNULL,LLNULL,(PTR_LLND)(yyvsp[(3) - (3)].ll_node));
	      thiswasbranch = YES;
	    ;}
    break;

  case 524:
#line 4380 "gram1.y"
    { PTR_SYMB p;

	      if((yyvsp[(3) - (3)].hash_entry)->id_attr)
		p = (yyvsp[(3) - (3)].hash_entry)->id_attr;
	      else {
	        p = make_scalar((yyvsp[(3) - (3)].hash_entry), TYNULL, LOCAL);
		p->variant = LABEL_VAR;
	      }

	      if(p->variant == LABEL_VAR) {
		  (yyval.bf_node) = get_bfnd(fi,ASSGOTO_NODE,p,LLNULL,LLNULL,LLNULL);
		  thiswasbranch = YES;
	      }
	      else {
		  err("Must go to assigned variable", 48);
		  (yyval.bf_node) = BFNULL;
	      }
	    ;}
    break;

  case 525:
#line 4399 "gram1.y"
    { PTR_SYMB p;

	      if((yyvsp[(3) - (7)].hash_entry)->id_attr)
		p = (yyvsp[(3) - (7)].hash_entry)->id_attr;
	      else {
	        p = make_scalar((yyvsp[(3) - (7)].hash_entry), TYNULL, LOCAL);
		p->variant = LABEL_VAR;
	      }

	      if (p->variant == LABEL_VAR) {
		 (yyval.bf_node) = get_bfnd(fi,ASSGOTO_NODE,p,(yyvsp[(6) - (7)].ll_node),LLNULL,LLNULL);
		 thiswasbranch = YES;
	      }
	      else {
		err("Must go to assigned variable",48);
		(yyval.bf_node) = BFNULL;
	      }
	    ;}
    break;

  case 526:
#line 4418 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,COMGOTO_NODE, SMNULL, (yyvsp[(4) - (7)].ll_node), (yyvsp[(7) - (7)].ll_node), LLNULL); ;}
    break;

  case 529:
#line 4426 "gram1.y"
    { (yyval.symbol) = make_procedure((yyvsp[(3) - (4)].hash_entry), LOCAL); ;}
    break;

  case 530:
#line 4430 "gram1.y"
    { 
              (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node), LLNULL, EXPR_LIST);
              endioctl();
            ;}
    break;

  case 531:
#line 4435 "gram1.y"
    { 
               (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST);
               endioctl();
            ;}
    break;

  case 532:
#line 4442 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 533:
#line 4444 "gram1.y"
    { (yyval.ll_node)  = make_llnd(fi, KEYWORD_ARG, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 534:
#line 4446 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,LABEL_ARG,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 535:
#line 4449 "gram1.y"
    { (yyval.token) = PAUSE_NODE; ;}
    break;

  case 536:
#line 4450 "gram1.y"
    { (yyval.token) = STOP_STAT; ;}
    break;

  case 537:
#line 4461 "gram1.y"
    { if(parstate == OUTSIDE)
		{ PTR_BFND p;

		  p = get_bfnd(fi,PROG_HEDR, make_program(look_up_sym("_MAIN")), LLNULL, LLNULL, LLNULL);
		  set_blobs(p, global_bfnd, NEW_GROUP1);
		  add_scope_level(p, NO);
		  position = IN_PROC; 
		}
		if(parstate < INDATA) enddcl();
		parstate = INEXEC;
		yystno = 0;
	      ;}
    break;

  case 538:
#line 4476 "gram1.y"
    { intonly = YES; ;}
    break;

  case 539:
#line 4480 "gram1.y"
    { intonly = NO; ;}
    break;

  case 540:
#line 4488 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 541:
#line 4491 "gram1.y"
    { PTR_LLND p, q = LLNULL;

		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"unit";
		  q->type = global_string;
		  p = make_llnd(fi, SPEC_PAIR, q, (yyvsp[(2) - (2)].ll_node), SMNULL);
		  (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = p;
		  endioctl();
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 542:
#line 4501 "gram1.y"
    { PTR_LLND p, q, r;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"unit";
		  q->type = global_string;
		  r = make_llnd(fi, SPEC_PAIR, p, q, SMNULL);
		  (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = r;
		  endioctl();
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 543:
#line 4514 "gram1.y"
    { PTR_LLND p, q, r;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"**";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"unit";
		  q->type = global_string;
		  r = make_llnd(fi, SPEC_PAIR, p, q, SMNULL);
		  (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = r;
		  endioctl();
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 544:
#line 4527 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 545:
#line 4530 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 546:
#line 4532 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 547:
#line 4535 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 548:
#line 4538 "gram1.y"
    { (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (3)].ll_node);
		  (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 549:
#line 4542 "gram1.y"
    { (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (4)].ll_node);
		  (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (4)].bf_node); ;}
    break;

  case 550:
#line 4551 "gram1.y"
    { (yyvsp[(1) - (2)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (2)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (2)].bf_node); ;}
    break;

  case 551:
#line 4554 "gram1.y"
    { (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr2 = (yyvsp[(2) - (3)].ll_node);
		  (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 552:
#line 4558 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (1)].bf_node); ;}
    break;

  case 553:
#line 4560 "gram1.y"
    { (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node);
		  (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 554:
#line 4566 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 555:
#line 4570 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, BACKSPACE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 556:
#line 4572 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, REWIND_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 557:
#line 4574 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, ENDFILE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 558:
#line 4581 "gram1.y"
    { (yyval.bf_node) = (yyvsp[(1) - (3)].bf_node); ;}
    break;

  case 559:
#line 4585 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, OPEN_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 560:
#line 4587 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, CLOSE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 561:
#line 4591 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi, INQUIRE_STAT, SMNULL, LLNULL, (yyvsp[(4) - (4)].ll_node), LLNULL);;}
    break;

  case 562:
#line 4593 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi, INQUIRE_STAT, SMNULL, (yyvsp[(5) - (5)].ll_node), (yyvsp[(4) - (5)].ll_node), LLNULL);;}
    break;

  case 563:
#line 4597 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q = LLNULL;

		  if ((yyvsp[(1) - (1)].ll_node)->variant == INT_VAL)
 	          {
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(1) - (1)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		  }
		  else p = (yyvsp[(1) - (1)].ll_node); 
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		  endioctl();
		;}
    break;

  case 564:
#line 4616 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		  endioctl();
		;}
    break;

  case 565:
#line 4632 "gram1.y"
    { PTR_LLND p;

		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"unit";
		  p->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, p, (yyvsp[(2) - (3)].ll_node), SMNULL);
		  endioctl();
		;}
    break;

  case 566:
#line 4643 "gram1.y"
    { 
		  (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);
		  endioctl();
		 ;}
    break;

  case 567:
#line 4650 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); endioctl();;}
    break;

  case 568:
#line 4652 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST); endioctl();;}
    break;

  case 569:
#line 4656 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;
 
		  nioctl++;
		  if ((nioctl == 2) && ((yyvsp[(1) - (1)].ll_node)->variant == INT_VAL))
 	          {
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(1) - (1)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		  }
		  else p = (yyvsp[(1) - (1)].ll_node); 
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  if (nioctl == 1)
		        q->entry.string_val = (char *)"unit"; 
		  else {
                     if(((yyvsp[(1) - (1)].ll_node)->variant == VAR_REF) && (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol->variant == NAMELIST_NAME)
                       q->entry.string_val = (char *)"nml";
                     else
                       q->entry.string_val = (char *)"fmt";
                  }
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		;}
    break;

  case 570:
#line 4682 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;

		  nioctl++;
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  if (nioctl == 1)
		        q->entry.string_val = (char *)"unit"; 
		  else  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		;}
    break;

  case 571:
#line 4697 "gram1.y"
    { PTR_LLND p;
		  PTR_LLND q;

		  nioctl++;
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"**";
		  p->type = global_string;
		  q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  if (nioctl == 1)
		        q->entry.string_val = (char *)"unit"; 
		  else  q->entry.string_val = (char *)"fmt";
		  q->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
		;}
    break;

  case 572:
#line 4712 "gram1.y"
    { 
		  PTR_LLND p;
		  char *q;

		  q = (yyvsp[(1) - (2)].ll_node)->entry.string_val;
  		  if ((strcmp(q, "end") == 0) || (strcmp(q, "err") == 0) || (strcmp(q, "eor") == 0) || ((strcmp(q,"fmt") == 0) && ((yyvsp[(2) - (2)].ll_node)->variant == INT_VAL)))
 	          {
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(2) - (2)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		  }
		  else p = (yyvsp[(2) - (2)].ll_node);

		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), p, SMNULL); ;}
    break;

  case 573:
#line 4729 "gram1.y"
    { PTR_LLND p;
                  
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), p, SMNULL);
		;}
    break;

  case 574:
#line 4737 "gram1.y"
    { PTR_LLND p;
		  p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  p->entry.string_val = (char *)"*";
		  p->type = global_string;
		  (yyval.ll_node) = make_llnd(fi, SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), p, SMNULL);
		;}
    break;

  case 575:
#line 4746 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
		  (yyval.ll_node)->entry.string_val = copys(yytext);
		  (yyval.ll_node)->type = global_string;
	        ;}
    break;

  case 576:
#line 4754 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, READ_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 577:
#line 4759 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi, WRITE_STAT, SMNULL, LLNULL, LLNULL, LLNULL);;}
    break;

  case 578:
#line 4764 "gram1.y"
    {
	    PTR_LLND p, q, l;

	    if ((yyvsp[(3) - (4)].ll_node)->variant == INT_VAL)
		{
		        PTR_LABEL r;

			r = make_label_node(fi, (long) (yyvsp[(3) - (4)].ll_node)->entry.ival);
			r->scope = cur_scope();
			p = make_llnd_label(fi, LABEL_REF, r);
		}
	    else p = (yyvsp[(3) - (4)].ll_node);
	    
            q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	    q->entry.string_val = (char *)"fmt";
            q->type = global_string;
            l = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);

            (yyval.bf_node) = get_bfnd(fi, PRINT_STAT, SMNULL, LLNULL, l, LLNULL);
	    endioctl();
	   ;}
    break;

  case 579:
#line 4786 "gram1.y"
    { PTR_LLND p, q, r;
		
	     p = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	     p->entry.string_val = (char *)"*";
	     p->type = global_string;
	     q = make_llnd(fi, KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
	     q->entry.string_val = (char *)"fmt";
             q->type = global_string;
             r = make_llnd(fi, SPEC_PAIR, q, p, SMNULL);
	     (yyval.bf_node) = get_bfnd(fi, PRINT_STAT, SMNULL, LLNULL, r, LLNULL);
	     endioctl();
           ;}
    break;

  case 580:
#line 4802 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST);;}
    break;

  case 581:
#line 4804 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST);;}
    break;

  case 582:
#line 4808 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 583:
#line 4810 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) = (yyvsp[(4) - (5)].ll_node);
		;}
    break;

  case 584:
#line 4817 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST);  (yyval.ll_node)->type = (yyvsp[(1) - (1)].ll_node)->type;;}
    break;

  case 585:
#line 4819 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 586:
#line 4821 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 587:
#line 4825 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 588:
#line 4827 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 589:
#line 4829 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 590:
#line 4831 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 591:
#line 4833 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 592:
#line 4835 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); (yyval.ll_node)->type = (yyvsp[(1) - (3)].ll_node)->type;;}
    break;

  case 593:
#line 4839 "gram1.y"
    { (yyval.ll_node) =  set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST);
	          (yyval.ll_node)->type = global_complex; ;}
    break;

  case 594:
#line 4842 "gram1.y"
    { (yyval.ll_node) =  set_ll_list((yyvsp[(2) - (3)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (3)].ll_node)->type; ;}
    break;

  case 595:
#line 4845 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) =  set_ll_list((yyvsp[(4) - (5)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (5)].ll_node)->type; 
		;}
    break;

  case 596:
#line 4851 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) =  set_ll_list((yyvsp[(4) - (5)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (5)].ll_node)->type; 
		;}
    break;

  case 597:
#line 4857 "gram1.y"
    {
		  (yyvsp[(4) - (5)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(2) - (5)].ll_node);
		  (yyval.ll_node) =  set_ll_list((yyvsp[(4) - (5)].ll_node), LLNULL, EXPR_LIST);
                  (yyval.ll_node)->type = (yyvsp[(2) - (5)].ll_node)->type; 
		;}
    break;

  case 598:
#line 4865 "gram1.y"
    { inioctl = YES; ;}
    break;

  case 599:
#line 4869 "gram1.y"
    { startioctl();;}
    break;

  case 600:
#line 4877 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 601:
#line 4879 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node); ;}
    break;

  case 602:
#line 4883 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 603:
#line 4885 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 604:
#line 4887 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,(yyvsp[(2) - (3)].token), (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 605:
#line 4892 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,MULT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 606:
#line 4897 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,DIV_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 607:
#line 4902 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,EXP_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 608:
#line 4907 "gram1.y"
    {
	      if((yyvsp[(1) - (2)].token) == SUBT_OP)
		{
		  (yyval.ll_node) = make_llnd(fi,SUBT_OP, (yyvsp[(2) - (2)].ll_node), LLNULL, SMNULL);
		  set_expr_type((yyval.ll_node));
		}
	      else	(yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);
	    ;}
    break;

  case 609:
#line 4916 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,CONCAT_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);
	      set_expr_type((yyval.ll_node));
	    ;}
    break;

  case 610:
#line 4921 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 611:
#line 4926 "gram1.y"
    { comments = cur_comment = CMNULL; ;}
    break;

  case 612:
#line 4928 "gram1.y"
    { PTR_CMNT p;
	    p = make_comment(fi,*commentbuf, HALF);
	    if (cur_comment)
               cur_comment->next = p;
            else {
	       if ((pred_bfnd->control_parent->variant == LOGIF_NODE) ||(pred_bfnd->control_parent->variant == FORALL_STAT))

	           pred_bfnd->control_parent->entry.Template.cmnt_ptr = p;

	       else last_bfnd->entry.Template.cmnt_ptr = p;
            }
	    comments = cur_comment = CMNULL;
          ;}
    break;

  case 676:
#line 5011 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_TEMPLATE_STAT, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL); ;}
    break;

  case 677:
#line 5013 "gram1.y"
    { PTR_SYMB s;
                if((yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr2)
                {
                  s = (yyvsp[(3) - (3)].ll_node)->entry.Template.ll_ptr1->entry.Template.symbol;
                  s->attr = s->attr | COMMON_BIT;
                }
	        add_to_lowLevelList((yyvsp[(3) - (3)].ll_node), (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	      ;}
    break;

  case 678:
#line 5024 "gram1.y"
    {PTR_SYMB s;
	      PTR_LLND q;
	    /* 27.06.18
	      if(! explicit_shape)   
                err("Explicit shape specification is required", 50);
	    */  
	      s = make_array((yyvsp[(1) - (2)].hash_entry), TYNULL, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
              if(s->attr & TEMPLATE_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT))
                errstr( "Inconsistent declaration of identifier  %s ", s->ident, 16);
              else
	        s->attr = s->attr | TEMPLATE_BIT;
              if((yyvsp[(2) - (2)].ll_node)) s->attr = s->attr | DIMENSION_BIT;  
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
	      (yyval.ll_node) = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	     ;}
    break;

  case 679:
#line 5045 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_DYNAMIC_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 680:
#line 5049 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 681:
#line 5051 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 682:
#line 5055 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if(s->attr &  DYNAMIC_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & HEAP_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
              else
                s->attr = s->attr | DYNAMIC_BIT;        
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 683:
#line 5068 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_INHERIT_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 684:
#line 5072 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 685:
#line 5074 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 686:
#line 5078 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & ALIGN_BIT) || (s->attr & DISTRIBUTE_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
              else
                s->attr = s->attr | INHERIT_BIT;        
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 687:
#line 5089 "gram1.y"
    { PTR_LLND q;
             q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
              /* (void)fprintf(stderr,"hpf.gram: shadow\n");*/ 
	     (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_DIR,SMNULL,q,(yyvsp[(4) - (4)].ll_node),LLNULL);
            ;}
    break;

  case 688:
#line 5100 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);;}
    break;

  case 689:
#line 5104 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 690:
#line 5106 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 691:
#line 5110 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 692:
#line 5112 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);;}
    break;

  case 693:
#line 5114 "gram1.y"
    {
            if(parstate!=INEXEC) 
               err("Illegal shadow width specification", 56);  
            (yyval.ll_node) = make_llnd(fi,SHADOW_NAMES_OP, (yyvsp[(3) - (4)].ll_node), LLNULL, SMNULL);
          ;}
    break;

  case 694:
#line 5129 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if(s->attr & SHADOW_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & HEAP_BIT)) 
                      errstr( "Inconsistent declaration of identifier %s", s->ident, 16); 
              else
        	      s->attr = s->attr | SHADOW_BIT;  
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 695:
#line 5141 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
              err("Explicit shape specification is required", 50);
		/* $$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr &  PROCESSORS_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT) || (s->attr & INHERIT_BIT))
                errstr("Inconsistent declaration of identifier %s", s->ident, 16);
              else
	        s->attr = s->attr | PROCESSORS_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,HPF_PROCESSORS_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 696:
#line 5161 "gram1.y"
    { PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
              err("Explicit shape specification is required", 50);
		/* $$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr &  PROCESSORS_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & TASK_BIT) || (s->attr & DVM_POINTER_BIT) || (s->attr & INHERIT_BIT))
                errstr("Inconsistent declaration of identifier %s", s->ident, 16);
              else
	        s->attr = s->attr | PROCESSORS_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      (yyval.bf_node) = get_bfnd(fi,HPF_PROCESSORS_STAT, SMNULL, r, LLNULL, LLNULL);
	    ;}
    break;

  case 697:
#line 5181 "gram1.y"
    {  PTR_SYMB s;
	      PTR_LLND q, r;
	      if(! explicit_shape) {
		err("Explicit shape specification is required", 50);
		/*$$ = BFNULL;*/
	      }
	      s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, (yyvsp[(4) - (4)].ll_node), ndim, LOCAL);
              if(s->attr &  PROCESSORS_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & TASK_BIT) || (s->attr &  DVM_POINTER_BIT) || (s->attr & INHERIT_BIT) )
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16);
              else
	        s->attr = s->attr | PROCESSORS_BIT;
              if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
	      q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	      s->type->entry.ar_decl.ranges = (yyvsp[(4) - (4)].ll_node);
	      r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	      add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	;}
    break;

  case 698:
#line 5203 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_INDIRECT_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 699:
#line 5209 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	           ;
                ;}
    break;

  case 700:
#line 5218 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), REF_GROUP_NAME,global_default,LOCAL);
          if((yyval.symbol)->attr &  INDIRECT_BIT)
                errstr( "Multiple declaration of identifier  %s ", (yyval.symbol)->ident, 73);
           (yyval.symbol)->attr = (yyval.symbol)->attr | INDIRECT_BIT;
          ;}
    break;

  case 701:
#line 5226 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_REMOTE_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 702:
#line 5232 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
                ;}
    break;

  case 703:
#line 5240 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), REF_GROUP_NAME,global_default,LOCAL);
           if((yyval.symbol)->attr &  INDIRECT_BIT)
                errstr( "Inconsistent declaration of identifier  %s ", (yyval.symbol)->ident, 16);
          ;}
    break;

  case 704:
#line 5247 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_REDUCTION_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 705:
#line 5253 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
	           ;
                ;}
    break;

  case 706:
#line 5262 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), REDUCTION_GROUP_NAME,global_default,LOCAL);;}
    break;

  case 707:
#line 5266 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	           (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_GROUP_DIR, SMNULL, r, LLNULL, LLNULL);
                ;}
    break;

  case 708:
#line 5272 "gram1.y"
    {  PTR_LLND q,r;
                   q = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(3) - (3)].symbol));
                   r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
                   add_to_lowLevelList(r, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);	           
                ;}
    break;

  case 709:
#line 5280 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), CONSISTENT_GROUP_NAME,global_default,LOCAL);;}
    break;

  case 710:
#line 5294 "gram1.y"
    { PTR_SYMB s;
            if(parstate == INEXEC){
              if (!(s = (yyvsp[(2) - (3)].hash_entry)->id_attr))
              {
	         s = make_array((yyvsp[(2) - (3)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
	     	 s->decl = SOFT;
	      } 
            } else
              s = make_array((yyvsp[(2) - (3)].hash_entry), TYNULL, LLNULL, 0, LOCAL);

              (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (3)].ll_node), LLNULL, s);
            ;}
    break;

  case 711:
#line 5307 "gram1.y"
    { (yyval.ll_node) = LLNULL; opt_kwd_ = NO;;}
    break;

  case 712:
#line 5313 "gram1.y"
    { PTR_LLND q;
             if(!(yyvsp[(4) - (5)].ll_node))
               err("Distribution format list is omitted", 51);
            /* if($6)
               err("NEW_VALUE specification in DISTRIBUTE directive");*/
             q = set_ll_list((yyvsp[(3) - (5)].ll_node),LLNULL,EXPR_LIST);
	     (yyval.bf_node) = get_bfnd(fi,DVM_DISTRIBUTE_DIR,SMNULL,q,(yyvsp[(4) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node));
            ;}
    break;

  case 713:
#line 5329 "gram1.y"
    { PTR_LLND q;
                /*  if(!$4)
                  {err("Distribution format is omitted", 51); errcnt--;}
                 */
              q = set_ll_list((yyvsp[(3) - (6)].ll_node),LLNULL,EXPR_LIST);
                 /* r = LLNULL;
                   if($6){
                     r = set_ll_list($6,LLNULL,EXPR_LIST);
                     if($7) r = set_ll_list(r,$7,EXPR_LIST);
                   } else
                     if($7) r = set_ll_list(r,$7,EXPR_LIST);
                 */
	      (yyval.bf_node) = get_bfnd(fi,DVM_REDISTRIBUTE_DIR,SMNULL,q,(yyvsp[(4) - (6)].ll_node),(yyvsp[(6) - (6)].ll_node));;}
    break;

  case 714:
#line 5344 "gram1.y"
    {
                 /* r = LLNULL;
                    if($5){
                      r = set_ll_list($5,LLNULL,EXPR_LIST);
                      if($6) r = set_ll_list(r,$6,EXPR_LIST);
                    } else
                      if($6) r = set_ll_list(r,$6,EXPR_LIST);
                  */
	      (yyval.bf_node) = get_bfnd(fi,DVM_REDISTRIBUTE_DIR,SMNULL,(yyvsp[(8) - (8)].ll_node) ,(yyvsp[(3) - (8)].ll_node),(yyvsp[(5) - (8)].ll_node) );
             ;}
    break;

  case 715:
#line 5372 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 716:
#line 5374 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 717:
#line 5378 "gram1.y"
    {(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 718:
#line 5380 "gram1.y"
    {(yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 719:
#line 5384 "gram1.y"
    {  PTR_SYMB s;
 
          if(parstate == INEXEC){
            if (!(s = (yyvsp[(1) - (1)].hash_entry)->id_attr))
              {
	         s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
	     	 s->decl = SOFT;
	      } 
            if(s->attr & PROCESSORS_BIT)
              errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);
            if(s->attr & TASK_BIT)
              errstr( "Illegal use of task array name %s ", s->ident, 71);

          } else {
            s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
            if(s->attr & DISTRIBUTE_BIT)
              errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
            else if( (s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & INHERIT_BIT))
              errstr("Inconsistent declaration of identifier  %s",s->ident, 16);
            else
              s->attr = s->attr | DISTRIBUTE_BIT;
          } 
         if(s->attr & ALIGN_BIT)
               errstr("A distributee may not have the ALIGN attribute:%s",s->ident, 54);
          (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);               	  
	;}
    break;

  case 720:
#line 5413 "gram1.y"
    {  PTR_SYMB s;
          s = make_array((yyvsp[(1) - (4)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
        
          if(parstate != INEXEC) 
               errstr( "Illegal distributee:%s", s->ident, 312);
          else {
            if(s->attr & PROCESSORS_BIT)
               errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);  
            if(s->attr & TASK_BIT)
               errstr( "Illegal use of task array name %s ", s->ident, 71);        
            if(s->attr & ALIGN_BIT)
               errstr("A distributee may not have the ALIGN attribute:%s",s->ident, 54);
            if(!(s->attr & DVM_POINTER_BIT))
               errstr("Illegal distributee:%s", s->ident, 312);
          /*s->attr = s->attr | DISTRIBUTE_BIT;*/
	  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, s); 
          }
        
	;}
    break;

  case 721:
#line 5436 "gram1.y"
    {  PTR_SYMB s;
          if((s=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL)
            s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
          if((parstate == INEXEC) && !(s->attr & PROCESSORS_BIT))
               errstr( "'%s' is not processor array ", s->ident, 67);
	  (yyval.symbol) = s;
	;}
    break;

  case 722:
#line 5456 "gram1.y"
    { (yyval.ll_node) = LLNULL;  ;}
    break;

  case 723:
#line 5458 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (2)].ll_node);;}
    break;

  case 724:
#line 5462 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);;}
    break;

  case 725:
#line 5483 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 726:
#line 5485 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),EXPR_LIST); ;}
    break;

  case 727:
#line 5488 "gram1.y"
    { opt_kwd_ = YES; ;}
    break;

  case 728:
#line 5497 "gram1.y"
    {  
               (yyval.ll_node) = make_llnd(fi,BLOCK_OP, LLNULL, LLNULL, SMNULL);
        ;}
    break;

  case 729:
#line 5501 "gram1.y"
    {  err("Distribution format BLOCK(n) is not permitted in FDVM", 55);
          (yyval.ll_node) = make_llnd(fi,BLOCK_OP, (yyvsp[(4) - (5)].ll_node), LLNULL, SMNULL);
          endioctl();
        ;}
    break;

  case 730:
#line 5506 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,BLOCK_OP, LLNULL, LLNULL, (yyvsp[(3) - (4)].symbol)); ;}
    break;

  case 731:
#line 5508 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,BLOCK_OP,  (yyvsp[(5) - (6)].ll_node),  LLNULL,  (yyvsp[(3) - (6)].symbol)); ;}
    break;

  case 732:
#line 5510 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,BLOCK_OP,  LLNULL, (yyvsp[(3) - (4)].ll_node),  SMNULL); ;}
    break;

  case 733:
#line 5512 "gram1.y"
    { 
          (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
          (yyval.ll_node)->entry.string_val = (char *) "*";
          (yyval.ll_node)->type = global_string;
        ;}
    break;

  case 734:
#line 5518 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,INDIRECT_OP, LLNULL, LLNULL, (yyvsp[(3) - (4)].symbol)); ;}
    break;

  case 735:
#line 5520 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,INDIRECT_OP, (yyvsp[(3) - (4)].ll_node), LLNULL, SMNULL); ;}
    break;

  case 736:
#line 5524 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
       
	      (yyval.symbol) = s;
	   ;}
    break;

  case 737:
#line 5534 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DERIVED_OP, (yyvsp[(2) - (6)].ll_node), (yyvsp[(6) - (6)].ll_node), SMNULL); ;}
    break;

  case 738:
#line 5538 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 739:
#line 5540 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 740:
#line 5545 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 741:
#line 5547 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);;}
    break;

  case 742:
#line 5551 "gram1.y"
    { 
              (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol));
	    ;}
    break;

  case 743:
#line 5555 "gram1.y"
    { 
              (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, (yyvsp[(1) - (4)].symbol));
	    ;}
    break;

  case 744:
#line 5561 "gram1.y"
    { 
              if (!((yyval.symbol) = (yyvsp[(1) - (1)].hash_entry)->id_attr))
              {
	         (yyval.symbol) = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL,0,LOCAL);
	         (yyval.symbol)->decl = SOFT;
	      } 
            ;}
    break;

  case 745:
#line 5571 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 746:
#line 5573 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 747:
#line 5577 "gram1.y"
    {  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 748:
#line 5579 "gram1.y"
    {  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 749:
#line 5581 "gram1.y"
    {
                      (yyvsp[(2) - (3)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (3)].ll_node); 
                      (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);   
                   ;}
    break;

  case 750:
#line 5588 "gram1.y"
    { PTR_SYMB s;
            s = make_scalar((yyvsp[(1) - (1)].hash_entry),TYNULL,LOCAL);
	    (yyval.ll_node) = make_llnd(fi,DUMMY_REF, LLNULL, LLNULL, s);
            /*$$->type = global_int;*/
          ;}
    break;

  case 751:
#line 5605 "gram1.y"
    {  (yyval.ll_node) = LLNULL; ;}
    break;

  case 752:
#line 5607 "gram1.y"
    {  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 753:
#line 5611 "gram1.y"
    {  (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 754:
#line 5613 "gram1.y"
    {  (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 755:
#line 5617 "gram1.y"
    {  if((yyvsp[(1) - (1)].ll_node)->type->variant != T_STRING)
                 errstr( "Illegal type of shadow_name", 627);
               (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); 
            ;}
    break;

  case 756:
#line 5624 "gram1.y"
    { char *q;
          nioctl = 1;
          q = (yyvsp[(1) - (2)].ll_node)->entry.string_val;
          if((!strcmp(q,"shadow")) && ((yyvsp[(2) - (2)].ll_node)->variant == INT_VAL))                          (yyval.ll_node) = make_llnd(fi,SPEC_PAIR, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL);
          else
          {  err("Illegal shadow width specification", 56);
             (yyval.ll_node) = LLNULL;
          }
        ;}
    break;

  case 757:
#line 5634 "gram1.y"
    { char *ql, *qh;
          PTR_LLND p1, p2;
          nioctl = 2;
          ql = (yyvsp[(1) - (5)].ll_node)->entry.string_val;
          qh = (yyvsp[(4) - (5)].ll_node)->entry.string_val;
          if((!strcmp(ql,"low_shadow")) && ((yyvsp[(2) - (5)].ll_node)->variant == INT_VAL) && (!strcmp(qh,"high_shadow")) && ((yyvsp[(5) - (5)].ll_node)->variant == INT_VAL)) 
              {
                 p1 = make_llnd(fi,SPEC_PAIR, (yyvsp[(1) - (5)].ll_node), (yyvsp[(2) - (5)].ll_node), SMNULL);
                 p2 = make_llnd(fi,SPEC_PAIR, (yyvsp[(4) - (5)].ll_node), (yyvsp[(5) - (5)].ll_node), SMNULL);
                 (yyval.ll_node) = make_llnd(fi,CONS, p1, p2, SMNULL);
              } 
          else
          {  err("Illegal shadow width specification", 56);
             (yyval.ll_node) = LLNULL;
          }
        ;}
    break;

  case 758:
#line 5663 "gram1.y"
    { PTR_LLND q;
              q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
              (yyval.bf_node) = (yyvsp[(4) - (4)].bf_node);
              (yyval.bf_node)->entry.Template.ll_ptr1 = q;
            ;}
    break;

  case 759:
#line 5678 "gram1.y"
    { PTR_LLND q;
              q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
              (yyval.bf_node) = (yyvsp[(4) - (4)].bf_node);
              (yyval.bf_node)->variant = DVM_REALIGN_DIR; 
              (yyval.bf_node)->entry.Template.ll_ptr1 = q;
            ;}
    break;

  case 760:
#line 5685 "gram1.y"
    {
              (yyval.bf_node) = (yyvsp[(3) - (6)].bf_node);
              (yyval.bf_node)->variant = DVM_REALIGN_DIR; 
              (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(6) - (6)].ll_node);
            ;}
    break;

  case 761:
#line 5703 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 762:
#line 5705 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 763:
#line 5709 "gram1.y"
    {  PTR_SYMB s;
          s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
          if((s->attr & ALIGN_BIT)) 
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
          if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & INHERIT_BIT)) 
                errstr( "Inconsistent declaration of identifier  %s", s->ident, 16); 
          else  if(s->attr & DISTRIBUTE_BIT)
               errstr( "An alignee may not have the DISTRIBUTE attribute:'%s'", s->ident,57);             else
                s->attr = s->attr | ALIGN_BIT;     
	  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	;}
    break;

  case 764:
#line 5723 "gram1.y"
    {PTR_SYMB s;
        s = (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol;
        if(s->attr & PROCESSORS_BIT)
               errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);
        else  if(s->attr & TASK_BIT)
              errstr( "Illegal use of task array name %s ", s->ident, 71);
        else if( !(s->attr & DIMENSION_BIT) && !(s->attr & DVM_POINTER_BIT))
            errstr("The alignee %s isn't an array", s->ident, 58);
        else {
            /*  if(!(s->attr & DYNAMIC_BIT))
                 errstr("'%s' hasn't the DYNAMIC attribute", s->ident, 59);
             */
              if(!(s->attr & ALIGN_BIT) && !(s->attr & INHERIT_BIT))
                 errstr("'%s' hasn't the ALIGN attribute", s->ident, 60);
              if(s->attr & DISTRIBUTE_BIT)
                 errstr("An alignee may not have the DISTRIBUTE attribute: %s", s->ident, 57);

/*               if(s->entry.var_decl.local == IO)
 *                 errstr("An alignee may not be the dummy argument");
*/
          }
	  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	;}
    break;

  case 765:
#line 5749 "gram1.y"
    { /* PTR_LLND r;
              if($7) {
                r = set_ll_list($6,LLNULL,EXPR_LIST);
                r = set_ll_list(r,$7,EXPR_LIST);
              }
              else
                r = $6;
              */
            (yyval.bf_node) = get_bfnd(fi,DVM_ALIGN_DIR,SMNULL,LLNULL,(yyvsp[(2) - (6)].ll_node),(yyvsp[(6) - (6)].ll_node));
           ;}
    break;

  case 766:
#line 5762 "gram1.y"
    {
           (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, (yyvsp[(1) - (4)].symbol));        
          ;}
    break;

  case 767:
#line 5778 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 768:
#line 5780 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 769:
#line 5783 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 770:
#line 5785 "gram1.y"
    {
                  (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
                  (yyval.ll_node)->entry.string_val = (char *) "*";
                  (yyval.ll_node)->type = global_string;
                 ;}
    break;

  case 771:
#line 5791 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 772:
#line 5795 "gram1.y"
    { 
         /* if(parstate == INEXEC){ *for REALIGN directive*
              if (!($$ = $1->id_attr))
              {
	         $$ = make_array($1, TYNULL, LLNULL,0,LOCAL);
	     	 $$->decl = SOFT;
	      } 
          } else
             $$ = make_array($1, TYNULL, LLNULL, 0, LOCAL);
          */
          if (!((yyval.symbol) = (yyvsp[(1) - (1)].hash_entry)->id_attr))
          {
	       (yyval.symbol) = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL,0,LOCAL);
	       (yyval.symbol)->decl = SOFT;
	  } 
          (yyval.symbol)->attr = (yyval.symbol)->attr | ALIGN_BASE_BIT;
          if((yyval.symbol)->attr & PROCESSORS_BIT)
               errstr( "Illegal use of PROCESSORS name %s ", (yyval.symbol)->ident, 53);
          else  if((yyval.symbol)->attr & TASK_BIT)
               errstr( "Illegal use of task array name %s ", (yyval.symbol)->ident, 71);
          else
          if((parstate == INEXEC) /* for  REALIGN directive */
             &&   !((yyval.symbol)->attr & DIMENSION_BIT) && !((yyval.symbol)->attr & DVM_POINTER_BIT))
            errstr("The align-target %s isn't declared as array", (yyval.symbol)->ident, 61); 
         ;}
    break;

  case 773:
#line 5823 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 774:
#line 5825 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 775:
#line 5829 "gram1.y"
    { PTR_SYMB s;
            s = make_scalar((yyvsp[(1) - (1)].hash_entry),TYNULL,LOCAL);
            if(s->type->variant != T_INT || s->attr & PARAMETER_BIT)             
              errstr("The align-dummy %s isn't a scalar integer variable", s->ident, 62); 
	   (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
           (yyval.ll_node)->type = global_int;
         ;}
    break;

  case 776:
#line 5837 "gram1.y"
    {  
          (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
          (yyval.ll_node)->entry.string_val = (char *) "*";
          (yyval.ll_node)->type = global_string;
        ;}
    break;

  case 777:
#line 5843 "gram1.y"
    {   (yyval.ll_node) = make_llnd(fi,DDOT, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 778:
#line 5846 "gram1.y"
    { PTR_SYMB s;
	             PTR_LLND q, r, p;
                     int numdim;
                     if(type_options & PROCESSORS_BIT) {    /* 27.06.18 || (type_options & TEMPLATE_BIT)){ */
                       if(! explicit_shape) {
                         err("Explicit shape specification is required", 50);
		         /*$$ = BFNULL;*/
	               }
                     } 

                    /*  else {
                       if($6)
                         err("Shape specification is not permitted", 263);
                     } */

                     if(type_options & DIMENSION_BIT)
                       { p = attr_dims; numdim = attr_ndim;}
                     else
                       { p = LLNULL; numdim = 0; }
                     if((yyvsp[(6) - (6)].ll_node))          /*dimension information after the object name*/
                     { p = (yyvsp[(6) - (6)].ll_node); numdim = ndim;} /*overrides the DIMENSION attribute */
	             s = make_array((yyvsp[(5) - (6)].hash_entry), TYNULL, p, numdim, LOCAL);

                     if((type_options & COMMON_BIT) && !(type_options & TEMPLATE_BIT))
                     {
                        err("Illegal combination of attributes", 63);
                        type_options = type_options & (~COMMON_BIT);
                     }
                     if((type_options & PROCESSORS_BIT) &&((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT) ))
                        err("Illegal combination of attributes", 63);
                     else  if((type_options & PROCESSORS_BIT) && ((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT)) )
                     {  errstr("Inconsistent declaration of  %s", s->ident, 16);
                        type_options = type_options & (~PROCESSORS_BIT);
                     }
                     else if ((s->attr & PROCESSORS_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT))) 
                        errstr("Inconsistent declaration of  %s", s->ident, 16);
                     else if ((s->attr & INHERIT_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT)))
                        errstr("Inconsistent declaration of  %s", s->ident, 16);
                     if(( s->attr & DISTRIBUTE_BIT) &&  (type_options & DISTRIBUTE_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & ALIGN_BIT) &&  (type_options & ALIGN_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & SHADOW_BIT) &&  (type_options & SHADOW_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & TEMPLATE_BIT) &&  (type_options & TEMPLATE_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & PROCESSORS_BIT) &&  (type_options & PROCESSORS_BIT))
                           errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
	             s->attr = s->attr | type_options;
                     if((yyvsp[(6) - (6)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
                     if((s->attr & DISTRIBUTE_BIT) && (s->attr & ALIGN_BIT))
                       errstr("%s has the DISTRIBUTE and ALIGN attribute",s->ident, 64);
	             q = make_llnd(fi,ARRAY_REF, (yyvsp[(6) - (6)].ll_node), LLNULL, s);
	             if(p) s->type->entry.ar_decl.ranges = p;
	             r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	             (yyval.bf_node) = get_bfnd(fi,DVM_VAR_DECL, SMNULL, r, LLNULL,(yyvsp[(1) - (6)].ll_node));
	            ;}
    break;

  case 779:
#line 5904 "gram1.y"
    { PTR_SYMB s;
	             PTR_LLND q, r, p;
                     int numdim;
                    if(type_options & PROCESSORS_BIT) { /*23.10.18  || (type_options & TEMPLATE_BIT)){ */
                       if(! explicit_shape) {
                         err("Explicit shape specification is required", 50);
		         /*$$ = BFNULL;*/
	               }
                     } 
                    /* else {
                       if($4)
                         err("Shape specification is not permitted", 263);
                     } */
                     if(type_options & DIMENSION_BIT)
                       { p = attr_dims; numdim = attr_ndim;}
                     else
                       { p = LLNULL; numdim = 0; }
                     if((yyvsp[(4) - (4)].ll_node))                   /*dimension information after the object name*/
                     { p = (yyvsp[(4) - (4)].ll_node); numdim = ndim;}/*overrides the DIMENSION attribute */
	             s = make_array((yyvsp[(3) - (4)].hash_entry), TYNULL, p, numdim, LOCAL);

                     if((type_options & COMMON_BIT) && !(type_options & TEMPLATE_BIT))
                     {
                        err("Illegal combination of attributes", 63);
                        type_options = type_options & (~COMMON_BIT);
                     }
                     if((type_options & PROCESSORS_BIT) &&((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT) ))
                       err("Illegal combination of attributes", 63);
                     else  if((type_options & PROCESSORS_BIT) && ((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT)) )
                     {  errstr("Inconsistent declaration of identifier %s", s->ident, 16);
                        type_options = type_options & (~PROCESSORS_BIT);
                     }
                     else if ((s->attr & PROCESSORS_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT) ||(type_options & TEMPLATE_BIT) || (type_options & DYNAMIC_BIT) ||(type_options & SHADOW_BIT))) 
                          errstr("Inconsistent declaration of identifier  %s", s->ident,16);
                     else if ((s->attr & INHERIT_BIT) && ((type_options & ALIGN_BIT) ||(type_options & DISTRIBUTE_BIT)))
                          errstr("Inconsistent declaration of identifier %s", s->ident, 16);
                     if(( s->attr & DISTRIBUTE_BIT) &&  (type_options & DISTRIBUTE_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & ALIGN_BIT) &&  (type_options & ALIGN_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & SHADOW_BIT) &&  (type_options & SHADOW_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & TEMPLATE_BIT) &&  (type_options & TEMPLATE_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
                     if(( s->attr & PROCESSORS_BIT) &&  (type_options & PROCESSORS_BIT))
                          errstr( "Multiple declaration of identifier  %s ", s->ident, 73);   
	             s->attr = s->attr | type_options;
                     if((yyvsp[(4) - (4)].ll_node)) s->attr = s->attr | DIMENSION_BIT;
                     if((s->attr & DISTRIBUTE_BIT) && (s->attr & ALIGN_BIT))
                           errstr("%s has the DISTRIBUTE and ALIGN attribute",s->ident, 64);
	             q = make_llnd(fi,ARRAY_REF, (yyvsp[(4) - (4)].ll_node), LLNULL, s);
	             if(p) s->type->entry.ar_decl.ranges = p;
	             r = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);
	             add_to_lowLevelList(r, (yyvsp[(1) - (4)].bf_node)->entry.Template.ll_ptr1);
	            ;}
    break;

  case 780:
#line 5968 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); type_options = type_opt; ;}
    break;

  case 781:
#line 5970 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),EXPR_LIST); type_options = type_options | type_opt;;}
    break;

  case 782:
#line 5973 "gram1.y"
    { type_opt = TEMPLATE_BIT;
               (yyval.ll_node) = make_llnd(fi,TEMPLATE_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 783:
#line 5977 "gram1.y"
    { type_opt = PROCESSORS_BIT;
                (yyval.ll_node) = make_llnd(fi,PROCESSORS_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 784:
#line 5981 "gram1.y"
    { type_opt = PROCESSORS_BIT;
                (yyval.ll_node) = make_llnd(fi,PROCESSORS_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 785:
#line 5985 "gram1.y"
    { type_opt = DYNAMIC_BIT;
                (yyval.ll_node) = make_llnd(fi,DYNAMIC_OP,LLNULL,LLNULL,SMNULL);
               ;}
    break;

  case 786:
#line 6002 "gram1.y"
    {
                if(! explicit_shape) {
                  err("Explicit shape specification is required", 50);
                }
                if(! (yyvsp[(3) - (4)].ll_node)) {
                  err("No shape specification", 65);
	        }
                type_opt = DIMENSION_BIT;
                attr_ndim = ndim; attr_dims = (yyvsp[(3) - (4)].ll_node);
                (yyval.ll_node) = make_llnd(fi,DIMENSION_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);
	       ;}
    break;

  case 787:
#line 6014 "gram1.y"
    { type_opt = SHADOW_BIT;
                  (yyval.ll_node) = make_llnd(fi,SHADOW_OP,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
                 ;}
    break;

  case 788:
#line 6018 "gram1.y"
    { type_opt = ALIGN_BIT;
                  (yyval.ll_node) = make_llnd(fi,ALIGN_OP,(yyvsp[(3) - (7)].ll_node),(yyvsp[(7) - (7)].ll_node),SMNULL);
                 ;}
    break;

  case 789:
#line 6022 "gram1.y"
    { type_opt = ALIGN_BIT;
                  (yyval.ll_node) = make_llnd(fi,ALIGN_OP,LLNULL,SMNULL,SMNULL);
                ;}
    break;

  case 790:
#line 6032 "gram1.y"
    { 
                 type_opt = DISTRIBUTE_BIT;
                 (yyval.ll_node) = make_llnd(fi,DISTRIBUTE_OP,(yyvsp[(2) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),SMNULL);
                ;}
    break;

  case 791:
#line 6037 "gram1.y"
    { 
                 type_opt = DISTRIBUTE_BIT;
                 (yyval.ll_node) = make_llnd(fi,DISTRIBUTE_OP,LLNULL,LLNULL,SMNULL);
                ;}
    break;

  case 792:
#line 6042 "gram1.y"
    {
                 type_opt = COMMON_BIT;
                 (yyval.ll_node) = make_llnd(fi,COMMON_OP, LLNULL, LLNULL, SMNULL);
                ;}
    break;

  case 793:
#line 6049 "gram1.y"
    { 
	      PTR_LLND  l;
	      l = make_llnd(fi, TYPE_OP, LLNULL, LLNULL, SMNULL);
	      l->type = (yyvsp[(1) - (11)].data_type);
	      (yyval.bf_node) = get_bfnd(fi,DVM_POINTER_DIR, SMNULL, (yyvsp[(11) - (11)].ll_node),(yyvsp[(7) - (11)].ll_node), l);
	    ;}
    break;

  case 794:
#line 6057 "gram1.y"
    {ndim = 0;;}
    break;

  case 795:
#line 6058 "gram1.y"
    { PTR_LLND  q;
             if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		q = make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL);
	      ++ndim;
              (yyval.ll_node) = set_ll_list(q, LLNULL, EXPR_LIST);
	       /*$$ = make_llnd(fi,EXPR_LIST, q, LLNULL, SMNULL);*/
	       /*$$->type = global_default;*/
	    ;}
    break;

  case 796:
#line 6069 "gram1.y"
    { PTR_LLND  q;
             if(ndim == maxdim)
		err("Too many dimensions", 43);
	      else if(ndim < maxdim)
		q = make_llnd(fi,DDOT,LLNULL,LLNULL,SMNULL);
	      ++ndim;
              (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), q, EXPR_LIST);
            ;}
    break;

  case 797:
#line 6080 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 798:
#line 6082 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 799:
#line 6086 "gram1.y"
    {PTR_SYMB s;
           /* s = make_scalar($1,TYNULL,LOCAL);*/
            s = make_array((yyvsp[(1) - (1)].hash_entry),TYNULL,LLNULL,0,LOCAL);
            s->attr = s->attr | DVM_POINTER_BIT;
            if((s->attr & PROCESSORS_BIT) || (s->attr & TASK_BIT) || (s->attr & INHERIT_BIT))
               errstr( "Inconsistent declaration of identifier %s", s->ident, 16);     
            (yyval.ll_node) = make_llnd(fi,VAR_REF,LLNULL,LLNULL,s);
            ;}
    break;

  case 800:
#line 6097 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_HEAP_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 801:
#line 6101 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 802:
#line 6103 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 803:
#line 6107 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              s->attr = s->attr | HEAP_BIT;
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & ALIGN_BIT) || (s->attr & DISTRIBUTE_BIT) || (s->attr & INHERIT_BIT) || (s->attr & DYNAMIC_BIT) || (s->attr & SHADOW_BIT) || (s->attr & DVM_POINTER_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
      
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 804:
#line 6118 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 805:
#line 6122 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 806:
#line 6124 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 807:
#line 6128 "gram1.y"
    {  PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              s->attr = s->attr | CONSISTENT_BIT;
              if((s->attr & PROCESSORS_BIT) ||(s->attr & TASK_BIT)  || (s->attr & TEMPLATE_BIT) || (s->attr & ALIGN_BIT) || (s->attr & DISTRIBUTE_BIT) || (s->attr & INHERIT_BIT) || (s->attr & DYNAMIC_BIT) || (s->attr & SHADOW_BIT) || (s->attr & DVM_POINTER_BIT)) 
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16); 
      
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, LLNULL, LLNULL, s);
	   ;}
    break;

  case 808:
#line 6140 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCID_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 809:
#line 6142 "gram1.y"
    { PTR_LLND p;
              p = make_llnd(fi,COMM_LIST, LLNULL, LLNULL, SMNULL);              
              (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCID_DIR, SMNULL, (yyvsp[(8) - (8)].ll_node), p, LLNULL);
            ;}
    break;

  case 810:
#line 6149 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 811:
#line 6151 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 812:
#line 6155 "gram1.y"
    {  PTR_SYMB s;
              if((yyvsp[(2) - (2)].ll_node)){
                  s = make_array((yyvsp[(1) - (2)].hash_entry), global_default, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
		  s->variant = ASYNC_ID;
                  s->attr = s->attr | DIMENSION_BIT;
                  s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
                  (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);
              } else {
              s = make_local_entity((yyvsp[(1) - (2)].hash_entry), ASYNC_ID, global_default, LOCAL);
	      (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
              }
	   ;}
    break;

  case 813:
#line 6171 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_NEW_VALUE_DIR,SMNULL, LLNULL, LLNULL,LLNULL);;}
    break;

  case 814:
#line 6181 "gram1.y"
    {  if((yyvsp[(6) - (7)].ll_node) &&  (yyvsp[(6) - (7)].ll_node)->entry.Template.symbol->attr & TASK_BIT)
                        (yyval.bf_node) = get_bfnd(fi,DVM_PARALLEL_TASK_DIR,SMNULL,(yyvsp[(6) - (7)].ll_node),(yyvsp[(7) - (7)].ll_node),(yyvsp[(4) - (7)].ll_node));
                    else
                        (yyval.bf_node) = get_bfnd(fi,DVM_PARALLEL_ON_DIR,SMNULL,(yyvsp[(6) - (7)].ll_node),(yyvsp[(7) - (7)].ll_node),(yyvsp[(4) - (7)].ll_node));
                 ;}
    break;

  case 815:
#line 6190 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 816:
#line 6192 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 817:
#line 6196 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 818:
#line 6199 "gram1.y"
    { (yyval.ll_node) = LLNULL; opt_kwd_ = NO;;}
    break;

  case 819:
#line 6204 "gram1.y"
    {
          if((yyvsp[(1) - (4)].ll_node)->type->variant != T_ARRAY) 
             errstr("'%s' isn't array", (yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->ident, 66);
          (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
          (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
          (yyval.ll_node)->type = (yyvsp[(1) - (4)].ll_node)->type->entry.ar_decl.base_type;
        ;}
    break;

  case 820:
#line 6214 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 821:
#line 6216 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 822:
#line 6220 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 823:
#line 6222 "gram1.y"
    {
             (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
             (yyval.ll_node)->entry.string_val = (char *) "*";
             (yyval.ll_node)->type = global_string;
            ;}
    break;

  case 824:
#line 6230 "gram1.y"
    {  (yyval.ll_node) = LLNULL;;}
    break;

  case 825:
#line 6232 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 826:
#line 6236 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 827:
#line 6238 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (2)].ll_node),(yyvsp[(2) - (2)].ll_node),EXPR_LIST); ;}
    break;

  case 839:
#line 6256 "gram1.y"
    { if((yyvsp[(5) - (8)].symbol)->attr & INDIRECT_BIT)
                            errstr("'%s' is not remote group name", (yyvsp[(5) - (8)].symbol)->ident, 68);
                          (yyval.ll_node) = make_llnd(fi,REMOTE_ACCESS_OP,(yyvsp[(7) - (8)].ll_node),LLNULL,(yyvsp[(5) - (8)].symbol));
                        ;}
    break;

  case 840:
#line 6261 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,REMOTE_ACCESS_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 841:
#line 6265 "gram1.y"
    {
                          (yyval.ll_node) = make_llnd(fi,CONSISTENT_OP,(yyvsp[(7) - (8)].ll_node),LLNULL,(yyvsp[(5) - (8)].symbol));
                        ;}
    break;

  case 842:
#line 6269 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,CONSISTENT_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 843:
#line 6273 "gram1.y"
    {  
            if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL){
                errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
                (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),CONSISTENT_GROUP_NAME,global_default,LOCAL);
            } else {
                if((yyval.symbol)->variant != CONSISTENT_GROUP_NAME)
                   errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
            }
          ;}
    break;

  case 844:
#line 6286 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,NEW_SPEC_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 845:
#line 6290 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,NEW_SPEC_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 846:
#line 6294 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_PRIVATE_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 847:
#line 6298 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_CUDA_BLOCK_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 848:
#line 6301 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 849:
#line 6303 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 850:
#line 6305 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(3) - (5)].ll_node),EXPR_LIST); (yyval.ll_node) = set_ll_list((yyval.ll_node),(yyvsp[(5) - (5)].ll_node),EXPR_LIST);;}
    break;

  case 851:
#line 6309 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 852:
#line 6311 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 853:
#line 6315 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_TIE_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 854:
#line 6319 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 855:
#line 6321 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 856:
#line 6325 "gram1.y"
    { if(!((yyvsp[(5) - (8)].symbol)->attr & INDIRECT_BIT))
                         errstr("'%s' is not indirect group name", (yyvsp[(5) - (8)].symbol)->ident, 313);
                      (yyval.ll_node) = make_llnd(fi,INDIRECT_ACCESS_OP,(yyvsp[(7) - (8)].ll_node),LLNULL,(yyvsp[(5) - (8)].symbol));
                    ;}
    break;

  case 857:
#line 6330 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,INDIRECT_ACCESS_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 858:
#line 6334 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,STAGE_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 859:
#line 6338 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,ACROSS_OP,(yyvsp[(4) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 860:
#line 6340 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,ACROSS_OP,(yyvsp[(4) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),SMNULL);;}
    break;

  case 861:
#line 6344 "gram1.y"
    {  if((yyvsp[(3) - (5)].ll_node))
                     (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(3) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),SMNULL);
                   else
                     (yyval.ll_node) = (yyvsp[(4) - (5)].ll_node);
                ;}
    break;

  case 862:
#line 6352 "gram1.y"
    { opt_in_out = YES; ;}
    break;

  case 863:
#line 6356 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "in";
              (yyval.ll_node)->type = global_string;
            ;}
    break;

  case 864:
#line 6362 "gram1.y"
    {
	      (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "out";
              (yyval.ll_node)->type = global_string;
            ;}
    break;

  case 865:
#line 6368 "gram1.y"
    {  (yyval.ll_node) = LLNULL; opt_in_out = NO;;}
    break;

  case 866:
#line 6372 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 867:
#line 6374 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 868:
#line 6378 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 869:
#line 6380 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
                    (yyval.ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);  
                  ;}
    break;

  case 870:
#line 6384 "gram1.y"
    { /*  PTR_LLND p;
                       p = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
                       p->entry.string_val = (char *) "corner";
                       p->type = global_string;
                   */
                   (yyvsp[(1) - (7)].ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(3) - (7)].ll_node);  
                   (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),SMNULL);
                 ;}
    break;

  case 871:
#line 6396 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 872:
#line 6398 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 873:
#line 6402 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL);;}
    break;

  case 874:
#line 6406 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 875:
#line 6408 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 876:
#line 6412 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (5)].ll_node),make_llnd(fi,DDOT,(yyvsp[(3) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 877:
#line 6414 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),make_llnd(fi,DDOT,(yyvsp[(3) - (3)].ll_node),LLNULL,SMNULL),SMNULL); ;}
    break;

  case 878:
#line 6416 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),make_llnd(fi,DDOT,LLNULL,(yyvsp[(3) - (3)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 879:
#line 6418 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 880:
#line 6420 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,make_llnd(fi,DDOT,(yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 881:
#line 6422 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,make_llnd(fi,DDOT,(yyvsp[(1) - (1)].ll_node),LLNULL,SMNULL),SMNULL); ;}
    break;

  case 882:
#line 6424 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT,LLNULL,make_llnd(fi,DDOT,LLNULL,(yyvsp[(1) - (1)].ll_node),SMNULL),SMNULL); ;}
    break;

  case 883:
#line 6428 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 884:
#line 6432 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 885:
#line 6436 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(3) - (3)].ll_node);;}
    break;

  case 886:
#line 6440 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (3)].ll_node);;}
    break;

  case 887:
#line 6444 "gram1.y"
    {PTR_LLND q;
                /* q = set_ll_list($9,$6,EXPR_LIST); */
                 q = set_ll_list((yyvsp[(6) - (10)].ll_node),LLNULL,EXPR_LIST); /*podd 11.10.01*/
                 q = add_to_lowLevelList((yyvsp[(9) - (10)].ll_node),q);        /*podd 11.10.01*/
                 (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,q,LLNULL,SMNULL);
                ;}
    break;

  case 888:
#line 6451 "gram1.y"
    {PTR_LLND q;
                 q = set_ll_list((yyvsp[(6) - (8)].ll_node),LLNULL,EXPR_LIST);
                 (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,q,LLNULL,SMNULL);
                ;}
    break;

  case 889:
#line 6457 "gram1.y"
    {  (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,(yyvsp[(9) - (10)].ll_node),LLNULL,(yyvsp[(6) - (10)].symbol)); ;}
    break;

  case 890:
#line 6461 "gram1.y"
    { opt_kwd_r = YES; ;}
    break;

  case 891:
#line 6464 "gram1.y"
    { opt_kwd_r = NO; ;}
    break;

  case 892:
#line 6468 "gram1.y"
    { 
                  if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL) {
                      errstr("'%s' is not declared as reduction group", (yyvsp[(1) - (1)].hash_entry)->ident, 69);
                      (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),REDUCTION_GROUP_NAME,global_default,LOCAL);
                  } else {
                    if((yyval.symbol)->variant != REDUCTION_GROUP_NAME)
                      errstr("'%s' is not declared as reduction group", (yyvsp[(1) - (1)].hash_entry)->ident, 69);
                  }
                ;}
    break;

  case 893:
#line 6481 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 894:
#line 6483 "gram1.y"
    {(yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),EXPR_LIST);;}
    break;

  case 895:
#line 6487 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (4)].ll_node),(yyvsp[(3) - (4)].ll_node),SMNULL);;}
    break;

  case 896:
#line 6489 "gram1.y"
    {(yyvsp[(3) - (6)].ll_node) = set_ll_list((yyvsp[(3) - (6)].ll_node),(yyvsp[(5) - (6)].ll_node),EXPR_LIST);
            (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (6)].ll_node),(yyvsp[(3) - (6)].ll_node),SMNULL);;}
    break;

  case 897:
#line 6494 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "sum";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 898:
#line 6500 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "product";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 899:
#line 6506 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "min";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 900:
#line 6512 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "max";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 901:
#line 6518 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "or";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 902:
#line 6524 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "and";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 903:
#line 6530 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "eqv";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 904:
#line 6536 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "neqv";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 905:
#line 6542 "gram1.y"
    { err("Illegal reduction operation name", 70);
               errcnt--;
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "unknown";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 906:
#line 6551 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "maxloc";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 907:
#line 6557 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "minloc";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 908:
#line 6574 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_RENEW_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 909:
#line 6582 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_START_OP,LLNULL,LLNULL,(yyvsp[(4) - (4)].symbol));;}
    break;

  case 910:
#line 6590 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_WAIT_OP,LLNULL,LLNULL,(yyvsp[(4) - (4)].symbol));;}
    break;

  case 911:
#line 6592 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_COMP_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 912:
#line 6594 "gram1.y"
    {  (yyvsp[(5) - (9)].ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(7) - (9)].ll_node); (yyval.ll_node) = make_llnd(fi,SHADOW_COMP_OP,(yyvsp[(5) - (9)].ll_node),LLNULL,SMNULL);;}
    break;

  case 913:
#line 6598 "gram1.y"
    {(yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry), SHADOW_GROUP_NAME,global_default,LOCAL);;}
    break;

  case 914:
#line 6602 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);;}
    break;

  case 915:
#line 6604 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);;}
    break;

  case 916:
#line 6608 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 917:
#line 6610 "gram1.y"
    { PTR_LLND p;
          p = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
          p->entry.string_val = (char *) "corner";
          p->type = global_string;
          (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (5)].ll_node),p,SMNULL);
         ;}
    break;

  case 918:
#line 6618 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (5)].ll_node);
          (yyval.ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);  
        ;}
    break;

  case 919:
#line 6622 "gram1.y"
    { PTR_LLND p;
          p = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
          p->entry.string_val = (char *) "corner";
          p->type = global_string;
          (yyvsp[(1) - (9)].ll_node)-> entry.Template.ll_ptr1 = (yyvsp[(4) - (9)].ll_node);  
          (yyval.ll_node) = make_llnd(fi,ARRAY_OP,(yyvsp[(1) - (9)].ll_node),p,SMNULL);
       ;}
    break;

  case 920:
#line 6633 "gram1.y"
    { optcorner = YES; ;}
    break;

  case 921:
#line 6637 "gram1.y"
    { PTR_SYMB s;
         s = (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol;
         if(s->attr & PROCESSORS_BIT)
             errstr( "Illegal use of PROCESSORS name %s ", s->ident, 53);
         else if(s->attr & TASK_BIT)
             errstr( "Illegal use of task array name %s ", s->ident, 71);
         else
           if(s->type->variant != T_ARRAY) 
             errstr("'%s' isn't array", s->ident, 66);
           else 
              if((!(s->attr & DISTRIBUTE_BIT)) && (!(s->attr & ALIGN_BIT)))
               ; /*errstr("hpf.gram: %s is not distributed array", s->ident);*/
                
         (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
        ;}
    break;

  case 922:
#line 6655 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 923:
#line 6657 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 924:
#line 6661 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_START_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 925:
#line 6663 "gram1.y"
    {errstr("Missing DVM directive prefix", 49);;}
    break;

  case 926:
#line 6667 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_WAIT_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 927:
#line 6669 "gram1.y"
    {errstr("Missing DVM directive prefix", 49);;}
    break;

  case 928:
#line 6673 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_GROUP_DIR,(yyvsp[(3) - (6)].symbol),(yyvsp[(5) - (6)].ll_node),LLNULL,LLNULL);;}
    break;

  case 929:
#line 6677 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_REDUCTION_START_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 930:
#line 6681 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_REDUCTION_WAIT_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 931:
#line 6690 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_START_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 932:
#line 6694 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CONSISTENT_WAIT_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 933:
#line 6698 "gram1.y"
    { if(((yyvsp[(4) - (7)].symbol)->attr & INDIRECT_BIT))
                errstr("'%s' is not remote group name", (yyvsp[(4) - (7)].symbol)->ident, 68);
           (yyval.bf_node) = get_bfnd(fi,DVM_REMOTE_ACCESS_DIR,(yyvsp[(4) - (7)].symbol),(yyvsp[(6) - (7)].ll_node),LLNULL,LLNULL);
         ;}
    break;

  case 934:
#line 6703 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_REMOTE_ACCESS_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 935:
#line 6707 "gram1.y"
    {  
            if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL){
                errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
                (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),REF_GROUP_NAME,global_default,LOCAL);
            } else {
              if((yyval.symbol)->variant != REF_GROUP_NAME)
                errstr("'%s' is not declared as group", (yyvsp[(1) - (1)].hash_entry)->ident, 74);
            }
          ;}
    break;

  case 936:
#line 6719 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 937:
#line 6721 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 938:
#line 6725 "gram1.y"
    {
              (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
              (yyval.ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
            ;}
    break;

  case 939:
#line 6730 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 940:
#line 6734 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 941:
#line 6736 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 942:
#line 6740 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 943:
#line 6742 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, LLNULL, LLNULL, SMNULL);;}
    break;

  case 944:
#line 6746 "gram1.y"
    {  PTR_LLND q;
             q = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
             (yyval.bf_node) = get_bfnd(fi,DVM_TASK_DIR,SMNULL,q,LLNULL,LLNULL);
          ;}
    break;

  case 945:
#line 6751 "gram1.y"
    {   PTR_LLND q;
              q = make_llnd(fi,EXPR_LIST, (yyvsp[(3) - (3)].ll_node), LLNULL, SMNULL);
	      add_to_lowLevelList(q, (yyvsp[(1) - (3)].bf_node)->entry.Template.ll_ptr1);
          ;}
    break;

  case 946:
#line 6758 "gram1.y"
    { 
             PTR_SYMB s;
	      s = make_array((yyvsp[(1) - (2)].hash_entry), global_int, (yyvsp[(2) - (2)].ll_node), ndim, LOCAL);
              if((yyvsp[(2) - (2)].ll_node)){
                  s->attr = s->attr | DIMENSION_BIT;
                  s->type->entry.ar_decl.ranges = (yyvsp[(2) - (2)].ll_node);
              }
              else
                  err("No dimensions in TASK directive", 75);
              if(ndim > 1)
                  errstr("Illegal rank of '%s'", s->ident, 76);
              if(s->attr & TASK_BIT)
                errstr( "Multiple declaration of identifier  %s ", s->ident, 73);
              if((s->attr & ALIGN_BIT) ||(s->attr & DISTRIBUTE_BIT) ||(s->attr & TEMPLATE_BIT) || (s->attr & DYNAMIC_BIT) ||(s->attr & SHADOW_BIT) || (s->attr & PROCESSORS_BIT)  || (s->attr & DVM_POINTER_BIT) || (s->attr & INHERIT_BIT))
                errstr("Inconsistent declaration of identifier  %s", s->ident, 16);
              else
	        s->attr = s->attr | TASK_BIT;
    
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(2) - (2)].ll_node), LLNULL, s);	  
	    ;}
    break;

  case 947:
#line 6781 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 948:
#line 6783 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (4)].symbol),(yyvsp[(4) - (4)].ll_node),LLNULL,LLNULL);;}
    break;

  case 949:
#line 6785 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (4)].symbol),LLNULL,(yyvsp[(4) - (4)].ll_node),LLNULL);;}
    break;

  case 950:
#line 6787 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (5)].symbol),(yyvsp[(4) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),LLNULL);;}
    break;

  case 951:
#line 6789 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_TASK_REGION_DIR,(yyvsp[(3) - (5)].symbol),(yyvsp[(5) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),LLNULL);;}
    break;

  case 952:
#line 6793 "gram1.y"
    { PTR_SYMB s;
              if((s=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL)
                s = make_array((yyvsp[(1) - (1)].hash_entry), TYNULL, LLNULL, 0, LOCAL);
              
              if(!(s->attr & TASK_BIT))
                 errstr("'%s' is not task array", s->ident, 77);
              (yyval.symbol) = s;
              ;}
    break;

  case 953:
#line 6804 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_END_TASK_REGION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 954:
#line 6808 "gram1.y"
    {  PTR_SYMB s;
              PTR_LLND q;
             /*
              s = make_array($1, TYNULL, LLNULL, 0, LOCAL);                           
	      if((parstate == INEXEC) && !(s->attr & TASK_BIT))
                 errstr("'%s' is not task array", s->ident, 77);  
              q =  set_ll_list($3,LLNULL,EXPR_LIST);
	      $$ = make_llnd(fi,ARRAY_REF, q, LLNULL, s);
              */

              s = (yyvsp[(1) - (4)].symbol);
              q =  set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, q, LLNULL, s);
	   ;}
    break;

  case 955:
#line 6823 "gram1.y"
    {  PTR_LLND q; 
              q =  set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	      (yyval.ll_node) = make_llnd(fi,ARRAY_REF, q, LLNULL, (yyvsp[(1) - (4)].symbol));
	   ;}
    break;

  case 956:
#line 6830 "gram1.y"
    {              
         (yyval.bf_node) = get_bfnd(fi,DVM_ON_DIR,SMNULL,(yyvsp[(3) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),LLNULL);
    ;}
    break;

  case 957:
#line 6836 "gram1.y"
    {(yyval.ll_node) = LLNULL;;}
    break;

  case 958:
#line 6838 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 959:
#line 6842 "gram1.y"
    {(yyval.bf_node) = get_bfnd(fi,DVM_END_ON_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 960:
#line 6846 "gram1.y"
    { PTR_LLND q;
        /* if(!($6->attr & PROCESSORS_BIT))
           errstr("'%s' is not processor array", $6->ident, 67);
         */
        q = make_llnd(fi,ARRAY_REF, (yyvsp[(7) - (7)].ll_node), LLNULL, (yyvsp[(6) - (7)].symbol));
        (yyval.bf_node) = get_bfnd(fi,DVM_MAP_DIR,SMNULL,(yyvsp[(3) - (7)].ll_node),q,LLNULL);
      ;}
    break;

  case 961:
#line 6854 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_MAP_DIR,SMNULL,(yyvsp[(3) - (6)].ll_node),LLNULL,(yyvsp[(6) - (6)].ll_node)); ;}
    break;

  case 962:
#line 6858 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_PREFETCH_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 963:
#line 6862 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_RESET_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 964:
#line 6870 "gram1.y"
    { if(!((yyvsp[(4) - (7)].symbol)->attr & INDIRECT_BIT))
                         errstr("'%s' is not indirect group name", (yyvsp[(4) - (7)].symbol)->ident, 313);
                      (yyval.bf_node) = get_bfnd(fi,DVM_INDIRECT_ACCESS_DIR,(yyvsp[(4) - (7)].symbol),(yyvsp[(6) - (7)].ll_node),LLNULL,LLNULL);
                    ;}
    break;

  case 965:
#line 6875 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_INDIRECT_ACCESS_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 966:
#line 6889 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 967:
#line 6891 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 968:
#line 6895 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 969:
#line 6897 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node); (yyval.ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);;}
    break;

  case 970:
#line 6906 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 971:
#line 6908 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);;}
    break;

  case 972:
#line 6910 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL, LLNULL, (yyvsp[(3) - (3)].ll_node), LLNULL);;}
    break;

  case 973:
#line 6912 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,HPF_INDEPENDENT_DIR,SMNULL, (yyvsp[(3) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node),LLNULL);;}
    break;

  case 974:
#line 6948 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,REDUCTION_OP,(yyvsp[(5) - (6)].ll_node),LLNULL,SMNULL);;}
    break;

  case 975:
#line 6952 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCHRONOUS_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 976:
#line 6956 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ENDASYNCHRONOUS_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 977:
#line 6960 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ASYNCWAIT_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 978:
#line 6964 "gram1.y"
    {  
            if(((yyval.symbol)=(yyvsp[(1) - (1)].hash_entry)->id_attr) == SMNULL) {
                errstr("'%s' is not declared as ASYNCID", (yyvsp[(1) - (1)].hash_entry)->ident, 115);
                (yyval.symbol) = make_local_entity((yyvsp[(1) - (1)].hash_entry),ASYNC_ID,global_default,LOCAL);
            } else {
              if((yyval.symbol)->variant != ASYNC_ID)
                errstr("'%s' is not declared as ASYNCID", (yyvsp[(1) - (1)].hash_entry)->ident, 115);
            }
     ;}
    break;

  case 979:
#line 6976 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,VAR_REF, LLNULL, LLNULL, (yyvsp[(1) - (1)].symbol));;}
    break;

  case 980:
#line 6978 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ARRAY_REF, (yyvsp[(3) - (4)].ll_node), LLNULL, (yyvsp[(1) - (4)].symbol));;}
    break;

  case 981:
#line 6982 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_F90_DIR,SMNULL,(yyvsp[(3) - (5)].ll_node),(yyvsp[(5) - (5)].ll_node),LLNULL);;}
    break;

  case 982:
#line 6985 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_DEBUG_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 983:
#line 6987 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_DEBUG_DIR,SMNULL,(yyvsp[(3) - (6)].ll_node),(yyvsp[(5) - (6)].ll_node),LLNULL);;}
    break;

  case 984:
#line 6991 "gram1.y"
    { 
              (yyval.ll_node) = set_ll_list((yyvsp[(2) - (2)].ll_node), LLNULL, EXPR_LIST);
              endioctl();
            ;}
    break;

  case 985:
#line 6996 "gram1.y"
    { 
              (yyval.ll_node) = set_ll_list((yyvsp[(1) - (4)].ll_node), (yyvsp[(4) - (4)].ll_node), EXPR_LIST);
              endioctl();
            ;}
    break;

  case 986:
#line 7003 "gram1.y"
    { (yyval.ll_node)  = make_llnd(fi, KEYWORD_ARG, (yyvsp[(1) - (2)].ll_node), (yyvsp[(2) - (2)].ll_node), SMNULL); ;}
    break;

  case 987:
#line 7006 "gram1.y"
    {
	         (yyval.ll_node) = make_llnd(fi,INT_VAL, LLNULL, LLNULL, SMNULL);
	         (yyval.ll_node)->entry.ival = atoi(yytext);
	         (yyval.ll_node)->type = global_int;
	        ;}
    break;

  case 988:
#line 7014 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ENDDEBUG_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 989:
#line 7018 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_INTERVAL_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 990:
#line 7022 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 991:
#line 7025 "gram1.y"
    { if((yyvsp[(1) - (1)].ll_node)->type->variant != T_INT)             
                    err("Illegal interval number", 78);
                  (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
                 ;}
    break;

  case 992:
#line 7033 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_EXIT_INTERVAL_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 993:
#line 7037 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_ENDINTERVAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 994:
#line 7041 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TRACEON_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 995:
#line 7045 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TRACEOFF_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 996:
#line 7049 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_BARRIER_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 997:
#line 7053 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CHECK_DIR,SMNULL,(yyvsp[(9) - (9)].ll_node),(yyvsp[(5) - (9)].ll_node),LLNULL); ;}
    break;

  case 998:
#line 7057 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_IO_MODE_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 999:
#line 7060 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1000:
#line 7062 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1001:
#line 7066 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_ASYNC_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1002:
#line 7068 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_LOCAL_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1003:
#line 7070 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,PARALLEL_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1004:
#line 7074 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_SHADOW_ADD_DIR,SMNULL,(yyvsp[(4) - (9)].ll_node),(yyvsp[(6) - (9)].ll_node),(yyvsp[(9) - (9)].ll_node)); ;}
    break;

  case 1005:
#line 7078 "gram1.y"
    {
                 if((yyvsp[(1) - (4)].ll_node)->type->variant != T_ARRAY) 
                    errstr("'%s' isn't array", (yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->ident, 66);
                 if(!((yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->attr & TEMPLATE_BIT))
                    errstr("'%s' isn't TEMPLATE", (yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->ident, 628);
                 (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                 (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
                 /*$$->type = $1->type->entry.ar_decl.base_type;*/
               ;}
    break;

  case 1006:
#line 7090 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1007:
#line 7092 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1008:
#line 7096 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 1009:
#line 7098 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 1010:
#line 7102 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1011:
#line 7104 "gram1.y"
    { (yyval.ll_node) = LLNULL; opt_kwd_ = NO;;}
    break;

  case 1012:
#line 7108 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_LOCALIZE_DIR,SMNULL,(yyvsp[(4) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),LLNULL); ;}
    break;

  case 1013:
#line 7112 "gram1.y"
    {
                 if((yyvsp[(1) - (1)].ll_node)->type->variant != T_ARRAY) 
                    errstr("'%s' isn't array", (yyvsp[(1) - (1)].ll_node)->entry.Template.symbol->ident, 66); 
                 (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);
                ;}
    break;

  case 1014:
#line 7118 "gram1.y"
    {
                 if((yyvsp[(1) - (4)].ll_node)->type->variant != T_ARRAY) 
                    errstr("'%s' isn't array", (yyvsp[(1) - (4)].ll_node)->entry.Template.symbol->ident, 66); 
                                 
                 (yyvsp[(1) - (4)].ll_node)->entry.Template.ll_ptr1 = (yyvsp[(3) - (4)].ll_node);
                 (yyval.ll_node) = (yyvsp[(1) - (4)].ll_node);
                 (yyval.ll_node)->type = (yyvsp[(1) - (4)].ll_node)->type->entry.ar_decl.base_type;
                ;}
    break;

  case 1015:
#line 7130 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1016:
#line 7132 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1017:
#line 7136 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 1018:
#line 7138 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,DDOT, LLNULL, LLNULL, SMNULL);;}
    break;

  case 1019:
#line 7142 "gram1.y"
    { 
            (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL, LLNULL, LLNULL, SMNULL);
            (yyval.ll_node)->entry.string_val = (char *) "*";
            (yyval.ll_node)->type = global_string;
          ;}
    break;

  case 1020:
#line 7150 "gram1.y"
    { 
                PTR_LLND q;
                if((yyvsp[(16) - (16)].ll_node))
                  q = make_llnd(fi,ARRAY_OP, (yyvsp[(14) - (16)].ll_node), (yyvsp[(16) - (16)].ll_node), SMNULL);
                else
                  q = (yyvsp[(14) - (16)].ll_node);                  
                (yyval.bf_node) = get_bfnd(fi,DVM_CP_CREATE_DIR,SMNULL,(yyvsp[(3) - (16)].ll_node),(yyvsp[(8) - (16)].ll_node),q); 
              ;}
    break;

  case 1021:
#line 7161 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 1022:
#line 7163 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, PARALLEL_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 1023:
#line 7165 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_LOCAL_OP, LLNULL, LLNULL, SMNULL); ;}
    break;

  case 1024:
#line 7169 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CP_LOAD_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL); ;}
    break;

  case 1025:
#line 7173 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CP_SAVE_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL); ;}
    break;

  case 1026:
#line 7175 "gram1.y"
    {
                PTR_LLND q;
                q = make_llnd(fi,ACC_ASYNC_OP,LLNULL,LLNULL,SMNULL);
                (yyval.bf_node) = get_bfnd(fi,DVM_CP_SAVE_DIR,SMNULL,(yyvsp[(3) - (6)].ll_node),q,LLNULL);
              ;}
    break;

  case 1027:
#line 7183 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_CP_WAIT_DIR,SMNULL,(yyvsp[(3) - (9)].ll_node),(yyvsp[(8) - (9)].ll_node),LLNULL); ;}
    break;

  case 1028:
#line 7187 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TEMPLATE_CREATE_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL); ;}
    break;

  case 1029:
#line 7190 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 1030:
#line 7192 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 1031:
#line 7196 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,DVM_TEMPLATE_DELETE_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL); ;}
    break;

  case 1059:
#line 7230 "gram1.y"
    {
          (yyval.bf_node) = get_bfnd(fi,OMP_ONETHREAD_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1060:
#line 7236 "gram1.y"
    {
  	   (yyval.bf_node) = make_endparallel();
	;}
    break;

  case 1061:
#line 7242 "gram1.y"
    {
  	   (yyval.bf_node) = make_parallel();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1062:
#line 7248 "gram1.y"
    {
  	   (yyval.bf_node) = make_parallel();
	   opt_kwd_ = NO;
	;}
    break;

  case 1063:
#line 7254 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1064:
#line 7258 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);	
	;}
    break;

  case 1074:
#line 7275 "gram1.y"
    {
		(yyval.ll_node) = (yyvsp[(4) - (5)].ll_node);
        ;}
    break;

  case 1075:
#line 7280 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_PRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1076:
#line 7285 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_FIRSTPRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1077:
#line 7291 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_LASTPRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1078:
#line 7297 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_COPYIN,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1079:
#line 7303 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_SHARED,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1080:
#line 7308 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_DEFAULT,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1081:
#line 7314 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "private";
		(yyval.ll_node)->type = global_string;
	;}
    break;

  case 1082:
#line 7320 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "shared";
		(yyval.ll_node)->type = global_string;
	;}
    break;

  case 1083:
#line 7326 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "none";
		(yyval.ll_node)->type = global_string;
	;}
    break;

  case 1084:
#line 7333 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_IF,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1085:
#line 7339 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_NUM_THREADS,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1086:
#line 7345 "gram1.y"
    {
		PTR_LLND q;
		q = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
		(yyval.ll_node) = make_llnd(fi,OMP_REDUCTION,q,LLNULL,SMNULL);
	;}
    break;

  case 1087:
#line 7352 "gram1.y"
    {(yyval.ll_node) = make_llnd(fi,DDOT,(yyvsp[(2) - (4)].ll_node),(yyvsp[(4) - (4)].ll_node),SMNULL);;}
    break;

  case 1089:
#line 7358 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "+";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1090:
#line 7364 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "-";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1091:
#line 7371 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "*";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1092:
#line 7377 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "/";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1093:
#line 7383 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "min";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1094:
#line 7389 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "max";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1095:
#line 7395 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".or.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1096:
#line 7401 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".and.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1097:
#line 7407 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".eqv.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1098:
#line 7413 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) ".neqv.";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1099:
#line 7419 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "iand";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1100:
#line 7425 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "ieor";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1101:
#line 7431 "gram1.y"
    {
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "ior";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1102:
#line 7437 "gram1.y"
    { err("Illegal reduction operation name", 70);
               errcnt--;
              (yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
              (yyval.ll_node)->entry.string_val = (char *) "unknown";
              (yyval.ll_node)->type = global_string;
             ;}
    break;

  case 1103:
#line 7447 "gram1.y"
    {
  	   (yyval.bf_node) = make_sections((yyvsp[(4) - (4)].ll_node));
	   opt_kwd_ = NO;
	;}
    break;

  case 1104:
#line 7452 "gram1.y"
    {
  	   (yyval.bf_node) = make_sections(LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1105:
#line 7458 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1106:
#line 7462 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1111:
#line 7474 "gram1.y"
    {
		PTR_LLND q;
   	        (yyval.bf_node) = make_endsections();
		q = set_ll_list((yyvsp[(4) - (4)].ll_node),LLNULL,EXPR_LIST);
                (yyval.bf_node)->entry.Template.ll_ptr1 = q;
                opt_kwd_ = NO;
	;}
    break;

  case 1112:
#line 7482 "gram1.y"
    {
   	        (yyval.bf_node) = make_endsections();
	        opt_kwd_ = NO; 
	;}
    break;

  case 1113:
#line 7488 "gram1.y"
    {
           (yyval.bf_node) = make_ompsection();
	;}
    break;

  case 1114:
#line 7494 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_DO_DIR,SMNULL,(yyvsp[(4) - (4)].ll_node),LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1115:
#line 7499 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1116:
#line 7505 "gram1.y"
    {
		PTR_LLND q;
		q = set_ll_list((yyvsp[(4) - (4)].ll_node),LLNULL,EXPR_LIST);
	        (yyval.bf_node) = get_bfnd(fi,OMP_END_DO_DIR,SMNULL,q,LLNULL,LLNULL);
      	        opt_kwd_ = NO;
	;}
    break;

  case 1117:
#line 7512 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_END_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1118:
#line 7518 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1119:
#line 7522 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1126:
#line 7536 "gram1.y"
    {
		/*$$ = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		$$->entry.string_val = (char *) "ORDERED";
		$$->type = global_string;*/
                (yyval.ll_node) = make_llnd(fi,OMP_ORDERED,LLNULL,LLNULL,SMNULL);
	;}
    break;

  case 1127:
#line 7545 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_SCHEDULE,(yyvsp[(4) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),SMNULL);
	;}
    break;

  case 1128:
#line 7549 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_SCHEDULE,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1129:
#line 7555 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "STATIC";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1130:
#line 7562 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "DYNAMIC";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1131:
#line 7569 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "GUIDED";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1132:
#line 7576 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,KEYWORD_VAL,LLNULL,LLNULL,SMNULL);
		(yyval.ll_node)->entry.string_val = (char *) "RUNTIME";
		(yyval.ll_node)->type = global_string;
		
	;}
    break;

  case 1133:
#line 7585 "gram1.y"
    {
  	   (yyval.bf_node) = make_single();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1134:
#line 7591 "gram1.y"
    {
  	   (yyval.bf_node) = make_single();
	   opt_kwd_ = NO;
	;}
    break;

  case 1135:
#line 7597 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1136:
#line 7601 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1139:
#line 7611 "gram1.y"
    {
  	   (yyval.bf_node) = make_endsingle();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1140:
#line 7617 "gram1.y"
    {
  	   (yyval.bf_node) = make_endsingle();
	   opt_kwd_ = NO;
	;}
    break;

  case 1141:
#line 7623 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1142:
#line 7627 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1145:
#line 7638 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_COPYPRIVATE,(yyvsp[(2) - (2)].ll_node),LLNULL,SMNULL);
	;}
    break;

  case 1146:
#line 7644 "gram1.y"
    {
		(yyval.ll_node) = make_llnd(fi,OMP_NOWAIT,LLNULL,LLNULL,SMNULL);
	;}
    break;

  case 1147:
#line 7650 "gram1.y"
    {
           (yyval.bf_node) = make_workshare();
	;}
    break;

  case 1148:
#line 7655 "gram1.y"
    {
		PTR_LLND q;
   	        (yyval.bf_node) = make_endworkshare();
		q = set_ll_list((yyvsp[(4) - (4)].ll_node),LLNULL,EXPR_LIST);
                (yyval.bf_node)->entry.Template.ll_ptr1 = q;
  	        opt_kwd_ = NO;
	;}
    break;

  case 1149:
#line 7663 "gram1.y"
    {
   	        (yyval.bf_node) = make_endworkshare();
                opt_kwd_ = NO;
	;}
    break;

  case 1150:
#line 7669 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_PARALLEL_DO_DIR,SMNULL,(yyvsp[(4) - (4)].ll_node),LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1151:
#line 7674 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_PARALLEL_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1152:
#line 7681 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(3) - (4)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1153:
#line 7685 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node),(yyvsp[(4) - (5)].ll_node),EXPR_LIST);
	;}
    break;

  case 1165:
#line 7705 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_END_PARALLEL_DO_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1166:
#line 7710 "gram1.y"
    {
           (yyval.bf_node) = make_parallelsections((yyvsp[(4) - (4)].ll_node));
	   opt_kwd_ = NO;
	;}
    break;

  case 1167:
#line 7715 "gram1.y"
    {
           (yyval.bf_node) = make_parallelsections(LLNULL);
	   opt_kwd_ = NO;
	;}
    break;

  case 1168:
#line 7722 "gram1.y"
    {
           (yyval.bf_node) = make_endparallelsections();
	;}
    break;

  case 1169:
#line 7727 "gram1.y"
    {
           (yyval.bf_node) = make_parallelworkshare();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (4)].ll_node);
	   opt_kwd_ = NO;
	;}
    break;

  case 1170:
#line 7733 "gram1.y"
    {
           (yyval.bf_node) = make_parallelworkshare();
	   opt_kwd_ = NO;
	;}
    break;

  case 1171:
#line 7739 "gram1.y"
    {
           (yyval.bf_node) = make_endparallelworkshare();
	;}
    break;

  case 1172:
#line 7744 "gram1.y"
    { 
	   (yyval.bf_node) = get_bfnd(fi,OMP_THREADPRIVATE_DIR, SMNULL, (yyvsp[(3) - (3)].ll_node), LLNULL, LLNULL);
	;}
    break;

  case 1173:
#line 7749 "gram1.y"
    {
  	   (yyval.bf_node) = make_master();
	;}
    break;

  case 1174:
#line 7754 "gram1.y"
    {
  	   (yyval.bf_node) = make_endmaster();
	;}
    break;

  case 1175:
#line 7758 "gram1.y"
    {
  	   (yyval.bf_node) = make_ordered();
	;}
    break;

  case 1176:
#line 7763 "gram1.y"
    {
  	   (yyval.bf_node) = make_endordered();
	;}
    break;

  case 1177:
#line 7768 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_BARRIER_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1178:
#line 7772 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_ATOMIC_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1179:
#line 7777 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_FLUSH_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);
	;}
    break;

  case 1180:
#line 7781 "gram1.y"
    {
           (yyval.bf_node) = get_bfnd(fi,OMP_FLUSH_DIR,SMNULL,LLNULL,LLNULL,LLNULL);
	;}
    break;

  case 1181:
#line 7787 "gram1.y"
    {
  	   (yyval.bf_node) = make_critical();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	;}
    break;

  case 1182:
#line 7792 "gram1.y"
    {
  	   (yyval.bf_node) = make_critical();
	;}
    break;

  case 1183:
#line 7798 "gram1.y"
    {
  	   (yyval.bf_node) = make_endcritical();
           (yyval.bf_node)->entry.Template.ll_ptr1 = (yyvsp[(4) - (5)].ll_node);
	;}
    break;

  case 1184:
#line 7803 "gram1.y"
    {
  	   (yyval.bf_node) = make_endcritical();
	;}
    break;

  case 1185:
#line 7809 "gram1.y"
    { 
		PTR_SYMB s;
		PTR_LLND l;
		s = make_common((yyvsp[(2) - (5)].hash_entry)); 
		l = make_llnd(fi,VAR_REF, LLNULL, LLNULL, s);
		(yyval.ll_node) = make_llnd(fi,OMP_THREADPRIVATE, l, LLNULL, SMNULL);
	;}
    break;

  case 1186:
#line 7819 "gram1.y"
    {
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1187:
#line 7823 "gram1.y"
    {	
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST);
	;}
    break;

  case 1188:
#line 7827 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);
	;}
    break;

  case 1189:
#line 7831 "gram1.y"
    { 
		(yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST);
	;}
    break;

  case 1190:
#line 7836 "gram1.y"
    {
		operator_slash = 1;
	;}
    break;

  case 1191:
#line 7839 "gram1.y"
    {
		operator_slash = 0;
	;}
    break;

  case 1199:
#line 7853 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_REGION_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1200:
#line 7857 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_CHECKSECTION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1201:
#line 7861 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_GET_ACTUAL_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1202:
#line 7863 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_GET_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1203:
#line 7865 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_GET_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1204:
#line 7869 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ACTUAL_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1205:
#line 7871 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1206:
#line 7873 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ACTUAL_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1207:
#line 7877 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 1208:
#line 7879 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node); ;}
    break;

  case 1209:
#line 7883 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1210:
#line 7885 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1211:
#line 7889 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1212:
#line 7892 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1213:
#line 7895 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1214:
#line 7900 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_INOUT_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1215:
#line 7902 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_IN_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1216:
#line 7904 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_OUT_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1217:
#line 7906 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_LOCAL_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1218:
#line 7908 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_INLOCAL_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1219:
#line 7912 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_TARGETS_OP,(yyvsp[(3) - (4)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1220:
#line 7916 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_ASYNC_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1221:
#line 7921 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(1) - (1)].ll_node);;}
    break;

  case 1222:
#line 7925 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1223:
#line 7927 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1224:
#line 7931 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_HOST_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1225:
#line 7933 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_CUDA_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1226:
#line 7937 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_END_REGION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1227:
#line 7941 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_END_CHECKSECTION_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1228:
#line 7945 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,ACC_ROUTINE_DIR,SMNULL,(yyvsp[(3) - (3)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1229:
#line 7949 "gram1.y"
    { (yyval.ll_node) = LLNULL; ;}
    break;

  case 1230:
#line 7951 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(2) - (2)].ll_node);;}
    break;

  case 1237:
#line 7963 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,SPF_ANALYSIS_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1238:
#line 7967 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,SPF_PARALLEL_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1239:
#line 7971 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,SPF_TRANSFORM_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1240:
#line 7975 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,SPF_PARALLEL_REG_DIR,(yyvsp[(3) - (3)].symbol),LLNULL,LLNULL,LLNULL);;}
    break;

  case 1241:
#line 7977 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,SPF_PARALLEL_REG_DIR,(yyvsp[(3) - (10)].symbol),(yyvsp[(8) - (10)].ll_node),(yyvsp[(10) - (10)].ll_node),LLNULL);;}
    break;

  case 1242:
#line 7979 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,SPF_PARALLEL_REG_DIR,(yyvsp[(3) - (10)].symbol),(yyvsp[(10) - (10)].ll_node),(yyvsp[(8) - (10)].ll_node),LLNULL);;}
    break;

  case 1243:
#line 7983 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1244:
#line 7985 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1245:
#line 7989 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_CODE_COVERAGE_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1246:
#line 7993 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 1247:
#line 7995 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(5) - (6)].ll_node);;}
    break;

  case 1248:
#line 7999 "gram1.y"
    { (yyval.ll_node) = LLNULL;;}
    break;

  case 1249:
#line 8001 "gram1.y"
    { (yyval.ll_node) = (yyvsp[(5) - (6)].ll_node);;}
    break;

  case 1250:
#line 8005 "gram1.y"
    { (yyval.bf_node) = get_bfnd(fi,SPF_END_PARALLEL_REG_DIR,SMNULL,LLNULL,LLNULL,LLNULL);;}
    break;

  case 1251:
#line 8009 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1252:
#line 8011 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1256:
#line 8020 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,REDUCTION_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL); ;}
    break;

  case 1257:
#line 8024 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_PRIVATE_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1258:
#line 8028 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_PARAMETER_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1259:
#line 8031 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 1260:
#line 8033 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 1261:
#line 8037 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi, ASSGN_OP, (yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), SMNULL); ;}
    break;

  case 1262:
#line 8041 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1263:
#line 8043 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1267:
#line 8052 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SHADOW_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1268:
#line 8056 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACROSS_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1269:
#line 8060 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,REMOTE_ACCESS_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1270:
#line 8064 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1271:
#line 8066 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1272:
#line 8070 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_NOINLINE_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1273:
#line 8072 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_FISSION_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1274:
#line 8074 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_EXPAND_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1275:
#line 8076 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_EXPAND_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1276:
#line 8079 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_SHRINK_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1277:
#line 8081 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_UNROLL_OP,LLNULL,LLNULL,SMNULL);;}
    break;

  case 1278:
#line 8083 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_UNROLL_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1279:
#line 8087 "gram1.y"
    {
               (yyval.ll_node) = set_ll_list((yyvsp[(1) - (5)].ll_node), (yyvsp[(3) - (5)].ll_node), EXPR_LIST);
               (yyval.ll_node) = set_ll_list((yyval.ll_node), (yyvsp[(5) - (5)].ll_node), EXPR_LIST);
             ;}
    break;

  case 1280:
#line 8094 "gram1.y"
    { (yyval.symbol) = make_parallel_region((yyvsp[(1) - (1)].hash_entry));;}
    break;

  case 1281:
#line 8098 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node), LLNULL, EXPR_LIST); ;}
    break;

  case 1282:
#line 8100 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node), (yyvsp[(3) - (3)].ll_node), EXPR_LIST); ;}
    break;

  case 1283:
#line 8104 "gram1.y"
    {  (yyval.bf_node) = get_bfnd(fi,SPF_CHECKPOINT_DIR,SMNULL,(yyvsp[(4) - (5)].ll_node),LLNULL,LLNULL);;}
    break;

  case 1284:
#line 8108 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1285:
#line 8110 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1286:
#line 8114 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_TYPE_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1287:
#line 8116 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_VARLIST_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1288:
#line 8118 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_EXCEPT_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1289:
#line 8120 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_FILES_COUNT_OP,(yyvsp[(4) - (5)].ll_node),LLNULL,SMNULL);;}
    break;

  case 1290:
#line 8122 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_INTERVAL_OP,(yyvsp[(4) - (7)].ll_node),(yyvsp[(6) - (7)].ll_node),SMNULL);;}
    break;

  case 1291:
#line 8126 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (1)].ll_node),LLNULL,EXPR_LIST); ;}
    break;

  case 1292:
#line 8128 "gram1.y"
    { (yyval.ll_node) = set_ll_list((yyvsp[(1) - (3)].ll_node),(yyvsp[(3) - (3)].ll_node),EXPR_LIST); ;}
    break;

  case 1293:
#line 8132 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,ACC_ASYNC_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1294:
#line 8134 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_FLEXIBLE_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1295:
#line 8138 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_TIME_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1296:
#line 8140 "gram1.y"
    { (yyval.ll_node) = make_llnd(fi,SPF_ITER_OP, LLNULL,LLNULL,SMNULL);;}
    break;

  case 1297:
#line 8144 "gram1.y"
    { if(position==IN_OUTSIDE)
                 err("Misplaced SPF-directive",103);
             ;}
    break;


/* Line 1267 of yacc.c.  */
#line 14119 "gram1.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



