      program loops_combiner_test
      implicit none
      parameter (l = 16,m = 6)
      real :: a(l),b(l),c(l)

! shouldn't be combined because of print:
      do it1 = 1,l
         a(it1) = it1
         print *, 'test print' 
      enddo 

      do it2 = 1,l
         b(it2) = it2
      enddo  

! shouldn't be combined because of goto:
      do it3 = 1,l - 1
         c(it3) = it3
      enddo 

      do it4 = 1,l - 1
         do  k4 = 1,l
            a(k4) = k4 + it4
            go to 100
         enddo  
      enddo 
	  
100   a(0) = 0

      end

