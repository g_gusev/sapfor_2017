
      program test_cascade 
      double precision :: y(10, 10), x(10, 10)
      integer :: n, m

      n=10
      m=15
! y can be removed in 3 steps:
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        do j = 1, m
          y(1, j) = 2
        enddo
        do j = 1, m
          y(1, j) = y(1, j) * 10
        enddo
        do j = 1, m
          x(j, i) = y(1, j)
!          correct result:
!          x(j, i) = 2 * 10
        enddo

        do j = 1, m 
          y(2, j) = y(1, i) * 20
        enddo
        do j = 1, m
          x(j, i) = y(2, j)
!          correct result:
!          x(j, i) = 2 * 10 * 20
        enddo 
      enddo

! y can be removed is 2 steps:
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        y(1, i) = 2
        do j = 1, m
          y(1, i) = y(1, i) * 10
          x(j, i) = y(1, i)
!          correct result:
!          x(j, i) = 2 * 10
        enddo
      enddo
 
      end  
