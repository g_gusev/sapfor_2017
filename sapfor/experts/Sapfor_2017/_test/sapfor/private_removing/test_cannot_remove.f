
      program test_cannot_remove
      double precision :: y(10, 10), x(10, 10), a
      integer :: n, m

      n=10
      m=15
! y cannot be removed - it is uninitialized
! (cannot find reaching definition)
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        y(1, i) = 100
        x(i, i) = y(2, i)
      enddo

! y cannot be removed - more than one reaching definition
! reaches the statement
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        if (i .eq. 1) then
          y(1, i) = 100
        else
          y(1, i) = 200
        endif
        
        x(i, i) = y(1, i)
      enddo

! y cannot be removed - y doesn't match any fixed dimensions mask
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        do j = 1, m
          y(1, i) = j * i
        enddo
        do j = 1, m
          x(j, i) = y(j, i)
        enddo
      enddo

! y cannot be removed - y doesn't match any fixed dimensions mask
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        do j = 1, m
          y(1, i - 1) = j * i
        enddo
        do j = 1, m
          x(j, i) = y(1, i)
        enddo
      enddo


! y cannot be removed - y doesn't match any fixed dimensions mask
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        do j = 1, m
          y(i, 1) = j * i
        enddo
        do j = 1, m
          x(j, i) = y(1, i)
        enddo
      enddo

! y cannot be removed - y depends on non-invariant var 'a'
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        do j = 1, m
          a = 2 * i
          y(j, i) = j * i + a
        enddo
        do j = 1, m
          x(j, i) = y(j, i)
        enddo
      enddo

      end  
