
      program test
      double precision :: y(10, 10), x(10, 10), a
      integer :: n, m

      n=10
      m=15
! y can be removed
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        do j = 1, m
          y(1, j) = j * i
          y(2, j) = j / i
        enddo
        do jj = 1, m
          x(1, jj) = y(1, jj)
          x(2, jj) = y(2, jj)
! correct result:
!         x(1, jj) = jj * i
!         x(2, jj) = jj / i
        enddo

        do j = 1, m
          y(2, j) = j * 100
          y(3, j) = j / 200
        enddo
        do jj = 2, m - 1
          x(1, jj) = y(2, jj - 1)
          x(2, jj) = y(3, jj + 1)
! correct result:
!         x(1, jj) = (jj - 1) * 100
!         x(2, jj) = (jj + 1) / 200
        enddo
      enddo

! y can be removed
!$SPF ANALYSIS(PRIVATE(y))
      do i = 1, n
        do j = 1, m
          y(j, i) = j * i
        enddo
        do jj = 1, m
          x(jj, i) = y(jj, i)
! correct result:
!         x(jj, i) = jj * i
        enddo
      enddo


      end  
