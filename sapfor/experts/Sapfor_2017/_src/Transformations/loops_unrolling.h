#pragma once

#include "dvm.h"
#include "../GraphLoop/graph_loops.h"
#include <vector>

int unrollLoops(SgFile* file, std::vector<LoopGraph*>& loopGraph, std::vector<Messages>& messages);