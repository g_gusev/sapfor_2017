#pragma once

#include <map>
#include <vector>
#include <set>
#include <string>
#include <tuple>
#include <algorithm>

#include "dvm.h"
#include "../Utils/SgUtils.h"

#include "../LoopAnalyzer/loop_analyzer.h"
#include "../ExpressionTransform/expr_transform.h"


struct DeclInfo // for error messages
{
    std::string varName;
    std::string fileName;
    int lineNum;

    DeclInfo() : varName(""), fileName(""), lineNum(0) {};
    DeclInfo(const std::string& vn, const std::string& fn, int ln) : varName(vn), fileName(fn), lineNum(ln) {};
};

struct CommConstraint  // TODO: add variable attributes
{
    int size = 0;
    std::string identifier;
    bool used;
    int typeVariant = 0;
    Distribution::Array* arrayInfo = NULL;
    SgType* type = NULL;
    std::vector<DeclInfo> uses; // info about variables with this constraint

    CommConstraint() : identifier(""), typeVariant(0), size(0), used(false) {};
    CommConstraint(const std::string& name, int sz) : identifier(name), typeVariant(0), size(sz), used(false) {};
    CommConstraint(const std::string& name, SgType* t, bool u);
    CommConstraint(const std::string& name, SgType* t, bool u, std::vector<DeclInfo>& uses); //
    CommConstraint(const Variable* var, bool u, const std::string& funcName, const std::string& fileName);
};


void fixCommonBlocks(const std::map<std::string, std::vector<FuncInfo*>> allFuncInfo, const std::map<std::string, CommonBlock*> allCommonBlocks, SgProject* project);