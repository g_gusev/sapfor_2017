#include "dvm.h"
#include "../GraphLoop/graph_loops.h"
#include <string>
#include <vector>
#include <set>


int privateArraysResizing(SgFile *file, const std::vector<LoopGraph*> &loopGraphs, std::vector<Messages> &messages, const std::map<std::pair<std::string, int>, std::set<SgStatement*>>& usersDirectives, bool isExpand);
void analyzeShrinking(SgFile* file, const std::vector<LoopGraph*>& loopGraphs, std::vector<Messages>& messages, const std::map<std::pair<std::string, int>, std::set<SgStatement*>>& usersDirectives);