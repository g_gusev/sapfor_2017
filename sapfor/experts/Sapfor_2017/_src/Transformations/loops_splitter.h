#pragma once

#include <vector>
#include <map>

#include "dvm.h"
#include "../GraphLoop/graph_loops.h"
#include "../SageAnalysisTool/depGraph.h"
#include "../Utils/CommonBlock.h"

int splitLoops(SgFile *file, std::vector<LoopGraph*> &loopGraphs, std::vector<Messages> &messages, const std::map<LoopGraph*, depGraph*>& depInfoForLoopGraph, const std::map<std::string, CommonBlock*>& commonBlocks, const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo, int& countOfTransform);
