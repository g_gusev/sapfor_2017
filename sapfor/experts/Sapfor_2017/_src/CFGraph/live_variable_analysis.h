#include "../Utils/SgUtils.h"
#include "CFGraph.h"

template <class IT, class DEST>
void insertIfVar(IT begin, IT end, DEST& to) {
    for (auto it = begin; it != end; it++)
        if (*it && (*it)->getType() == SAPFOR::CFG_ARG_TYPE::VAR)
            to.insert(*it);
};

void runLiveVariableAnalysis(const std::map<FuncInfo*, std::vector<SAPFOR::BasicBlock*>>& CFGraph_for_project);

void doDumpLive(const std::map<FuncInfo*, std::vector<SAPFOR::BasicBlock*>>& CFGraph_for_project);

bool joinGlobalsWithParameters(const std::vector<SAPFOR::Argument*>& params,
                               const std::map<std::string, std::pair<std::set<int>, std::set<SAPFOR::Argument*>>>& params_and_globals,
                               const std::string& func_name, std::set<SAPFOR::Argument*>& result);