#include "../Utils/SgUtils.h"
#include "../GraphLoop/graph_loops.h"
#include "CFGraph.h"

#include <string>
#include <map>

void runPrivateVariableAnalysis(const std::map<std::string, std::vector<LoopGraph*>>& loopGraph,
                                const std::map<FuncInfo*, std::vector<SAPFOR::BasicBlock*>>& CFGraph_for_project,
                                const std::map<std::string, CommonBlock*>& commonBlocks,
                                std::map<std::string, std::vector<Messages>>& messages);