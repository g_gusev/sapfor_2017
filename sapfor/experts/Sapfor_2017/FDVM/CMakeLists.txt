cmake_minimum_required(VERSION 3.0)

set(project FDVM)
#set(CMAKE_C_STANDARD 11)
#also named as libSage++
if (TARGET ${project})
    return()
endif ()
project(${project})
message("processing ${project}")

# Read pathes to external sapfor directories
if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/../paths.txt")
    message("Found paths.txt, using custom paths.")
    FILE(STRINGS ../paths.txt SAPFOR_PATHS)
else ()
    message("Not found paths.txt, using default paths.")
    FILE(STRINGS ../paths.default.txt SAPFOR_PATHS)
endif ()
foreach (NameAndValue ${SAPFOR_PATHS})
    # Strip leading spaces
    string(REGEX REPLACE "^[ ]+" "" NameAndValue ${NameAndValue})
    # Find variable name
    string(REGEX MATCH "^[^=]+" Name ${NameAndValue})
    # Find the value
    string(REPLACE "${Name}=" "" Value ${NameAndValue})
    # Set the variable, note the ../ because we are deeper than the file
    set(${Name} "../${Value}")
endforeach ()

set(SOURCE_LIB
        ${sagepp_sources}/libSage++.cpp
        ${sage_include_1}/libSage++.h)

		
# if not default ${sagepp_sources} must be set in ../paths.txt
set(SOURCE_EXE
        ${fdvm_sources}/acc.cpp
		 ${fdvm_sources}/acc_across.cpp
		 ${fdvm_sources}/acc_across_analyzer.cpp
	     ${fdvm_sources}/acc_analyzer.cpp
         ${fdvm_sources}/acc_data.cpp
		 ${fdvm_sources}/acc_f2c.cpp
		 ${fdvm_sources}/acc_f2c_handlers.cpp
		 ${fdvm_sources}/acc_rtc.cpp
		 ${fdvm_sources}/acc_rtc.cpp
         ${fdvm_sources}/acc_utilities.cpp
		 ${fdvm_sources}/aks_analyzeLoops.cpp
		 ${fdvm_sources}/aks_structs.cpp
		 ${fdvm_sources}/checkpoint.cpp
		 ${fdvm_sources}/debug.cpp		
		 ${fdvm_sources}/dvm.cpp
         ${fdvm_sources}/calls.cpp
		 ${fdvm_sources}/funcall.cpp
		 ${fdvm_sources}/help.cpp
		 ${fdvm_sources}/hpf.cpp
		 ${fdvm_sources}/io.cpp
		 ${fdvm_sources}/omp.cpp
		 ${fdvm_sources}/ompdebug.cpp
		 ${fdvm_sources}/parloop.cpp
		 ${fdvm_sources}/stmt.cpp)

source_group (SageLib FILES ${SOURCE_LIB})

# if not default ${fdvm_include}, ${sage_include_1}, ${sage_include_2} must be set in ../paths.txt
include_directories(${fdvm_include} ${sage_include_1} ${sage_include_2})
add_executable(FDVM ${SOURCE_EXE} ${SOURCE_LIB})

if (MSVC_IDE)
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP")
   set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /MP")
else()
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3")
endif()

add_definitions("-D _CRT_SECURE_NO_WARNINGS")
add_definitions("-D SYS5")
add_definitions("-D YYDEBUG")

add_subdirectory(../SageOldSrc ${CMAKE_CURRENT_BINARY_DIR}/SageOldSrc)
add_subdirectory(../SageNewSrc ${CMAKE_CURRENT_BINARY_DIR}/SageNewSrc)
target_link_libraries(FDVM SageOldSrc SageNewSrc)
